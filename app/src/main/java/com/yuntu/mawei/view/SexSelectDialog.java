package com.yuntu.mawei.view;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;

import com.yuntu.mawei.R;
import com.yuntu.mawei.util.Baseutils;
import com.yuntu.mawei.util.Logl;
import com.yuntu.mawei.widget.MProgressWheel;
import com.yuntu.mawei.widget.WheelView;

import java.util.Arrays;
import java.util.List;

/**
 * Created by tanghao 2017/8/22.
 * meail: 86882259@qq.com
 */

public class SexSelectDialog extends Dialog implements View.OnClickListener {

    private WheelView wheelView;
    private IndexCallBack listener;

    public SexSelectDialog(Context context) {
        super(context, R.style.LoadingDialog);
        /**设置对话框背景透明*/
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        View view = LayoutInflater.from(context).inflate(R.layout.dialog_sex_select, null);
        wheelView = view.findViewById(R.id.wheelview);
        view.findViewById(R.id.bt_cancel).setOnClickListener(this);
        view.findViewById(R.id.bt_sure).setOnClickListener(this);
        view.setLayoutParams(new LinearLayout.LayoutParams(Baseutils.intance().DM_width, Baseutils.intance().DM_height * 4 / 10));
        setContentView(view);
        setCanceledOnTouchOutside(true);
        show();
    }

    public void initData(List<String> list, IndexCallBack listener) {
        wheelView.setItems(list);
        this.listener = listener;
    }

    @Override
    public void show() {
        setGravity(Gravity.BOTTOM);
        setSize(Baseutils.intance().DM_width, 0);
        if (wheelView != null) {
            wheelView.setVisibility(View.VISIBLE);
        }
        super.show();
    }

    @Override
    public void dismiss() {
        super.dismiss();
    }

    @Override
    public void cancel() {
        super.cancel();
    }

    /**
     * @param gravity 上中下  三个展现位置
     */
    public void setGravity(int gravity) {
        this.getWindow().setGravity(gravity);
    }

    //按照百分比来  0为不设置
    public void setSize(int w, int h) {
        WindowManager.LayoutParams lp = this.getWindow().getAttributes();
        lp.width = w; //设置宽度
        this.getWindow().setAttributes(lp);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.bt_cancel:
                dismiss();
                break;
            case R.id.bt_sure:
                Logl.e("index: " + wheelView.getSeletedIndex());
                listener.getIndex(wheelView.getSeletedIndex());
                dismiss();
                break;
        }
    }

    public interface IndexCallBack {
        void getIndex(int index);
    }

}
