package com.yuntu.mawei.view;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;

import com.bigkoo.convenientbanner.holder.Holder;
import com.yuntu.mawei.bean.banner.BannerBean;
import com.yuntu.mawei.util.DpUtil;
import com.yuntu.mawei.util.GlideHelper;
import com.yuntu.mawei.util.Logl;
import com.yuntu.mawei.widget.RoundImageView;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by tanghao on 2019/1/21
 */
public class NetworkImageHolderView implements Holder<BannerBean> {
    private RoundImageView imageView;

    @Override
    public View createView(Context context) {
        imageView = new RoundImageView(context, 1, DpUtil.dip2px(context, 4));
        imageView.setScaleType(ImageView.ScaleType.FIT_XY);
        return imageView;
    }

    @Override
    public void UpdateUI(Context context, int position, BannerBean data) {
        GlideHelper.loadListPic(context, data.navigationUrl, imageView);
    }

}
