package com.yuntu.mawei.view;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.umeng.socialize.ShareAction;
import com.umeng.socialize.UMShareAPI;
import com.umeng.socialize.UMShareListener;
import com.umeng.socialize.bean.SHARE_MEDIA;
import com.umeng.socialize.media.UMImage;
import com.umeng.socialize.media.UMWeb;
import com.yuntu.mawei.R;
import com.yuntu.mawei.bean.base.ResponseBean;
import com.yuntu.mawei.bean.music.Music;
import com.yuntu.mawei.bean.music.PlayModeEnum;
import com.yuntu.mawei.bean.rx.SuccessSubscriber;
import com.yuntu.mawei.ui.playing.gift.GiftAdapter;
import com.yuntu.mawei.ui.playing.gift.GiftBean;
import com.yuntu.mawei.ui.playing.service.AudioPlayer;
import com.yuntu.mawei.ui.playing.service.OnPlayerEventListener;
import com.yuntu.mawei.util.RxUtils;
import com.yuntu.mawei.util.ToastUtil;
import com.yuntu.mawei.util.http.HttpUtil;
import com.yuntu.mawei.util.play.Preferences;
import com.yuntu.mawei.view.adapter.helper.RecyclerViewHelper;

import java.util.List;

/**
 * @author tanghao
 * @Date 2018/5/7.
 */
public class ShareDialog extends Activity implements View.OnClickListener {

    String title, intro, pic, url;
    UMWeb web;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_share);
        init();
        initWindow();
    }

    private void initWindow() {
        Window dialogWindow = getWindow();
        dialogWindow.setGravity(Gravity.BOTTOM);
        dialogWindow.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
    }

    private SHARE_MEDIA[] shareStyle = {SHARE_MEDIA.WEIXIN, SHARE_MEDIA.WEIXIN_CIRCLE};

    public void init() {
        findViewById(R.id.bt_weixin_share).setOnClickListener(this);
        findViewById(R.id.bt_qq_share).setOnClickListener(this);
        findViewById(R.id.view_kongbai).setOnClickListener(this);
        findViewById(R.id.bt_cancel).setOnClickListener(this);
        title = getIntent().getStringExtra("title");
        intro = getIntent().getStringExtra("intro");
        pic = getIntent().getStringExtra("pic");
        url = getIntent().getStringExtra("url");

    }

    public void startShare(int position) {
        web = new UMWeb(url);
        web.setTitle(title);
        web.setThumb(new UMImage(this, pic));
        web.setDescription(intro);

        new ShareAction(this)
                .withText(title)
                .withMedia(web)
                .setPlatform(shareStyle[position])
                .setCallback(shareListener).share();
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.bt_weixin_share:
//                startShare(0);
                break;
            case R.id.bt_qq_share:
//                startShare(1);
                break;
            case R.id.view_kongbai:
                finish();
                break;
            case R.id.bt_cancel:
                finish();
                break;
        }
    }

    private UMShareListener shareListener = new UMShareListener() {
        @Override
        public void onStart(SHARE_MEDIA share_media) {
        }

        @Override
        public void onResult(SHARE_MEDIA platform) {
            finish();
        }

        @Override
        public void onError(SHARE_MEDIA platform, Throwable t) {
        }

        @Override
        public void onCancel(SHARE_MEDIA platform) {
        }
    };

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        UMShareAPI.get(this).onActivityResult(requestCode, resultCode, data);
    }

    public static void launch(Context context, String title, String intro, String pic, String url) {
        Intent intent = new Intent(context, ShareDialog.class);
        intent.putExtra("title", title);
        intent.putExtra("intro", intro);
        intent.putExtra("pic", pic);
        intent.putExtra("url", url);
        context.startActivity(intent);
    }

    public static void launch(Context context) {
        Intent intent = new Intent(context, ShareDialog.class);
        context.startActivity(intent);
    }

}
