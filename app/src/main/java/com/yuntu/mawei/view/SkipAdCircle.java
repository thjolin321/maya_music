package com.yuntu.mawei.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.widget.ProgressBar;

import com.yuntu.mawei.R;

public class SkipAdCircle extends ProgressBar {


    private static final int DEFAULT_REACHED_COLOR = 0xffE4403A;
    private static final int DEFAULT_UNREACHED_COLOR = 0xffbebebe;
    private static final int DEFAULT_CIRCLE_BG = 0xffffffff;
    private static final int DEFAULT_TEXTSIZE = 12; //sp
    private static final int DEFAULT_TEXTCOLOR = 0xffffffff;
    private static final int DEFAULT_RADIOS = 20; //dp

    private int reachedColor = DEFAULT_REACHED_COLOR;
    private int textSize = sp2px(DEFAULT_TEXTSIZE);
    private int textColor = DEFAULT_TEXTCOLOR;
    private int radios = dp2px(DEFAULT_RADIOS);
    private int reachedSize = 8;

    private int mMaxPaintWidth;

    private String text = "跳过";

    private Paint mPaint = new Paint();

    private RectF re;

    private boolean yiciCreate = true;

    public SkipAdCircle(Context context) {
        this(context, null);
    }

    public SkipAdCircle(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public SkipAdCircle(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        obtainTypeArrays(attrs);
        text = getResources().getString(R.string.skip_circle);
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setTextSize(textSize);
        mPaint.setAntiAlias(true);// 抗锯齿
        mPaint.setDither(true);
        mPaint.setStrokeCap(Paint.Cap.ROUND);

    }

    private void obtainTypeArrays(AttributeSet attrs) {
        TypedArray ta = getContext().obtainStyledAttributes(attrs, R.styleable.SkipAdCircle);
        reachedColor = ta.getColor(R.styleable.SkipAdCircle_skip_reached_color, reachedColor);
        textSize = (int) ta.getDimension(R.styleable.SkipAdCircle_skip_textsize, textSize);
        textColor = (int) ta.getDimension(R.styleable.SkipAdCircle_skip_textcolor, textColor);
        radios = (int) ta.getDimension(R.styleable.SkipAdCircle_skip_circle_radios, radios);
        ta.recycle();
    }

    @Override
    protected synchronized void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        mMaxPaintWidth = 8;

        int expect = radios * 2 + mMaxPaintWidth + getPaddingLeft() * 2;
        int width = resolveSize(expect, widthMeasureSpec);
        int height = resolveSize(expect, heightMeasureSpec);

        int readWidth = Math.min(width, height);

        radios = (readWidth - getPaddingLeft() - getPaddingRight() - mMaxPaintWidth) / 2;

        if(yiciCreate) {
            re = new RectF(0, 0, radios * 2, radios * 2);
            yiciCreate = false;
        }
        setMeasuredDimension(readWidth, readWidth);
    }

    @Override
    protected synchronized void onDraw(Canvas canvas) {

        canvas.save();
        canvas.translate(getPaddingLeft() + mMaxPaintWidth / 2, getPaddingTop() + mMaxPaintWidth / 2);

        mPaint.setColor(DEFAULT_CIRCLE_BG);
        mPaint.setStyle(Paint.Style.FILL);
        canvas.drawCircle(radios,radios,radios,mPaint);


        mPaint.setStyle(Paint.Style.STROKE);
        // draw reach bar
        mPaint.setColor(reachedColor);
        mPaint.setStrokeWidth(reachedSize);
        float sweepAngle = getProgress() * 1.0f / getMax() * 360;

        canvas.drawArc(re, 270, sweepAngle, false, mPaint);

        canvas.restore();
    }


    private int dp2px(int dpVal) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dpVal, getResources().getDisplayMetrics());
    }

    private int sp2px(int spVal) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, spVal, getResources().getDisplayMetrics());
    }

}
