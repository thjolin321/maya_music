package com.yuntu.mawei.view;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.yuntu.mawei.R;
import com.yuntu.mawei.bean.music.Music;
import com.yuntu.mawei.bean.music.PlayModeEnum;
import com.yuntu.mawei.ui.playing.PlayListAdapterWhite;
import com.yuntu.mawei.ui.playing.service.AudioPlayer;
import com.yuntu.mawei.ui.playing.service.OnPlayerEventListener;
import com.yuntu.mawei.util.GsonUtil;
import com.yuntu.mawei.util.Logl;
import com.yuntu.mawei.util.ToastUtil;
import com.yuntu.mawei.util.play.Preferences;
import com.yuntu.mawei.view.adapter.helper.RecyclerViewHelper;

/**
 * @author tanghao
 * @Date 2018/5/7.
 */
public class PlayListDialogWhite extends Activity implements View.OnClickListener, OnPlayerEventListener {

    private TextView tvNum, tvMode;
    private ImageView ivMode;
    private RecyclerView recyclerView;
    private PlayListAdapterWhite adapter;
    //目标项是否在最后一个可见项之后
    private boolean mShouldScroll;
    //记录目标项位置
    private int mToPosition;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_play_list_white);
        init();
        initWindow();
    }

    private void initWindow() {
        Window dialogWindow = getWindow();
        dialogWindow.setGravity(Gravity.BOTTOM);
        dialogWindow.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
    }

    public void init() {
        AudioPlayer.get().addOnPlayEventListener(this);
        tvNum = findViewById(R.id.tv_total_num);
        tvMode = findViewById(R.id.tv_play_order);
        recyclerView = findViewById(R.id.play_recyclerview);
        adapter = new PlayListAdapterWhite(this);
        RecyclerViewHelper.initRecyclerViewV(this, recyclerView, adapter);
        adapter.addItems(AudioPlayer.get().getMusicList());
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (mShouldScroll && RecyclerView.SCROLL_STATE_IDLE == newState) {
                    mShouldScroll = false;
                    smoothMoveToPosition(recyclerView, mToPosition);
                }
            }
        });
        smoothMoveToPosition(recyclerView, AudioPlayer.get().getPlayPosition());
        tvNum.setText(AudioPlayer.get().getMusicList().size()+"首");

        findViewById(R.id.iv_play_order).setOnClickListener(this);
        findViewById(R.id.iv_delete).setOnClickListener(this);
        findViewById(R.id.bt_cancel).setOnClickListener(this);
        findViewById(R.id.view_kongbai).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    /**
     * 滑动到指定位置
     */
    private void smoothMoveToPosition(RecyclerView mRecyclerView, final int position) {
        // 第一个可见位置
        int firstItem = mRecyclerView.getChildLayoutPosition(mRecyclerView.getChildAt(0));
        // 最后一个可见位置
        int lastItem = mRecyclerView.getChildLayoutPosition(mRecyclerView.getChildAt(mRecyclerView.getChildCount() - 1));
        if (position < firstItem) {
            // 第一种可能:跳转位置在第一个可见位置之前，使用smoothScrollToPosition
            mRecyclerView.smoothScrollToPosition(position);
        } else if (position <= lastItem) {
            // 第二种可能:跳转位置在第一个可见位置之后，最后一个可见项之前
            int movePosition = position - firstItem;
            if (movePosition >= 0 && movePosition < mRecyclerView.getChildCount()) {
                int top = mRecyclerView.getChildAt(movePosition).getTop();
                // smoothScrollToPosition 不会有效果，此时调用smoothScrollBy来滑动到指定位置
                mRecyclerView.smoothScrollBy(0, top);
            }
        } else {
            // 第三种可能:跳转位置在最后可见项之后，则先调用smoothScrollToPosition将要跳转的位置滚动到可见位置
            // 再通过onScrollStateChanged控制再次调用smoothMoveToPosition，执行上一个判断中的方法
            mRecyclerView.smoothScrollToPosition(position);
            mToPosition = position;
            mShouldScroll = true;
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_play_order:
                switchPlayMode();
                break;
            case R.id.iv_delete:
                adapter.getData().clear();
                adapter.notifyDataSetChanged();
                AudioPlayer.get().deleteAll();
                break;
            case R.id.bt_cancel:
                finish();
                break;
        }
    }

    public void switchPlayMode() {
        PlayModeEnum mode = PlayModeEnum.valueOf(Preferences.getPlayMode());
        switch (mode) {
            case LOOP:
                mode = PlayModeEnum.SHUFFLE;
                ToastUtil.showToast(R.string.mode_shuffle);
                tvMode.setText(R.string.mode_shuffle);
                break;
            case SHUFFLE:
                mode = PlayModeEnum.SINGLE;
                ToastUtil.showToast(R.string.mode_one);
                tvMode.setText(R.string.mode_one);
                break;
            case SINGLE:
                mode = PlayModeEnum.LOOP;
                ToastUtil.showToast(R.string.mode_loop);
                tvMode.setText(R.string.mode_loop);
                break;
        }
        Preferences.savePlayMode(mode.value());
        initPlayMode();

    }

    public void initPlayMode() {
        int mode = Preferences.getPlayMode();
        ivMode.setImageLevel(mode);
    }

    @Override
    public void onChange(Music music) {
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onPlayerStart() {

    }

    @Override
    public void onPlayerPause() {

    }

    @Override
    public void onPublish(int progress) {

    }

    @Override
    public void onBufferingUpdate(int percent) {

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        AudioPlayer.get().removeOnPlayEventListener(this);
    }

    public static void launch(Context context) {
        context.startActivity(new Intent(context, PlayListDialogWhite.class));
    }

}
