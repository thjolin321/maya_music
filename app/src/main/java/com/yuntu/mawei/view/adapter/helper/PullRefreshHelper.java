package com.yuntu.mawei.view.adapter.helper;

import in.srain.cube.views.ptr.PtrClassicFrameLayout;

/**
 * Created by long on 2016/12/1.
 * 下拉刷新帮助类
 */
public class PullRefreshHelper {

    private PullRefreshHelper() {
        throw new AssertionError();
    }

    /**
     * 使能刷新
     * @param refreshLayout
     * @param isEnable
     */
    public static void enableRefresh(PtrClassicFrameLayout refreshLayout, boolean isEnable) {
        if (refreshLayout != null) {
            refreshLayout.setEnabled(isEnable);
        }
    }

}
