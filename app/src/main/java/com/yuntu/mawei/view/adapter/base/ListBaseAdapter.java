package com.yuntu.mawei.view.adapter.base;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.List;

/**
 * Created by sanmu on 2016/6/22 0022.
 */
public abstract class ListBaseAdapter<T> extends BaseAdapter {
    public List<T> datas;
    public Context context;

    public ListBaseAdapter(List<T> datas, Context context) {
        super();
        this.datas = datas;
        this.context = context;
    }

    @Override
    public int getCount() {
        return datas.size();
    }

    @Override
    public Object getItem(int position) {
        return datas.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public abstract View getView(int position, View view, ViewGroup arg2);


}
