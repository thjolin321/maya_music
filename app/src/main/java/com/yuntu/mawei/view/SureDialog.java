package com.yuntu.mawei.view;

import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.yuntu.mawei.R;
import com.yuntu.mawei.util.Baseutils;
import com.yuntu.mawei.view.edit.BaseDialog;


/**
 * Created by sanmu on 2017/4/5.
 */
public class SureDialog extends BaseDialog implements View.OnClickListener {

    private TextView title;
    private TextView content;
    private TextView sureBtn;
    Context context;

    public SureDialog(Context context) {
        super(context, R.style.CustomDialog);
        this.context = context;
        setCustomDialog();
        setCancelable(true);
    }

    private void setCustomDialog() {
        View mView = LayoutInflater.from(getContext()).inflate(R.layout.dialog_sure, null);
        sureBtn = mView.findViewById(R.id.dialog_confirm_sure);
        content = mView.findViewById(R.id.dialog_confirm_title);
        title = mView.findViewById(R.id.dialog_confirm_titl);
        sureBtn.setOnClickListener(this);
        super.setContentView(mView);
    }


    public SureDialog setContent(String s) {
        content.setText(s);
        return this;
    }

    public SureDialog setTitle(String s) {
        title.setText(s);
        title.setVisibility(View.VISIBLE);
        return this;
    }

    public TextView getContent() {
        return content;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.dialog_confirm_sure:
                cancel();
                break;
        }
    }

    @Override
    public void show() {
        super.show();
        setGravity(Gravity.CENTER);
        setSize(Baseutils.intance().DM_width * 7 / 10, 0);
    }
}
