package com.yuntu.mawei.view.adapter.entity;

import java.io.Serializable;

/**
 * https://github.com/CymChad/BaseRecyclerViewAdapterHelper
 * @author Tang.Hao
 */
public abstract class MultiItemEntity implements Serializable {
    public int itemType;

    public MultiItemEntity(int itemType) {
        this.itemType = itemType;
    }

    public int getItemType() {
        return itemType;
    }

    public void setItemType(int itemType) {
        this.itemType = itemType;
    }
}
