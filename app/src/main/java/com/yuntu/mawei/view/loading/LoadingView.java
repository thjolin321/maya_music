package com.yuntu.mawei.view.loading;

import android.content.Context;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.yuntu.mawei.R;
import com.yuntu.mawei.util.Logl;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.srain.cube.views.ptr.PtrFrameLayout;
import in.srain.cube.views.ptr.PtrUIHandler;
import in.srain.cube.views.ptr.indicator.PtrIndicator;

/**
 * Created by tanghao on 2017/5/4.
 * Email: 86882259@qq.com
 */

public class LoadingView extends FrameLayout implements PtrUIHandler {

    private Context context;

    @BindView(R.id.tv_xiala_text)
    TextView tvText;

    @BindView(R.id.iv_loading_icon)
    ImageView ivIcon;

    private Animation rotate;

    private boolean haveSetAnim = false;
    private boolean isStartAnim = false;

    public LoadingView(Context context) {
        super(context);
        this.context = context;
        init();
    }

    private void init() {
        View.inflate(context, R.layout.xiala_view, this);
        ButterKnife.bind(this);
        rotate = new RotateAnimation(0f, 360f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        LinearInterpolator lin = new LinearInterpolator();
        rotate.setInterpolator(lin);
        rotate.setDuration(600);//设置动画持续时间
        rotate.setRepeatCount(-1);//设置重复次数
//        rotate.setFillAfter(true);//动画执行完后是否停留在执行完的状态
        Logl.e("init");
    }


    @Override
    public void onUIReset(PtrFrameLayout frame) {
        Logl.e("onUIReset");
        if (isStartAnim) {
            rotate.cancel();
            ivIcon.clearAnimation();
            isStartAnim = false;
        }
        tvText.setText("下拉刷新");
    }

    @Override
    public void onUIRefreshPrepare(PtrFrameLayout frame) {
        tvText.setText("下拉刷新");
        Logl.e("onUIRefreshPrepare");
    }

    @Override
    public void onUIRefreshBegin(PtrFrameLayout frame) {
        if (!isStartAnim) {
            Logl.e("isStartAnim");
            ivIcon.startAnimation(rotate);
            isStartAnim = true;
        }
        tvText.setText("正在刷新");
    }

    @Override
    public void onUIRefreshComplete(PtrFrameLayout frame) {
        Logl.e("onUIRefreshComplete");
        if (isStartAnim) {
            rotate.cancel();
            ivIcon.clearAnimation();
            isStartAnim = false;
        }
        tvText.setText("刷新完成");
    }

    @Override
    public void onUIPositionChange(PtrFrameLayout frame, boolean isUnderTouch, byte status, PtrIndicator ptrIndicator) {
        float percent = Math.min(1f, ptrIndicator.getCurrentPercent());
        if (status == PtrFrameLayout.PTR_STATUS_PREPARE) {
            if (percent != 1f) {
                ivIcon.setRotation(360 * percent);
            }
        }
        if (ptrIndicator.getCurrentPosY() > frame.getOffsetToRefresh() && ptrIndicator.getLastPosY() <= frame.getOffsetToRefresh()) {
            if (isUnderTouch && status == PtrFrameLayout.PTR_STATUS_PREPARE) {
                tvText.setText("松开刷新");
            }
        }

    }
}
