package com.yuntu.mawei.view;

import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.yuntu.mawei.R;
import com.yuntu.mawei.util.Baseutils;
import com.yuntu.mawei.view.edit.BaseDialog;


/**
 * Created by sanmu on 2017/4/5.
 */
public class ConfirmDialog extends BaseDialog implements View.OnClickListener {

    Callback callback;
    private TextView title;
    private TextView content;
    private TextView sureBtn;
    private TextView cancleBtn;
    Context context;

    public ConfirmDialog(Context context, Callback callback) {
        super(context, R.style.CustomDialogNoDismiss);
        this.callback = callback;
        this.context = context;
        setCustomDialog();
        setCancelable(false);
    }

    private void setCustomDialog() {
        View mView = LayoutInflater.from(getContext()).inflate(R.layout.dialog_confirm, null);
        sureBtn = mView.findViewById(R.id.dialog_confirm_sure);
        cancleBtn = mView.findViewById(R.id.dialog_confirm_cancle);
        content = mView.findViewById(R.id.dialog_confirm_title);
        title = mView.findViewById(R.id.dialog_confirm_titl);
        sureBtn.setOnClickListener(this);
        cancleBtn.setOnClickListener(this);
        super.setContentView(mView);
    }


    public ConfirmDialog setContent(String s) {
        content.setText(s);
        return this;
    }

    public ConfirmDialog setTitle(String s) {
        title.setText(s);
        title.setVisibility(View.VISIBLE);
        return this;
    }

    public ConfirmDialog setBtnName(String ls, String rs) {
        cancleBtn.setText(ls);
        sureBtn.setText(rs);
        return this;
    }


    public TextView getContent() {
        return content;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.dialog_confirm_cancle:
                callback.onCancel();
                break;
            case R.id.dialog_confirm_sure:
                callback.callback();
                break;
        }
    }

    @Override
    public void show() {
        super.show();
        setGravity(Gravity.CENTER);
        setSize(Baseutils.intance().DM_width * 8 / 10, 0);
    }

    public interface Callback {
        void callback();
        void onCancel();
    }
}
