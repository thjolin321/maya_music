package com.yuntu.mawei.storage.db.greendao;

import java.util.Map;

import org.greenrobot.greendao.AbstractDao;
import org.greenrobot.greendao.AbstractDaoSession;
import org.greenrobot.greendao.database.Database;
import org.greenrobot.greendao.identityscope.IdentityScopeType;
import org.greenrobot.greendao.internal.DaoConfig;

import com.yuntu.mawei.bean.music.Music;
import com.yuntu.mawei.bean.music.MusicLocal;

import com.yuntu.mawei.storage.db.greendao.MusicDao;
import com.yuntu.mawei.storage.db.greendao.MusicLocalDao;

// THIS CODE IS GENERATED BY greenDAO, DO NOT EDIT.

/**
 * {@inheritDoc}
 * 
 * @see org.greenrobot.greendao.AbstractDaoSession
 */
public class DaoSession extends AbstractDaoSession {

    private final DaoConfig musicDaoConfig;
    private final DaoConfig musicLocalDaoConfig;

    private final MusicDao musicDao;
    private final MusicLocalDao musicLocalDao;

    public DaoSession(Database db, IdentityScopeType type, Map<Class<? extends AbstractDao<?, ?>>, DaoConfig>
            daoConfigMap) {
        super(db);

        musicDaoConfig = daoConfigMap.get(MusicDao.class).clone();
        musicDaoConfig.initIdentityScope(type);

        musicLocalDaoConfig = daoConfigMap.get(MusicLocalDao.class).clone();
        musicLocalDaoConfig.initIdentityScope(type);

        musicDao = new MusicDao(musicDaoConfig, this);
        musicLocalDao = new MusicLocalDao(musicLocalDaoConfig, this);

        registerDao(Music.class, musicDao);
        registerDao(MusicLocal.class, musicLocalDao);
    }
    
    public void clear() {
        musicDaoConfig.clearIdentityScope();
        musicLocalDaoConfig.clearIdentityScope();
    }

    public MusicDao getMusicDao() {
        return musicDao;
    }

    public MusicLocalDao getMusicLocalDao() {
        return musicLocalDao;
    }

}
