package com.yuntu.mawei.widget;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

/**
 * Created by sanmu on 2017/3/21.
 * meail: 992759969@qq.com
 */

public class YanweiTextView extends android.support.v7.widget.AppCompatTextView {

    public YanweiTextView(Context context) {
        super(context);
        if(isInEditMode()) { return; }
        this.setTypeface(Typeface.createFromAsset(context.getAssets(), "yanweiapp.ttf"));
    }

    public YanweiTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        if(isInEditMode()) { return; }
        this.setTypeface(Typeface.createFromAsset(context.getAssets(), "yanweiapp.ttf"));
    }

    public YanweiTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        if(isInEditMode()) { return; }
        this.setTypeface(Typeface.createFromAsset(context.getAssets(), "yanweiapp.ttf"));
    }
}
