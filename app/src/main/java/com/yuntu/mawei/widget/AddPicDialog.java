package com.yuntu.mawei.widget;

import android.app.Activity;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.yuntu.mawei.R;
import com.yuntu.mawei.util.Baseutils;
import com.yuntu.mawei.view.edit.BaseDialog;
import com.yuntu.mawei.view.edit.IntCallback;

/**
 * Created by sanmu on 2016/7/6 0006.
 */
public class AddPicDialog extends BaseDialog implements View.OnClickListener {
    public static final int TAKEPHOTO = 1;//相机
    public static final int TAKEALBUM = 2;//相册

    View view;
    TextView takePhoto, takeAlbum, cancel;
    private Activity context;

    public AddPicDialog(Activity context, IntCallback callback) {
        super(context, callback);
        view = LayoutInflater.from(context).inflate(R.layout.dialog_addpic, null);
        takePhoto = (TextView) view.findViewById(R.id.takephoto);
        takePhoto.setOnClickListener(this);
        takeAlbum = (TextView) view.findViewById(R.id.takealbum);
        takeAlbum.setOnClickListener(this);
        cancel = (TextView) view.findViewById(R.id.pic_cancel);
        cancel.setOnClickListener(this);
        this.setContentView(view);
        getWindow().setWindowAnimations(R.style.timepopwindow_anim_style);
        this.context = context;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.takephoto:
                callback.callback(TAKEPHOTO);
                break;

            case R.id.takealbum:
                callback.callback(TAKEALBUM);
                break;

            case R.id.pic_cancel:
                dismiss();
                break;
        }
    }

    @Override
    public void show() {
        super.show();
        setGravity(Gravity.BOTTOM);
        setSize(Baseutils.intance().DM_width, 0);
    }
}
