package com.yuntu.mawei.widget;

import android.content.Context;
import android.graphics.Typeface;

import net.lucode.hackware.magicindicator.buildins.commonnavigator.titles.ColorTransitionPagerTitleView;

/**
 * @author feng
 * @Date 2018/8/2.
 */
public class ColorStyleTransitionPagerTitleView extends ColorTransitionPagerTitleView {

    private Typeface normalTypeface;
    private Typeface selectedTypeface;

    public ColorStyleTransitionPagerTitleView(Context context) {
        super(context);
    }


    public void setNormalTypeface(Typeface normalTypeface) {
    }

    public void setSelectedTypeface(Typeface selectedTypeface) {
    }

    @Override
    public void onDeselected(int index, int totalCount) {
        super.onDeselected(index, totalCount);
        if (normalTypeface != null) {
            this.setTypeface(normalTypeface);
        }
    }

    @Override
    public void onSelected(int index, int totalCount) {
        super.onSelected(index, totalCount);
        if (selectedTypeface != null) {
            this.setTypeface(selectedTypeface);
        }
    }
}
