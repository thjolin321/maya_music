package com.yuntu.mawei.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by sanmu on 2017/4/18.
 * meail: 992759969@qq.com
 */

public class BoldTextView extends TextView {
    public BoldTextView(Context context) {
        super(context);
        this.getPaint().setFakeBoldText(true);
    }

    public BoldTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.getPaint().setFakeBoldText(true);
    }


}
