package com.yuntu.mawei.ui.mine.user;

import java.io.Serializable;

/**
 * Created by tanghao on 2019/1/29
 */
public class UserBean implements Serializable {

    public int userId;
    public String appUserSex;
    public String appUserAvatar;
    public String appUserPhone;
    public String appUserName;
    public String appUserAge;
    public String appUserWxCode;
    public String appUserEmail;
    public String vipCountdown;
    public double appUserMoney;
    public boolean isVIP;
    public long appLevelOverTime;

}
