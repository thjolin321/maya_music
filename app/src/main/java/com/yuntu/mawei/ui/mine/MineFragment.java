package com.yuntu.mawei.ui.mine;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.gyf.barlibrary.ImmersionBar;
import com.yuntu.mawei.BuildConfig;
import com.yuntu.mawei.R;
import com.yuntu.mawei.base.TaipingApplication;
import com.yuntu.mawei.base.home.BaseFragment;
import com.yuntu.mawei.bean.base.ResponseBean;
import com.yuntu.mawei.bean.money.ImageUploadBean;
import com.yuntu.mawei.bean.rx.SuccessSubscriber;
import com.yuntu.mawei.ui.login.LoginActivity;
import com.yuntu.mawei.ui.mine.set.SettingActivity;
import com.yuntu.mawei.ui.mine.user.UserBean;
import com.yuntu.mawei.ui.mine.user.UserInfoActivity;
import com.yuntu.mawei.util.GlideHelper;
import com.yuntu.mawei.util.ImageFileUtils;
import com.yuntu.mawei.util.Logl;
import com.yuntu.mawei.util.RxUtils;
import com.yuntu.mawei.util.SharedPreferenceUtil;
import com.yuntu.mawei.util.ToastUtil;
import com.yuntu.mawei.util.http.HttpUtil;
import com.yuntu.mawei.view.adapter.base.BaseQuickAdapter;
import com.yuntu.mawei.view.adapter.helper.RecyclerViewHelper;
import com.yuntu.mawei.view.edit.IntCallback;
import com.yuntu.mawei.view.loading.LoadingDialog;
import com.yuntu.mawei.widget.AddPicDialog;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import kr.co.namee.permissiongen.PermissionFail;
import kr.co.namee.permissiongen.PermissionGen;
import kr.co.namee.permissiongen.PermissionSuccess;

/**
 * Created by tanghao on 2019/1/24
 */
public class MineFragment extends BaseFragment {

    @BindView(R.id.rv_mine)
    RecyclerView rvMine;
    @BindView(R.id.bt_login_icon)
    ImageView ivUser;
    @BindView(R.id.bt_login_text)
    TextView tvUser;

    private AddPicDialog dialogPic;
    private List<MineBean> mineBeans;

    private File mOutputFile;
    private File mOnputFile;
    private String sdPath;
    private UserBean userBean;
    private BaseQuickAdapter mineAdapter;

    private static final int ICON_FROM_CAMERA = 0;
    private static final int ICON_FROM_ALBUM = 1;
    private static final int ICON_CROP = 2;

    @Override
    public int getLayoutId() {
        return R.layout.mine_fragment;
    }

    @Override
    public void initView(View view) {
        ImmersionBar.setTitleBar(getActivity(), view.findViewById(R.id.layout_icon));
        initMine();
    }

    private void initMine() {
        sdPath = TaipingApplication.tpApp.getAppCacheDir();
        Logl.e("sdPath:" + sdPath);
        mOutputFile = new File(sdPath, System.currentTimeMillis() + ".tmp");
        mineBeans = new ArrayList<>();
        mineBeans.add(new MineBean(1, R.mipmap.mine_icon_1, R.string.mine_str_1));
        mineBeans.add(new MineBean(2, R.mipmap.mine_icon_2, R.string.mine_str_2));
        mineBeans.add(new MineBean(3, R.mipmap.mine_icon_3, R.string.mine_str_3));
        mineBeans.add(new MineBean(4, R.mipmap.mine_icon_4, R.string.mine_str_4));
        mineBeans.add(new MineBean(5, R.mipmap.mine_icon_5, R.string.mine_str_5));
        mineBeans.add(new MineBean(6, R.mipmap.mine_icon_6, R.string.mine_str_6));
        mineBeans.add(new MineBean(7, R.mipmap.mine_icon_7, R.string.mine_str_7));
        mineBeans.add(new MineBean(8, R.mipmap.mine_icon_8, R.string.mine_str_8));
        mineBeans.add(new MineBean(9, R.mipmap.mine_icon_9, R.string.mine_str_9));
        mineAdapter = new MineAdapter(mContext);
        RecyclerViewHelper.initRecyclerViewV(mContext, rvMine, true, mineAdapter);
        mineAdapter.addItems(mineBeans);
    }

    @Override
    public void onResume() {
        super.onResume();
        initHttpData();
    }

    private void initHttpData() {
        if (!TaipingApplication.tpApp.getIsLogin()) {
            ivUser.setImageResource(R.mipmap.mine_login);
            tvUser.setText(R.string.click_login);
            return;
        }
        HttpUtil.getInstance().getApiService().getUserInfo().compose(RxUtils.rxSchedulerHelper())
                .subscribe(new SuccessSubscriber<ResponseBean<UserBean>>() {
                    @Override
                    public void onSuccess(ResponseBean<UserBean> responseBean) {
                        userBean = responseBean.getResult();
                        if (userBean != null) {
                            tvUser.setText(!TextUtils.isEmpty(userBean.appUserName) ?
                                    userBean.appUserName : userBean.appUserPhone);
                            ivUser.setImageResource(R.mipmap.music_defaul_icon);
                            GlideHelper.loadListPic(mContext, userBean.appUserAvatar, ivUser);
                            TaipingApplication.tpApp.isVIP = responseBean.getResult().isVIP;
                            TaipingApplication.tpApp.setPhone(userBean.appUserPhone);
                            if (userBean.appLevelOverTime != 0) {
                                long day = (userBean.appLevelOverTime - System.currentTimeMillis());
                                day = day / 1000 / 60 / 60 / 24;
                                String time = "您会员还有" + day + "天到期";
                                mineBeans.get(1).time = time;
                                mineAdapter.notifyDataSetChanged();
                            }else{
                                mineBeans.get(1).time = "";
                                mineAdapter.notifyDataSetChanged();
                            }
                        }
                    }

                    @Override
                    public void onFailed(String str) {
                    }
                });
    }

    @OnClick({R.id.bt_set, R.id.layout_icon, R.id.bt_login_icon})
    public void onMineClick(View view) {
        switch (view.getId()) {
            case R.id.layout_icon:
                if (!TaipingApplication.tpApp.getIsLogin()) {
                    LoginActivity.launch(mContext);
                } else {
                    UserInfoActivity.launch(mContext);
                }
                break;
            case R.id.bt_set:
                SettingActivity.launch(mContext);
                break;
            case R.id.bt_login_icon:
                if (!TaipingApplication.tpApp.getIsLogin()) {
                    LoginActivity.launch(mContext);
                    return;
                }
                dialogPic = new AddPicDialog(getActivity(), new IntCallback() {
                    @Override
                    public void callback(int i) {
                        switch (i) {
                            case 1:
                                if (ContextCompat.checkSelfPermission(mContext,
                                        Manifest.permission.CAMERA)
                                        != PackageManager.PERMISSION_GRANTED) {
                                    PermissionGen.with(getActivity())
                                            .addRequestCode(234)
                                            .permissions(
                                                    Manifest.permission.CAMERA,
                                                    Manifest.permission.READ_EXTERNAL_STORAGE
                                            )
                                            .request();
                                } else {
                                    openCamera();
                                }
                                break;
                            case 2:
                                if (ContextCompat.checkSelfPermission(mContext,
                                        Manifest.permission.READ_EXTERNAL_STORAGE)
                                        != PackageManager.PERMISSION_GRANTED) {
                                    PermissionGen.with(getActivity())
                                            .addRequestCode(235)
                                            .permissions(
                                                    Manifest.permission.READ_EXTERNAL_STORAGE
                                            )
                                            .request();
                                } else {
                                    openAlbum();
                                }
                                break;
                        }
                        dialogPic.dismiss();
                    }
                });
                dialogPic.show();
                break;
        }
    }


    // 启动相机
    public void openCamera() {
        // Uri uri = Uri.fromFile(mOutputFile);
        Uri uri = Uri.parse("file://" + mOutputFile.getAbsolutePath());
        Intent newIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            uri = FileProvider.getUriForFile(mContext, BuildConfig.APPLICATION_ID + ".fileprovider",
                    new File(uri.getPath()));
            newIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION
                    | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
        }

        newIntent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
        startActivityForResult(newIntent, ICON_FROM_CAMERA);
    }

    // 启动相册
    public void openAlbum() {
        Intent intent_album = new Intent("android.intent.action.GET_CONTENT");
        intent_album.setType("image/*");
        startActivityForResult(intent_album, ICON_FROM_ALBUM);
    }

    //裁剪图片
    private void clipPhoto(Uri uri) {
        Intent intent = new Intent("com.android.camera.action.CROP");

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            uri = FileProvider.getUriForFile(mContext, BuildConfig.APPLICATION_ID + ".fileprovider",
                    new File(uri.getPath()));
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        }


        intent.setDataAndType(uri, "image/*");
        // 下面这个crop=true是设置在开启的Intent中设置显示的VIEW可裁剪
        intent.putExtra("crop", "true");
        // aspectX aspectY 是宽高的比例
        intent.putExtra("aspectX", 1);
        intent.putExtra("aspectY", 1);
        // outputX outputY 是裁剪图片宽高
        intent.putExtra("outputX", 150);
        intent.putExtra("outputY", 150);
        intent.putExtra("circleCrop", true);
        mOnputFile = new File(sdPath, System.currentTimeMillis() + ".png");
        // intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(mOnputFile));


        intent.putExtra(MediaStore.EXTRA_OUTPUT,
                Uri.parse("file://" + mOnputFile.getAbsolutePath()));

        startActivityForResult(intent, ICON_CROP);
        deleFile(mOnputFile);
    }

    // 删除文件
    void deleFile(File file) {
        file.delete();
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                           int[] grantResults) {
        PermissionGen.onRequestPermissionsResult(this, requestCode, permissions, grantResults);
        Logl.e("onRequestPermissionsResult");
    }

    @PermissionSuccess(requestCode = 234)
    private void doSthing() {
        openCamera();
    }

    @PermissionFail(requestCode = 234)
    private void doFailSthing() {
        ToastUtil.showToast("请打开相机权限哦");
    }

    @PermissionSuccess(requestCode = 235)
    private void doSthing1() {
        openAlbum();
    }

    @PermissionFail(requestCode = 235)
    private void doFailSthing1() {
        ToastUtil.showToast("请打开储存权限哦");
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case ICON_FROM_ALBUM: //从相册选择
                    if (data == null || data.getData() == null) {
                        Logl.e("居然就为null?");
                        return;
                    }
                    clipPhoto(Uri.fromFile(new File(ImageFileUtils.getPath(mContext, data.getData()))
                    )); //裁剪图片
                    break;

                case ICON_FROM_CAMERA: //拍照
                    clipPhoto(Uri.fromFile(mOutputFile));
                    break;

                case ICON_CROP: //裁剪图片
                    setImge(ivUser, mOnputFile.getAbsoluteFile());
                    if (dialogPic != null)
                        dialogPic.dismiss();
                    Logl.e("在这里上传图片");
                    LoadingDialog loadingDialog = new LoadingDialog(mContext);
                    loadingDialog.show();
                    HttpUtil.getInstance().uploadUserAvator(mOnputFile.getAbsolutePath())
                            .subscribe(new SuccessSubscriber<ResponseBean>() {
                                @Override
                                public void onSuccess(ResponseBean stringResponseBean) {
                                    loadingDialog.dismiss();
                                    initHttpData();
                                }

                                @Override
                                public void onFailed(String str) {
                                    ToastUtil.showToast(str);
                                    loadingDialog.dismiss();
                                }
                            });

                    break;
            }
        }
    }

    public void setImge(ImageView view, Object url) {
        if (url instanceof File) {
            GlideHelper.setFileToView(mContext, (File) url, view);
        } else if (url instanceof String) {
            GlideHelper.loadListPic(mContext, (String) url, view);
        }
    }

}
