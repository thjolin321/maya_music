package com.yuntu.mawei.ui.login;

import android.content.Context;
import android.content.Intent;
import android.webkit.WebView;

import com.yuntu.mawei.R;
import com.yuntu.mawei.base.home.BaseActivity;
import com.yuntu.mawei.bean.base.ResponseBean;
import com.yuntu.mawei.bean.base.XieyiBean;
import com.yuntu.mawei.bean.rx.SuccessSubscriber;
import com.yuntu.mawei.util.RxUtils;
import com.yuntu.mawei.util.ToastUtil;
import com.yuntu.mawei.util.http.HttpUtil;

import butterknife.BindView;

/**
 * Created by tanghao on 2019/2/27
 */
public class MyWebViewActivity extends BaseActivity {

    //0：操作指引，
    //1：会员说明，
    //2：注册协议，
    //3：帮助信息，
    //4：反馈信息,
    //5：充值说明，
    //6：会员特权说明

    int type;
    @BindView(R.id.webview)
    WebView webView;

    @Override
    public int attachLayout() {
        return R.layout.activity_webview;
    }

    @Override
    public void init() {
        type = getIntent().getIntExtra("type", 0);
        switch (type) {
            case 0:
                initTitle("关于我们");
                break;
            case 1:
                initTitle("会员特权说明");
                break;
            case 2:
                initTitle("注册协议");
                break;
            case 3:
                initTitle("帮助信息");
                break;
            case 4:
                initTitle("反馈信息");
                break;
            case 5:
                initTitle("充值说明");
                break;
            case 6:
                initTitle("会员特权说明");
                break;
        }

        HttpUtil.getInstance().getApiService().getXieyi(type)
                .compose(RxUtils.rxSchedulerHelper())
                .subscribe(new SuccessSubscriber<ResponseBean<XieyiBean>>() {
                    @Override
                    public void onSuccess(ResponseBean<XieyiBean> xieyiBeanResponseBean) {
                        webView.loadDataWithBaseURL(null, xieyiBeanResponseBean.getResult().deployDetail, "text/html", "UTF-8", null);
                    }

                    @Override
                    public void onFailed(String str) {
                        ToastUtil.showToast(str);
                    }
                });
    }

    public static void launch(Context context, int type) {
        Intent intent = new Intent(context, MyWebViewActivity.class);
        intent.putExtra("type", type);
        context.startActivity(intent);
    }
}
