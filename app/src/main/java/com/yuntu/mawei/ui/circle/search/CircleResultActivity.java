package com.yuntu.mawei.ui.circle.search;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import com.yuntu.mawei.R;
import com.yuntu.mawei.base.TaipingApplication;
import com.yuntu.mawei.base.home.BaseListActivity;
import com.yuntu.mawei.base.home.BaseListActivityPresenter;
import com.yuntu.mawei.ui.circle.CircleAdapter;
import com.yuntu.mawei.ui.circle.publish.PublishActivity;
import com.yuntu.mawei.ui.login.LoginActivity;
import com.yuntu.mawei.ui.search.result.SearchResultActivity;
import com.yuntu.mawei.util.Logl;
import com.yuntu.mawei.util.ToastUtil;
import com.yuntu.mawei.view.adapter.base.BaseQuickAdapter;
import com.yuntu.mawei.view.loading.LoadingView;

import java.util.Date;

import butterknife.BindView;
import butterknife.OnClick;
import in.srain.cube.views.ptr.PtrClassicFrameLayout;
import in.srain.cube.views.ptr.PtrDefaultHandler;
import in.srain.cube.views.ptr.PtrFrameLayout;
import in.srain.cube.views.ptr.PtrHandler;
import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;

public class CircleResultActivity extends BaseListActivity {

    @BindView(R.id.refreshable_view)
    PtrClassicFrameLayout refreshableView;
    @BindView(R.id.search_edit)
    EditText searchE;

    Realm realm;
    RealmResults<CircleSearchHistoryBean> realmlists;
    String str;
    CircleResultPresenter circleResultPresenter;

    @Override
    protected void initViews() {
        realm = Realm.getDefaultInstance();
        setRedBar();
        str = getIntent().getStringExtra("search");
        initEdit();
        refreshableView.setLoadingMinTime(500);
        refreshableView.setDurationToCloseHeader(800);
        LoadingView materialHeader = new LoadingView(this);
        refreshableView.setHeaderView(materialHeader);
        refreshableView.addPtrUIHandler(materialHeader);
        refreshableView.setPtrHandler(new PtrHandler() {
            @Override
            public boolean checkCanDoRefresh(PtrFrameLayout frame, View content, View header) {
                return PtrDefaultHandler.checkContentCanBePulledDown(frame, content, header);
            }

            @Override
            public void onRefreshBegin(final PtrFrameLayout frame) {
                //实现下拉刷新的功能
                updateViews(true);
            }
        });
    }

    public void finishRefresh() {
        if (refreshableView != null) {
            refreshableView.refreshComplete();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        updateViews(false);
    }

    @Override
    protected int attachLayoutRes() {
        return R.layout.activity_circle_result;
    }

    @Override
    protected BaseQuickAdapter attachAdapter() {
        return new CircleAdapter(this, false);
    }

    @Override
    protected BaseListActivityPresenter attachPresenter() {
        circleResultPresenter = new CircleResultPresenter(this, getIntent().getStringExtra("search"));
        return circleResultPresenter;
    }

    @OnClick(R.id.search_cancel)
    public void gotoSearch() {
        shutDownInput();
        finish();
    }


    private void shutDownInput() {
        try {
            InputMethodManager mInputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            mInputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        } catch (Exception e) {
        }
    }

    private void initEdit() {
        searchE.setText(str);
        searchE.setFocusable(true);
        searchE.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView arg0, int arg1, KeyEvent arg2) {
                if (arg1 == EditorInfo.IME_ACTION_SEARCH) {
                    String searchS;
                    if (!(searchS = searchE.getText().toString().trim()).equals("")) {
                        ((InputMethodManager) getSystemService(INPUT_METHOD_SERVICE))
                                .hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),
                                        InputMethodManager.HIDE_NOT_ALWAYS);
                        goToSearch(searchS);
                    } else {
                        ToastUtil.showToast("搜索不能为空");
                    }
                }
                return false;
            }
        });
    }

    private void goToSearch(String str) {
        addData(str);
        circleResultPresenter.str = str;
        updateViews(false);
    }


    public void addData(String s) {
        boolean isAdd = true;
        getRealmData();
        if (realmlists != null) {
            for (CircleSearchHistoryBean hasS : realmlists) {
                if (s.equals(hasS.getContent())) {
                    isAdd = false;
                    try {
                        realm.beginTransaction();
                        hasS.setUpdateTime(new Date().getTime());
                        realm.commitTransaction();
                    } catch (Exception e) {
                        Log.i("this", "Exception1:" + e.toString());
                    } finally {
                    }
                }
            }
        }
        if (isAdd) {
            try {
                CircleSearchHistoryBean searchHistory = new CircleSearchHistoryBean();
                searchHistory.setContent(s);
                long time = new Date().getTime();
                searchHistory.setCreateTime(time);
                searchHistory.setUpdateTime(time);
                realm.executeTransactionAsync(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        realm.copyToRealm(searchHistory);
                    }
                });
            } catch (Exception e) {
                Logl.e("插入出错？" + e.getMessage());
            } finally {
            }
        }
    }

    public void getRealmData() {
        try {
            realmlists = realm.where(CircleSearchHistoryBean.class).findAll();
            realmlists = realmlists.sort("updateTime", Sort.DESCENDING);
        } catch (Exception e) {
        } finally {
        }
        if (realmlists == null) return;
        return;
    }


    public static void launch(Context context, String str) {
        Intent intent = new Intent(context, CircleResultActivity.class);
        intent.putExtra("search", str);
        context.startActivity(intent);
    }

}
