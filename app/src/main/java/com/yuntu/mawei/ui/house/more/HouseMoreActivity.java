package com.yuntu.mawei.ui.house.more;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.GridLayoutManager;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.yuntu.mawei.R;
import com.yuntu.mawei.base.home.BaseListActivity;
import com.yuntu.mawei.base.home.BaseListActivityPresenter;
import com.yuntu.mawei.bean.music.Music;
import com.yuntu.mawei.bean.song.SongBean;
import com.yuntu.mawei.ui.homepage.mv.MvAdapter;
import com.yuntu.mawei.ui.house.detail.SongListAdapter;
import com.yuntu.mawei.ui.playing.PlayActivity;
import com.yuntu.mawei.ui.playing.service.AudioPlayer;
import com.yuntu.mawei.ui.playing.service.OnPlayerEventListener;
import com.yuntu.mawei.util.GlideHelper;
import com.yuntu.mawei.util.Logl;
import com.yuntu.mawei.view.PlayListDialog;
import com.yuntu.mawei.view.PlayListDialogWhite;
import com.yuntu.mawei.view.adapter.base.BaseQuickAdapter;
import com.yuntu.mawei.view.adapter.helper.RecyclerViewHelper;

import butterknife.BindView;
import butterknife.OnClick;

public class HouseMoreActivity extends BaseListActivity implements OnPlayerEventListener {

    @BindView(R.id.iv_song)
    ImageView ivSong;
    @BindView(R.id.tv_song)
    TextView tvSong;
    @BindView(R.id.tv_artist)
    TextView tvArtist;
    @BindView(R.id.bt_play)
    ImageView btPlay;
    @BindView(R.id.tv_top_title)
    TextView tvHotTitle;
    @BindView(R.id.more_progressbar)
    ProgressBar songProgress;
    @BindView(R.id.bt_house_more_bottom)
    View viewBottom;

    boolean isResume = true;
    private int type; // 4 for 标题变成乐库

    @Override
    protected void initViews() {
        AudioPlayer.get().addOnPlayEventListener(this);
        type = getIntent().getIntExtra("type", 0);
        Logl.e("状态: " + AudioPlayer.get().state);

        switch (type) {
            case 0:
                initTitle(R.string.home1);
                break;
            case 1:
                initTitle(R.string.home1);
                tvHotTitle.setText("推荐铃声");
                break;
            case 2:
                initTitle(R.string.home1);
                tvHotTitle.setText("MV推荐");
                viewBottom.setVisibility(View.GONE);
                break;
            case 3:
                tvHotTitle.setText("热门音乐");
                initTitle(R.string.home2);
                break;

        }

        if (type == 4) {
            initTitle(R.string.home_top_3);
        } else {
            initTitle(R.string.home1);
        }
        initMusicView(AudioPlayer.get().getPlayMusic());
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (AudioPlayer.get().isPlaying()) {
            btPlay.setImageResource(R.mipmap.pause_play_icon);
        } else {
            btPlay.setImageResource(R.mipmap.play_icon_red);
        }
        if (!isResume) {
            isResume = true;
            return;
        }
    }

    @Override
    protected BaseQuickAdapter attachAdapter() {
        if (type == 2) {
            return new MvAdapter(this);
        } else {
            return new SongMoreAdapter(this);
        }
    }

    @Override
    protected BaseListActivityPresenter attachPresenter() {
        return new HouseMorePresenter(type, this);
    }

    @Override
    protected int attachLayoutRes() {
        return R.layout.hot_song_with_recyclerview;
    }

    @Override
    public void initSwipeRefresh() {
        Logl.e("initSwipeRefresh调用了的啊");
        if (type == 2) {
            RecyclerViewHelper.initRecyclerViewG(this, recyclerView, adapter, 1);
        } else {
            RecyclerViewHelper.initRecyclerViewG(this, recyclerView, adapter, 3);
            GridLayoutManager manager = (GridLayoutManager) recyclerView.getLayoutManager();
            manager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
                @Override
                public int getSpanSize(int position) {
                    if (adapter.getItemViewType(position) == SongBean.SONG_TITLE_ITEM
                            || adapter.getItemViewType(position) == BaseQuickAdapter.LOADING_VIEW) {
                        return 3;
                    }
                    return 1;
                }
            });
        }
        updateViews(false);

    }

    public static void launch(Context context, int type) {
        Intent intent = new Intent(context, HouseMoreActivity.class);
        intent.putExtra("type", type);
        context.startActivity(intent);
    }

    public void initMusicView(Music music) {
        if (music == null) {
            ivSong.setImageResource(R.mipmap.home_no_song_icon_1);
            tvSong.setText("");
            tvArtist.setText("");
            songProgress.setProgress(0);
            return;
        }
        try {
            GlideHelper.loadListPic(HouseMoreActivity.this, music.coverPath, ivSong);
            tvSong.setText(music.title + "");
            tvArtist.setText(music.artist + "");
            songProgress.setMax((int) music.duration);
        } catch (Exception e) {
        }
    }

    @OnClick({R.id.bt_play, R.id.bt_playlist, R.id.bt_house_more_bottom})
    public void onMore(View view) {
        switch (view.getId()) {
            case R.id.bt_play:
                if (AudioPlayer.get().isPlaying()) {
                    btPlay.setImageResource(R.mipmap.play_icon_red);
                } else {
                    btPlay.setImageResource(R.mipmap.pause_play_icon);
                }
                AudioPlayer.get().playPause();
                Logl.e("状态: " + AudioPlayer.get().state);
                break;
            case R.id.bt_playlist:
                PlayListDialogWhite.launch(this);
                break;
            case R.id.bt_house_more_bottom:
                PlayActivity.launch(this);
                break;
        }
    }

    @Override
    public void onChange(Music music) {
        initMusicView(music);
    }

    @Override
    public void onPlayerStart() {
    }

    @Override
    public void onPlayerPause() {
    }

    @Override
    public void onPublish(int progress) {
        songProgress.setProgress(progress);
    }

    @Override
    public void onBufferingUpdate(int percent) {

    }
}
