package com.yuntu.mawei.ui.house.detail;

import com.yuntu.mawei.base.home.BaseListActivity;
import com.yuntu.mawei.base.home.BaseListActivityPresenter;
import com.yuntu.mawei.bean.base.ResponseBean;
import com.yuntu.mawei.bean.house.MusicHouseBean;
import com.yuntu.mawei.bean.rx.SuccessSubscriber;
import com.yuntu.mawei.util.Logl;
import com.yuntu.mawei.util.RxUtils;
import com.yuntu.mawei.util.http.HttpUtil;
import com.yuntu.mawei.util.music.MusicUtil;

/**
 * Created by tanghao on 2019/1/25
 */
public class SongListPresenter extends BaseListActivityPresenter {

    long songTypeId;
    int isFree;
    SongListActivity view;

    public SongListPresenter(SongListActivity mView, long songTypeId) {
        super(mView);
        view = mView;
        this.songTypeId = songTypeId;
    }

    public SongListPresenter(SongListActivity mView, long songTypeId, int isFree) {
        super(mView);
        view = mView;
        this.songTypeId = songTypeId;
        this.isFree = isFree;
    }

    @Override
    public void getData(boolean isPullRefresh) {
        Logl.e("songTypeId:" + songTypeId);
        HttpUtil.getInstance().getApiService().getHouseSongListWithType(songTypeId, isFree, 1, 1000)
                .compose(RxUtils.rxSchedulerHelper()).subscribe(new SuccessSubscriber<ResponseBean<MusicHouseBean>>() {
            @Override
            public void onSuccess(ResponseBean<MusicHouseBean> musicHouseBeanResponseBean) {
                mView.loadData(MusicUtil.sortByZimu(musicHouseBeanResponseBean.getResult().songMap));
                view.initTotalNum("共" + musicHouseBeanResponseBean.getResult().songCount + "首");
            }

            @Override
            public void onFailed(String str) {
            }
        });
    }

    @Override
    public void getMoreData() {
        mView.adapter.loadComplete();
        mView.adapter.noMoreData();
    }
}
