package com.yuntu.mawei.ui.homepage.singer;

import android.content.Context;
import android.view.View;

import com.yuntu.mawei.R;
import com.yuntu.mawei.bean.song.SingerBean;
import com.yuntu.mawei.ui.homepage.singer.detail.SingerDetailActivity;
import com.yuntu.mawei.ui.house.detail.SongListActivity;
import com.yuntu.mawei.util.GlideHelper;
import com.yuntu.mawei.view.adapter.base.BaseQuickAdapter;
import com.yuntu.mawei.view.adapter.base.BaseViewHolder;

public class SingerAdapter extends BaseQuickAdapter<SingerBean> {

    public SingerAdapter(Context context) {
        super(context);
    }

    @Override
    protected int attachLayoutRes() {
        return R.layout.adapter_singer;
    }

    @Override
    protected void convert(BaseViewHolder holder, SingerBean item) {
        GlideHelper.loadListPic(mContext, item.singerAvatar, holder.getView(R.id.iv_singer));
        holder.setText(R.id.tv_singer, item.singerName);
        holder.setText(R.id.tv_play_num, "播放量：" + item.listenCount+"  ");
        holder.getConvertView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SingerDetailActivity.launch(mContext,item);
            }
        });
    }
}
