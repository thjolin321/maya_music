package com.yuntu.mawei.ui.mine.money;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;

import com.yuntu.mawei.R;
import com.yuntu.mawei.base.TaipingApplication;
import com.yuntu.mawei.base.home.BaseActivity;
import com.yuntu.mawei.bean.base.ResponseBean;
import com.yuntu.mawei.bean.rx.SuccessSubscriber;
import com.yuntu.mawei.ui.login.LoginActivity;
import com.yuntu.mawei.ui.mine.set.FeedbackFragment;
import com.yuntu.mawei.ui.mine.set.HelpFragment;
import com.yuntu.mawei.ui.mine.user.UserBean;
import com.yuntu.mawei.util.RxUtils;
import com.yuntu.mawei.util.http.HttpUtil;
import com.yuntu.mawei.widget.ColorStyleTransitionPagerTitleView;

import net.lucode.hackware.magicindicator.MagicIndicator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.CommonNavigator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.CommonNavigatorAdapter;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.IPagerIndicator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.IPagerTitleView;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.indicators.LinePagerIndicator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindColor;
import butterknife.BindView;

/**
 * Created by tanghao on 2019/1/24
 */
public class MoneyActivity extends BaseActivity {

    @BindView(R.id.indicator)
    MagicIndicator indicator;
    @BindView(R.id.view_pager)
    ViewPager viewPager;

    @BindColor(R.color.mall_black)
    int colorBlack;
    @BindColor(R.color.mawei_red)
    int colorBlue;

    private List<Fragment> fragments;
    private FragmentPagerAdapter mAdapter;

    @Override
    public int attachLayout() {
        return R.layout.activity_help;
    }

    @Override
    public void init() {
        if (!TaipingApplication.tpApp.getIsLogin()) {
            LoginActivity.launch(this);
            finish();
        }
        initTitle(R.string.mine_str_3);
        initIndicator();
        initFragments();
    }

    public static void launch(Context context) {
        context.startActivity(new Intent(context, MoneyActivity.class));
    }

    private void initFragments() {
        fragments = new ArrayList<>();
        fragments.add(new MoneyFragment());
        fragments.add(new XiaofeiFragment());

        mAdapter = new FragmentPagerAdapter(getSupportFragmentManager()) {
            @Override
            public int getCount() {
                return fragments.size();
            }

            @Override
            public Fragment getItem(int position) {
                return fragments.get(position);
            }

            @Override
            public long getItemId(int position) {
                return position;
            }
        };
        viewPager.setAdapter(mAdapter);
        viewPager.addOnPageChangeListener(onPageChangeListener);
    }

    private void initIndicator(){
        List<String> titles = Arrays.asList(" 钱包余额 ", " 消费记录 ");
        CommonNavigator commonNavigator = new CommonNavigator(this);
        commonNavigator.setAdjustMode(true);
        List<String> finalTitles = titles;
        commonNavigator.setAdapter(new CommonNavigatorAdapter() {

            @Override
            public int getCount() {
                return finalTitles.size();
            }

            @Override
            public IPagerTitleView getTitleView(Context context, final int index) {
                ColorStyleTransitionPagerTitleView colorTransitionPagerTitleView = new
                        ColorStyleTransitionPagerTitleView(context);
                colorTransitionPagerTitleView.setLayoutParams(new ViewGroup.LayoutParams
                        (ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
                colorTransitionPagerTitleView.setNormalColor(colorBlack);
                colorTransitionPagerTitleView.setTextSize(14);
                colorTransitionPagerTitleView.setSelectedColor(colorBlue);
                colorTransitionPagerTitleView.setText(finalTitles.get(index));
                colorTransitionPagerTitleView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        viewPager.setCurrentItem(index);
                    }
                });
                return colorTransitionPagerTitleView;
            }

            @Override
            public IPagerIndicator getIndicator(Context context) {
                LinePagerIndicator indicator = new LinePagerIndicator(context);
                indicator.setMode(LinePagerIndicator.MODE_WRAP_CONTENT);
                indicator.setColors(colorBlue);
                return indicator;
            }

            @Override
            public float getTitleWeight(Context context, int index) {
                return 1.0f;
            }
        });

        indicator.setNavigator(commonNavigator);
    }


    ViewPager.OnPageChangeListener onPageChangeListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            indicator.onPageScrolled(position, positionOffset, positionOffsetPixels);
        }

        @Override
        public void onPageSelected(int position) {
            indicator.onPageSelected(position);
        }

        @Override
        public void onPageScrollStateChanged(int state) {
            indicator.onPageScrollStateChanged(state);
        }
    };


}
