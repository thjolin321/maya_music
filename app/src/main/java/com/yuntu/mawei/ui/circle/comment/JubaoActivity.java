package com.yuntu.mawei.ui.circle.comment;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.widget.EditText;
import android.widget.TextView;

import com.yuntu.mawei.R;
import com.yuntu.mawei.base.home.BaseActivity;
import com.yuntu.mawei.bean.base.ResponseBean;
import com.yuntu.mawei.bean.circle.CircleBean;
import com.yuntu.mawei.bean.rx.SuccessSubscriber;
import com.yuntu.mawei.util.Logl;
import com.yuntu.mawei.util.RxUtils;
import com.yuntu.mawei.util.ToastUtil;
import com.yuntu.mawei.util.http.HttpUtil;

import butterknife.BindView;
import butterknife.OnClick;

public class JubaoActivity extends BaseActivity {
    @BindView(R.id.title_right)
    TextView tvRigt;
    @BindView(R.id.et_jubao)
    EditText etJubao;
    @BindView(R.id.tv_jubao)
    TextView tvJubao;
    CircleBean circleBean;
    int commentId;

    @Override
    public int attachLayout() {
        return R.layout.activity_jubao;
    }

    @Override
    public void init() {
        initTitle("举报");
        tvRigt.setText("确定");
        circleBean = (CircleBean) getIntent().getSerializableExtra("circleBean");
        commentId = getIntent().getIntExtra("commentId", -1);
        if (circleBean != null) {
            tvJubao.setText("标题    " + circleBean.themeName);
        }
    }

    @OnClick(R.id.title_right)
    public void publishJubao() {
        if (TextUtils.isEmpty(etJubao.getText())) {
            ToastUtil.showToast("内容不能为空");
            return;
        }
        HttpUtil.getInstance().getApiService().jubao(commentId == -1 ? circleBean.themeId : commentId
                , etJubao.getText().toString())
                .compose(RxUtils.rxSchedulerHelper())
                .subscribe(new SuccessSubscriber<ResponseBean>() {
                    @Override
                    public void onSuccess(ResponseBean responseBean) {
                        ToastUtil.showToast(responseBean.getMessage());
                        finish();
                    }

                    @Override
                    public void onFailed(String str) {
                        ToastUtil.showToast(str);
                    }
                });
    }

    public static void launch(Context context, CircleBean circleBean) {
        Intent intent = new Intent(context, JubaoActivity.class);
        intent.putExtra("circleBean", circleBean);
        context.startActivity(intent);
    }

    public static void launch(Context context, CircleBean circleBean, int commentId) {
        Intent intent = new Intent(context, JubaoActivity.class);
        intent.putExtra("circleBean", circleBean);
        intent.putExtra("commentId", commentId);
        context.startActivity(intent);
    }
}
