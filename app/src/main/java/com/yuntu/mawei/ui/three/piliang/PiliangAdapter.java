package com.yuntu.mawei.ui.three.piliang;

import android.content.Context;

import com.yuntu.mawei.R;
import com.yuntu.mawei.bean.song.SongBean;
import com.yuntu.mawei.view.adapter.base.BaseQuickAdapter;
import com.yuntu.mawei.view.adapter.base.BaseViewHolder;

/**
 * Created by tanghao on 2019/2/15
 */
public class PiliangAdapter extends BaseQuickAdapter<SongBean> {

    public PiliangAdapter(Context context) {
        super(context);
    }

    @Override
    protected int attachLayoutRes() {
        return R.layout.adapter_piliang;
    }

    @Override
    protected void convert(BaseViewHolder holder, SongBean item) {
        if (item.isSelected) {
            holder.setImageResource(R.id.bt_piliang_selected, R.mipmap.gou_selected);
        } else {
            holder.setImageResource(R.id.bt_piliang_selected, R.mipmap.gou_unselected);
        }
        holder.setText(R.id.tv_song, item.songName);
        if (item.singer != null) {
            holder.setText(R.id.tv_artist_and_album, item.singer.singerName + " - 《"
                    + (item.album != null ? item.album.albumName : "") + "》");
        }
        holder.setOnClickListener(R.id.bt_piliang_selected, v -> {
            if (item.isSelected) {
                item.isSelected = false;
            } else {
                item.isSelected = true;
            }
            notifyDataSetChanged();
        });
    }

}
