package com.yuntu.mawei.ui.house.detail;

import com.yuntu.mawei.base.home.BaseListActivity;
import com.yuntu.mawei.base.home.BaseListActivityPresenter;
import com.yuntu.mawei.bean.base.ResponseBean;
import com.yuntu.mawei.bean.house.MusicHouseBean;
import com.yuntu.mawei.bean.rx.SuccessSubscriber;
import com.yuntu.mawei.util.RxUtils;
import com.yuntu.mawei.util.http.HttpUtil;

public class SingerListPresenter extends BaseListActivityPresenter {

    int type, page;

    public SingerListPresenter(BaseListActivity mView, int type) {
        super(mView);
        this.type = type;
    }

    @Override
    public void getData(boolean isPullRefresh) {
        HttpUtil.getInstance().getApiService().getSongListWithType(type, 1, 20)
                .compose(RxUtils.rxSchedulerHelper())
                .subscribe(new SuccessSubscriber<ResponseBean<MusicHouseBean>>() {
                    @Override
                    public void onSuccess(ResponseBean<MusicHouseBean> musicHouseBeanResponseBean) {
                        if (musicHouseBeanResponseBean.getResult() == null) return;
                        mView.loadData(musicHouseBeanResponseBean.getResult().singerList);
                        page = 2;
                    }

                    @Override
                    public void onFailed(String str) {
                    }
                });
    }

    @Override
    public void getMoreData() {
        HttpUtil.getInstance().getApiService().getSongListWithType(type, page, 20)
                .compose(RxUtils.rxSchedulerHelper())
                .subscribe(new SuccessSubscriber<ResponseBean<MusicHouseBean>>() {
                    @Override
                    public void onSuccess(ResponseBean<MusicHouseBean> musicHouseBeanResponseBean) {
                        if (musicHouseBeanResponseBean.getResult() == null) return;
                        mView.loadMoreData(musicHouseBeanResponseBean.getResult().singerList);
                        page++;
                    }

                    @Override
                    public void onFailed(String str) {
                    }
                });
    }
}
