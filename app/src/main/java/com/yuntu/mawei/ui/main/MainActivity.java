package com.yuntu.mawei.ui.main;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.hwangjr.rxbus.RxBus;
import com.yuntu.mawei.R;
import com.yuntu.mawei.base.TaipingApplication;
import com.yuntu.mawei.base.home.BaseActivity;
import com.yuntu.mawei.bean.base.ResponseBean;
import com.yuntu.mawei.bean.music.Music;
import com.yuntu.mawei.bean.rx.SuccessSubscriber;
import com.yuntu.mawei.ui.circle.CircleFragment;
import com.yuntu.mawei.ui.homepage.HomepageFragment;
import com.yuntu.mawei.ui.house.MainHouseFragment;
import com.yuntu.mawei.ui.mine.MineFragment;
import com.yuntu.mawei.ui.mine.user.UserBean;
import com.yuntu.mawei.ui.playing.PlayActivity;
import com.yuntu.mawei.ui.playing.service.AudioPlayer;
import com.yuntu.mawei.ui.playing.service.OnPlayerEventListener;
import com.yuntu.mawei.util.GlideHelper;
import com.yuntu.mawei.util.Logl;
import com.yuntu.mawei.util.RxUtils;
import com.yuntu.mawei.util.ToastUtil;
import com.yuntu.mawei.util.http.HttpUtil;
import com.yuntu.mawei.view.SkipAdCircle;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.BindViews;
import kr.co.namee.permissiongen.PermissionFail;
import kr.co.namee.permissiongen.PermissionGen;
import kr.co.namee.permissiongen.PermissionSuccess;

/**
 * @author tanghao
 * @date 2017/8/31
 * meail: 86882259@qq.com
 */

public class MainActivity extends BaseActivity implements OnPlayerEventListener {

    @BindViews({R.id.tab_home_1, R.id.tab_home_2, R.id.tab_home_3, R.id
            .tab_home_4, R.id.tab_playing})
    List<View> menus;

    @BindViews({R.id.tab_image_1, R.id.tab_image_2, R.id.tab_image_3, R.id
            .tab_image_4})
    List<View> images;

    @BindViews({R.id.tab_text_1, R.id.tab_text_2, R.id.tab_text_3, R.id.tab_text_4})
    List<View> texts;

    @BindView(R.id.main_nav)
    View tabMain;
    @BindView(R.id.song_progress)
    SkipAdCircle songProgress;
    @BindView(R.id.iv_music_avator)
    ImageView ivMusic;
    @BindView(R.id.iv_playing_bg)
    ImageView ivPlayingIcon;

    public int currentIndex;
    private MenuTabAdapter adapter;
    private List<Fragment> fragments;

    private final String FRAGMENTS_TAG = "android:support:fragments";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        Logl.e("MainActivity onCreate:" + TaipingApplication.tpApp.getToken());
        clearInstanceState(savedInstanceState);
        super.onCreate(savedInstanceState);
        RxBus.get().register(this);
        AudioPlayer.get().addOnPlayEventListener(this);
    }

    @Override
    public void init() {
        setRedBar();
        addFragment();
    }


    @Override
    public int attachLayout() {
        return R.layout.activity_main;
    }


    private void requestPermission() {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            PermissionGen.with(this)
                    .addRequestCode(234)
                    .permissions(
                            Manifest.permission.READ_EXTERNAL_STORAGE,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE,
                            Manifest.permission.CAMERA
                    )
                    .request();
        }
    }

    private void clearInstanceState(@Nullable Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            savedInstanceState.putParcelable(FRAGMENTS_TAG, null);
        }
    }

    private void addFragment() {
        if (fragments == null) {
            fragments = new ArrayList<>();
        }
        fragments.add(new HomepageFragment());
        fragments.add(new MainHouseFragment());
        fragments.add(new CircleFragment());
        fragments.add(new MineFragment());
        adapter = new MenuTabAdapter(this, menus, fragments, R.id.main_content);
        adapter.setOnMenuClickListener(new MenuTabAdapter.OnMenuClickListener() {
            @Override
            public void onDoubleClick(View view) {
            }

            @Override
            public void onClick(View v) {
                switch (v.getId()) {
                    case R.id.tab_home_1:
                        changeMenu(0);
                        break;
                    case R.id.tab_home_2:
                        changeMenu(1);
                        break;
                    case R.id.tab_home_3:
                        changeMenu(2);
                        break;
                    case R.id.tab_home_4:
                        changeMenu(3);
                        break;
                    case R.id.tab_playing:
                        // 隐藏tab
                        Logl.e("点击事件 first ");
                        PlayActivity.launch(MainActivity.this);
                        break;
                    default:
                        break;
                }
            }
        });
        changeMenu(0);
    }


    public void changeMenu(int index) {
        currentIndex = index;
        if (adapter != null) {
            adapter.change(index);
            changeMenuStatus(index);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        super.onSaveInstanceState(outState, outPersistentState);
    }

    public void changeMenuStatus(int index) {
        int imageSize = images.size();
        for (int i = 0; i < imageSize; i++) {
            if (i == index) {
                this.images.get(i).setSelected(true);
                this.texts.get(i).setSelected(true);
            } else {
                this.images.get(i).setSelected(false);
                this.texts.get(i).setSelected(false);
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (AudioPlayer.get().isPlaying()) {
            ivPlayingIcon.setVisibility(View.GONE);
        } else {
            if (AudioPlayer.get().getPlayMusic() != null) {
                ivPlayingIcon.setVisibility(View.VISIBLE);
            }else {
                ivMusic.setImageResource(R.mipmap.home_no_song_icon_1);
                ivPlayingIcon.setVisibility(View.GONE);
            }
        }
        if (AudioPlayer.get().getPlayMusic() != null) {
            GlideHelper.loadListPic(MainActivity.this, AudioPlayer.get().getPlayMusic().coverPath, ivMusic);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        requestPermission();
        initHttpData();
    }

    private void initHttpData() {
        if (!TaipingApplication.tpApp.getIsLogin()) {
            return;
        }
        HttpUtil.getInstance().getApiService().getUserInfo().compose(RxUtils.rxSchedulerHelper())
                .subscribe(new SuccessSubscriber<ResponseBean<UserBean>>() {
                    @Override
                    public void onSuccess(ResponseBean<UserBean> responseBean) {
                        if (responseBean.getResult() == null) return;
                        TaipingApplication.tpApp.isVIP = responseBean.getResult().isVIP;
                    }

                    @Override
                    public void onFailed(String str) {
                        ToastUtil.showToast(str);
                    }
                });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onBackPressed() {
        if (!BackHandlerHelper.handleBackPress(this)) {
            _exit();
        }

    }

    private long mExitTime = 0;

    private void _exit() {
        if (System.currentTimeMillis() - mExitTime > 2000) {
            Toast.makeText(this, "再按一次退出程序", Toast.LENGTH_SHORT).show();
            mExitTime = System.currentTimeMillis();
        } else {
            finish();
            System.exit(0);
        }
    }

    public static void launch(Context context) {
        Intent intent = new Intent(context, MainActivity.class);
        context.startActivity(intent);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        RxBus.get().unregister(this);
        AudioPlayer.get().removeOnPlayEventListener(this);
    }

    @Override
    public void onChange(Music music) {
        if (music != null) {
            songProgress.setMax((int) music.duration);
            GlideHelper.loadListPic(MainActivity.this, music.coverPath, ivMusic);
        }
    }

    @Override
    public void onPlayerStart() {
        ivPlayingIcon.setVisibility(View.GONE);
    }

    @Override
    public void onPlayerPause() {
        ivPlayingIcon.setVisibility(View.VISIBLE);
    }

    @Override
    public void onPublish(int progress) {
        songProgress.setProgress(progress);
    }

    @Override
    public void onBufferingUpdate(int percent) {

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                           int[] grantResults) {
        PermissionGen.onRequestPermissionsResult(this, requestCode, permissions, grantResults);
        Logl.e("onRequestPermissionsResult activity");
    }

    @PermissionSuccess(requestCode = 234)
    private void doSthing() {
    }

    @PermissionFail(requestCode = 234)
    private void doFailSthing() {
        ToastUtil.showToast("为了更好地服务你，请打开相关权限");
    }

}