package com.yuntu.mawei.ui.homepage;

import com.yuntu.mawei.bean.base.ResponseBean;
import com.yuntu.mawei.bean.rx.SuccessSubscriber;
import com.yuntu.mawei.bean.song.SongBean;
import com.yuntu.mawei.ui.login.TokenBean;
import com.yuntu.mawei.util.GsonUtil;
import com.yuntu.mawei.util.Logl;
import com.yuntu.mawei.util.RxUtils;
import com.yuntu.mawei.util.http.HttpUtil;

import java.util.List;

/**
 * Created by tanghao on 2019/1/25
 */
public class HomepagePresenter {
    public HomepageFragment fragment;

    public HomepagePresenter(HomepageFragment fragment) {
        this.fragment = fragment;
    }

    public void getHotSong() {
        Logl.e("getHotSong");
        HttpUtil.getInstance().getApiService().getHotSong(0, 1, 3).compose(RxUtils.rxSchedulerHelper()).subscribe(new SuccessSubscriber<ResponseBean<List<SongBean>>>() {
            @Override
            public void onSuccess(ResponseBean<List<SongBean>> listResponseBean) {
                fragment.showSong(listResponseBean.getResult());
            }

            @Override
            public void onFailed(String str) {
            }
        });

        HttpUtil.getInstance().getApiService().getHotSong(1, 1, 3).compose(RxUtils.rxSchedulerHelper()).subscribe(new SuccessSubscriber<ResponseBean<List<SongBean>>>() {
            @Override
            public void onSuccess(ResponseBean<List<SongBean>> listResponseBean) {
                fragment.showRing(listResponseBean.getResult());
            }

            @Override
            public void onFailed(String str) {
            }
        });
        HttpUtil.getInstance().getApiService().getHotSong(2, 1, 3).compose(RxUtils.rxSchedulerHelper()).subscribe(new SuccessSubscriber<ResponseBean<List<SongBean>>>() {
            @Override
            public void onSuccess(ResponseBean<List<SongBean>> listResponseBean) {
                fragment.showMv(listResponseBean.getResult());
            }

            @Override
            public void onFailed(String str) {
            }
        });
    }

}
