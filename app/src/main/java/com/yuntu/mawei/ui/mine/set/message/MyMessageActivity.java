package com.yuntu.mawei.ui.mine.set.message;

import android.content.Context;
import android.content.Intent;

import com.yuntu.mawei.R;
import com.yuntu.mawei.base.home.BaseListActivity;
import com.yuntu.mawei.base.home.BaseListActivityPresenter;
import com.yuntu.mawei.view.adapter.base.BaseQuickAdapter;

public class MyMessageActivity extends BaseListActivity {

    @Override
    protected void initViews() {
        initTitle(R.string.my_message);
    }

    @Override
    protected BaseQuickAdapter attachAdapter() {
        return new MyMessageAdapter(this);
    }

    @Override
    protected BaseListActivityPresenter attachPresenter() {
        return new MyMessagePresenter(this);
    }

    @Override
    protected int attachLayoutRes() {
        return R.layout.title_with_recyclerview;
    }

    public static void launch(Context context) {
        context.startActivity(new Intent(context, MyMessageActivity.class));
    }

}
