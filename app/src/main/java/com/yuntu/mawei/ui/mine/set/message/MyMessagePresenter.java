package com.yuntu.mawei.ui.mine.set.message;

import com.yuntu.mawei.base.home.BaseListActivity;
import com.yuntu.mawei.base.home.BaseListActivityPresenter;
import com.yuntu.mawei.bean.base.ResponseBean;
import com.yuntu.mawei.bean.rx.SuccessSubscriber;
import com.yuntu.mawei.util.RxUtils;
import com.yuntu.mawei.util.ToastUtil;
import com.yuntu.mawei.util.http.HttpUtil;

import java.util.List;

public class MyMessagePresenter extends BaseListActivityPresenter {

    public MyMessagePresenter(BaseListActivity mView) {
        super(mView);
    }

    @Override
    public void getData(boolean isPullRefresh) {
        HttpUtil.getInstance().getApiService().getMessageList(1,1000)
                .compose(RxUtils.rxSchedulerHelper())
                .subscribe(new SuccessSubscriber<ResponseBean<List<MessageBean>>>() {
            @Override
            public void onSuccess(ResponseBean<List<MessageBean>> listResponseBean) {
                mView.loadData(listResponseBean.getResult());
            }
            @Override
            public void onFailed(String str) {
                ToastUtil.showToast(str);
            }
        });
    }

    @Override
    public void getMoreData() {
        mView.adapter.noMoreData();
        mView.adapter.loadComplete();
    }
}
