package com.yuntu.mawei.ui.mine.user;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.yuntu.mawei.R;
import com.yuntu.mawei.base.home.BaseActivity;
import com.yuntu.mawei.bean.base.ResponseBean;
import com.yuntu.mawei.bean.rx.SuccessSubscriber;
import com.yuntu.mawei.ui.login.ChangePhoneActivity;
import com.yuntu.mawei.ui.mine.MineBean;
import com.yuntu.mawei.ui.mine.member.MemberActivity;
import com.yuntu.mawei.util.Logl;
import com.yuntu.mawei.util.RxUtils;
import com.yuntu.mawei.util.ToastUtil;
import com.yuntu.mawei.util.http.HttpUtil;
import com.yuntu.mawei.view.SexSelectDialog;
import com.yuntu.mawei.view.edit.EditDialog;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * Created by tanghao on 2019/1/24
 */
public class UserInfoActivity extends BaseActivity {

    @BindView(R.id.title_right)
    TextView tvRight;
    @BindView(R.id.lv_user)
    ListView listView;

    List<MineBean> mineBeanList = new ArrayList<>();
    LayoutInflater layoutInflater;
    UserAdapter userAdapter;
    UserBean userBean;
    private boolean isChanged = false;
    private boolean isPhone = true;

    @Override
    public int attachLayout() {
        return R.layout.activity_user_info;
    }

    @Override
    public void init() {
        layoutInflater = LayoutInflater.from(this);
        initUserBean();
        initListView();
        initTitle(R.string.user_info);
        tvRight.setOnClickListener(v -> saveHttpData());
    }

    private void initListView() {
        userAdapter = new UserAdapter();
        listView.setAdapter(userAdapter);
    }

    private void initUserBean() {
        mineBeanList.add(new MineBean(1, R.mipmap.user_icon_1, R.string.user_title_1));
        mineBeanList.add(new MineBean(2, R.mipmap.user_icon_2, R.string.user_title_2));
        mineBeanList.add(new MineBean(3, R.mipmap.user_icon_3, R.string.user_title_3));
        mineBeanList.add(new MineBean(4, R.mipmap.user_icon_4, R.string.user_title_4));
        mineBeanList.add(new MineBean(5, R.mipmap.user_icon_5, R.string.user_title_5));
        initHttpData();
    }


    private void initHttpData() {
        HttpUtil.getInstance().getApiService().getUserInfo().compose(RxUtils.rxSchedulerHelper())
                .subscribe(new SuccessSubscriber<ResponseBean<UserBean>>() {
                    @Override
                    public void onSuccess(ResponseBean<UserBean> responseBean) {
                        userBean = responseBean.getResult();
                        if (userBean == null) return;
                        mineBeanList.get(1).content = userBean.appUserPhone;
                        if (isPhone) {
                            mineBeanList.get(0).content = userBean.appUserName;
                            mineBeanList.get(2).content = userBean.appUserSex;
                            mineBeanList.get(3).content = userBean.appUserAge;
                            if (!userBean.isVIP) {
                                mineBeanList.get(4).content = "普通会员";
                            }
                            isPhone = false;
                        }
                        userAdapter.notifyDataSetChanged();
                    }

                    @Override
                    public void onFailed(String str) {
                        ToastUtil.showToast(str);
                    }
                });
    }


    public static void launch(Context context) {
        context.startActivity(new Intent(context, UserInfoActivity.class));
    }

    public class UserAdapter extends BaseAdapter {
        ImageView ivIcon, ivYou;
        TextView tvTitle, tvContent;
        View btUser;

        @Override
        public int getCount() {
            return 5;
        }

        @Override
        public Object getItem(int position) {
            return mineBeanList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            convertView = layoutInflater.inflate(R.layout.adapter_user, null);
            ivIcon = convertView.findViewById(R.id.iv_user);
            ivYou = convertView.findViewById(R.id.iv_you);
            btUser = convertView.findViewById(R.id.bt_user);
            tvTitle = convertView.findViewById(R.id.tv_user_title);
            tvContent = convertView.findViewById(R.id.tv_user);
            MineBean mineBean = mineBeanList.get(position);
            ivIcon.setImageResource(mineBean.icon);
            tvTitle.setText(mineBean.title);
            if (position == 2) {
                if (mineBean.content != null) {
                    switch (mineBean.content) {
                        case "0":
                            tvContent.setText("男");
                            break;
                        case "1":
                            tvContent.setText("女");
                            break;
                        case "2":
                            tvContent.setText("保密");
                            break;
                        case "3":
                            tvContent.setText("保密");
                            break;
                    }
                } else {
                    tvContent.setText("保密");
                }
            } else {
                tvContent.setText(mineBean.content != null ? mineBean.content : "0");
            }
            if (position == 4) {
                ivYou.setImageResource(R.mipmap.you_jiantou);
            } else {
                ivYou.setImageResource(R.mipmap.user_alter);
            }
            btUser.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    isChanged = true;
                    tvRight.setText("保存");
                    switch (mineBean.id) {
                        case 1:
                            EditDialog editDialog2 = new EditDialog(UserInfoActivity.this, 1).setParms(mineBeanList.get(0).content,
                                    "请输入新的昵称", 8);
                            editDialog2.setNums(false, 8);
                            editDialog2.setEdiClickInterface(new EditDialog.EditDialogClickInterface() {
                                @Override
                                public void doConfirm(String str) {
                                    mineBeanList.get(0).content = str;
                                    notifyDataSetChanged();
                                    if (userBean != null) {
                                        userBean.appUserName = str;
                                    }
                                }
                            });
                            editDialog2.show();
                            break;
                        case 2:
                            ChangePhoneActivity.launch(UserInfoActivity.this);
                            break;
                        case 3:
                            SexSelectDialog dialog = new SexSelectDialog(UserInfoActivity.this);
                            List<String> list = new ArrayList<>();
                            list.add("男");
                            list.add("女");
                            list.add("保密");
                            dialog.initData(list, new SexSelectDialog.IndexCallBack() {
                                @Override
                                public void getIndex(int selectedIndex) {
                                    Logl.e("index: " + selectedIndex);
                                    mineBeanList.get(2).content = String.valueOf(selectedIndex);
                                    if (selectedIndex < 2) {
                                        mineBeanList.get(2).content = String.valueOf(selectedIndex);
                                        userBean.appUserSex = String.valueOf(selectedIndex);
                                    } else {
                                        mineBeanList.get(2).content = "3";
                                        userBean.appUserSex = "3";
                                    }
                                    notifyDataSetChanged();
                                }
                            });
                            break;
                        case 4:
                            EditDialog editDialog4 = new EditDialog(UserInfoActivity.this, 1).setInputNumber().setParms(mineBeanList.get(3).content,
                                    "请输入年龄", 2);
                            editDialog4.setNums(false, 2);
                            editDialog4.setEdiClickInterface(new EditDialog.EditDialogClickInterface() {
                                @Override
                                public void doConfirm(String str) {
                                    mineBeanList.get(3).content = str;
                                    notifyDataSetChanged();
                                    if (userBean != null) {
                                        userBean.appUserAge = str;
                                    }
                                }
                            });
                            editDialog4.show();
                            break;
                        case 5:
                            MemberActivity.launch(UserInfoActivity.this);
                            break;
                    }
                }
            });

            return convertView;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void saveHttpData() {
        if (!isChanged) {
            finish();
            return;
        }
        HttpUtil.getInstance().getApiService()
                .updateUserBean(userBean.appUserName, userBean.appUserPhone,
                        userBean.appUserSex, userBean.appUserAge, null).compose(RxUtils.rxSchedulerHelper()).subscribe(new SuccessSubscriber<ResponseBean>() {
            @Override
            public void onSuccess(ResponseBean responseBean) {
                finish();
                ToastUtil.showToast(R.string.sace_success);
            }

            @Override
            public void onFailed(String str) {
                finish();
                ToastUtil.showToast(str);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == 0x256) {
            initHttpData();
        }
    }
}
