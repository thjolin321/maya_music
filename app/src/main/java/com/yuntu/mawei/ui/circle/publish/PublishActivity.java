package com.yuntu.mawei.ui.circle.publish;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.webkit.WebSettings;
import android.webkit.WebView;

import com.yuntu.mawei.BuildConfig;
import com.yuntu.mawei.R;
import com.yuntu.mawei.base.TaipingApplication;
import com.yuntu.mawei.base.home.BaseActivity;
import com.yuntu.mawei.bean.base.ResponseBean;
import com.yuntu.mawei.bean.rx.SuccessSubscriber;
import com.yuntu.mawei.ui.login.LoginActivity;
import com.yuntu.mawei.ui.mine.user.UserBean;
import com.yuntu.mawei.util.RxUtils;
import com.yuntu.mawei.util.ToastUtil;
import com.yuntu.mawei.util.http.HttpUtil;
import android.annotation.TargetApi;
import android.content.ClipData;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;


import butterknife.BindView;
import kr.co.namee.permissiongen.PermissionGen;

public class PublishActivity extends BaseActivity {

    @BindView(R.id.webview)
    WebView webView;

    int themeId;
    ValueCallback<Uri> uploadMessage;
    ValueCallback<Uri[]> uploadMessageAboveL;
    final static int FILE_CHOOSER_RESULT_CODE = 10000;

    @Override
    public int attachLayout() {
        return R.layout.activity_publish;
    }

    @Override
    public void init() {
        initTitle(R.string.home3);
        if(!TaipingApplication.tpApp.getIsLogin()){
            LoginActivity.launch(this);
            return;
        }
        themeId = getIntent().getIntExtra("themeId", -1);
        HttpUtil.getInstance().getApiService().getUserInfo().compose(RxUtils.rxSchedulerHelper())
                .subscribe(new SuccessSubscriber<ResponseBean<UserBean>>() {
                    @Override
                    public void onSuccess(ResponseBean<UserBean> responseBean) {
                        if (responseBean.getResult() == null) return;
                        initWebView(responseBean.getResult().userId);
                    }

                    @Override
                    public void onFailed(String str) {
                        ToastUtil.showToast(str);
                    }
                });


    }

    private void initWebView(int id) {
        WebSettings settings = webView.getSettings();
        settings.setUseWideViewPort(true);
        settings.setLoadWithOverviewMode(true);
        settings.setJavaScriptEnabled(true);
        webView.setWebChromeClient(new WebChromeClient() {

            // For Android < 3.0
            public void openFileChooser(ValueCallback<Uri> valueCallback) {
                uploadMessage = valueCallback;
                openImageChooserActivity();
            }

            // For Android  >= 3.0
            public void openFileChooser(ValueCallback valueCallback, String acceptType) {
                uploadMessage = valueCallback;
                openImageChooserActivity();
            }

            //For Android  >= 4.1
            public void openFileChooser(ValueCallback<Uri> valueCallback, String acceptType, String capture) {
                uploadMessage = valueCallback;
                openImageChooserActivity();
            }

            // For Android >= 5.0
            @Override
            public boolean onShowFileChooser(WebView webView, ValueCallback<Uri[]> filePathCallback, WebChromeClient.FileChooserParams fileChooserParams) {
                uploadMessageAboveL = filePathCallback;
                openImageChooserActivity();
                return true;
            }
        });
        if (themeId == -1) {
            webView.loadUrl(BuildConfig.MALL_HOST +
                    "app/user/musicTheme/edit?userId="
                    + id);
        } else {
            webView.loadUrl(BuildConfig.MALL_HOST +
                    "app/user/musicTheme/edit?userId="
                    + id + "&themeId=" + themeId);
        }
    }

    private void openImageChooserActivity() {
        Intent i = new Intent(Intent.ACTION_GET_CONTENT);
        i.addCategory(Intent.CATEGORY_OPENABLE);
        i.setType("*/*");
        startActivityForResult(Intent.createChooser(i, "Image Chooser"), FILE_CHOOSER_RESULT_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == FILE_CHOOSER_RESULT_CODE) {
            if (null == uploadMessage && null == uploadMessageAboveL) return;
            Uri result = data == null || resultCode != RESULT_OK ? null : data.getData();
            if (uploadMessageAboveL != null) {
                onActivityResultAboveL(requestCode, resultCode, data);
            } else if (uploadMessage != null) {
                uploadMessage.onReceiveValue(result);
                uploadMessage = null;
            }
        }
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private void onActivityResultAboveL(int requestCode, int resultCode, Intent intent) {
        if (requestCode != FILE_CHOOSER_RESULT_CODE || uploadMessageAboveL == null)
            return;
        Uri[] results = null;
        if (resultCode == Activity.RESULT_OK) {
            if (intent != null) {
                String dataString = intent.getDataString();
                ClipData clipData = intent.getClipData();
                if (clipData != null) {
                    results = new Uri[clipData.getItemCount()];
                    for (int i = 0; i < clipData.getItemCount(); i++) {
                        ClipData.Item item = clipData.getItemAt(i);
                        results[i] = item.getUri();
                    }
                }
                if (dataString != null)
                    results = new Uri[]{Uri.parse(dataString)};
            }
        }
        uploadMessageAboveL.onReceiveValue(results);
        uploadMessageAboveL = null;
    }


    @Override
    protected void onStart() {
        super.onStart();
        requestPermission();
    }

    private void requestPermission() {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            PermissionGen.with(this)
                    .addRequestCode(234)
                    .permissions(
                            Manifest.permission.READ_EXTERNAL_STORAGE,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE,
                            Manifest.permission.CAMERA
                    )
                    .request();
        }
    }

    public static void launch(Context context, int themeId) {
        Intent intent = new Intent(context, PublishActivity.class);
        intent.putExtra("themeId", themeId);
        context.startActivity(intent);
    }

}
