package com.yuntu.mawei.ui.house.detail;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;

import com.yuntu.mawei.R;
import com.yuntu.mawei.bean.song.SingerBean;
import com.yuntu.mawei.ui.homepage.singer.detail.SingerDetailActivity;
import com.yuntu.mawei.util.GlideHelper;
import com.yuntu.mawei.view.adapter.base.BaseQuickAdapter;
import com.yuntu.mawei.view.adapter.base.BaseViewHolder;

public class SingerListAdapter extends BaseQuickAdapter<SingerBean> {

    public SingerListAdapter(Context context) {
        super(context);
    }

    @Override
    protected int attachLayoutRes() {
        return R.layout.adapter_singer_list;
    }

    @Override
    protected void convert(BaseViewHolder holder, SingerBean item) {
        ImageView ivRank = holder.getView(R.id.iv_rank_icon);
        ivRank.setVisibility(View.GONE);
        switch (holder.getAdapterPosition()) {
            case 0:
                ivRank.setVisibility(View.VISIBLE);
                ivRank.setImageResource(R.mipmap.rank_icon_1);
                break;
            case 1:
                ivRank.setVisibility(View.VISIBLE);
                ivRank.setImageResource(R.mipmap.rank_icon_2);
                break;
            case 2:
                ivRank.setVisibility(View.VISIBLE);
                ivRank.setImageResource(R.mipmap.rank_icon_3);
                break;
            default:
                holder.setText(R.id.tv_rank, (holder.getAdapterPosition() + 1) + "");
                break;
        }

        GlideHelper.loadListPic(mContext, item.singerAvatar, holder.getView(R.id.iv_singer));
        holder.setText(R.id.tv_singer, item.singerName);
        holder.getConvertView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SingerDetailActivity.launch(mContext, item);
            }
        });
    }
}
