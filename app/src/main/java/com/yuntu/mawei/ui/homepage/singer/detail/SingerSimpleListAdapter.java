package com.yuntu.mawei.ui.homepage.singer.detail;

import android.content.Context;

import com.yuntu.mawei.R;
import com.yuntu.mawei.bean.song.SongBean;
import com.yuntu.mawei.ui.homepage.mv.detail.MvDetailActivity;
import com.yuntu.mawei.ui.playing.service.AudioPlayer;
import com.yuntu.mawei.ui.three.piliang.PiliangActivity;
import com.yuntu.mawei.util.Logl;
import com.yuntu.mawei.util.ToastUtil;
import com.yuntu.mawei.util.music.MusicUtil;
import com.yuntu.mawei.view.adapter.base.BaseMultiItemQuickAdapter;
import com.yuntu.mawei.view.adapter.base.BaseViewHolder;

import java.util.ArrayList;
import java.util.List;

public class SingerSimpleListAdapter extends BaseMultiItemQuickAdapter<SongBean> {

    List<SongBean> listLocal;
    boolean onceInit = true;

    public SingerSimpleListAdapter(Context context) {
        super(context);

    }

    @Override
    protected void attachItemType() {
        Logl.e("attachItemType会调用几次");
        addItemType(SongBean.SONG_TITLE_ITEM, R.layout.adapter_song_simple_title);
        addItemType(SongBean.SONG_ITEM, R.layout.adapter_song_rank_list);
    }

    private void initLocal() {
        listLocal = new ArrayList<>();
        for (SongBean songBean : getData()) {
            if (songBean.getItemType() == SongBean.SONG_ITEM) {
                listLocal.add(songBean);
            }
        }
    }

    @Override
    protected void convert(BaseViewHolder holder, SongBean item) {
        if (onceInit) {
            onceInit = false;
            initLocal();
        }
        switch (item.itemType) {
            case SongBean.SONG_TITLE_ITEM:
                holder.setText(R.id.tv_zimu_title, item.zimuTitle);
                if (holder.getLayoutPosition() == 0) {
                    holder.setVisible(R.id.bt_piliang, true);
                    holder.setText(R.id.bt_piliang, "批量播放");
                    holder.setOnClickListener(R.id.bt_piliang, v -> {
                        List<SongBean> listNow = new ArrayList<>();
                        for (SongBean songBean : getData()) {
                            if (songBean.getItemType() == 0) {
                                listNow.add(songBean);
                            }
                        }
                        for (int i = 0; i < listNow.size(); i++) {
                            if (i == 0) {
                                MusicUtil.playMusicNoLaunch(mContext, listNow.get(i));
                            } else {
                                AudioPlayer.get().addJust(MusicUtil.mapToMusic(listNow.get(i)));
                            }
                        }
                    });
                } else {
                    holder.setVisible(R.id.bt_piliang, false);
                }
                break;
            case SongBean.SONG_ITEM:
                holder.setText(R.id.tv_song, item.songName);
                if (item.singer != null) {
                    holder.setText(R.id.tv_artist_and_album, item.singer.singerName + " - 《" + (item.album != null ? item.album.albumName : "") + "》");
                }
                holder.setText(R.id.tv_rank, MusicUtil.getIndex(item, listLocal));
                holder.getConvertView().setOnClickListener(v -> {
                    if (item.musicLocal != null) {
                        ToastUtil.showToast("已添加到播放列表");
                        AudioPlayer.get().addAndPlayWithUpdate(item.musicLocal);
                    } else {
                        if (item.songType == 2) {
                            MvDetailActivity.launch(mContext, item);
                        } else {
                            MusicUtil.playMusic(mContext, item);
                        }
                    }
                });
                break;
        }
    }
}
