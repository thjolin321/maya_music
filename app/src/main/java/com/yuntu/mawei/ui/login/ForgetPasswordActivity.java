package com.yuntu.mawei.ui.login;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.CountDownTimer;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.yuntu.mawei.R;
import com.yuntu.mawei.base.home.BaseActivity;
import com.yuntu.mawei.bean.base.ResponseBean;
import com.yuntu.mawei.bean.rx.SuccessSubscriber;
import com.yuntu.mawei.util.RxUtils;
import com.yuntu.mawei.util.ToastUtil;
import com.yuntu.mawei.util.http.HttpUtil;

import butterknife.BindView;
import butterknife.OnClick;

public class ForgetPasswordActivity extends BaseActivity {

    @BindView(R.id.et_input_phone)
    EditText etPhone;
    @BindView(R.id.et_input_vertify)
    EditText etVertify;
    @BindView(R.id.bt_get)
    TextView tvCountTime;

    @Override
    public int attachLayout() {
        return R.layout.activity_forget_password;
    }

    @Override
    public void init() {
        initTitle("忘记密码");
    }

    @OnClick({R.id.bt_chongzhi,R.id.bt_get})
    public void onForgetClick(View view) {
        switch (view.getId()) {
            case R.id.bt_chongzhi:
                // 验证验证码是否正确
                if (TextUtils.isEmpty(etPhone.getText())) {
                    ToastUtil.showToast(R.string.input_phone);
                    return;
                }
                if (TextUtils.isEmpty(etVertify.getText())) {
                    ToastUtil.showToast(R.string.input_vertify);
                    return;
                }
                HttpUtil.getInstance().getApiService().vertifyForget(etPhone.getText().toString()
                        , etVertify.getText().toString()).compose(RxUtils.rxSchedulerHelper())
                        .subscribe(new SuccessSubscriber<ResponseBean>() {
                            @Override
                            public void onSuccess(ResponseBean tokenBeanResponseBean) {
                                ResettingPasswordActivity.launch(ForgetPasswordActivity.this,
                                        etPhone.getText().toString());
                                finish();
                            }

                            @Override
                            public void onFailed(String str) {
                                ToastUtil.showToast(str);
                            }
                        });

                break;
            case R.id.bt_get:
                sendMessage();
                break;
        }
    }


    private void sendMessage() {
        if (TextUtils.isEmpty(etPhone.getText())) {
            ToastUtil.showToast("请输入手机号码");
            return;
        }
        String phone = etPhone.getText().toString().trim().replace(" ", "");

        if (phone.length() != 11) {
            ToastUtil.showToast("请验证手机号码是否正确");
            return;
        }

        HttpUtil.getInstance().getApiService().sendMessage(etPhone.getText().toString())
                .compose(RxUtils.rxSchedulerHelper())
                .subscribe(new SuccessSubscriber<ResponseBean>() {
                    @Override
                    public void onSuccess(ResponseBean responseBean) {
                        ToastUtil.showToast("发送验证码成功");
                        countDown();
                    }

                    @Override
                    public void onFailed(String str) {
                        ToastUtil.showToast(str);
                    }
                });
    }

    CountDownTimer timer;

    private void countDown() {
        CountDownTimer timer = new CountDownTimer(60000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                tvCountTime.setTextColor(Color.parseColor("#8C8C8C"));
                tvCountTime.setEnabled(false);
                tvCountTime.setText((millisUntilFinished / 1000) + "秒后重发");
            }

            @Override
            public void onFinish() {
                tvCountTime.setTextColor(Color.parseColor("#E8413B"));
                tvCountTime.setEnabled(true);
                tvCountTime.setText("重发验证码");
            }
        };
        timer.start();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
    }

    public static void launch(Context context) {
        Intent intent = new Intent(context, ForgetPasswordActivity.class);
        context.startActivity(intent);
    }


}
