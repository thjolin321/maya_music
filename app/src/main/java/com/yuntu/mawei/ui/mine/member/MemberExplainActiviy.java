package com.yuntu.mawei.ui.mine.member;

import android.content.Context;
import android.content.Intent;

import com.yuntu.mawei.R;
import com.yuntu.mawei.base.home.BaseActivity;

public class MemberExplainActiviy extends BaseActivity {


    @Override
    public int attachLayout() {
        return R.layout.activity_member_explain;
    }

    @Override
    public void init() {
        initTitle("会员特权说明");
    }

    public static void launch(Context context){
        context.startActivity(new Intent(context,MemberExplainActiviy.class));
    }
}
