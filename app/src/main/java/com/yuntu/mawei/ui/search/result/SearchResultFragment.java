package com.yuntu.mawei.ui.search.result;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.yuntu.mawei.R;
import com.yuntu.mawei.base.home.BaseListFragment;
import com.yuntu.mawei.base.home.BaseListFragmentPresenter;
import com.yuntu.mawei.bean.song.SongBean;
import com.yuntu.mawei.ui.homepage.singer.SingerActivity;
import com.yuntu.mawei.ui.homepage.singer.detail.AlbumSimpleListAdapter;
import com.yuntu.mawei.ui.homepage.singer.detail.SingerDetailActivity;
import com.yuntu.mawei.ui.three.MvSimpleListAdapter;
import com.yuntu.mawei.ui.three.SongSimpleListAdapter;
import com.yuntu.mawei.ui.three.collect.CollectSongListPresenter;
import com.yuntu.mawei.util.GlideHelper;
import com.yuntu.mawei.util.Logl;
import com.yuntu.mawei.view.adapter.base.BaseQuickAdapter;

import butterknife.BindView;

public class SearchResultFragment extends BaseListFragment {

    @BindView(R.id.ll_singer)
    View singerLayout;
    @BindView(R.id.iv_singer)
    ImageView ivSinger;
    @BindView(R.id.tv_singer)
    TextView tvSinger;
    @BindView(R.id.tv_singer_intro)
    TextView tvSingerIntro;

    int type; // 搜索类别(0歌曲1铃声2mv3专辑4歌手)

    public static SearchResultFragment newInstance(int type, String str) {
        SearchResultFragment fragment = new SearchResultFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("type", type);
        bundle.putString("search", str);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    protected void initViews() {
    }

    @Override
    protected int attachLayoutRes() {
        return R.layout.fragment_song_result;
    }

    @Override
    protected BaseQuickAdapter attachAdapter() {
        type = getArguments().getInt("type");
        Logl.e("传入的type:" + type);
        // 搜索类别(0歌曲1铃声2mv3专辑4歌手)
        switch (type) {
            case 0:
                return new SongSimpleListAdapter(getActivity(), 0);
            case 1:
                return new SongSimpleListAdapter(getActivity(), 0);
            case 2:
                return new MvSearchListAdapter(getActivity());
            case 3:
                return new AlbumSimpleListAdapter(getActivity());
            default:
                return new SongSimpleListAdapter(getActivity(), 0);
        }
    }

    @Override
    protected BaseListFragmentPresenter attachPresenter() {
        return new ResultPresenter(this, type, getArguments().getString("search"));
    }

    public void initSinger(SongBean songBean) {
        singerLayout.setVisibility(View.VISIBLE);
        adapter.addHeaderView(new View(getActivity()));
        if (songBean.singer != null) {
            GlideHelper.loadListPic(getActivity(), songBean.singer.singerAvatar, ivSinger);
            tvSinger.setText(songBean.singer.singerName);
            tvSingerIntro.setText("歌曲：" + songBean.singer.songNumber
                    + "  " + "专辑：" + songBean.singer.albumNumber);
            singerLayout.setOnClickListener(v -> {
                SingerDetailActivity.launch(getActivity(), songBean.singer);
            });
        }
    }

    public void refreshCurrent(String str) {
        presenter = new ResultPresenter(this, type, str);
        presenter.getData(false);
    }

}
