package com.yuntu.mawei.ui.house.detail;

import android.content.Context;
import android.view.View;

import com.yuntu.mawei.R;
import com.yuntu.mawei.bean.song.SongBean;
import com.yuntu.mawei.ui.three.piliang.PiliangActivity;
import com.yuntu.mawei.util.GlideHelper;
import com.yuntu.mawei.util.music.MusicUtil;
import com.yuntu.mawei.view.adapter.base.BaseMultiItemQuickAdapter;
import com.yuntu.mawei.view.adapter.base.BaseViewHolder;

public class SongListAdapter extends BaseMultiItemQuickAdapter<SongBean> {


    public SongListAdapter(Context context) {
        super(context);
    }

    @Override
    protected void attachItemType() {
        addItemType(SongBean.SONG_TITLE_ITEM, R.layout.adapter_song_simple_title);
        addItemType(SongBean.SONG_ITEM, R.layout.adapter_song_list);
    }

    @Override
    protected void convert(BaseViewHolder holder, SongBean item) {
        switch (item.getItemType()) {
            case SongBean.SONG_TITLE_ITEM:
                holder.setText(R.id.tv_zimu_title, item.zimuTitle);
                if (holder.getLayoutPosition() == 0) {
                    holder.setVisible(R.id.bt_piliang, true);
                    holder.setOnClickListener(R.id.bt_piliang, v -> {
                        PiliangActivity.launch(mContext, getData(), 4);
                    });
                } else {
                    holder.setVisible(R.id.bt_piliang, false);
                }
                break;
            case SongBean.SONG_ITEM:
                GlideHelper.loadListPic(mContext, item.songAvatar, holder.getView(R.id.iv_song));
                holder.setText(R.id.tv_song, item.songName);
                if (item.singer != null) {
                    holder.setText(R.id.tv_song_artist, item.singer.singerName);
                }
                holder.getConvertView().setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        MusicUtil.playMusic(mContext, item);
                    }
                });
                break;
        }
    }
}
