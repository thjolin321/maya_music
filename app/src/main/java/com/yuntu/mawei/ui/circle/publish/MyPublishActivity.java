package com.yuntu.mawei.ui.circle.publish;

import android.content.Context;
import android.content.Intent;

import com.yuntu.mawei.R;
import com.yuntu.mawei.base.home.BaseListActivity;
import com.yuntu.mawei.base.home.BaseListActivityPresenter;
import com.yuntu.mawei.ui.circle.CircleAdapter;
import com.yuntu.mawei.ui.circle.CircleFragment;
import com.yuntu.mawei.util.DpUtil;
import com.yuntu.mawei.view.adapter.base.BaseQuickAdapter;

public class MyPublishActivity extends BaseListActivity {

    @Override
    protected void initViews() {
        initTitle(R.string.mine_str_9);
    }

    @Override
    protected BaseQuickAdapter attachAdapter() {
        recyclerView.setPadding(DpUtil.dip2px(this,16),0,DpUtil.dip2px(this,16),0);
        return new CircleAdapter(this, true);
    }

    @Override
    protected BaseListActivityPresenter attachPresenter() {
        return new MyPublishPresenter(this);
    }

    @Override
    protected int attachLayoutRes() {
        return R.layout.title_with_recyclerview;
    }

    public static void launch(Context context) {
        context.startActivity(new Intent(context, MyPublishActivity.class));
    }

    @Override
    public void onResume() {
        super.onResume();
        updateViews(false);
    }

}
