package com.yuntu.mawei.ui.homepage.ring;

import android.content.Context;

import com.yuntu.mawei.R;
import com.yuntu.mawei.bean.song.SongBean;
import com.yuntu.mawei.util.GlideHelper;
import com.yuntu.mawei.util.music.MusicUtil;
import com.yuntu.mawei.view.adapter.base.BaseQuickAdapter;
import com.yuntu.mawei.view.adapter.base.BaseViewHolder;

public class RingAdapter extends BaseQuickAdapter<SongBean> {

    public RingAdapter(Context context) {
        super(context);
    }

    @Override
    protected int attachLayoutRes() {
        return R.layout.adapter_item_ring;
    }

    @Override
    protected void convert(BaseViewHolder holder, SongBean item) {
        holder.setText(R.id.tv_song, item.songName);
        if (item.singer != null && item.album != null) {
            holder.setText(R.id.tv_artist_and_album, item.singer.singerName + " - 《" + item.album.albumIntroduction + "》");
        }
        holder.setText(R.id.tv_download_count, item.downloadCount + "");
        holder.getConvertView().setOnClickListener(v -> MusicUtil.playMusicNoLaunch(mContext, item));

    }
}
