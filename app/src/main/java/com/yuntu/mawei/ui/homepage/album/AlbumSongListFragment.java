package com.yuntu.mawei.ui.homepage.album;

import android.os.Bundle;

import com.yuntu.mawei.R;
import com.yuntu.mawei.base.home.BaseListFragment;
import com.yuntu.mawei.base.home.BaseListFragmentPresenter;
import com.yuntu.mawei.bean.song.AlbumBean;
import com.yuntu.mawei.ui.three.MvSimpleListAdapter;
import com.yuntu.mawei.ui.three.SongSimpleListAdapter;
import com.yuntu.mawei.ui.three.collect.CollectSongListPresenter;
import com.yuntu.mawei.view.adapter.base.BaseQuickAdapter;

public class AlbumSongListFragment extends BaseListFragment {

    AlbumBean albumBean;

    public static AlbumSongListFragment newInstance(AlbumBean albumBean) {
        AlbumSongListFragment fragment = new AlbumSongListFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable("albumBean", albumBean);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    protected void initViews() {

    }

    @Override
    protected int attachLayoutRes() {
        return R.layout.recyclerview_just;
    }

    @Override
    protected BaseQuickAdapter attachAdapter() {
        albumBean = (AlbumBean) getArguments().getSerializable("albumBean");
        return new SongSimpleListAdapter(getActivity(), 0);
    }

    @Override
    protected BaseListFragmentPresenter attachPresenter() {
        return new AlbumSongListPresenter(this,albumBean);
    }

    boolean isSecondExecute;

    @Override
    public void onResume() {
        super.onResume();
        if (isSecondExecute && getUserVisibleHint()) {
            updateViews(false);
        }
        isSecondExecute = true;
    }
}
