package com.yuntu.mawei.ui.playing.gift;

import android.content.Context;
import android.view.View;

import com.yuntu.mawei.R;
import com.yuntu.mawei.util.BigDeUtil;
import com.yuntu.mawei.util.GlideHelper;
import com.yuntu.mawei.view.adapter.base.BaseQuickAdapter;
import com.yuntu.mawei.view.adapter.base.BaseViewHolder;

public class GiftAdapter extends BaseQuickAdapter<GiftBean> {

    private Callback callback;
    private int index;

    public GiftAdapter(Context context, Callback callback) {
        super(context);
        this.callback = callback;
    }

    @Override
    protected int attachLayoutRes() {
        return R.layout.adapter_gift;
    }

    @Override
    protected void convert(BaseViewHolder holder, GiftBean item) {
        GlideHelper.loadListPic(mContext, item.commodityUrl, holder.getView(R.id.iv_gift));
        holder.setText(R.id.tv_name, item.commodityName + "\n" + BigDeUtil.div(item.commodityValue, 100.0) + "元");
        View view = holder.getView(R.id.bg_gift);
        view.setBackground(null);
        if (index == holder.getAdapterPosition()) {
            view.setBackgroundResource(R.drawable.bg_red_line_text);
        }
        holder.getConvertView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                index = holder.getAdapterPosition();
                notifyDataSetChanged();
                callback.callback(item.commodityId);
            }
        });
    }

    public interface Callback {
        void callback(int id);
    }
}
