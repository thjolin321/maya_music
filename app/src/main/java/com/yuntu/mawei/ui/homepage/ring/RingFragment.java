package com.yuntu.mawei.ui.homepage.ring;

import android.os.Bundle;

import com.yuntu.mawei.R;
import com.yuntu.mawei.base.home.BaseListFragment;
import com.yuntu.mawei.base.home.BaseListFragmentPresenter;
import com.yuntu.mawei.ui.homepage.mv.MvAdapter;
import com.yuntu.mawei.ui.homepage.mv.MvPresenter;
import com.yuntu.mawei.view.adapter.base.BaseQuickAdapter;

public class RingFragment extends BaseListFragment {

    public static RingFragment newInstance(int type) {
        RingFragment fragment = new RingFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("type", type);
        fragment.setArguments(bundle);
        return fragment;
    }
    
    @Override
    protected void initViews() {
    }

    @Override
    protected int attachLayoutRes() {
        return R.layout.recyclerview_just;
    }

    @Override
    protected BaseQuickAdapter attachAdapter() {
        return new RingAdapter(getActivity());
    }

    @Override
    protected BaseListFragmentPresenter attachPresenter() {
        return new RingPresenter(this, getArguments().getInt("type"));
    }

}
