package com.yuntu.mawei.ui.mine.set;

import android.view.View;

import com.tencent.smtt.sdk.WebView;
import com.yuntu.mawei.R;
import com.yuntu.mawei.base.home.BaseFragment;
import com.yuntu.mawei.bean.base.ResponseBean;
import com.yuntu.mawei.bean.base.XieyiBean;
import com.yuntu.mawei.bean.rx.SuccessSubscriber;
import com.yuntu.mawei.util.RxUtils;
import com.yuntu.mawei.util.ToastUtil;
import com.yuntu.mawei.util.http.HttpUtil;

import butterknife.BindView;

public class HelpFragment extends BaseFragment {

    //0：操作指引，
    //1：会员说明，
    //2：注册协议，
    //3：帮助信息，
    //4：反馈信息

    @BindView(R.id.webview)
    WebView webView;

    @Override
    public int getLayoutId() {
        return R.layout.fragment_help;
    }

    @Override
    public void initView(View view) {
        HttpUtil.getInstance().getApiService().getXieyi(3)
                .compose(RxUtils.rxSchedulerHelper())
                .subscribe(new SuccessSubscriber<ResponseBean<XieyiBean>>() {
                    @Override
                    public void onSuccess(ResponseBean<XieyiBean> xieyiBeanResponseBean) {
                        webView.loadDataWithBaseURL(null, xieyiBeanResponseBean.getResult().deployDetail, "text/html", "UTF-8", null);
                    }
                    @Override
                    public void onFailed(String str) {
                        ToastUtil.showToast(str);
                    }
                });
    }
}
