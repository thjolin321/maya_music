package com.yuntu.mawei.ui.homepage.rank;

import android.os.Bundle;

import com.yuntu.mawei.R;
import com.yuntu.mawei.base.home.BaseListFragment;
import com.yuntu.mawei.base.home.BaseListFragmentPresenter;
import com.yuntu.mawei.view.adapter.base.BaseQuickAdapter;

public class RankSongFragment extends BaseListFragment {

    int type; // 2 for song, 3 for ring ,4 for singer

    public static RankSongFragment newInstance(int type) {
        RankSongFragment fragment = new RankSongFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("type", type);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    protected void initViews() {
    }

    @Override
    protected int attachLayoutRes() {
        return R.layout.recyclerview_just;
    }

    @Override
    protected BaseQuickAdapter attachAdapter() {
        type = getArguments().getInt("type");
        switch (type) {
            case 2:
                return new SongRankAdapter(getActivity(), 0);
            case 3:
                return new RankSimpleAdapter(getActivity(), 0);
            case 4:
                return new SongRankAdapter(getActivity(), 1);
            default:
                return new SongRankAdapter(getActivity(), 0);
        }
    }

    @Override
    protected BaseListFragmentPresenter attachPresenter() {
        return new RankFragmentPresenter(this, type);
    }
}
