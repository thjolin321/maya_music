package com.yuntu.mawei.ui.three.local;

import android.os.Bundle;

import com.yuntu.mawei.R;
import com.yuntu.mawei.base.home.BaseListFragment;
import com.yuntu.mawei.base.home.BaseListFragmentPresenter;
import com.yuntu.mawei.ui.three.MvSimpleListAdapter;
import com.yuntu.mawei.ui.three.SongSimpleListAdapter;
import com.yuntu.mawei.ui.three.collect.CollectSongListPresenter;
import com.yuntu.mawei.view.adapter.base.BaseQuickAdapter;

public class LocaltSongListFragment extends BaseListFragment {

    int type; // 0 for song,1 for ring,2 for mv

    public static LocaltSongListFragment newInstance(int type) {
        LocaltSongListFragment fragment = new LocaltSongListFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("type", type);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    protected void initViews() {
    }

    @Override
    protected int attachLayoutRes() {
        return R.layout.recyclerview_just;
    }

    @Override
    protected BaseQuickAdapter attachAdapter() {
        type = getArguments().getInt("type");
        if (type == 2) {
            return new MvSimpleListAdapter(getActivity(), 1);
        } else {
            return new SongSimpleListAdapter(getActivity(), 1);
        }
    }

    @Override
    protected BaseListFragmentPresenter attachPresenter() {
        return new LocalSongListPresenter(this, type);
    }

    boolean isSecondExecute;
    @Override
    public void onResume() {
        super.onResume();
        if (isSecondExecute) {
            updateViews(false);
        }
        isSecondExecute = true;
    }
}
