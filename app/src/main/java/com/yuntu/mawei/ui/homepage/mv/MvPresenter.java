package com.yuntu.mawei.ui.homepage.mv;

import com.yuntu.mawei.base.home.BaseListFragment;
import com.yuntu.mawei.base.home.BaseListFragmentPresenter;
import com.yuntu.mawei.bean.base.ResponseBean;
import com.yuntu.mawei.bean.rx.SuccessSubscriber;
import com.yuntu.mawei.bean.song.SingerBean;
import com.yuntu.mawei.bean.song.SongBean;
import com.yuntu.mawei.util.RxUtils;
import com.yuntu.mawei.util.http.HttpUtil;

import java.util.List;

public class MvPresenter extends BaseListFragmentPresenter {

    int type, page;

    public MvPresenter(BaseListFragment mView, int type) {
        super(mView);
        this.type = type;
    }

    @Override
    public void getData(boolean isPullRefresh) {
        HttpUtil.getInstance().getApiService()
                .getHomeSongList(2, type, 1, 10)
                .compose(RxUtils.rxSchedulerHelper())
                .subscribe(new SuccessSubscriber<ResponseBean<List<SongBean>>>() {
                    @Override
                    public void onSuccess(ResponseBean<List<SongBean>> listResponseBean) {
                        mView.loadData(listResponseBean.getResult());
                        page = 2;
                    }

                    @Override
                    public void onFailed(String str) {

                    }
                });
    }

    @Override
    public void getMoreData() {
        HttpUtil.getInstance().getApiService()
                .getHomeSongList(2, type, page, 10)
                .compose(RxUtils.rxSchedulerHelper())
                .subscribe(new SuccessSubscriber<ResponseBean<List<SongBean>>>() {
                    @Override
                    public void onSuccess(ResponseBean<List<SongBean>> listResponseBean) {
                        if (listResponseBean.getResult() == null) return;
                        mView.loadMoreData(listResponseBean.getResult());
                        page++;
                    }

                    @Override
                    public void onFailed(String str) {
                    }
                });
    }
}

