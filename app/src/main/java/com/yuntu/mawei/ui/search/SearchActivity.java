package com.yuntu.mawei.ui.search;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import com.yuntu.mawei.R;
import com.yuntu.mawei.base.home.BaseActivity;
import com.yuntu.mawei.bean.base.ResponseBean;
import com.yuntu.mawei.bean.rx.SuccessSubscriber;
import com.yuntu.mawei.bean.song.SongBean;
import com.yuntu.mawei.ui.search.result.SearchResultActivity;
import com.yuntu.mawei.util.Logl;
import com.yuntu.mawei.util.RxUtils;
import com.yuntu.mawei.util.ToastUtil;
import com.yuntu.mawei.util.http.HttpUtil;
import com.yuntu.mawei.view.FluidLayout;
import com.yuntu.mawei.view.adapter.helper.RecyclerViewHelper;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.BindColor;
import butterknife.BindView;
import butterknife.OnClick;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmResults;
import io.realm.Sort;

/**
 * Created by tanghao on 2017/6/12.
 * meail: 86882259@qq.com
 */

public class SearchActivity extends BaseActivity {

    @BindView(R.id.fl_liushi)
    FluidLayout fluidLayout;
    @BindView(R.id.rv_news_list)
    RecyclerView recyclerView;
    @BindView(R.id.search_edit)
    EditText searchE;
    @BindView(R.id.search_cancel)
    TextView tvCancle;
    SearchHistoryAdapter adapter;

    @BindColor(R.color.mall_black)
    int black;

    Realm realm;
    RealmResults<SearchHistoryBean> realmlists;

    @Override
    public int attachLayout() {
        return R.layout.activity_search;
    }

    @Override
    public void init() {
        initRealm();
        realm = Realm.getDefaultInstance();
        setRedBar();
        initEdit();
        getHotSearchData();
        getRealmData();
    }

    private void initRealm(){
        Realm.init(this);
        RealmConfiguration config = new RealmConfiguration.Builder()
                .name("mawei.realm")
                .deleteRealmIfMigrationNeeded()
                .build();
        Realm.setDefaultConfiguration(config);
    }


    private void initEdit() {
        searchE.setFocusable(true);
        searchE.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView arg0, int arg1, KeyEvent arg2) {
                if (arg1 == EditorInfo.IME_ACTION_SEARCH) {
                    String searchS;
                    if (!(searchS = searchE.getText().toString().trim()).equals("")) {
                        ((InputMethodManager) getSystemService(INPUT_METHOD_SERVICE))
                                .hideSoftInputFromWindow(SearchActivity.this.getCurrentFocus().getWindowToken(),
                                        InputMethodManager.HIDE_NOT_ALWAYS);
                        goToSearch(searchS);
                    } else {
                        ToastUtil.showToast("搜索不能为空");
                    }
                }
                return false;
            }
        });
    }

    public void goToSearch(String str) {
        addData(str);
        SearchResultActivity.launch(SearchActivity.this, str);
    }

    private void getHotSearchData() {
        HttpUtil.getInstance().getApiService().getHotSearchData()
                .compose(RxUtils.rxSchedulerHelper())
                .subscribe(new SuccessSubscriber<ResponseBean<List<SongBean>>>() {
                    @Override
                    public void onSuccess(ResponseBean<List<SongBean>> listResponseBean) {
                        if (listResponseBean.getResult() == null) return;
                        initFluid(listResponseBean.getResult());
                    }

                    @Override
                    public void onFailed(String str) {
                        ToastUtil.showToast(str);
                    }
                });

    }

    private void initHistorySearch(List<SearchHistoryBean> list) {
        if (adapter == null) {
            adapter = new SearchHistoryAdapter(this);
            RecyclerViewHelper.initRecyclerViewV(this, recyclerView, adapter);
            View view = LayoutInflater.from(this).inflate(R.layout.adapter_search_clear, null);
            view.setOnClickListener(v -> {
                clearData();
            });
            adapter.addFooterView(view);
        }
        adapter.updateItems(list);
    }

    private void initFluid(List<SongBean> list) {
        for (int i = 0; i < list.size(); i++) {
            SongBean songBean = list.get(i);
            TextView tv = new TextView(this);

            tv.setTextSize(14);
            tv.setTextColor(black);
            tv.setGravity(Gravity.CENTER);
            tv.setBackgroundResource(R.drawable.bg_hot_search);
            tv.setText(songBean.songName);
            tv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    goToSearch(songBean.songName);
                }
            });
            FluidLayout.LayoutParams params = new FluidLayout.LayoutParams(
                    ViewGroup.LayoutParams.WRAP_CONTENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT
            );
            params.setMargins(20, 8, 20, 8);
            fluidLayout.addView(tv, params);
        }
    }


    /**
     * 从数据库获取历史记录
     *
     * @return
     */
    public void getRealmData() {
        try {
            realmlists = realm.where(SearchHistoryBean.class).findAll();
            realmlists = realmlists.sort("updateTime", Sort.DESCENDING);
        } catch (Exception e) {
        } finally {
        }
        if (realmlists == null) return;
        List<SearchHistoryBean> list = new ArrayList<>();
        for (SearchHistoryBean home : realmlists) {
            list.add(home);
        }
        initHistorySearch(list);
        return;
    }

    /**
     * 添加历史
     */
    public void addData(String s) {
        boolean isAdd = true;
        if (realmlists != null) {
            for (SearchHistoryBean hasS : realmlists) {
                if (s.equals(hasS.getContent())) {
                    isAdd = false;
                    try {
                        realm.beginTransaction();
                        hasS.setUpdateTime(new Date().getTime());
                        realm.commitTransaction();
                    } catch (Exception e) {
                        Log.i("this", "Exception1:" + e.toString());
                    } finally {
                    }
                }
            }
        }
        if (isAdd) {
            try {
                SearchHistoryBean searchHistory = new SearchHistoryBean();
                searchHistory.setContent(s);
                long time = new Date().getTime();
                searchHistory.setCreateTime(time);
                searchHistory.setUpdateTime(time);
//                if (realmlists == null) {
//                    searchHistory.setMainId(1);
//                } else {
//                    searchHistory.setMainId(realmlists.size() + 1);
//                }
                realm.executeTransactionAsync(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        realm.copyToRealm(searchHistory);
                    }
                });
            } catch (Exception e) {
                Logl.e("插入出错？" + e.getMessage());
            } finally {
            }
        }
        getRealmData();
    }

    public void deletData(int index) {
        try {
            realm.beginTransaction();
            realmlists.get(index).deleteFromRealm();
            realm.commitTransaction();
            getRealmData();
        } catch (Exception e) {
        } finally {
        }

    }

    public void clearData() {
        try {
            realm.beginTransaction();
            realm.delete(SearchHistoryBean.class);
            realm.commitTransaction();
        } catch (Exception e) {
        } finally {
        }
        getRealmData();
    }

    @OnClick(R.id.search_cancel)
    public void onCancle() {
        shutDownInput();
        finish();
    }


    private void shutDownInput() {
        try {
            InputMethodManager mInputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            mInputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        } catch (Exception e) {
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        realm.close();
    }

    public static void launch(Context context) {
        context.startActivity(new Intent(context, SearchActivity.class));
    }

}
