package com.yuntu.mawei.ui.three.piliang;

import com.yuntu.mawei.base.home.BaseListActivity;
import com.yuntu.mawei.base.home.BaseListActivityPresenter;
import com.yuntu.mawei.bean.song.SongBean;

import java.util.List;

/**
 * Created by tanghao on 2019/2/15
 */
public class PiliangPresenter extends BaseListActivityPresenter {


    List<SongBean> list;
    public PiliangPresenter(BaseListActivity mView, List<SongBean> list) {
        super(mView);
        this.list = list;
    }

    @Override
    public void getData(boolean isPullRefresh) {
        mView.loadData(list);
    }

    @Override
    public void getMoreData() {
        mView.adapter.noMoreData();
        mView.adapter.loadComplete();
    }
}
