package com.yuntu.mawei.ui.homepage.album;

import com.yuntu.mawei.base.home.BaseListFragment;
import com.yuntu.mawei.base.home.BaseListFragmentPresenter;
import com.yuntu.mawei.bean.base.ResponseBean;
import com.yuntu.mawei.bean.rx.SuccessSubscriber;
import com.yuntu.mawei.bean.song.AlbumBean;
import com.yuntu.mawei.bean.song.SongBean;
import com.yuntu.mawei.util.RxUtils;
import com.yuntu.mawei.util.http.HttpUtil;

import java.util.List;

/**
 * Created by tanghao on 2019/1/25
 */
public class AlbumSongListPresenter extends BaseListFragmentPresenter {

    AlbumBean albumBean;

    public AlbumSongListPresenter(BaseListFragment mView, AlbumBean albumBean) {
        super(mView);
        this.albumBean = albumBean;
    }

    @Override
    public void getData(boolean isPullRefresh) {
        HttpUtil.getInstance().getApiService().getAlbumSongList(albumBean.albumId, 0, 1, 1000).compose(RxUtils.rxSchedulerHelper()).subscribe(new SuccessSubscriber<ResponseBean<List<SongBean>>>() {
            @Override
            public void onSuccess(ResponseBean<List<SongBean>> responseBean) {
                if (responseBean.getResult() == null) return;
                mView.loadData(responseBean.getResult());
                AlbumDetailActivity activity = ((AlbumDetailActivity) (mView.getActivity()));
                activity.songNum = responseBean.getResult().size();
                if (activity != null) {
                    activity.initIndicator();
                }
            }

            @Override
            public void onFailed(String str) {
            }
        });
    }

    @Override
    public void getMoreData() {
        mView.adapter.loadComplete();
        mView.adapter.noMoreData();
    }
}
