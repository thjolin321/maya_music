package com.yuntu.mawei.ui.playing.gift;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.yuntu.mawei.R;
import com.yuntu.mawei.bean.base.ResponseBean;
import com.yuntu.mawei.bean.music.Music;
import com.yuntu.mawei.bean.music.PlayModeEnum;
import com.yuntu.mawei.bean.rx.SuccessSubscriber;
import com.yuntu.mawei.ui.playing.service.AudioPlayer;
import com.yuntu.mawei.ui.playing.service.OnPlayerEventListener;
import com.yuntu.mawei.util.GsonUtil;
import com.yuntu.mawei.util.Logl;
import com.yuntu.mawei.util.RxUtils;
import com.yuntu.mawei.util.ToastUtil;
import com.yuntu.mawei.util.http.HttpUtil;
import com.yuntu.mawei.util.play.Preferences;
import com.yuntu.mawei.view.adapter.helper.RecyclerViewHelper;

import java.util.List;

/**
 * @author tanghao
 * @Date 2018/5/7.
 */
public class GiftDialog extends Activity implements View.OnClickListener, OnPlayerEventListener {

    private TextView tvNum, tvMode;
    private ImageView ivMode;
    private RecyclerView recyclerView;
    private GiftAdapter adapter;
    private int idGift;
    private int num = 1;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_gift);
        init();
        initWindow();
    }

    private void initWindow() {
        Window dialogWindow = getWindow();
        dialogWindow.setGravity(Gravity.BOTTOM);
        dialogWindow.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
    }

    public void init() {
        tvNum = findViewById(R.id.tv_num);
        tvMode = findViewById(R.id.tv_play_order);
        recyclerView = findViewById(R.id.play_recyclerview);
        adapter = new GiftAdapter(this, new GiftAdapter.Callback() {
            @Override
            public void callback(int id) {
                idGift = id;
            }
        });
        RecyclerViewHelper.initRecyclerViewH(this, recyclerView, adapter);
        findViewById(R.id.iv_delete).setOnClickListener(this);
        findViewById(R.id.bt_sure).setOnClickListener(this);
        findViewById(R.id.bt_add).setOnClickListener(this);
        findViewById(R.id.bt_jian).setOnClickListener(this);

        findViewById(R.id.view_kongbai).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        initHttpData();
    }

    private void initHttpData() {
        HttpUtil.getInstance().getApiService().getGiftList(1, 1000)
                .compose(RxUtils.rxSchedulerHelper()).subscribe(new SuccessSubscriber<ResponseBean<List<GiftBean>>>() {
            @Override
            public void onSuccess(ResponseBean<List<GiftBean>> listResponseBean) {
                if (listResponseBean.getResult() == null || listResponseBean.getResult().size() == 0)
                    return;
                idGift = listResponseBean.getResult().get(0).commodityId;
                adapter.updateItems(listResponseBean.getResult());
            }

            @Override
            public void onFailed(String str) {
                ToastUtil.showToast(str);
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_delete:
                finish();
                break;
            case R.id.bt_sure:
                // TODO 开始调接口支付
                HttpUtil.getInstance().getApiService().pay(2, idGift, num, getIntent().getIntExtra("songId", 0))
                        .compose(RxUtils.rxSchedulerHelper())
                        .subscribe(new SuccessSubscriber<ResponseBean>() {
                            @Override
                            public void onSuccess(ResponseBean responseBean) {
                                ToastUtil.showToast(responseBean.getMessage());
                            }

                            @Override
                            public void onFailed(String str) {
                                ToastUtil.showToast(str);
                            }
                        });
                break;

            case R.id.bt_add:
                num++;
                tvNum.setText(String.valueOf(num));
                break;
            case R.id.bt_jian:
                if (num > 1) {
                    num--;
                    tvNum.setText(String.valueOf(num));
                }
                break;
        }
    }

    public void switchPlayMode() {
        PlayModeEnum mode = PlayModeEnum.valueOf(Preferences.getPlayMode());
        switch (mode) {
            case LOOP:
                mode = PlayModeEnum.SHUFFLE;
                ToastUtil.showToast(R.string.mode_shuffle);
                tvMode.setText(R.string.mode_shuffle);
                break;
            case SHUFFLE:
                mode = PlayModeEnum.SINGLE;
                ToastUtil.showToast(R.string.mode_one);
                tvMode.setText(R.string.mode_one);
                break;
            case SINGLE:
                mode = PlayModeEnum.LOOP;
                ToastUtil.showToast(R.string.mode_loop);
                tvMode.setText(R.string.mode_loop);
                break;
        }
        Preferences.savePlayMode(mode.value());
        initPlayMode();

    }

    public void initPlayMode() {
        int mode = Preferences.getPlayMode();
        ivMode.setImageLevel(mode);
    }

    @Override
    public void onChange(Music music) {
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onPlayerStart() {

    }

    @Override
    public void onPlayerPause() {

    }

    @Override
    public void onPublish(int progress) {

    }

    @Override
    public void onBufferingUpdate(int percent) {

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        AudioPlayer.get().removeOnPlayEventListener(this);
    }

    public static void launch(Context context, int songId) {
        Intent intent = new Intent(context, GiftDialog.class);
        intent.putExtra("songId", songId);
        context.startActivity(intent);
    }

}
