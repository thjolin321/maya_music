package com.yuntu.mawei.ui.house;

import android.content.Context;
import android.view.View;

import com.yuntu.mawei.R;
import com.yuntu.mawei.bean.house.MusicHouseBean;
import com.yuntu.mawei.ui.house.detail.SongListActivity;
import com.yuntu.mawei.util.GlideHelper;
import com.yuntu.mawei.view.adapter.base.BaseQuickAdapter;
import com.yuntu.mawei.view.adapter.base.BaseViewHolder;

public class SongHouseAdapter extends BaseQuickAdapter<MusicHouseBean> {

    int isFree;

    public SongHouseAdapter(Context context) {
        super(context);
    }

    public SongHouseAdapter(Context context, int isFree) {
        super(context);
        this.isFree = isFree;
    }

    @Override
    protected int attachLayoutRes() {
        return R.layout.adapter_music_house;
    }

    @Override
    protected void convert(BaseViewHolder holder, MusicHouseBean item) {
        GlideHelper.loadListPic(mContext, item.songTypeImage, holder.getView(R.id.iv_cover));
        holder.getConvertView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SongListActivity.launch(mContext, item.songTypeImage, item.typeName, item.songTypeId, isFree, false, true);
            }
        });
        if (item.songList == null || item.songList.size() == 0) {
            return;
        }
        switch (item.songList.size()) {
            case 3:
                holder.setText(R.id.tv_title_3, item.songList.get(2).songName);
                holder.setText(R.id.tv_title_2, item.songList.get(1).songName);
                holder.setText(R.id.tv_title_1, item.songList.get(0).songName);
                if (item.songList.get(2).singer != null) {
                    holder.setText(R.id.tv_artist_3, "- " + item.songList.get(2).singer.singerName);
                }
                if (item.songList.get(1).singer != null) {
                    holder.setText(R.id.tv_artist_2, "- " + item.songList.get(1).singer.singerName);
                }
                if (item.songList.get(0).singer != null) {
                    holder.setText(R.id.tv_artist_1, "- " + item.songList.get(0).singer.singerName);
                }
                break;
            case 2:
                holder.setText(R.id.tv_title_3, "暂无");
                holder.setText(R.id.tv_artist_3, "");
                holder.setText(R.id.tv_title_2, item.songList.get(1).songName);
                holder.setText(R.id.tv_title_1, item.songList.get(0).songName);
                if (item.songList.get(1).singer != null) {
                    holder.setText(R.id.tv_artist_2, "- " + item.songList.get(1).singer.singerName);
                }
                if (item.songList.get(0).singer != null) {
                    holder.setText(R.id.tv_artist_1, "- " + item.songList.get(0).singer.singerName);
                }
                break;
            case 1:
                holder.setText(R.id.tv_title_3, "暂无");
                holder.setText(R.id.tv_artist_3, "");
                holder.setText(R.id.tv_title_2, "暂无");
                holder.setText(R.id.tv_artist_2, "");
                holder.setText(R.id.tv_title_1, item.songList.get(0).songName);
                if (item.songList.get(0).singer != null) {
                    holder.setText(R.id.tv_artist_1, "- " + item.songList.get(0).singer.singerName);
                }
                break;
        }
    }
}
