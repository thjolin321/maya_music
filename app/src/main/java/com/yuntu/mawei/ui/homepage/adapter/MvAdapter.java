package com.yuntu.mawei.ui.homepage.adapter;

import android.content.Context;

import com.yuntu.mawei.R;
import com.yuntu.mawei.bean.music.Music;
import com.yuntu.mawei.bean.song.SongBean;
import com.yuntu.mawei.ui.homepage.mv.detail.MvDetailActivity;
import com.yuntu.mawei.util.GlideHelper;
import com.yuntu.mawei.view.adapter.base.BaseQuickAdapter;
import com.yuntu.mawei.view.adapter.base.BaseViewHolder;

/**
 * Created by tanghao on 2019/1/24
 */
public class MvAdapter extends BaseQuickAdapter<SongBean> {

    public MvAdapter(Context context) {
        super(context);
    }

    @Override
    protected int attachLayoutRes() {
        return R.layout.adapter_mv;
    }

    @Override
    protected void convert(BaseViewHolder holder, SongBean item) {
        GlideHelper.loadListPic(mContext, item.songAvatar, holder.getView(R.id.iv_mv));
        holder.setText(R.id.tv_recommend, item.recommendValue + "想要");
        holder.getConvertView().setOnClickListener(v -> {
            MvDetailActivity.launch(mContext, item);
        });
    }
}
