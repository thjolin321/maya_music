package com.yuntu.mawei.ui.homepage.rank;

import com.yuntu.mawei.base.home.BaseListFragment;
import com.yuntu.mawei.base.home.BaseListFragmentPresenter;
import com.yuntu.mawei.bean.base.ResponseBean;
import com.yuntu.mawei.bean.house.MusicRankBean;
import com.yuntu.mawei.bean.rx.SuccessSubscriber;
import com.yuntu.mawei.util.RxUtils;
import com.yuntu.mawei.util.http.HttpUtil;

import java.util.List;

public class RankFragmentPresenter extends BaseListFragmentPresenter {

    int type;

    public RankFragmentPresenter(BaseListFragment mView, int type) {
        super(mView);
        this.type = type;
    }

    @Override
    public void getData(boolean isPullRefresh) {
        switch (type) {
            case 2:
                HttpUtil.getInstance().getApiService().getRankList(type)
                        .compose(RxUtils.rxSchedulerHelper())
                        .subscribe(new SuccessSubscriber<ResponseBean<List<MusicRankBean>>>() {
                            @Override
                            public void onSuccess(ResponseBean<List<MusicRankBean>> listResponseBean) {
                                if (listResponseBean.getResult() == null) return;
                                mView.loadData(listResponseBean.getResult());

                            }

                            @Override
                            public void onFailed(String str) {
                            }
                        });
                break;
            case 3:
                HttpUtil.getInstance().getApiService().getRankList(type)
                        .compose(RxUtils.rxSchedulerHelper())
                        .subscribe(new SuccessSubscriber<ResponseBean<List<MusicRankBean>>>() {
                            @Override
                            public void onSuccess(ResponseBean<List<MusicRankBean>> listResponseBean) {
                                if (listResponseBean.getResult() == null
                                        || listResponseBean.getResult().size() == 0) return;

                                mView.loadData(listResponseBean.getResult().get(0).songList);
                            }

                            @Override
                            public void onFailed(String str) {
                            }
                        });
                break;
            case 4:
                HttpUtil.getInstance().getApiService().getSingerList()
                        .compose(RxUtils.rxSchedulerHelper())
                        .subscribe(new SuccessSubscriber<ResponseBean<List<MusicRankBean>>>() {
                            @Override
                            public void onSuccess(ResponseBean<List<MusicRankBean>> listResponseBean) {
                                if (listResponseBean.getResult() == null) return;
                                mView.loadData(listResponseBean.getResult());
                            }

                            @Override
                            public void onFailed(String str) {
                            }
                        });
                break;
        }


    }

    @Override
    public void getMoreData() {
        mView.adapter.noMoreData();
        mView.adapter.loadComplete();
    }
}
