package com.yuntu.mawei.ui.circle.search;

import com.yuntu.mawei.R;
import com.yuntu.mawei.view.adapter.base.BaseQuickAdapter;
import com.yuntu.mawei.view.adapter.base.BaseViewHolder;

public class CircleSearchHistoryAdapter extends BaseQuickAdapter<CircleSearchHistoryBean> {

    CircleSearchActivity activity;

    public CircleSearchHistoryAdapter(CircleSearchActivity activity) {
        super(activity);
        this.activity = activity;
    }

    @Override
    protected int attachLayoutRes() {
        return R.layout.adapter_search_history;
    }

    @Override
    protected void convert(BaseViewHolder holder, CircleSearchHistoryBean item) {
        holder.setText(R.id.tv_name, item.getContent());
        holder.getConvertView().setOnClickListener(v -> {
            activity.goToSearch(item.getContent() );
        });
        holder.setOnClickListener(R.id.bt_cha, v -> {
            activity.deletData(holder.getAdapterPosition());
        });
    }

}
