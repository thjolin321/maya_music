package com.yuntu.mawei.ui.three;

import com.yuntu.mawei.R;
import com.yuntu.mawei.base.home.BaseListFragment;
import com.yuntu.mawei.base.home.BaseListFragmentPresenter;
import com.yuntu.mawei.view.adapter.base.BaseQuickAdapter;

public class SongSimpleListFragment extends BaseListFragment {
    @Override
    protected void initViews() {
    }

    @Override
    protected int attachLayoutRes() {
        return R.layout.recyclerview_just;
    }

    @Override
    protected BaseQuickAdapter attachAdapter() {
        return new SongSimpleListAdapter(getActivity(), 1);
    }

    @Override
    protected BaseListFragmentPresenter attachPresenter() {
        return new SongSimpleListPresenter(this);
    }

}
