package com.yuntu.mawei.ui.login.guide;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.yuntu.mawei.R;

/**
 * Created by tanghao on 2017/7/3.
 * Email: 86882259@qq.com
 */

public class YingdaoPicFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_yingdao, null);
        return v;
    }

}
