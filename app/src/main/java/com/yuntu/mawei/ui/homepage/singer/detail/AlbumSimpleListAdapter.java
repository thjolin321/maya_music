package com.yuntu.mawei.ui.homepage.singer.detail;

import android.content.Context;
import android.view.View;

import com.yuntu.mawei.R;
import com.yuntu.mawei.bean.song.AlbumBean;
import com.yuntu.mawei.bean.song.AlbumBean;
import com.yuntu.mawei.ui.homepage.album.AlbumDetailActivity;
import com.yuntu.mawei.ui.homepage.mv.detail.MvDetailActivity;
import com.yuntu.mawei.ui.playing.service.AudioPlayer;
import com.yuntu.mawei.ui.three.piliang.PiliangActivity;
import com.yuntu.mawei.util.GlideHelper;
import com.yuntu.mawei.util.music.MusicUtil;
import com.yuntu.mawei.view.adapter.base.BaseMultiItemQuickAdapter;
import com.yuntu.mawei.view.adapter.base.BaseViewHolder;

public class AlbumSimpleListAdapter extends BaseMultiItemQuickAdapter<AlbumBean> {


    public AlbumSimpleListAdapter(Context context) {
        super(context);
    }

    @Override
    protected void attachItemType() {
        addItemType(AlbumBean.SONG_TITLE_ITEM, R.layout.adapter_song_simple_title);
        addItemType(AlbumBean.SONG_ITEM, R.layout.adapter_mv_simple_list);
    }

    @Override
    protected void convert(BaseViewHolder holder, AlbumBean item) {
        switch (item.itemType) {
            case AlbumBean.SONG_TITLE_ITEM:
                holder.setText(R.id.tv_zimu_title, item.zimuTitle);
                holder.setVisible(R.id.bt_piliang, false);
                break;
            case AlbumBean.SONG_ITEM:
                GlideHelper.loadListPic(mContext, item.albumAvatar, holder.getView(R.id.iv_song));
                holder.setText(R.id.tv_song, item.albumName);
                holder.setText(R.id.tv_artist_and_album, item.createTime);
                holder.getConvertView().setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        AlbumDetailActivity.launch(mContext, item);
                    }
                });
                break;
        }
    }
}
