package com.yuntu.mawei.ui.login;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.CountDownTimer;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.yuntu.mawei.R;
import com.yuntu.mawei.base.TaipingApplication;
import com.yuntu.mawei.base.home.BaseActivity;
import com.yuntu.mawei.bean.base.ResponseBean;
import com.yuntu.mawei.bean.rx.SuccessSubscriber;
import com.yuntu.mawei.ui.main.MainActivity;
import com.yuntu.mawei.util.RxUtils;
import com.yuntu.mawei.util.ToastUtil;
import com.yuntu.mawei.util.http.HttpUtil;
import com.yuntu.mawei.util.music.MusicUtil;

import butterknife.BindView;
import butterknife.OnClick;

public class BindActivity extends BaseActivity {

    @BindView(R.id.et_phone)
    EditText etPhone;
    @BindView(R.id.et_yanzheng)
    EditText etYanzheng;
    @BindView(R.id.bt_yanzheng)
    TextView tvCountTime;
    @BindView(R.id.et_new)
    EditText etNew;
    @BindView(R.id.et_new_again)
    EditText etNewAgain;

    String wxId;


    @Override
    public int attachLayout() {
        return R.layout.activity_bind;
    }

    @Override
    public void init() {
        wxId = getIntent().getStringExtra("wxId");
        initTitle(R.string.password_resetting);
    }

    @OnClick({R.id.bt_sure, R.id.bt_yanzheng})
    public void onSure(View v) {
        switch (v.getId()) {
            case R.id.bt_sure:
                if (TextUtils.isEmpty(etPhone.getText())) {
                    ToastUtil.showToast(R.string.input_phone);
                    return;
                }
                if (etPhone.getText().toString().length() != 11) {
                    ToastUtil.showToast(R.string.input_phone_right);
                    return;
                }
                if (TextUtils.isEmpty(etYanzheng.getText())) {
                    ToastUtil.showToast(R.string.input_vertify);
                    return;
                }
                if (TextUtils.isEmpty(etNew.getText())) {
                    ToastUtil.showToast(R.string.input_new_password);
                    return;
                }
                if (TextUtils.isEmpty(etNewAgain.getText())) {
                    ToastUtil.showToast(R.string.input_password_again);
                    return;
                }
                if (!etNew.getText().toString().equals(etNewAgain.getText().toString())) {
                    ToastUtil.showToast(R.string.password_not_equal);
                    return;
                }
                if (!MusicUtil.isLetterOrDigit(etNew.getText().toString())) {
                    ToastUtil.showToast(R.string.mima_daxiaoxie);
                    return;
                }
                HttpUtil.getInstance().getApiService().wxBind(wxId, etPhone.getText().toString(),
                        etNew.getText().toString(), etYanzheng.getText().toString())
                        .compose(RxUtils.rxSchedulerHelper())
                        .subscribe(new SuccessSubscriber<ResponseBean>() {
                            @Override
                            public void onSuccess(ResponseBean responseBean) {
                                HttpUtil.getInstance().getApiService()
                                        .login(etPhone.getText().toString(), etNew.getText().toString())
                                        .compose(RxUtils.rxSchedulerHelper()).subscribe(new SuccessSubscriber<ResponseBean<TokenBean>>() {
                                    @Override
                                    public void onSuccess(ResponseBean<TokenBean> responseBean) {
                                        if (responseBean.getResult() == null) return;
                                        TaipingApplication.tpApp.setToken(responseBean.getResult().token);
                                        TaipingApplication.tpApp.setPhone(etPhone.getText().toString());
                                        MainActivity.launch(BindActivity.this);
                                        finish();
                                    }

                                    @Override
                                    public void onFailed(String str) {
                                    }
                                });
                            }

                            @Override
                            public void onFailed(String str) {
                                ToastUtil.showToast(str);
                            }
                        });
                break;
            case R.id.bt_yanzheng:
                sendMessage();
                break;
        }
    }


    private void sendMessage() {
        if (TextUtils.isEmpty(etPhone.getText())) {
            ToastUtil.showToast("请输入手机号码");
            return;
        }
        String phone = etPhone.getText().toString().trim().replace(" ", "");

        if (phone.length() != 11) {
            ToastUtil.showToast("请验证手机号码是否正确");
            return;
        }

        HttpUtil.getInstance().getApiService().sendMessage(etPhone.getText().toString())
                .compose(RxUtils.rxSchedulerHelper())
                .subscribe(new SuccessSubscriber<ResponseBean>() {
                    @Override
                    public void onSuccess(ResponseBean responseBean) {
                        ToastUtil.showToast("发送验证码成功");
                        countDown();
                    }

                    @Override
                    public void onFailed(String str) {
                        ToastUtil.showToast(str);
                    }
                });
    }

    CountDownTimer timer;

    private void countDown() {
        CountDownTimer timer = new CountDownTimer(60000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                tvCountTime.setTextColor(Color.parseColor("#8C8C8C"));
                tvCountTime.setEnabled(false);
                tvCountTime.setText((millisUntilFinished / 1000) + "秒后重发");
            }

            @Override
            public void onFinish() {
                tvCountTime.setTextColor(Color.parseColor("#E8413B"));
                tvCountTime.setEnabled(true);
                tvCountTime.setText("重发验证码");
            }
        };
        timer.start();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
    }

    public static void launch(Context context, String wxId) {
        Intent intent = new Intent(context, BindActivity.class);
        intent.putExtra("wxId", wxId);
        context.startActivity(intent);
    }

}
