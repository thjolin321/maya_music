package com.yuntu.mawei.ui.three.local;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;

import com.yuntu.mawei.R;
import com.yuntu.mawei.base.TaipingApplication;
import com.yuntu.mawei.base.home.BaseActivity;
import com.yuntu.mawei.ui.login.LoginActivity;
import com.yuntu.mawei.widget.ColorStyleTransitionPagerTitleView;

import net.lucode.hackware.magicindicator.MagicIndicator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.CommonNavigator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.CommonNavigatorAdapter;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.IPagerIndicator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.IPagerTitleView;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.indicators.LinePagerIndicator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindColor;
import butterknife.BindView;
import me.wcy.music.loader.MusicLoaderCallback;

/**
 * Created by tanghao on 2019/1/24
 */
public class LocalMusicActivity extends BaseActivity {

    @BindView(R.id.indicator)
    MagicIndicator indicator;
    @BindView(R.id.view_pager)
    ViewPager viewPager;

    @BindColor(R.color.mall_black)
    int colorBlack;
    @BindColor(R.color.mawei_red)
    int colorBlue;

    private List<Fragment> fragments;
    private FragmentPagerAdapter mAdapter;

    List<String> finalTitles = Arrays.asList("歌曲", "铃声");
    public int songNum, mvNum, ringNum;

    @Override
    public int attachLayout() {
        return R.layout.activity_help;
    }

    @Override
    public void init() {
        initTitle(R.string.mine_str_4);
        initIndicator();
        initFragments();
    }

    public static void launch(Context context) {
        context.startActivity(new Intent(context, LocalMusicActivity.class));
    }

    private void initFragments() {
        fragments = new ArrayList<>();
        fragments.add(LocaltSongListFragment.newInstance(0));
        fragments.add(LocaltSongListFragment.newInstance(1));
        mAdapter = new FragmentPagerAdapter(getSupportFragmentManager()) {
            @Override
            public int getCount() {
                return fragments.size();
            }

            @Override
            public Fragment getItem(int position) {
                return fragments.get(position);
            }

            @Override
            public long getItemId(int position) {
                return position;
            }
        };
        viewPager.setAdapter(mAdapter);
        viewPager.setOffscreenPageLimit(3);
        viewPager.addOnPageChangeListener(onPageChangeListener);
    }

    public void initIndicator() {
        CommonNavigator commonNavigator = new CommonNavigator(this);
        commonNavigator.setAdjustMode(true);
        commonNavigator.setAdapter(new CommonNavigatorAdapter() {

            @Override
            public int getCount() {
                return finalTitles.size();
            }

            @Override
            public IPagerTitleView getTitleView(Context context, final int index) {
                ColorStyleTransitionPagerTitleView colorTransitionPagerTitleView = new
                        ColorStyleTransitionPagerTitleView(context);
                colorTransitionPagerTitleView.setSelectedTypeface(Typeface.defaultFromStyle(Typeface.BOLD));
                colorTransitionPagerTitleView.setNormalTypeface(Typeface.defaultFromStyle(Typeface.NORMAL));
                colorTransitionPagerTitleView.setLayoutParams(new ViewGroup.LayoutParams
                        (ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
                colorTransitionPagerTitleView.setNormalColor(colorBlack);
                colorTransitionPagerTitleView.setTextSize(14);
                colorTransitionPagerTitleView.setSelectedColor(colorBlue);
                int nowNum = 0;
                switch (index) {
                    case 0:
                        nowNum = songNum;
                        break;
                    case 1:
                        nowNum = ringNum;
                        break;
                    case 2:
                        nowNum = ringNum;
                        break;
                }
                colorTransitionPagerTitleView.setText(finalTitles.get(index) + "/" + nowNum);
                colorTransitionPagerTitleView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        viewPager.setCurrentItem(index);
                    }
                });
                return colorTransitionPagerTitleView;
            }

            @Override
            public IPagerIndicator getIndicator(Context context) {
                LinePagerIndicator indicator = new LinePagerIndicator(context);
                indicator.setMode(LinePagerIndicator.MODE_WRAP_CONTENT);
                indicator.setColors(colorBlue);
                return indicator;
            }

            @Override
            public float getTitleWeight(Context context, int index) {
                return 1.0f;
            }
        });
        indicator.setNavigator(commonNavigator);

    }

    ViewPager.OnPageChangeListener onPageChangeListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            indicator.onPageScrolled(position, positionOffset, positionOffsetPixels);
        }

        @Override
        public void onPageSelected(int position) {
            indicator.onPageSelected(position);
        }

        @Override
        public void onPageScrollStateChanged(int state) {
            indicator.onPageScrollStateChanged(state);
        }
    };
}
