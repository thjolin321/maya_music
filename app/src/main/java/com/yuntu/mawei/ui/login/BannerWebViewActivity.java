package com.yuntu.mawei.ui.login;

import android.content.Context;
import android.content.Intent;
import android.webkit.WebView;

import com.yuntu.mawei.R;
import com.yuntu.mawei.base.home.BaseActivity;
import com.yuntu.mawei.bean.base.ResponseBean;
import com.yuntu.mawei.bean.base.XieyiBean;
import com.yuntu.mawei.bean.rx.SuccessSubscriber;
import com.yuntu.mawei.util.RxUtils;
import com.yuntu.mawei.util.ToastUtil;
import com.yuntu.mawei.util.http.HttpUtil;

import butterknife.BindView;

/**
 * Created by tanghao on 2019/2/27
 */
public class BannerWebViewActivity extends BaseActivity {

    String url;
    @BindView(R.id.webview)
    WebView webView;

    @Override
    public int attachLayout() {
        return R.layout.activity_banner_webview;
    }

    @Override
    public void init() {
        url = getIntent().getStringExtra("url");
        String name = getIntent().getStringExtra("bannerName");
        if(name!=null){
            initTitle(name);
        }else{
            initTitle("");
        }
        webView.loadUrl(url);
    }

    public static void launch(Context context, String url,String bannerName) {
        Intent intent = new Intent(context, BannerWebViewActivity.class);
        intent.putExtra("url", url);
        intent.putExtra("bannerName", bannerName);
        context.startActivity(intent);
    }
}
