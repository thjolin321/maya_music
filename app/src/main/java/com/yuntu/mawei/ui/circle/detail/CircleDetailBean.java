package com.yuntu.mawei.ui.circle.detail;

import com.yuntu.mawei.bean.circle.CommentBean;
import com.yuntu.mawei.view.adapter.entity.MultiItemEntity;

public class CircleDetailBean extends MultiItemEntity {

    public static final int ITEM_WEB = 1;
    public static final int ITEM_XIAN = 2;
    public static final int ITEM_COMMENT = 3;

    public CommentBean commentBean;
    public CircleDetailBean(int itemType) {
        super(itemType);
    }

}
