package com.yuntu.mawei.ui.login;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.CountDownTimer;
import android.text.TextUtils;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.yuntu.mawei.R;
import com.yuntu.mawei.base.TaipingApplication;
import com.yuntu.mawei.base.home.BaseActivity;
import com.yuntu.mawei.bean.base.ResponseBean;
import com.yuntu.mawei.bean.music.Music;
import com.yuntu.mawei.bean.rx.SuccessSubscriber;
import com.yuntu.mawei.ui.main.MainActivity;
import com.yuntu.mawei.util.RxUtils;
import com.yuntu.mawei.util.SharedPreferenceUtil;
import com.yuntu.mawei.util.ToastUtil;
import com.yuntu.mawei.util.http.HttpUtil;
import com.yuntu.mawei.util.music.MusicUtil;
import com.yuntu.mawei.view.loading.LoadingDialog;

import butterknife.BindView;
import butterknife.OnClick;
import rx.Subscriber;

public class RegisterActivity extends BaseActivity {

    @BindView(R.id.et_input_phone)
    EditText etPhone;
    @BindView(R.id.et_input_password)
    EditText etPassword;
    @BindView(R.id.et_input_vertify)
    EditText etVertify;
    @BindView(R.id.bt_agree)
    ImageView btAgree;
    @BindView(R.id.bt_password_see)
    ImageView ivSee;
    @BindView(R.id.bt_get)
    TextView tvCountTime;


    private boolean showPassword;
    private boolean isAgree = true;

    @Override
    public int attachLayout() {
        return R.layout.activity_register;
    }

    @Override
    public void init() {
        initTitle("注册");
    }

    @OnClick({R.id.bt_register, R.id.bt_password_see, R.id.bt_get,
            R.id.bt_has_user, R.id.bt_agree, R.id.bt_xieyi})
    public void onRegisterClick(View view) {
        switch (view.getId()) {
            case R.id.bt_get:
                sendMessage();
                break;
            case R.id.bt_register:
                register();
                break;
            case R.id.bt_password_see:
                if (showPassword) {
                    showPassword = false;
                } else {
                    showPassword = true;
                }
                if (showPassword) {
                    //如果选中，显示密码
                    ivSee.setImageResource(R.mipmap.password_unsee);
                    etPassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                } else {
                    //否则隐藏密码
                    ivSee.setImageResource(R.mipmap.password_see);
                    etPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
                }
                break;
            case R.id.bt_has_user:
                finish();
                break;
            case R.id.bt_agree:
                if (isAgree) {
                    isAgree = false;
                } else {
                    isAgree = true;
                }
                if (isAgree) {
                    btAgree.setImageResource(R.mipmap.gou_red);
                } else {
                    btAgree.setImageResource(R.mipmap.gou_grey);
                }
                break;
            case R.id.bt_xieyi:
                MyWebViewActivity.launch(this, 2);
                break;
        }
    }


    private void register() {
        if (TextUtils.isEmpty(etPhone.getText())) {
            ToastUtil.showToast(R.string.input_phone);
            return;
        }
        if (etPhone.getText().toString().length() != 11) {
            ToastUtil.showToast(R.string.input_phone_right);
            return;
        }
        if (TextUtils.isEmpty(etPassword.getText())) {
            ToastUtil.showToast(R.string.input_password);
            return;
        }
        if (etPassword.getText().toString().length() < 6) {
            ToastUtil.showToast(R.string.input_password_6);
            return;
        }
        if (TextUtils.isEmpty(etVertify.getText())) {
            ToastUtil.showToast(R.string.input_vertify);
            return;
        }
        if (!isAgree) {
            ToastUtil.showToast(R.string.agree_register);
            return;
        }
        if(!MusicUtil.isLetterOrDigit(etPassword.getText().toString())){
            ToastUtil.showToast(R.string.mima_daxiaoxie);
            return;
        }

        HttpUtil.getInstance().getApiService().register(etPhone.getText().toString(),
                etPassword.getText().toString(), etVertify.getText().toString())
                .compose(RxUtils.rxSchedulerHelper()).subscribe(new SuccessSubscriber<ResponseBean>() {
            @Override
            public void onSuccess(ResponseBean responseBean) {
                HttpUtil.getInstance().getApiService()
                        .login(etPhone.getText().toString(), etPassword.getText().toString())
                        .compose(RxUtils.rxSchedulerHelper()).subscribe(new SuccessSubscriber<ResponseBean<TokenBean>>() {
                    @Override
                    public void onSuccess(ResponseBean<TokenBean> responseBean) {
                        TaipingApplication.tpApp.setToken(responseBean.getResult().token);
                        TaipingApplication.tpApp.setPhone(etPhone.getText().toString());
                        MainActivity.launch(RegisterActivity.this);
                        finish();
                    }

                    @Override
                    public void onFailed(String str) {
                        LoginActivity.launch(RegisterActivity.this);
                        finish();
                    }
                });
            }

            @Override
            public void onFailed(String str) {
                ToastUtil.showToast(str);
            }
        });
    }

    private void sendMessage() {
        if (TextUtils.isEmpty(etPhone.getText())) {
            ToastUtil.showToast("请输入手机号码");
            return;
        }
        String phone = etPhone.getText().toString().trim().replace(" ", "");

        if (phone.length() != 11) {
            ToastUtil.showToast("请验证手机号码是否正确");
            return;
        }

        HttpUtil.getInstance().getApiService().sendMessage(etPhone.getText().toString())
                .compose(RxUtils.rxSchedulerHelper())
                .subscribe(new SuccessSubscriber<ResponseBean>() {
                    @Override
                    public void onSuccess(ResponseBean responseBean) {
                        ToastUtil.showToast("发送验证码成功");
                        countDown();
                    }

                    @Override
                    public void onFailed(String str) {
                        ToastUtil.showToast(str);
                    }
                });
    }

    CountDownTimer timer;

    private void countDown() {
        CountDownTimer timer = new CountDownTimer(60000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                tvCountTime.setTextColor(Color.parseColor("#8C8C8C"));
                tvCountTime.setEnabled(false);
                tvCountTime.setText((millisUntilFinished / 1000) + "秒后重发");
            }

            @Override
            public void onFinish() {
                tvCountTime.setTextColor(Color.parseColor("#E8413B"));
                tvCountTime.setEnabled(true);
                tvCountTime.setText("重发验证码");
            }
        };
        timer.start();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
    }

    public static void launch(Context context) {
        Intent intent = new Intent(context, RegisterActivity.class);
        context.startActivity(intent);
    }
}
