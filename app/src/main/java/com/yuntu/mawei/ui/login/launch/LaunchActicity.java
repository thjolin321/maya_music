package com.yuntu.mawei.ui.login.launch;

import android.os.Handler;
import android.text.TextUtils;

import com.yuntu.mawei.R;
import com.yuntu.mawei.base.TaipingApplication;
import com.yuntu.mawei.base.home.BaseActivity;
import com.yuntu.mawei.ui.login.LoginActivity;
import com.yuntu.mawei.ui.login.guide.GuideActicity;
import com.yuntu.mawei.ui.main.MainActivity;
import com.yuntu.mawei.util.SharedPreferenceUtil;

/**
 * Created by tanghao on 2015/9/14.
 */
public class LaunchActicity extends BaseActivity {

    @Override
    public int attachLayout() {
        return R.layout.activity_launch;
    }

    @Override
    public void init() {
        SharedPreferenceUtil sharedPreferenceUtil = SharedPreferenceUtil.getInstance();
        boolean isFirst = sharedPreferenceUtil.getBoolean("first", true);
        if (isFirst) {
            GuideActicity.launch(LaunchActicity.this);
        } else {
            MainActivity.launch(this);
        }
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                finish();
            }
        },500);
    }
}
