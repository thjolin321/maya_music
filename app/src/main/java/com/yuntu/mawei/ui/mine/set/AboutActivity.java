package com.yuntu.mawei.ui.mine.set;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.TextView;

import com.yuntu.mawei.R;
import com.yuntu.mawei.base.home.BaseActivity;
import com.yuntu.mawei.ui.login.LoginActivity;
import com.yuntu.mawei.ui.login.ResettingPasswordActivity;
import com.yuntu.mawei.util.SharedPreferenceUtil;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by tanghao on 2019/1/24
 */
public class AboutActivity extends BaseActivity {

    @BindView(R.id.tv_about)
    TextView tvAbout;

    @Override
    public int attachLayout() {
        return R.layout.activity_about;
    }

    @Override
    public void init() {
        initTitle(R.string.mine_str_7);
    }

    public static void launch(Context context) {
        context.startActivity(new Intent(context, AboutActivity.class));
    }


}
