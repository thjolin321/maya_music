package com.yuntu.mawei.ui.login;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import com.yuntu.mawei.R;
import com.yuntu.mawei.base.home.BaseActivity;
import com.yuntu.mawei.bean.base.ResponseBean;
import com.yuntu.mawei.bean.rx.SuccessSubscriber;
import com.yuntu.mawei.util.RxUtils;
import com.yuntu.mawei.util.ToastUtil;
import com.yuntu.mawei.util.http.HttpUtil;
import com.yuntu.mawei.util.music.MusicUtil;

import butterknife.BindView;
import butterknife.OnClick;

public class ResettingPasswordActivity extends BaseActivity {

    @BindView(R.id.et_input_password)
    EditText etPassword;
    @BindView(R.id.et_input_password_again)
    EditText etPasswordAgain;
    @BindView(R.id.bt_password_see)
    ImageView ivSee;
    @BindView(R.id.bt_password_see_again)
    ImageView ivSeeAgain;

    private String phone;

    private boolean showPassword, showPasswordAgain;

    @Override
    public int attachLayout() {
        return R.layout.activity_resetting;
    }

    @Override
    public void init() {
        initTitle("忘记密码");
        phone = getIntent().getStringExtra("phone");
    }

    @OnClick({R.id.bt_sure, R.id.bt_password_see, R.id.bt_password_see_again})
    public void onForgetClick(View view) {
        switch (view.getId()) {
            case R.id.bt_sure:
                if (TextUtils.isEmpty(etPassword.toString())) {
                    ToastUtil.showToast(R.string.input_password);
                    return;
                }
                if (TextUtils.isEmpty(etPassword.toString())) {
                    ToastUtil.showToast(R.string.input_password_again);
                    return;
                }
                if (!etPassword.getText().toString().equals(etPasswordAgain.getText().toString())) {
                    ToastUtil.showToast(R.string.password_not_equal);
                    return;
                }
                if (!MusicUtil.isLetterOrDigit(etPassword.getText().toString())) {
                    ToastUtil.showToast(R.string.mima_daxiaoxie);
                    return;
                }
                HttpUtil.getInstance().getApiService().resettingPassword(phone,
                        etPassword.getText().toString()).compose(RxUtils.rxSchedulerHelper()).subscribe(new SuccessSubscriber<ResponseBean>() {
                    @Override
                    public void onSuccess(ResponseBean tokenBeanResponseBean) {
                        ToastUtil.showToast(R.string.chongzhi_success);
                        LoginActivity.launch(ResettingPasswordActivity.this);
                        finish();
                    }

                    @Override
                    public void onFailed(String str) {
                    }
                });

                break;
            case R.id.bt_password_see:
                showOrshutPassword();
                break;
            case R.id.bt_password_see_again:
                showOrshutPasswordAgain();
                break;
        }
    }

    private void showOrshutPassword() {
        if (showPassword) {
            showPassword = false;
        } else {
            showPassword = true;
        }

        if (showPassword) {
            //如果选中，显示密码
            ivSee.setImageResource(R.mipmap.password_unsee);
            etPassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
        } else {
            //否则隐藏密码
            ivSee.setImageResource(R.mipmap.password_see);
            etPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
        }
    }

    private void showOrshutPasswordAgain() {
        if (showPasswordAgain) {
            showPasswordAgain = false;
        } else {
            showPasswordAgain = true;
        }

        if (showPasswordAgain) {
            //如果选中，显示密码
            ivSeeAgain.setImageResource(R.mipmap.password_unsee);
            etPasswordAgain.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
        } else {
            //否则隐藏密码
            ivSeeAgain.setImageResource(R.mipmap.password_see);
            etPasswordAgain.setTransformationMethod(PasswordTransformationMethod.getInstance());
        }
    }

    public static void launch(Context context, String phone) {
        Intent intent = new Intent(context, ResettingPasswordActivity.class);
        intent.putExtra("phone", phone);
        context.startActivity(intent);
    }


}
