package com.yuntu.mawei.ui.playing.service;

import android.content.Context;
import android.content.IntentFilter;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Handler;
import android.os.Looper;

import com.yuntu.mawei.base.TaipingApplication;
import com.yuntu.mawei.bean.music.Music;
import com.yuntu.mawei.bean.music.PlayModeEnum;
import com.yuntu.mawei.receiver.NoisyAudioStreamReceiver;
import com.yuntu.mawei.storage.db.greendao.MusicDao;
import com.yuntu.mawei.util.Logl;
import com.yuntu.mawei.util.ToastUtil;
import com.yuntu.mawei.util.db.DBManager;
import com.yuntu.mawei.util.play.Preferences;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by tanghao on 2018/1/26.
 */
public class AudioPlayer {
    private static final int STATE_IDLE = 0;
    private static final int STATE_PREPARING = 1;
    private static final int STATE_PLAYING = 2;
    private static final int STATE_PAUSE = 3;

    private static final long TIME_UPDATE = 300L;

    private Context context;
    private AudioFocusManager audioFocusManager;
    private MediaPlayer mediaPlayer;
    private Handler handler;
    private NoisyAudioStreamReceiver noisyReceiver;
    private IntentFilter noisyFilter;
    private List<Music> musicList;
    private final List<OnPlayerEventListener> listeners = new ArrayList<>();

    public int state = STATE_IDLE;

    public static AudioPlayer get() {
        return SingletonHolder.instance;
    }

    private static class SingletonHolder {
        private static AudioPlayer instance = new AudioPlayer();
    }

    private AudioPlayer() {
    }

    public void init(Context context) {
        this.context = context.getApplicationContext();
        musicList = DBManager.get().getMusicDao().queryBuilder().where(MusicDao.Properties.SpareStr2.eq(TaipingApplication.tpApp.phone)).build().list();
        audioFocusManager = new AudioFocusManager(context);
        mediaPlayer = new MediaPlayer();
        handler = new Handler(Looper.getMainLooper());
        noisyReceiver = new NoisyAudioStreamReceiver();
        noisyFilter = new IntentFilter(AudioManager.ACTION_AUDIO_BECOMING_NOISY);
        mediaPlayer.setOnCompletionListener(mp -> next());
        mediaPlayer.setOnPreparedListener(mp -> {
            if (isPreparing()) {
                startPlayer();
            }
        });
        mediaPlayer.setOnBufferingUpdateListener((mp, percent) -> {
            for (OnPlayerEventListener listener : listeners) {
                listener.onBufferingUpdate(percent);
            }
        });
    }

    public void changUserPhone() {
        if (musicList == null) return;
        for (Music music : musicList) {
            music.spareStr2 = TaipingApplication.tpApp.phone;
            DBManager.get().getMusicDao().update(music);
        }
    }


    public void initList() {
        musicList = DBManager.get().getMusicDao().queryBuilder().where(MusicDao.Properties.SpareStr2.eq(TaipingApplication.tpApp.phone)).build().list();
    }


    public void addOnPlayEventListener(OnPlayerEventListener listener) {
        if (!listeners.contains(listener)) {
            listeners.add(listener);
        }
    }

    public void removeOnPlayEventListener(OnPlayerEventListener listener) {
        listeners.remove(listener);
    }

    public void addAndPlay(Music music) {
        if (musicList == null) {
            musicList = new ArrayList<>();
        }
        int position = musicList.indexOf(music);
        Logl.e("是否调用position：" + position + " id  " + music.songId);
        if (position < 0) {
            musicList.add(music);
            DBManager.get().getMusicDao().insert(music);
            position = musicList.size() - 1;
        }
        play(position);
    }

    public void addNoPlay(Music music) {
        if (musicList == null) {
            musicList = new ArrayList<>();
        }
        int position = musicList.indexOf(music);
        Logl.e("是否调用position：" + position + " id  " + music.songId);
        if (position < 0) {
            musicList.add(music);
            try {
                DBManager.get().getMusicDao().insert(music);
            }catch (Exception e){}
            position = musicList.size() - 1;
        }
        noPlay(position);
    }

    public void addAndPlayWithUpdate(Music music) {
        if (musicList == null) {
            musicList = new ArrayList<>();
        }
        int position = musicList.indexOf(music);
        Logl.e("是否调用position：" + position + " id  " + music.songId);
        if (position < 0) {
            musicList.add(music);
            DBManager.get().getMusicDao().insert(music);
            position = musicList.size() - 1;
        }
        play(position, music.path);
    }

    public void addPlayList(List<Music> list) {
        for (Music music : list) {
            addJust(music);
        }
    }

    public void addJust(Music music) {
        if (musicList == null) {
            musicList = new ArrayList<>();
        }
        int position = musicList.indexOf(music);
        if (position < 0) {
            musicList.add(music);
            DBManager.get().getMusicDao().insert(music);
        }
    }

    public void play(int position, String newPath) {
        if (musicList == null || musicList.isEmpty()) {
            return;
        }
        if (position < 0) {
            position = musicList.size() - 1;
        } else if (position >= musicList.size()) {
            position = 0;
        }
        setPlayPosition(position);
        Music music = getPlayMusic();

        try {
            mediaPlayer.reset();
            Logl.e("newPath:" + newPath);
            mediaPlayer.setDataSource(newPath);
            mediaPlayer.prepareAsync();
            state = STATE_PREPARING;
            for (OnPlayerEventListener listener : listeners) {
                listener.onChange(music);
            }
            Notifier.get().showPlay(music);
            MediaSessionManager.get().updateMetaData(music);
            MediaSessionManager.get().updatePlaybackState();
        } catch (IOException e) {
            e.printStackTrace();
            Logl.e("当前歌曲无法播放:" + e.getMessage());
            ToastUtil.showToast("当前歌曲无法播放");
        }
    }

    public void play(int position) {
        if (musicList == null || musicList.isEmpty()) {
            return;
        }
        if (position < 0) {
            position = musicList.size() - 1;
        } else if (position >= musicList.size()) {
            position = 0;
        }
        setPlayPosition(position);
        Music music = getPlayMusic();
        if (music == null) return;
        DBManager.get().getMusicDao().update(music);
        try {
            mediaPlayer.reset();
            Logl.e("歌曲path:" + music.path);
            mediaPlayer.setDataSource(music.getPath());
            mediaPlayer.prepareAsync();
            state = STATE_PREPARING;
            for (OnPlayerEventListener listener : listeners) {
                listener.onChange(music);
            }
            Notifier.get().showPlay(music);
            MediaSessionManager.get().updateMetaData(music);
            MediaSessionManager.get().updatePlaybackState();
        } catch (IOException e) {
            e.printStackTrace();
            Logl.e("当前歌曲无法播放:" + e.getMessage());
            ToastUtil.showToast("当前歌曲无法播放");
        }
    }

    public void noPlay(int position) {
        if (musicList == null || musicList.isEmpty()) {
            return;
        }
        if (position < 0) {
            position = musicList.size() - 1;
        } else if (position >= musicList.size()) {
            position = 0;
        }
        setPlayPosition(position);
        Music music = getPlayMusic();

        try {
            mediaPlayer.reset();
            Logl.e("歌曲path:" + music.path);
//            mediaPlayer.setDataSource(music.getPath());
//            mediaPlayer.prepareAsync();
            state = STATE_PREPARING;
            for (OnPlayerEventListener listener : listeners) {
                listener.onChange(music);
            }
            Notifier.get().showPlay(music);
            MediaSessionManager.get().updateMetaData(music);
            MediaSessionManager.get().updatePlaybackState();
        } catch (Exception e) {
            e.printStackTrace();
            Logl.e("当前歌曲无法播放:" + e.getMessage());
            ToastUtil.showToast("当前歌曲无法播放");
        }
    }


    public void delete(int position) {
        int playPosition = getPlayPosition();
        if (position >= musicList.size()) {
            return;
        }
        Music music = musicList.remove(position);
        try {
            DBManager.get().getMusicDao().delete(music);
        }catch (Exception e){}
        if (musicList.size() == 0) {
            stopPlayer();
            for (OnPlayerEventListener listener : listeners) {
                listener.onChange(getPlayMusic());
            }
            return;
        }
        if (playPosition > position) {
            setPlayPosition(playPosition - 1);
        } else if (playPosition == position) {
            if (isPlaying() || isPreparing()) {
                setPlayPosition(playPosition - 1);
                next();
            } else {
                stopPlayer();
                for (OnPlayerEventListener listener : listeners) {
                    listener.onChange(getPlayMusic());
                }
            }
        }
    }

    public void deleteAll() {
        stopPlayer();
        musicList.clear();
        DBManager.get().getMusicDao().deleteAll();
        for (OnPlayerEventListener listener : listeners) {
            listener.onChange(getPlayMusic());
        }
    }


    public void playPause() {
        if (isPreparing()) {
            stopPlayer();
        } else if (isPlaying()) {
            pausePlayer();
        } else if (isPausing()) {
            startPlayer();
        } else {
            play(getPlayPosition());
        }
    }

    public void startPlayer() {
        if (!isPreparing() && !isPausing()) {
            return;
        }

        if (audioFocusManager.requestAudioFocus()) {
            mediaPlayer.start();
            state = STATE_PLAYING;
            handler.post(mPublishRunnable);
            Notifier.get().showPlay(getPlayMusic());
            MediaSessionManager.get().updatePlaybackState();
            context.registerReceiver(noisyReceiver, noisyFilter);

            for (OnPlayerEventListener listener : listeners) {
                listener.onPlayerStart();
            }
        }
    }

    public void pausePlayer() {
        pausePlayer(true);
    }

    public void pausePlayer(boolean abandonAudioFocus) {
        if (!isPlaying()) {
            return;
        }
        mediaPlayer.pause();
        state = STATE_PAUSE;
        handler.removeCallbacks(mPublishRunnable);
        Notifier.get().showPause(getPlayMusic());
        MediaSessionManager.get().updatePlaybackState();
        context.unregisterReceiver(noisyReceiver);
        if (abandonAudioFocus) {
            audioFocusManager.abandonAudioFocus();
        }

        for (OnPlayerEventListener listener : listeners) {
            listener.onPlayerPause();
        }
    }

    public void stopPlayer() {
        if (isIdle()) {
            return;
        }

        pausePlayer();
        mediaPlayer.reset();
        state = STATE_IDLE;
    }

    public void next() {
        if (musicList == null || musicList.isEmpty()) {
            return;
        }

        PlayModeEnum mode = PlayModeEnum.valueOf(Preferences.getPlayMode());
        switch (mode) {
            case SHUFFLE:
                play(new Random().nextInt(musicList.size()));
                break;
            case SINGLE:
                play(getPlayPosition());
                break;
            case LOOP:
            default:
                play(getPlayPosition() + 1);
                break;
        }
    }

    public void prev() {
        if (musicList == null || musicList.isEmpty()) {
            return;
        }

        PlayModeEnum mode = PlayModeEnum.valueOf(Preferences.getPlayMode());
        switch (mode) {
            case SHUFFLE:
                play(new Random().nextInt(musicList.size()));
                break;
            case SINGLE:
                play(getPlayPosition());
                break;
            case LOOP:
            default:
                play(getPlayPosition() - 1);
                break;
        }
    }

    /**
     * 跳转到指定的时间位置
     *
     * @param msec 时间
     */
    public void seekTo(int msec) {
        if (isPlaying() || isPausing()) {
            mediaPlayer.seekTo(msec);
            MediaSessionManager.get().updatePlaybackState();
            for (OnPlayerEventListener listener : listeners) {
                listener.onPublish(msec);
            }
        }
    }

    private Runnable mPublishRunnable = new Runnable() {
        @Override
        public void run() {
            if (isPlaying()) {
                for (OnPlayerEventListener listener : listeners) {
                    listener.onPublish(mediaPlayer.getCurrentPosition());
                }
            }
            handler.postDelayed(this, TIME_UPDATE);
        }
    };

    public int getAudioSessionId() {
        return mediaPlayer.getAudioSessionId();
    }

    public long getAudioPosition() {
        if (isPlaying() || isPausing()) {
            return mediaPlayer.getCurrentPosition();
        } else {
            return 0;
        }
    }

    public Music getPlayMusic() {
        if (musicList == null || musicList.isEmpty()) {
            return null;
        }
        return musicList.get(getPlayPosition());
    }

    public MediaPlayer getMediaPlayer() {
        return mediaPlayer;
    }

    public List<Music> getMusicList() {
        return musicList;
    }

    public boolean isPlaying() {
        return state == STATE_PLAYING;
    }

    public boolean isPausing() {
        return state == STATE_PAUSE;
    }

    public boolean isPreparing() {
        return state == STATE_PREPARING;
    }

    public boolean isIdle() {
        return state == STATE_IDLE;
    }

    public int getPlayPosition() {
        int position = Preferences.getPlayPosition();
        if (position < 0 || position >= musicList.size()) {
            position = 0;
            Preferences.savePlayPosition(position);
        }
        return position;
    }

    private void setPlayPosition(int position) {
        Preferences.savePlayPosition(position);
    }
}
