package com.yuntu.mawei.ui.homepage.rank;

import android.content.Context;
import android.view.View;

import com.yuntu.mawei.R;
import com.yuntu.mawei.bean.house.MusicRankBean;
import com.yuntu.mawei.ui.homepage.singer.detail.SingerDetailActivity;
import com.yuntu.mawei.ui.house.detail.SingerListActivity;
import com.yuntu.mawei.ui.house.detail.SongListActivity;
import com.yuntu.mawei.util.GlideHelper;
import com.yuntu.mawei.view.adapter.base.BaseQuickAdapter;
import com.yuntu.mawei.view.adapter.base.BaseViewHolder;

public class SongRankAdapter extends BaseQuickAdapter<MusicRankBean> {

    private int type; // 0 for 歌曲，1 for 歌手

    public SongRankAdapter(Context context, int type) {
        super(context);
        this.type = type;
    }

    @Override
    protected int attachLayoutRes() {
        return R.layout.adapter_music_house;
    }

    @Override
    protected void convert(BaseViewHolder holder, MusicRankBean item) {
        GlideHelper.loadListPic(mContext, item.rankImage, holder.getView(R.id.iv_cover));
        holder.setVisible(R.id.ll_1, false);
        holder.setVisible(R.id.ll_2, false);
        holder.setVisible(R.id.ll_3, false);
        if (item.songList == null && item.singerList == null) {
            return;
        }
        if (type == 0) {
            holder.getConvertView().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    SongListActivity.launch(mContext, item.rankImage,item.rankName, item.rankId, true);
                }
            });
            switch (item.songList.size()) {
                case 3:
                    holder.setVisible(R.id.ll_3, true);
                    if (type == 0) {
                        holder.setText(R.id.tv_title_3, item.songList.get(2).songName);
                        if (item.songList.get(2).singer != null) {
                            holder.setText(R.id.tv_artist_3, "- " + item.songList.get(2).singer.singerName);
                        }
                    } else {
                        holder.setText(R.id.tv_title_3, item.singerList.get(2).singerName);
                        holder.setText(R.id.tv_artist_3, "");
                    }
                case 2:
                    holder.setVisible(R.id.ll_2, true);
                    if (type == 0) {
                        holder.setText(R.id.tv_title_2, item.songList.get(1).songName);
                        if (item.songList.get(1).singer != null) {
                            holder.setText(R.id.tv_artist_2, "- " + item.songList.get(1).singer.singerName);
                        }
                    } else {
                        holder.setText(R.id.tv_title_2, item.singerList.get(1).singerName);
                        holder.setText(R.id.tv_artist_2, "");
                    }
                case 1:
                    holder.setVisible(R.id.ll_1, true);
                    if (type == 0) {
                        holder.setText(R.id.tv_title_1, item.songList.get(0).songName);
                        if (item.songList.get(0).singer != null) {
                            holder.setText(R.id.tv_artist_1, "- " + item.songList.get(0).singer.singerName);
                        }
                    } else {
                        holder.setText(R.id.tv_title_1, item.singerList.get(0).singerName);
                        holder.setText(R.id.tv_artist_1, "");
                    }
                    break;

            }
        } else {
            holder.getConvertView().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    SingerListActivity.launch(mContext, item.rankImage, item.rankId, item.rankName);
                }
            });
            switch (item.singerList.size()) {
                case 3:
                    holder.setVisible(R.id.ll_3, true);
                    if (type == 0) {
                        holder.setText(R.id.tv_title_3, item.songList.get(2).songName);
                        if (item.songList.get(2).singer != null) {
                            holder.setText(R.id.tv_artist_3, "- " + item.songList.get(2).singer.singerName);
                        }
                    } else {
                        holder.setText(R.id.tv_title_3, item.singerList.get(2).singerName);
                        holder.setText(R.id.tv_artist_3, "");
                    }
                case 2:
                    holder.setVisible(R.id.ll_2, true);
                    if (type == 0) {
                        holder.setText(R.id.tv_title_2, item.songList.get(1).songName);
                        if (item.songList.get(1).singer != null) {
                            holder.setText(R.id.tv_artist_2, "- " + item.songList.get(1).singer.singerName);
                        }
                    } else {
                        holder.setText(R.id.tv_title_2, item.singerList.get(1).singerName);
                        holder.setText(R.id.tv_artist_2, "");
                    }
                case 1:
                    holder.setVisible(R.id.ll_1, true);
                    if (type == 0) {
                        holder.setText(R.id.tv_title_1, item.songList.get(0).songName);
                        if (item.songList.get(0).singer != null) {
                            holder.setText(R.id.tv_artist_1, "- " + item.songList.get(0).singer.singerName);
                        }
                    } else {
                        holder.setText(R.id.tv_title_1, item.singerList.get(0).singerName);
                        holder.setText(R.id.tv_artist_1, "");
                    }
                    break;

            }
        }
    }
}
