package com.yuntu.mawei.ui.homepage.singer;

import com.yuntu.mawei.base.home.BaseListFragment;
import com.yuntu.mawei.base.home.BaseListFragmentPresenter;
import com.yuntu.mawei.bean.base.ResponseBean;
import com.yuntu.mawei.bean.rx.SuccessSubscriber;
import com.yuntu.mawei.bean.song.SingerBean;
import com.yuntu.mawei.util.RxUtils;
import com.yuntu.mawei.util.http.HttpUtil;

import java.util.List;

public class SingerRecentPresenter extends BaseListFragmentPresenter {

    int type;
    boolean isYici = false;
    public SingerRecentPresenter(BaseListFragment mView, int type) {
        super(mView);
        this.type = type;
    }

    @Override
    public void getData(boolean isPullRefresh) {
        if(!isYici){
            isYici = true;
            return;
        }
        HttpUtil.getInstance().getApiService()
                .getRecentSingerList(1, 1000)
                .compose(RxUtils.rxSchedulerHelper())
                .subscribe(new SuccessSubscriber<ResponseBean<List<SingerBean>>>() {
                    @Override
                    public void onSuccess(ResponseBean<List<SingerBean>> listResponseBean) {
                        mView.loadData(listResponseBean.getResult());
                    }

                    @Override
                    public void onFailed(String str) {

                    }
                });

    }

    @Override
    public void getMoreData() {
        mView.adapter.noMoreData();
        mView.adapter.loadComplete();
    }
}
