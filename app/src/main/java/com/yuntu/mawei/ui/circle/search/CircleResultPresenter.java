package com.yuntu.mawei.ui.circle.search;

import com.yuntu.mawei.base.home.BaseListActivityPresenter;
import com.yuntu.mawei.bean.base.ResponseBean;
import com.yuntu.mawei.bean.circle.CircleBean;
import com.yuntu.mawei.bean.rx.SuccessSubscriber;
import com.yuntu.mawei.util.RxUtils;
import com.yuntu.mawei.util.http.HttpUtil;

import java.util.List;

public class CircleResultPresenter extends BaseListActivityPresenter {

    int page;

    CircleResultActivity fragment;
    public String str;

    public CircleResultPresenter(CircleResultActivity mView, String str) {
        super(mView);
        fragment = mView;
        this.str = str;
    }

    @Override
    public void getData(boolean isPullRefresh) {
        HttpUtil.getInstance().getApiService().getCircleList(1, 10, str)
                .compose(RxUtils.rxSchedulerHelper())
                .subscribe(new SuccessSubscriber<ResponseBean<List<CircleBean>>>() {
                    @Override
                    public void onSuccess(ResponseBean<List<CircleBean>> listResponseBean) {
                        if (listResponseBean.getResult() == null) return;
                        mView.loadData(listResponseBean.getResult());
                        fragment.finishRefresh();
                        page = 2;
                    }

                    @Override
                    public void onFailed(String str) {
                    }
                });
    }

    @Override
    public void getMoreData() {
        HttpUtil.getInstance().getApiService().getCircleList(page, 10,str)
                .compose(RxUtils.rxSchedulerHelper())
                .subscribe(new SuccessSubscriber<ResponseBean<List<CircleBean>>>() {
                    @Override
                    public void onSuccess(ResponseBean<List<CircleBean>> listResponseBean) {
                        if (listResponseBean.getResult() == null) return;
                        mView.loadMoreData(listResponseBean.getResult());
                        page++;
                    }

                    @Override
                    public void onFailed(String str) {
                    }
                });
    }
}
