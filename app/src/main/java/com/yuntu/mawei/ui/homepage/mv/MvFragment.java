package com.yuntu.mawei.ui.homepage.mv;

import android.os.Bundle;

import com.yuntu.mawei.R;
import com.yuntu.mawei.base.home.BaseListFragment;
import com.yuntu.mawei.base.home.BaseListFragmentPresenter;
import com.yuntu.mawei.view.adapter.base.BaseQuickAdapter;

public class MvFragment extends BaseListFragment {

    public static MvFragment newInstance(int type) {
        MvFragment fragment = new MvFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("type", type);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    protected void initViews() {
    }

    @Override
    protected int attachLayoutRes() {
        return R.layout.recyclerview_just;
    }

    @Override
    protected BaseQuickAdapter attachAdapter() {
        return new MvAdapter(getActivity());
    }

    @Override
    protected BaseListFragmentPresenter attachPresenter() {
        return new MvPresenter(this, getArguments().getInt("type"));
    }
}
