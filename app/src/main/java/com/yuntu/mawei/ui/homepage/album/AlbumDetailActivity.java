package com.yuntu.mawei.ui.homepage.album;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.yuntu.mawei.R;
import com.yuntu.mawei.base.home.BaseActivity;
import com.yuntu.mawei.bean.song.AlbumBean;
import com.yuntu.mawei.ui.homepage.singer.detail.SingerSongListFragment;
import com.yuntu.mawei.util.GlideHelper;
import com.yuntu.mawei.util.music.MusicUtil;
import com.yuntu.mawei.widget.ColorStyleTransitionPagerTitleView;
import net.lucode.hackware.magicindicator.MagicIndicator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.CommonNavigator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.CommonNavigatorAdapter;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.IPagerIndicator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.IPagerTitleView;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.indicators.LinePagerIndicator;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindColor;
import butterknife.BindView;

/**
 * Created by tanghao on 2019/2/25
 */
public class AlbumDetailActivity extends BaseActivity {

    @BindView(R.id.indicator)
    MagicIndicator indicator;
    @BindView(R.id.view_pager)
    ViewPager viewPager;
    @BindView(R.id.iv_singer)
    ImageView ivSinger;
    @BindView(R.id.tv_singer)
    TextView tvSinger;
    @BindView(R.id.tv_singer_intro)
    TextView tvIntro;
    @BindView(R.id.iv_song_top)
    ImageView ivBg;

    @BindColor(R.color.mall_black)
    int colorBlack;
    @BindColor(R.color.mawei_red)
    int colorBlue;
    AlbumBean albumBean;

    private List<Fragment> fragments;
    private FragmentPagerAdapter mAdapter;


    List<String> finalTitles = Arrays.asList("歌曲", "专辑信息");
    public int songNum;

    @Override
    public int attachLayout() {
        return R.layout.activity_album;
    }

    @Override
    public void init() {
        initTitle("");
        albumBean = (AlbumBean) getIntent().getSerializableExtra("albumBean");
        initIndicator();
        initFragments();
        initAlbumView();
    }

    private void initAlbumView() {
        GlideHelper.loadListPic(this, albumBean.albumAvatar, ivSinger);
        tvSinger.setText("专辑：" + albumBean.albumName);
        tvIntro.setText("发行时间：" + albumBean.createTime);
        MusicUtil.initBg(this, albumBean.albumAvatar, ivBg);
    }

    private void initFragments() {
        fragments = new ArrayList<>();
        // 0 for song,1 for album,2 for mv
        fragments.add(AlbumSongListFragment.newInstance(albumBean));
        fragments.add(AlbumInfoFragment.newInstance(albumBean));
        mAdapter = new FragmentPagerAdapter(getSupportFragmentManager()) {
            @Override
            public int getCount() {
                return fragments.size();
            }

            @Override
            public Fragment getItem(int position) {
                return fragments.get(position);
            }

            @Override
            public long getItemId(int position) {
                return position;
            }
        };
        viewPager.setAdapter(mAdapter);
        viewPager.setOffscreenPageLimit(3);
        viewPager.addOnPageChangeListener(onPageChangeListener);
    }

    public void initIndicator() {
        CommonNavigator commonNavigator = new CommonNavigator(this);
        commonNavigator.setAdjustMode(true);
        commonNavigator.setAdapter(new CommonNavigatorAdapter() {

            @Override
            public int getCount() {
                return finalTitles.size();
            }

            @Override
            public IPagerTitleView getTitleView(Context context, final int index) {
                ColorStyleTransitionPagerTitleView colorTransitionPagerTitleView = new
                        ColorStyleTransitionPagerTitleView(context);
                colorTransitionPagerTitleView.setSelectedTypeface(Typeface.defaultFromStyle(Typeface.BOLD));
                colorTransitionPagerTitleView.setNormalTypeface(Typeface.defaultFromStyle(Typeface.NORMAL));
                colorTransitionPagerTitleView.setLayoutParams(new ViewGroup.LayoutParams
                        (ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
                colorTransitionPagerTitleView.setNormalColor(colorBlack);
                colorTransitionPagerTitleView.setTextSize(14);
                colorTransitionPagerTitleView.setSelectedColor(colorBlue);
                if(index==0) {
                    colorTransitionPagerTitleView.setText(finalTitles.get(index) + "/" + songNum);
                }else{
                    colorTransitionPagerTitleView.setText(finalTitles.get(index));
                }

                colorTransitionPagerTitleView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        viewPager.setCurrentItem(index);
                    }
                });
                return colorTransitionPagerTitleView;
            }

            @Override
            public IPagerIndicator getIndicator(Context context) {
                LinePagerIndicator indicator = new LinePagerIndicator(context);
                indicator.setMode(LinePagerIndicator.MODE_WRAP_CONTENT);
                indicator.setColors(colorBlue);
                return indicator;
            }

            @Override
            public float getTitleWeight(Context context, int index) {
                return 1.0f;
            }
        });

        indicator.setNavigator(commonNavigator);

    }

    ViewPager.OnPageChangeListener onPageChangeListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            indicator.onPageScrolled(position, positionOffset, positionOffsetPixels);
        }

        @Override
        public void onPageSelected(int position) {
            indicator.onPageSelected(position);
        }

        @Override
        public void onPageScrollStateChanged(int state) {
            indicator.onPageScrollStateChanged(state);
        }
    };

    public static void launch(Context context, AlbumBean bean) {
        Intent intent = new Intent(context, AlbumDetailActivity.class);
        intent.putExtra("albumBean", bean);
        context.startActivity(intent);
    }
}
