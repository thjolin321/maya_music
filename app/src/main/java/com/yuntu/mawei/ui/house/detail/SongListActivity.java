package com.yuntu.mawei.ui.house.detail;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.GridLayoutManager;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.yuntu.mawei.R;
import com.yuntu.mawei.base.home.BaseListActivity;
import com.yuntu.mawei.base.home.BaseListActivityPresenter;
import com.yuntu.mawei.bean.song.SongBean;
import com.yuntu.mawei.ui.homepage.rank.RankSimpleAdapter;
import com.yuntu.mawei.ui.homepage.rank.RankSongListPresenter;
import com.yuntu.mawei.util.GlideHelper;
import com.yuntu.mawei.util.Logl;
import com.yuntu.mawei.view.adapter.base.BaseQuickAdapter;
import com.yuntu.mawei.view.adapter.helper.RecyclerViewHelper;

import butterknife.BindView;

public class SongListActivity extends BaseListActivity {

    @BindView(R.id.iv_song_top)
    ImageView ivSongTop;
    @BindView(R.id.tv_song_total)
    TextView tvSongTotal;

    String typeImage;
    int songTypeId;
    boolean isRank;

    @Override
    protected void initViews() {
        initTypeData();
    }

    private void initTypeData() {
        typeImage = getIntent().getStringExtra("typeImage");
        songTypeId = getIntent().getIntExtra("songTypeId", 0);
        isRank = getIntent().getBooleanExtra("isNeedShowNum", false);
        initTitle(getIntent().getStringExtra("titleName"));
        boolean showViewPiliang = getIntent().getBooleanExtra("showViewPiliang", false);
        if (isRank) {
            tvSongTotal.setVisibility(View.GONE);
        }
        GlideHelper.loadListPic(this, typeImage, ivSongTop);
    }


    @Override
    protected BaseQuickAdapter attachAdapter() {
        if (isRank) {
            return new RankSimpleAdapter(this, 0);
        }
        return new SongListAdapter(this);
    }

    @Override
    protected BaseListActivityPresenter attachPresenter() {
        if (isRank) {
            return new RankSongListPresenter(this, songTypeId);
        }
        return new SongListPresenter(this, songTypeId, getIntent().getIntExtra("isFree", 0));
    }

    @Override
    protected int attachLayoutRes() {
        return R.layout.activity_song_list;
    }

    @Override
    public void initSwipeRefresh() {
        Logl.e("initSwipeRefresh调用了的啊");
        if (isRank) {
            RecyclerViewHelper.initRecyclerViewV(this, recyclerView, adapter);
        } else {
            RecyclerViewHelper.initRecyclerViewG(this, recyclerView, adapter, 3);
            GridLayoutManager manager = (GridLayoutManager) recyclerView.getLayoutManager();
            manager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
                @Override
                public int getSpanSize(int position) {
                    if (adapter.getItemViewType(position) == SongBean.SONG_TITLE_ITEM
                            || adapter.getItemViewType(position) == BaseQuickAdapter.LOADING_VIEW) {
                        return 3;
                    }
                    return 1;
                }
            });
        }
        updateViews(false);
    }

    public void initTotalNum(String num) {
        tvSongTotal.setText(num);
    }

    public static void launch(Context context, String typeImage, String titleName, int songTypeId, boolean isNeedShowNum) {
        Intent intent = new Intent(context, SongListActivity.class);
        intent.putExtra("typeImage", typeImage);
        intent.putExtra("titleName", titleName);
        intent.putExtra("songTypeId", songTypeId);
        intent.putExtra("isNeedShowNum", isNeedShowNum);
        context.startActivity(intent);
    }

    public static void launch(Context context, String typeImage, String titleName, int songTypeId, int isFree, boolean isNeedShowNum, boolean showViewPiliang) {
        Intent intent = new Intent(context, SongListActivity.class);
        intent.putExtra("typeImage", typeImage);
        intent.putExtra("titleName", titleName);
        intent.putExtra("songTypeId", songTypeId);
        intent.putExtra("isFree", isFree);
        intent.putExtra("isNeedShowNum", isNeedShowNum);
        intent.putExtra("showViewPiliang", showViewPiliang);
        context.startActivity(intent);
    }


}
