package com.yuntu.mawei.ui.homepage.rank;

import com.yuntu.mawei.base.home.BaseListActivity;
import com.yuntu.mawei.base.home.BaseListActivityPresenter;
import com.yuntu.mawei.bean.base.ResponseBean;
import com.yuntu.mawei.bean.house.MusicHouseBean;
import com.yuntu.mawei.bean.rx.SuccessSubscriber;
import com.yuntu.mawei.util.RxUtils;
import com.yuntu.mawei.util.ToastUtil;
import com.yuntu.mawei.util.http.HttpUtil;
import com.yuntu.mawei.util.music.MusicUtil;

/**
 * Created by tanghao on 2019/1/25
 */
public class RankSongListPresenter extends BaseListActivityPresenter {

    int page;
    long songTypeId;

    public RankSongListPresenter(BaseListActivity mView, long songTypeId) {
        super(mView);
        this.songTypeId = songTypeId;
    }

    @Override
    public void getData(boolean isPullRefresh) {
        HttpUtil.getInstance().getApiService().getRankSongList(songTypeId, 1, 20)
                .compose(RxUtils.rxSchedulerHelper()).subscribe(new SuccessSubscriber<ResponseBean<MusicHouseBean>>() {
            @Override
            public void onSuccess(ResponseBean<MusicHouseBean> musicHouseBeanResponseBean) {
                if (musicHouseBeanResponseBean.getResult() == null) return;
                mView.loadData(musicHouseBeanResponseBean.getResult().songList);
                page = 2;
            }

            @Override
            public void onFailed(String str) {
                ToastUtil.showToast(str);
            }
        });
    }

    @Override
    public void getMoreData() {
        HttpUtil.getInstance().getApiService().getRankSongList(songTypeId, page, 20)
                .compose(RxUtils.rxSchedulerHelper()).subscribe(new SuccessSubscriber<ResponseBean<MusicHouseBean>>() {
            @Override
            public void onSuccess(ResponseBean<MusicHouseBean> musicHouseBeanResponseBean) {
                if (musicHouseBeanResponseBean.getResult() == null) return;
                mView.loadMoreData(musicHouseBeanResponseBean.getResult().songList);
                page++;
            }

            @Override
            public void onFailed(String str) {
            }
        });
    }
}
