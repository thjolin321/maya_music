package com.yuntu.mawei.ui.mine.money;

import android.content.Context;

import com.yuntu.mawei.R;
import com.yuntu.mawei.bean.money.XiaofeiBean;
import com.yuntu.mawei.view.adapter.base.BaseQuickAdapter;
import com.yuntu.mawei.view.adapter.base.BaseViewHolder;

import java.text.SimpleDateFormat;

public class XiaofeiAdapter extends BaseQuickAdapter<XiaofeiBean> {

    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");

    public XiaofeiAdapter(Context context) {
        super(context);
    }

    @Override
    protected int attachLayoutRes() {
        return R.layout.adapter_xiaofei_record;
    }

    @Override
    protected void convert(BaseViewHolder holder, XiaofeiBean item) {
        holder.setText(R.id.tv_song, item.orderName);
        switch (item.payType) {
            case 0:
                holder.setText(R.id.tv_pay_type, "微信支付");
                break;
            case 1:
                holder.setText(R.id.tv_pay_type, "支付宝支付");
                break;
            case 2:
                holder.setText(R.id.tv_pay_type, "余额支付");
                break;
        }
        holder.setText(R.id.tv_num, item.orderValue + "");
        holder.setText(R.id.tv_time, simpleDateFormat.format(item.payTime));
    }
}
