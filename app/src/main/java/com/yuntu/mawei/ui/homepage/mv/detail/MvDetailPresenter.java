package com.yuntu.mawei.ui.homepage.mv.detail;

import com.yuntu.mawei.base.home.BaseListActivity;
import com.yuntu.mawei.base.home.BaseListActivityPresenter;
import com.yuntu.mawei.bean.base.ResponseBean;
import com.yuntu.mawei.bean.rx.SuccessSubscriber;
import com.yuntu.mawei.util.RxUtils;
import com.yuntu.mawei.util.http.HttpUtil;

/**
 * Created by tanghao on 2019/2/19
 */
public class MvDetailPresenter extends BaseListActivityPresenter {

    MvDetailActivity mViewLocal;
    long id;
    public MvDetailPresenter(MvDetailActivity mView,long id) {
        super(mView);
        mViewLocal = mView;
        this.id = id;
    }

    @Override
    public void getData(boolean isPullRefresh) {
        HttpUtil.getInstance().getApiService().getMvDetail(id)
                .compose(RxUtils.rxSchedulerHelper()).subscribe(new SuccessSubscriber<ResponseBean<MvDetailBean>>() {
            @Override
            public void onSuccess(ResponseBean<MvDetailBean> mvDetailBeanResponseBean) {
                mViewLocal.initHeaderView(mvDetailBeanResponseBean.getResult());
            }

            @Override
            public void onFailed(String str) {
            }
        });
    }

    @Override
    public void getMoreData() {
        mView.adapter.noMoreData();
        mView.adapter.loadComplete();
    }
}
