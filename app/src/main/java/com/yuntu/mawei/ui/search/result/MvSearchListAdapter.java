package com.yuntu.mawei.ui.search.result;

import android.content.Context;

import com.yuntu.mawei.R;
import com.yuntu.mawei.bean.song.SongBean;
import com.yuntu.mawei.ui.homepage.mv.detail.MvDetailActivity;
import com.yuntu.mawei.ui.playing.service.AudioPlayer;
import com.yuntu.mawei.ui.three.piliang.PiliangActivity;
import com.yuntu.mawei.util.GlideHelper;
import com.yuntu.mawei.util.music.MusicUtil;
import com.yuntu.mawei.view.adapter.base.BaseMultiItemQuickAdapter;
import com.yuntu.mawei.view.adapter.base.BaseViewHolder;

public class MvSearchListAdapter extends BaseMultiItemQuickAdapter<SongBean> {

    public MvSearchListAdapter(Context context) {
        super(context);
    }

    @Override
    protected void attachItemType() {
        addItemType(SongBean.SONG_ITEM, R.layout.adapter_mv_simple_list);
    }

    @Override
    protected void convert(BaseViewHolder holder, SongBean item) {
        switch (item.itemType) {
            case SongBean.SONG_ITEM:
                if (item.musicLocal == null) {
                    GlideHelper.loadListPic(mContext, item.songAvatar, holder.getView(R.id.iv_song));
                    holder.setText(R.id.tv_song, item.songName);
                    if (item.singer != null && item.album != null) {
                        holder.setText(R.id.tv_artist_and_album, item.singer.singerName + " - 《" + item.albumIntroduction + "》");
                    }
                    holder.getConvertView().setOnClickListener(v -> {
                        if (item.songType == 2) {
                            MvDetailActivity.launch(mContext, item);
                        } else {
                            MusicUtil.playMusic(mContext, item);
                        }
                    });
                } else {
                    GlideHelper.loadListPic(mContext, item.musicLocal.coverPath, holder.getView(R.id.iv_song));
                    holder.setText(R.id.tv_song, item.musicLocal.title);
                    if (item.singer != null && item.album != null) {
                        holder.setText(R.id.tv_artist_and_album, item.musicLocal.title + " - 《" + item.musicLocal.album + "》");
                    }
                    holder.getConvertView().setOnClickListener(v -> {
                        if (item.musicLocal.songType == 2) {
                            MvDetailActivity.launch(mContext, item);
                        } else {
                            AudioPlayer.get().addAndPlayWithUpdate(item.musicLocal);
                        }
                    });
                }
                break;
        }
    }
}
