package com.yuntu.mawei.ui.playing;

import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.media.AudioManager;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.hwangjr.rxbus.RxBus;
import com.yuntu.mawei.R;
import com.yuntu.mawei.base.TaipingApplication;
import com.yuntu.mawei.base.home.BaseActivity;
import com.yuntu.mawei.bean.base.ResponseBean;
import com.yuntu.mawei.bean.executor.DownloadOnlineMusic;
import com.yuntu.mawei.bean.executor.SearchLrc;
import com.yuntu.mawei.bean.music.Music;
import com.yuntu.mawei.bean.music.OnlineMusic;
import com.yuntu.mawei.bean.music.PlayModeEnum;
import com.yuntu.mawei.bean.rx.SuccessSubscriber;
import com.yuntu.mawei.bean.rx.SuccessWithCodeSubscriber;
import com.yuntu.mawei.bean.song.HaveCollectBean;
import com.yuntu.mawei.constant.Actions;
import com.yuntu.mawei.storage.db.greendao.MusicLocalDao;
import com.yuntu.mawei.ui.login.LoginActivity;
import com.yuntu.mawei.ui.playing.service.AudioPlayer;
import com.yuntu.mawei.ui.playing.service.OnPlayerEventListener;
import com.yuntu.mawei.util.BigDeUtil;
import com.yuntu.mawei.util.FastBlurUtil;
import com.yuntu.mawei.util.GlideHelper;
import com.yuntu.mawei.util.Logl;
import com.yuntu.mawei.util.RxUtils;
import com.yuntu.mawei.util.SystemUtils;
import com.yuntu.mawei.util.ToastUtil;
import com.yuntu.mawei.util.db.DBManager;
import com.yuntu.mawei.util.http.HttpUtil;
import com.yuntu.mawei.util.music.MusicUtil;
import com.yuntu.mawei.util.play.CoverLoader;
import com.yuntu.mawei.util.play.FileUtils;
import com.yuntu.mawei.util.play.Preferences;
import com.yuntu.mawei.view.ConfirmDialog;
import com.yuntu.mawei.ui.playing.gift.GiftDialog;
import com.yuntu.mawei.view.PlayListDialog;
import com.yuntu.mawei.view.ShareDialog;
import com.yuntu.mawei.view.SureDialog;
import com.yuntu.mawei.widget.IndicatorLayout;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import jp.wasabeef.glide.transformations.BlurTransformation;
import me.wcy.lrcview.LrcView;

/**
 * 正在播放界面
 * Created by wcy on 2015/11/27.
 */
public class PlayActivity extends BaseActivity implements View.OnClickListener,
        ViewPager.OnPageChangeListener, SeekBar.OnSeekBarChangeListener, OnPlayerEventListener,
        LrcView.OnPlayClickListener {

    protected Handler handler;
    @BindView(R.id.ll_content)
    public LinearLayout llContent;
    @BindView(R.id.iv_play_page_bg)
    public ImageView ivPlayingBg;
    @BindView(R.id.iv_back)
    public ImageView ivBack;
    @BindView(R.id.tv_title)
    public TextView tvTitle;
    @BindView(R.id.tv_artist)
    public TextView tvArtist;
    @BindView(R.id.vp_play_page)
    public ViewPager vpPlay;
    @BindView(R.id.il_indicator)
    public IndicatorLayout ilIndicator;
    @BindView(R.id.sb_progress)
    public SeekBar sbProgress;
    @BindView(R.id.tv_current_time)
    public TextView tvCurrentTime;
    @BindView(R.id.tv_total_time)
    public TextView tvTotalTime;
    @BindView(R.id.iv_mode)
    public ImageView ivMode;
    @BindView(R.id.iv_play)
    public ImageView ivPlay;
    @BindView(R.id.iv_next)
    public ImageView ivNext;
    @BindView(R.id.iv_prev)
    public ImageView ivPrev;
    @BindView(R.id.bt_gift)
    public ImageView ivGift;
    @BindView(R.id.bt_collect)
    public ImageView ivCollect;
    @BindView(R.id.bt_download)
    public ImageView ivDownload;
    @BindView(R.id.bt_share)
    public ImageView ivShare;
    public ImageView mAlbumCoverView;
    public TextView tvIntroduce;
    public LrcView mLrcViewSingle;
    public AudioManager mAudioManager;
    public List<View> mViewPagerContent;
    public int mLastProgress;
    public boolean isDraggingProgress;
    private PlayDownloadReceiver downloadReceiver;

    private Music currentMusic;
    private boolean isDownload;
    private boolean isNeedDelete;
    private ConfirmDialog confirmDialog;


    @Override
    public int attachLayout() {
        return R.layout.fragment_play;
    }

    @Override
    public void init() {
        handler = new Handler();
        RxBus.get().register(this);
        initViewPager();
        setListener();
        ilIndicator.create(mViewPagerContent.size());
        initPlayMode();
        onChangeImpl(AudioPlayer.get().getPlayMusic());
        AudioPlayer.get().addOnPlayEventListener(this);
        downloadReceiver = new PlayDownloadReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.intent.action.DOWNLOAD_COMPLETE");
        registerReceiver(downloadReceiver, intentFilter);
    }

    @Override
    public void onResume() {
        super.onResume();
        IntentFilter filter = new IntentFilter(Actions.VOLUME_CHANGED_ACTION);
        registerReceiver(mVolumeReceiver, filter);
    }

    protected void setListener() {
        ivBack.setOnClickListener(this);
        ivMode.setOnClickListener(this);
        ivPlay.setOnClickListener(this);
        ivPrev.setOnClickListener(this);
        ivNext.setOnClickListener(this);
        findViewById(R.id.iv_play_list).setOnClickListener(this);
        sbProgress.setOnSeekBarChangeListener(this);
        vpPlay.addOnPageChangeListener(this);
    }


    public void initViewPager() {
        View coverView = LayoutInflater.from(this).inflate(R.layout.fragment_play_page_cover, null);
        View lrcView = LayoutInflater.from(this).inflate(R.layout.fragment_play_page_lrc, null);
        tvIntroduce = lrcView.findViewById(R.id.tv_album);
        mAlbumCoverView = coverView.findViewById(R.id.iv_song_bg);
        mLrcViewSingle = coverView.findViewById(R.id.lrc_view_single);
        initVolume();
        mViewPagerContent = new ArrayList<>(2);
        mViewPagerContent.add(coverView);
        mViewPagerContent.add(lrcView);
        vpPlay.setAdapter(new PlayPagerAdapter(mViewPagerContent));
    }

    public void initVolume() {
        mAudioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
    }

    public void initPlayMode() {
        int mode = Preferences.getPlayMode();
        ivMode.setImageLevel(mode);
    }

    @Override
    public void onChange(Music music) {
        onChangeImpl(music);
    }

    @Override
    public void onPlayerStart() {
        ivPlay.setSelected(true);
        GlideHelper.loadPicPlayPage(this, AudioPlayer.get().getPlayMusic().getCoverPath(), mAlbumCoverView);
    }

    @Override
    public void onPlayerPause() {
        ivPlay.setSelected(false);
    }

    /**
     * 更新播放进度
     */

    int progressNow = 0;

    @Override
    public void onPublish(int progress) {
        if (!isDraggingProgress) {
            sbProgress.setProgress(progress);
        }

        if (mLrcViewSingle.hasLrc()) {
            mLrcViewSingle.updateTime(progress);
        }

        progress = progress / 1000 * 1000;
        if (progress < 4000) return;
        if (progress == progressNow) return;
        progressNow = progress;
        Logl.e("progress now: " + progress);
        if (progress % 4000 == 0) {
            if (bgIndex < (bgs.length - 1)) {
                bgIndex++;
            } else {
                bgIndex = 0;
            }
            Logl.e("index: " + bgIndex);
            Glide.with(this)
                    .load(bgs[bgIndex])
                    .bitmapTransform(new BlurTransformation(this, 14, 3))
                    .into(ivPlayingBg);
        }

        Logl.e("progress:" + progress);
    }

    @Override
    public void onBufferingUpdate(int percent) {
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.iv_mode:
                switchPlayMode();
                break;
            case R.id.iv_play:
                play();
                break;
            case R.id.iv_next:
                next();
                break;
            case R.id.iv_prev:
                prev();
                break;
            case R.id.iv_play_list:
                PlayListDialog.launch(PlayActivity.this);
                break;
        }
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
    }

    @Override
    public void onPageSelected(int position) {
        ilIndicator.setCurrent(position);
    }

    @Override
    public void onPageScrollStateChanged(int state) {
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        if (seekBar == sbProgress) {
            if (Math.abs(progress - mLastProgress) >= DateUtils.SECOND_IN_MILLIS) {
                tvCurrentTime.setText(formatTime(progress));
                mLastProgress = progress;
            }
        }
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
        if (seekBar == sbProgress) {
            isDraggingProgress = true;
        }
    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        if (seekBar == sbProgress) {
            isDraggingProgress = false;
            if (AudioPlayer.get().isPlaying() || AudioPlayer.get().isPausing()) {
                int progress = seekBar.getProgress();
                AudioPlayer.get().seekTo(progress);

                if (mLrcViewSingle.hasLrc()) {
                    mLrcViewSingle.updateTime(progress);
                }
            } else {
                seekBar.setProgress(0);
            }
        }
    }

    @Override
    public boolean onPlayClick(long time) {
        if (AudioPlayer.get().isPlaying() || AudioPlayer.get().isPausing()) {
            AudioPlayer.get().seekTo((int) time);
            if (AudioPlayer.get().isPausing()) {
                AudioPlayer.get().playPause();
            }
            return true;
        }
        return false;
    }

    public void onChangeImpl(Music music) {
        if (music == null) {
            tvTitle.setText("");
            tvArtist.setText("");
            GlideHelper.loadPicPlayPage(this, "", mAlbumCoverView);
            ivPlayingBg.setImageResource(R.drawable.play_page_default_bg);
            setLrcLabel("暂无歌词");
            return;
        }
        currentMusic = music;
        long downloadCount = DBManager.get().getMusicLocalDao().queryBuilder()
                .where(MusicLocalDao.Properties.SongId.eq(currentMusic.songId)).count();
        if (downloadCount >= 1L) {
            isDownload = true;
            ivDownload.setImageResource(R.mipmap.have_download_icon);
        } else {
            isDownload = false;
        }
        HttpUtil.getInstance().getApiService().getIsCollect(currentMusic.songId)
                .compose(RxUtils.rxSchedulerHelper())
                .subscribe(new SuccessSubscriber<ResponseBean<HaveCollectBean>>() {
                    @Override
                    public void onSuccess(ResponseBean<HaveCollectBean> haveCollectBeanResponseBean) {
                        if (haveCollectBeanResponseBean.getResult() == null) return;
                        if (haveCollectBeanResponseBean.getResult().isCollect == 1) {
                            ivCollect.setImageResource(R.mipmap.collect_icon_1);
                        } else {
                            ivCollect.setImageResource(R.mipmap.collect_icon);
                        }
                        // 设置背景故事
                        tvIntroduce.setText(haveCollectBeanResponseBean.getResult().songStory + "");
                        //405不存在406下架407删除
                        HttpUtil.getInstance().
                                getApiService().
                                playCountAdd(currentMusic.songId)
                                .compose(RxUtils.rxSchedulerHelper()).
                                subscribe(new SuccessWithCodeSubscriber<ResponseBean>() {
                                    @Override
                                    public void onSuccess(ResponseBean responseBean) {
                                        TaipingApplication.tpApp.isShowPlay = false;
                                        HttpUtil.getInstance().getApiService().putCount((int) currentMusic.songId).compose(RxUtils.ioSchedulerHelper()).subscribe(new SuccessSubscriber<ResponseBean>() {
                                            @Override
                                            public void onSuccess(ResponseBean responseBean) {
                                            }

                                            @Override
                                            public void onFailed(String str) {
                                            }
                                        });
                                    }

                                    @Override
                                    public void onFailed(int code, String str) {
                                        TaipingApplication.tpApp.isShowPlay = false;
                                        AudioPlayer.get().pausePlayer();
                                        handler.postDelayed(new Runnable() {
                                            @Override
                                            public void run() {
                                                AudioPlayer.get().pausePlayer();
                                            }
                                        }, 2000);
                                        AudioPlayer.get().delete(0);
                                        if (code >= 405 && code <= 407) {
                                            SureDialog sureDialog = new SureDialog(PlayActivity.this);
                                            sureDialog.setContent(str);
                                            sureDialog.show();
                                            return;
                                        }
                                        confirmDialog = new ConfirmDialog(PlayActivity.this, new ConfirmDialog.Callback() {
                                            @Override
                                            public void callback() {
                                                if (!TaipingApplication.tpApp.getIsLogin()) {
                                                    LoginActivity.launch(PlayActivity.this);
                                                    finish();
                                                    return;
                                                }
                                                Logl.e("开始购买:" + music.songId);                    // 39
                                                HttpUtil.getInstance().getApiService().pay(2, 9, 1, music.songId)
                                                        .compose(RxUtils.rxSchedulerHelper())
                                                        .subscribe(new SuccessSubscriber<ResponseBean>() {
                                                            @Override
                                                            public void onSuccess(ResponseBean responseBean) {
                                                                ToastUtil.showToast(responseBean.getMessage());
                                                                confirmDialog.dismiss();
                                                                isNeedDelete = false;
                                                                music.isFree = true;
                                                                AudioPlayer.get().addAndPlay(music);
                                                            }

                                                            @Override
                                                            public void onFailed(String str) {
                                                                ToastUtil.showToast(str);
                                                            }
                                                        });
                                            }

                                            @Override
                                            public void onCancel() {
                                                confirmDialog.dismiss();
                                                isNeedDelete = true;
                                                if (isNeedDelete) {
                                                    AudioPlayer.get().delete(0);
                                                }
                                                finish();
                                            }
                                        });
                                        confirmDialog.setContent(str)
                                                .setBtnName("取消", "购买").show();
                                    }
                                });
                    }

                    @Override
                    public void onFailed(String str) {
                    }
                });

        tvTitle.setText(music.getTitle());
        tvArtist.setText("一 " + music.getArtist() + " 一");
        sbProgress.setProgress((int) AudioPlayer.get().getAudioPosition());
        sbProgress.setSecondaryProgress(0);
        sbProgress.setMax((int) music.getDuration());
        mLastProgress = 0;
        tvCurrentTime.setText(R.string.play_time_start);
        tvTotalTime.setText(formatTime(music.getDuration()));
        setCoverAndBg();
        setLrc(music);
        if (AudioPlayer.get().isPlaying() || AudioPlayer.get().isPreparing()) {
            ivPlay.setSelected(true);
            GlideHelper.loadPicPlayPage(this, AudioPlayer.get().getPlayMusic().getCoverPath(), mAlbumCoverView);
        } else {
            ivPlay.setSelected(false);
        }
    }

    public void play() {
        AudioPlayer.get().playPause();
    }

    public void next() {
        AudioPlayer.get().next();
    }

    public void prev() {
        AudioPlayer.get().prev();
    }

    public void switchPlayMode() {
        PlayModeEnum mode = PlayModeEnum.valueOf(Preferences.getPlayMode());
        switch (mode) {
            case LOOP:
                mode = PlayModeEnum.SHUFFLE;
                break;
            case SHUFFLE:
                mode = PlayModeEnum.SINGLE;
                break;
            case SINGLE:
                mode = PlayModeEnum.LOOP;
                break;
        }
        Preferences.savePlayMode(mode.value());
        initPlayMode();
    }


    String[] bgs;
    int bgIndex;

    private void setCoverAndBg() {
        Logl.e("bggg: " + currentMusic.songBackground);
        GlideHelper.loadListPic(this, AudioPlayer.get().getPlayMusic().getCoverPath(), mAlbumCoverView);
        if (currentMusic.songBackground == null) return;
        bgs = currentMusic.songBackground.split("\\|");
        Logl.e("bgggs:leth " + bgs.length);
        Glide.with(this)
                .load(bgs[bgIndex])
                .bitmapTransform(new BlurTransformation(this, 14, 3))
                .into(ivPlayingBg);
    }

    public void setLrc(final Music music) {
        if (music.getType() == Music.Type.LOCAL) {
            String lrcPath = FileUtils.getLrcFilePath(music);
            if (!TextUtils.isEmpty(lrcPath)) {
                loadLrc(lrcPath);
            } else {
                new SearchLrc(music.getArtist(), music.getTitle()) {
                    @Override
                    public void onPrepare() {
                        // 设置tag防止歌词下载完成后已切换歌曲
                        vpPlay.setTag(music);
                        loadLrc("");
                        setLrcLabel("正在搜索歌词");
                    }

                    @Override
                    public void onExecuteSuccess(@NonNull String lrcPath) {
                        if (vpPlay.getTag() != music) {
                            return;
                        }
                        // 清除tag
                        vpPlay.setTag(null);
                        loadLrc(lrcPath);
                        setLrcLabel("暂无歌词");
                    }

                    @Override
                    public void onExecuteFail(Exception e) {
                        if (vpPlay.getTag() != music) {
                            return;
                        }
                        // 清除tag
                        vpPlay.setTag(null);
                        setLrcLabel("暂无歌词");
                    }
                }.execute();
            }
        } else {
            String lrcPath = FileUtils.getLrcDir() + FileUtils.getLrcFileName(music.getArtist(), music.getTitle());
            loadLrc(lrcPath);
        }
    }

    public void loadLrc(String path) {
        File file = new File(path);
        mLrcViewSingle.loadLrc(file);
    }

    public void setLrcLabel(String label) {
        mLrcViewSingle.setLabel(label);
    }

    public String formatTime(long time) {
        return SystemUtils.formatTime("mm:ss", time);
    }

    public BroadcastReceiver mVolumeReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
        }
    };

    public static void launch(Activity activity) {

        activity.startActivity(new Intent(activity, PlayActivity.class));
        activity.overridePendingTransition(R.anim.fragment_slide_up, R.anim.out_to_right);
    }


    @OnClick({R.id.bt_collect, R.id.bt_download, R.id.bt_gift, R.id.bt_share})
    public void onPlayClick(View view) {
        switch (view.getId()) {
            case R.id.bt_collect:
                if (currentMusic == null) {
                    ToastUtil.showToast("当前不能识别正在播放的歌曲");
                    return;
                }
                HttpUtil.getInstance().getApiService().collect(String.valueOf(currentMusic.songId))
                        .compose(RxUtils.rxSchedulerHelper()).subscribe(new SuccessSubscriber<ResponseBean>() {
                    @Override
                    public void onSuccess(ResponseBean responseBean) {
                        if (responseBean.getMessage() != null) {
                            if (responseBean.getMessage().startsWith("歌曲收藏")) {
                                currentMusic.isCollect = true;
                                ivCollect.setImageResource(R.mipmap.collect_icon_1);
                            } else {
                                currentMusic.isCollect = false;
                                ivCollect.setImageResource(R.mipmap.collect_icon);
                            }
                        }
                        DBManager.get().getMusicDao().update(currentMusic);
                    }

                    @Override
                    public void onFailed(String str) {
                    }
                });
                break;

            case R.id.bt_download:
                if (currentMusic.spareNum1 == 1) {
                    new SureDialog(this).setContent("该歌曲不能下载").show();
                    return;
                }
                if (!TaipingApplication.tpApp.getIsLogin() || currentMusic == null) {
                    LoginActivity.launch(this);
                    finish();
                    return;
                }
                if (isDownload) {
                    ToastUtil.showToast("该歌曲已下载");
                    return;
                }
                isDownload = true;
                OnlineMusic onlineMusic = MusicUtil.mapToOnlineMusic(AudioPlayer.get().getPlayMusic());
                new DownloadOnlineMusic(this, onlineMusic) {
                    @Override
                    public void onPrepare() {
                        Logl.e("onPrepare");
                    }

                    @Override
                    public void onExecuteSuccess(Void aVoid) {
                        Logl.e("onExecuteSuccess");
                        ToastUtil.showToast(getString(R.string.now_download, onlineMusic.getTitle()));
                    }

                    @Override
                    public void onExecuteFail(Exception e) {
                        Logl.e("onExecuteFail");
                        ToastUtil.showToast(R.string.unable_to_download);
                    }
                }.execute();
                break;
            case R.id.bt_gift:
                if (!TaipingApplication.tpApp.getIsLogin() || currentMusic == null) {
                    LoginActivity.launch(this);
                    finish();
                    return;
                }
                GiftDialog.launch(this, (int) currentMusic.songId);
                break;
            case R.id.bt_share:
                if (!TaipingApplication.tpApp.getIsLogin() || currentMusic == null) {
                    LoginActivity.launch(this);
                    finish();
                    return;
                }
                ShareDialog.launch(this);
                break;
        }
    }


    @Override
    public void onDestroy() {
        unregisterReceiver(mVolumeReceiver);
        RxBus.get().unregister(this);
        AudioPlayer.get().removeOnPlayEventListener(this);
        unregisterReceiver(downloadReceiver);
        super.onDestroy();
    }

    public class PlayDownloadReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            ivDownload.setImageResource(R.mipmap.have_download_icon);
        }
    }

}
