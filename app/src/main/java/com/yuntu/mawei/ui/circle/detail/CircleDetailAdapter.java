package com.yuntu.mawei.ui.circle.detail;

import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.view.View;

import com.tencent.smtt.sdk.WebSettings;
import com.tencent.smtt.sdk.WebView;
import com.tencent.smtt.sdk.WebViewClient;
import com.yuntu.mawei.BuildConfig;
import com.yuntu.mawei.R;
import com.yuntu.mawei.base.TaipingApplication;
import com.yuntu.mawei.bean.base.ResponseBean;
import com.yuntu.mawei.bean.circle.CircleBean;
import com.yuntu.mawei.bean.rx.SuccessSubscriber;
import com.yuntu.mawei.ui.circle.comment.JubaoActivity;
import com.yuntu.mawei.ui.mine.user.UserBean;
import com.yuntu.mawei.util.GlideHelper;
import com.yuntu.mawei.util.GsonUtil;
import com.yuntu.mawei.util.Logl;
import com.yuntu.mawei.util.RxUtils;
import com.yuntu.mawei.util.ToastUtil;
import com.yuntu.mawei.util.http.HttpUtil;
import com.yuntu.mawei.view.adapter.base.BaseMultiItemQuickAdapter;
import com.yuntu.mawei.view.adapter.base.BaseViewHolder;

import java.util.List;


/**
 * Created by tanghao on 2017/6/5.
 * Email: 86882259@qq.com
 */

public class CircleDetailAdapter extends BaseMultiItemQuickAdapter<CircleDetailBean> {

    private CircleDetailActivity mContext;

    private WebView webView;

    private WebSettings webSetting;
    private boolean isAdapterHandler = true;
    public CircleBean circleBean;

    private int userLocalId;


    public CircleDetailAdapter(CircleDetailActivity context, CircleBean circleBean) {
        super(context);
        this.mContext = context;
        this.circleBean = circleBean;
        if (!TaipingApplication.tpApp.getIsLogin()) {
            return;
        }
        HttpUtil.getInstance().getApiService().getUserInfo().compose(RxUtils.rxSchedulerHelper())
                .subscribe(new SuccessSubscriber<ResponseBean<UserBean>>() {
                    @Override
                    public void onSuccess(ResponseBean<UserBean> userBeanResponseBean) {
                        if (userBeanResponseBean.getResult() == null) return;
                        userLocalId = userBeanResponseBean.getResult().userId;
                    }

                    @Override
                    public void onFailed(String str) {
                    }
                });
    }

    @Override
    protected void attachItemType() {
        addItemType(CircleDetailBean.ITEM_WEB, R.layout.item_news_detail_web);
        addItemType(CircleDetailBean.ITEM_COMMENT, R.layout.adapter_comment_item);
        addItemType(CircleDetailBean.ITEM_XIAN, R.layout.adapter_circle_detail_title);
    }

    @Override
    protected void convert(BaseViewHolder holder, CircleDetailBean item) {
        switch (item.getItemType()) {
            case CircleDetailBean.ITEM_WEB:
                handleWeb(holder, item);
                break;
            case CircleDetailBean.ITEM_COMMENT:
                initComment(holder, item);
                break;
            case CircleDetailBean.ITEM_XIAN:
                initTitle(holder);
                break;
        }
    }


    private boolean yiciLoadWeb = true;

    private void handleWeb(BaseViewHolder holder, CircleDetailBean bean) {
        if (isAdapterHandler && yiciLoadWeb) {
            yiciLoadWeb = false;
            webView = holder.getView(R.id.article_web);
            webView.loadUrl(BuildConfig.MALL_HOST +
                    "app/user/musicTheme/getMusicTheme?themeId=" + circleBean.themeId);
            initWebViewSettings();
            webView.setWebViewClient(new WebViewClient() {
                @Override
                public boolean shouldOverrideUrlLoading(WebView webView, String s) {
                    return super.shouldOverrideUrlLoading(webView, s);
                }

                @Override
                public void onPageFinished(WebView webView, String s) {
                    super.onPageFinished(webView, s);
                    if (isAdapterHandler) {
                        isAdapterHandler = false;
                    }
                }
            });
        }
    }

    private void initTitle(BaseViewHolder holder) {
        holder.setText(R.id.tv_num, "评论" + circleBean.commentCount);
    }

    private void initComment(BaseViewHolder holder, CircleDetailBean bean) {
        GlideHelper.loadListPic(mContext, bean.commentBean.appUserAvatar, holder.getView(R.id.iv_user));
        holder.setText(R.id.tv_name, bean.commentBean.appUserName);
        holder.setText(R.id.tv_content, bean.commentBean.classCapacity);
        if (bean.commentBean.appUserId == userLocalId) {
            holder.setVisible(R.id.bt_delete, true);
            holder.setOnClickListener(R.id.bt_delete, new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    HttpUtil.getInstance().getApiService().deleteComment(bean.commentBean.themeId).compose(RxUtils.rxSchedulerHelper())
                            .subscribe(new SuccessSubscriber<ResponseBean>() {
                                @Override
                                public void onSuccess(ResponseBean responseBean) {
                                    mContext.updateViews(false);
                                }

                                @Override
                                public void onFailed(String str) {
                                    ToastUtil.showToast(str);
                                }
                            });
                }
            });
        } else {
            holder.setVisible(R.id.bt_delete, false);
        }
        holder.setOnClickListener(R.id.bt_jubao, v -> JubaoActivity.launch(mContext, circleBean,bean.commentBean.themeId));
    }


    private void initWebViewSettings() {
        webSetting = webView.getSettings();
        webSetting.setJavaScriptEnabled(true);
        webSetting.setJavaScriptCanOpenWindowsAutomatically(true);
        webSetting.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.NARROW_COLUMNS);
        webSetting.setSupportZoom(true);
        webSetting.setBuiltInZoomControls(false);
        webSetting.setUseWideViewPort(true);
        webSetting.setSupportMultipleWindows(true);
        webSetting.setLoadWithOverviewMode(true);
        webSetting.setDatabaseEnabled(true);
        webSetting.setDomStorageEnabled(true);
        webSetting.setGeolocationEnabled(true);
        webSetting.setAllowFileAccess(true);
        webSetting.setAppCacheEnabled(true);
        webSetting.setAppCacheMaxSize(1024 * 1024 * 60); // 配置缓存大小
        webSetting.setPluginState(WebSettings.PluginState.ON_DEMAND);
        webSetting.setRenderPriority(WebSettings.RenderPriority.HIGH);
        webSetting.setCacheMode(WebSettings.LOAD_DEFAULT);

    }

}
