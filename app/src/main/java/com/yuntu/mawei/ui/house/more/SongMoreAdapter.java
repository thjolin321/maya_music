package com.yuntu.mawei.ui.house.more;

import android.content.Context;
import android.view.View;

import com.yuntu.mawei.R;
import com.yuntu.mawei.bean.song.SongBean;
import com.yuntu.mawei.ui.homepage.mv.detail.MvDetailActivity;
import com.yuntu.mawei.util.GlideHelper;
import com.yuntu.mawei.util.music.MusicUtil;
import com.yuntu.mawei.view.adapter.base.BaseMultiItemQuickAdapter;
import com.yuntu.mawei.view.adapter.base.BaseViewHolder;

public class SongMoreAdapter extends BaseMultiItemQuickAdapter<SongBean> {

    public boolean noShowIcon;

    public SongMoreAdapter(Context context) {
        super(context);
    }

    @Override
    protected void attachItemType() {
        addItemType(SongBean.SONG_TITLE_ITEM, R.layout.adapter_song_title);
        addItemType(SongBean.SONG_ITEM, R.layout.adapter_song_more);
    }

    @Override
    protected void convert(BaseViewHolder holder, SongBean item) {
        switch (item.getItemType()) {
            case SongBean.SONG_TITLE_ITEM:
                holder.setText(R.id.tv_zimu_title, item.zimuTitle);
                break;
            case SongBean.SONG_ITEM:
                GlideHelper.loadListPic(mContext, item.songAvatar, holder.getView(R.id.iv_song));
                holder.setText(R.id.tv_song, item.songName);
                if (item.singer != null) {
                    holder.setText(R.id.tv_artist, item.singer.singerName);
                }
                if (noShowIcon) {
                    holder.setVisible(R.id.iv_play_icon, false);
                }
                holder.getConvertView().setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (item.songType == 2) {
                            MvDetailActivity.launch(mContext, item);
                        } else {
                            MusicUtil.playMusic(mContext, item);
                        }
                    }
                });
                break;
        }

    }
}
