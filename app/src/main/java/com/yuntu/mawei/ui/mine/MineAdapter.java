package com.yuntu.mawei.ui.mine;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;

import com.yuntu.mawei.R;
import com.yuntu.mawei.base.TaipingApplication;
import com.yuntu.mawei.ui.login.LoginActivity;
import com.yuntu.mawei.ui.login.MyWebViewActivity;
import com.yuntu.mawei.ui.circle.publish.MyPublishActivity;
import com.yuntu.mawei.ui.mine.member.MemberActivity;
import com.yuntu.mawei.ui.mine.money.MoneyActivity;
import com.yuntu.mawei.ui.mine.set.HelpActivity;
import com.yuntu.mawei.ui.mine.user.UserInfoActivity;
import com.yuntu.mawei.ui.three.local.LocalMusicActivity;
import com.yuntu.mawei.ui.three.collect.MyCollectActivity;
import com.yuntu.mawei.ui.three.recent.RecentActivity;
import com.yuntu.mawei.util.ToastUtil;
import com.yuntu.mawei.view.adapter.base.BaseQuickAdapter;
import com.yuntu.mawei.view.adapter.base.BaseViewHolder;

/**
 * Created by tanghao on 2019/1/24
 */
public class MineAdapter extends BaseQuickAdapter<MineBean> {

    public MineAdapter(Context context) {
        super(context);
    }

    @Override
    protected int attachLayoutRes() {
        return R.layout.adapter_mine;
    }

    @Override
    protected void convert(BaseViewHolder holder, MineBean item) {
        holder.setImageResource(R.id.mine_icon, item.icon);
        holder.setText(R.id.mine_text, item.title);
        if (item.id == 2) {
            if (!TextUtils.isEmpty(item.time)) {
                holder.setText(R.id.member_time, item.time);
                holder.setVisible(R.id.bt_time, true);
            } else {
                holder.setVisible(R.id.bt_time, false);
            }
        } else {
            holder.setVisible(R.id.bt_time, false);
        }
        holder.getConvertView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (item.id) {
                    case 1:
                        if (!TaipingApplication.tpApp.getIsLogin()) {
                            ToastUtil.showToast("请先登录");
                            LoginActivity.launch(mContext);
                            return;
                        }
                        UserInfoActivity.launch(mContext);
                        break;
                    case 2:
                        MemberActivity.launch(mContext);
                        break;
                    case 3:
                        MoneyActivity.launch(mContext);
                        break;
                    case 4:
                        LocalMusicActivity.launch(mContext);
                        break;
                    case 5:
                        RecentActivity.launch(mContext);
                        break;
                    case 6:
                        MyCollectActivity.launch(mContext);
                        break;
                    case 7:
                        MyWebViewActivity.launch(mContext, 0);
                        break;
                    case 8:
                        HelpActivity.launch(mContext);
                        break;
                    case 9:
                        MyPublishActivity.launch(mContext);
                        break;
                }
            }
        });
    }
}
