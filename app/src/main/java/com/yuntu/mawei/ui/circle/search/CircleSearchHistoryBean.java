package com.yuntu.mawei.ui.circle.search;

import io.realm.RealmObject;

public class CircleSearchHistoryBean extends RealmObject {

    private String content;
    private Long createTime;
    private Long userId = -1L;
    private Long updateTime;

//    public int getMainId() {
//        return mainId;
//    }
//
//    public void setMainId(int mainId) {
//        this.mainId = mainId;
//    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Long updateTime) {
        this.updateTime = updateTime;
    }
}
