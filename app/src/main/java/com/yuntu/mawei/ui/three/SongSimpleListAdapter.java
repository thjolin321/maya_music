package com.yuntu.mawei.ui.three;

import android.app.Activity;
import android.content.Context;

import com.yuntu.mawei.R;
import com.yuntu.mawei.bean.song.SongBean;
import com.yuntu.mawei.ui.homepage.mv.detail.MvDetailActivity;
import com.yuntu.mawei.ui.playing.PlayActivity;
import com.yuntu.mawei.ui.playing.service.AudioPlayer;
import com.yuntu.mawei.ui.three.piliang.PiliangActivity;
import com.yuntu.mawei.util.GsonUtil;
import com.yuntu.mawei.util.Logl;
import com.yuntu.mawei.util.ToastUtil;
import com.yuntu.mawei.util.music.MusicUtil;
import com.yuntu.mawei.view.adapter.base.BaseMultiItemQuickAdapter;
import com.yuntu.mawei.view.adapter.base.BaseViewHolder;

public class SongSimpleListAdapter extends BaseMultiItemQuickAdapter<SongBean> {

    int type; // 控制批量操作页面 0 for local, 1 for collect, 2 for recent, 3 for mv play, 4 for local music play

    public SongSimpleListAdapter(Context context, int type) {
        super(context);
        this.type = type;
    }

    @Override
    protected void attachItemType() {
        addItemType(SongBean.SONG_TITLE_ITEM, R.layout.adapter_song_simple_title);
        addItemType(SongBean.SONG_ITEM, R.layout.adapter_song_simple_list);
    }

    @Override
    protected void convert(BaseViewHolder holder, SongBean item) {
        switch (item.itemType) {
            case SongBean.SONG_TITLE_ITEM:
                holder.setText(R.id.tv_zimu_title, item.zimuTitle);
                if (holder.getLayoutPosition() == 0) {
                    holder.setVisible(R.id.bt_piliang, true);
                    holder.setOnClickListener(R.id.bt_piliang, v -> {
                        PiliangActivity.launch(mContext, getData(), type);
                    });
                } else {
                    holder.setVisible(R.id.bt_piliang, false);
                }
                break;
            case SongBean.SONG_ITEM:
                holder.setText(R.id.tv_song, item.songName);
                if (type == 1) {
                    holder.setText(R.id.tv_artist_and_album, item.musicLocal.artist + " - 《" + item.musicLocal.album + "》");
                } else {
                    if (item.singer != null) {
                        holder.setText(R.id.tv_artist_and_album, item.singer.singerName + " - 《" + (item.album != null ? item.album.albumName : "") + "》");
                    }
                }
                holder.getConvertView().setOnClickListener(v -> {
                    if (item.musicLocal != null) {
                        AudioPlayer.get().addAndPlayWithUpdate(item.musicLocal);
                    } else {
                        if (item.songType == 2) {
                            MvDetailActivity.launch(mContext, item);
                        } else {
                            if (item.songType == 1) {
                                MusicUtil.playMusicNoLaunch(mContext, item);
                            } else {
                                MusicUtil.playMusic(mContext, item);
                            }
                        }
                    }
                });
                break;
        }
    }
}
