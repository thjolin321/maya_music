package com.yuntu.mawei.ui.mine.member;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.alipay.sdk.app.PayTask;
import com.yuntu.mawei.R;
import com.yuntu.mawei.base.TaipingApplication;
import com.yuntu.mawei.base.home.BaseActivity;
import com.yuntu.mawei.bean.base.ResponseBean;
import com.yuntu.mawei.bean.base.pay.ZhifubaoBean;
import com.yuntu.mawei.bean.rx.SuccessSubscriber;
import com.yuntu.mawei.ui.mine.user.UserBean;
import com.yuntu.mawei.util.BigDeUtil;
import com.yuntu.mawei.util.GsonUtil;
import com.yuntu.mawei.util.Logl;
import com.yuntu.mawei.util.RxUtils;
import com.yuntu.mawei.util.ToastUtil;
import com.yuntu.mawei.util.http.HttpUtil;

import java.util.List;
import java.util.Map;

import butterknife.BindColor;
import butterknife.BindView;
import butterknife.BindViews;
import butterknife.OnClick;
import retrofit2.http.Query;

public class MemberBuyActiviy extends BaseActivity {
    @BindViews({R.id.tv_1, R.id.tv_2, R.id.tv_3, R.id.tv_4})
    List<TextView> viewList1;
    @BindViews({R.id.tv_m_1, R.id.tv_m_2, R.id.tv_m_3, R.id.tv_m_4})
    List<TextView> viewList2;
    @BindViews({R.id.tv_y_1, R.id.tv_y_2, R.id.tv_y_3, R.id.tv_y_4})
    List<TextView> viewList3;
    @BindViews({R.id.goods_1, R.id.goods_2, R.id.goods_3, R.id.goods_4})
    List<View> viewList4;

    @BindView(R.id.bt_weixin)
    ImageView btWeixin;
    @BindView(R.id.bt_zhifubao)
    ImageView btZhifubao;
    @BindView(R.id.bt_yue)
    ImageView btYue;

    @BindColor(R.color.white)
    int white;
    @BindColor(R.color.mall_black)
    int yellow;
    @BindColor(R.color.member_b)
    int black;

    int type = 1; // 0 for 微信，1 for 支付宝
    List<MemberBean> list;
    int index;
    Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 0x123:
                    if (msg.obj != null) {
                        if(msg.obj.toString().contains("resultStatus=9000")){
                            ToastUtil.showToast("支付成功");
                        }
                    }
                    break;
                case 0x234:
                    break;
            }

            super.handleMessage(msg);
        }
    };

    @Override
    public int attachLayout() {
        return R.layout.activity_member_buy;
    }

    @Override
    public void init() {
        initTitle("会员购买");
        getHttpMember();
    }

    private void getHttpMember() {
        HttpUtil.getInstance().getApiService().getMemberList()
                .compose(RxUtils.rxSchedulerHelper())
                .subscribe(new SuccessSubscriber<ResponseBean<List<MemberBean>>>() {
                    @Override
                    public void onSuccess(ResponseBean<List<MemberBean>> listResponseBean) {
                        if (listResponseBean == null) return;
                        list = listResponseBean.getResult();
                        for (int i = 0; i < listResponseBean.getResult().size(); i++) {
                            viewList1.get(i).setText(listResponseBean.getResult().get(i).commodityName);
                            viewList2.get(i).setText(BigDeUtil.div(listResponseBean.getResult().get(i).commodityValue, 100) + "");
                        }
                    }

                    @Override
                    public void onFailed(String str) {
                        ToastUtil.showToast(str);
                    }
                });
    }

    public static void launch(Context context) {
        context.startActivity(new Intent(context, MemberBuyActiviy.class));
    }

    @OnClick({R.id.goods_1, R.id.goods_2, R.id.goods_3, R.id.goods_4, R.id.bt_become,
            R.id.all_click_1, R.id.all_click_2, R.id.all_click_3})
    public void onMenber(View v) {
        switch (v.getId()) {
            case R.id.goods_1:
                onCheck(0);
                break;
            case R.id.goods_2:
                onCheck(1);
                break;
            case R.id.goods_3:
                onCheck(2);
                break;
            case R.id.goods_4:
                onCheck(3);
                break;
            case R.id.all_click_1:
                type = 1;
                btWeixin.setImageResource(R.mipmap.gou_grey);
                btZhifubao.setImageResource(R.mipmap.gou_red);
                btYue.setImageResource(R.mipmap.gou_grey);
                break;
            case R.id.all_click_2:
                type = 0;
                btWeixin.setImageResource(R.mipmap.gou_red);
                btZhifubao.setImageResource(R.mipmap.gou_grey);
                btYue.setImageResource(R.mipmap.gou_grey);
                break;
            case R.id.all_click_3:
                type = 2;
                btYue.setImageResource(R.mipmap.gou_red);
                btWeixin.setImageResource(R.mipmap.gou_grey);
                btZhifubao.setImageResource(R.mipmap.gou_grey);
                break;
            case R.id.bt_become:
                if (index >= list.size()) {
                    return;
                }
                HttpUtil.getInstance().getApiService().pay(type, list.get(index).commodityId, 1, 0)
                        .compose(RxUtils.rxSchedulerHelper())
                        .subscribe(new SuccessSubscriber<ResponseBean>() {
                            @Override
                            public void onSuccess(ResponseBean responseBean) {
                                switch (type) {
                                    case 1:
                                        Runnable payRunnable = new Runnable() {

                                            @Override
                                            public void run() {
                                                PayTask alipay = new PayTask(MemberBuyActiviy.this);
                                                Map<String, String> result = alipay.payV2(responseBean.getMessage(), true);
                                                Message msg = new Message();
                                                msg.what = 0x123;
                                                msg.obj = result;
                                                mHandler.sendMessage(msg);
                                            }
                                        };
                                        // 必须异步调用
                                        Thread payThread = new Thread(payRunnable);
                                        payThread.start();
                                        break;
                                    case 0:
                                        ToastUtil.showToast(responseBean.getMessage());
                                        break;
                                    case 2:
                                        ToastUtil.showToast(responseBean.getMessage());
                                        break;
                                }


                            }

                            @Override
                            public void onFailed(String str) {
                                ToastUtil.showToast(str);
                            }
                        });
                break;
        }
    }

    private void onCheck(int index) {
        this.index = index;
        for (int i = 0; i < 4; i++) {
            viewList1.get(i).setTextColor(black);
            viewList2.get(i).setTextColor(yellow);
            viewList3.get(i).setTextColor(yellow);
            viewList4.get(i).setBackgroundResource(R.drawable.member_buy_unselect);
        }
        viewList1.get(index).setTextColor(white);
        viewList2.get(index).setTextColor(white);
        viewList3.get(index).setTextColor(white);
        viewList4.get(index).setBackgroundResource(R.drawable.member_buy_select);
    }

}
