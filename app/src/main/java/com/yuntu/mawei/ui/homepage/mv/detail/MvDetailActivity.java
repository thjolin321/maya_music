package com.yuntu.mawei.ui.homepage.mv.detail;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.annotations.SerializedName;
import com.yuntu.mawei.R;
import com.yuntu.mawei.base.TaipingApplication;
import com.yuntu.mawei.base.home.BaseActivity;
import com.yuntu.mawei.base.home.BaseListActivity;
import com.yuntu.mawei.base.home.BaseListActivityPresenter;
import com.yuntu.mawei.bean.base.ResponseBean;
import com.yuntu.mawei.bean.executor.DownloadOnlineMusic;
import com.yuntu.mawei.bean.music.Music;
import com.yuntu.mawei.bean.music.OnlineMusic;
import com.yuntu.mawei.bean.rx.SuccessSubscriber;
import com.yuntu.mawei.bean.song.HaveCollectBean;
import com.yuntu.mawei.bean.song.SongBean;
import com.yuntu.mawei.storage.db.greendao.MusicLocalDao;
import com.yuntu.mawei.ui.login.LoginActivity;
import com.yuntu.mawei.ui.playing.PlayActivity;
import com.yuntu.mawei.ui.playing.gift.GiftDialog;
import com.yuntu.mawei.ui.playing.service.AudioPlayer;
import com.yuntu.mawei.ui.three.SongSimpleListAdapter;
import com.yuntu.mawei.util.BigDeUtil;
import com.yuntu.mawei.util.DpUtil;
import com.yuntu.mawei.util.GlideHelper;
import com.yuntu.mawei.util.GsonUtil;
import com.yuntu.mawei.util.Logl;
import com.yuntu.mawei.util.RxUtils;
import com.yuntu.mawei.util.ToastUtil;
import com.yuntu.mawei.util.db.DBManager;
import com.yuntu.mawei.util.http.HttpUtil;
import com.yuntu.mawei.util.music.MusicUtil;
import com.yuntu.mawei.view.ConfirmDialog;
import com.yuntu.mawei.view.ShareDialog;
import com.yuntu.mawei.view.SureDialog;
import com.yuntu.mawei.view.adapter.base.BaseQuickAdapter;
import com.yuntu.mawei.widget.BannerVideoPlayer;

import butterknife.BindView;
import butterknife.OnClick;
import cn.jzvd.JZMediaManager;
import cn.jzvd.JZVideoPlayerStandard;


/**
 * Created by sanmu on 2017/7/6.
 * meail: 992759969@qq.com
 */

public class MvDetailActivity extends BaseListActivity {

    @BindView(R.id.video_player)
    BannerVideoPlayer mVideoPlayer;
    @BindView(R.id.bt_collect)
    ImageView ivCollect;
    @BindView(R.id.bt_download)
    ImageView ivDownload;
    @BindView(R.id.tv_artist_mv_detail)
    TextView tvMvsinger;
    private PlayDownloadReceiver downloadReceiver;

    ImageView ivAlbum, ivDianzan;
    TextView tvAlbumName, tvLabel, tvAlbumSinger, tvAlbumIntroduce, tvPrice, tvPublishTime, tvDianzan;
    View btIntro;

    long id;
    SongBean songBean;
    String songUrl, songSinger, songAvator, songName;
    MvDetailBean mvDetailBean;
    boolean isDownload;
    BaseQuickAdapter adapter;
    ConfirmDialog confirmDialog;


    @Override
    protected void initViews() {
        initAll(getIntent());
        judgMember();
    }

    private void judgMember() {
        HttpUtil.getInstance().
                getApiService().
                playCountAdd(songBean.songId)
                .compose(RxUtils.rxSchedulerHelper()).
                subscribe(new SuccessSubscriber<ResponseBean>() {
                    @Override
                    public void onSuccess(ResponseBean responseBean) {
                    }

                    @Override
                    public void onFailed(String str) {
                        confirmDialog = new ConfirmDialog(MvDetailActivity.this, new ConfirmDialog.Callback() {
                            @Override
                            public void callback() {
                                // TODO 调用支付接口
                                if (!TaipingApplication.tpApp.getIsLogin()) {
                                    LoginActivity.launch(MvDetailActivity.this);
                                    finish();
                                    return;
                                }
                                HttpUtil.getInstance().getApiService().pay(2, 9, 1, songBean.songId)
                                        .compose(RxUtils.rxSchedulerHelper())
                                        .subscribe(new SuccessSubscriber<ResponseBean>() {
                                            @Override
                                            public void onSuccess(ResponseBean responseBean) {
                                                ToastUtil.showToast(responseBean.getMessage());
                                                confirmDialog.dismiss();
                                            }

                                            @Override
                                            public void onFailed(String str) {
                                                ToastUtil.showToast(str);
                                            }
                                        });
                            }

                            @Override
                            public void onCancel() {
                                confirmDialog.dismiss();
                                finish();
                            }
                        });
                        confirmDialog.setContent(str)
                                .setBtnName("取消", "购买").show();
                    }
                });

    }

    int dianzanCount;
    int isAgree;

    private void initAll(Intent intent) {
        downloadReceiver = new PlayDownloadReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.intent.action.DOWNLOAD_COMPLETE");
        registerReceiver(downloadReceiver, intentFilter);
        songBean = (SongBean) intent.getSerializableExtra("songBean");
        if (songBean.musicLocal != null) {
            id = songBean.musicLocal.songId;
            songUrl = songBean.musicLocal.path;
            songName = songBean.musicLocal.title;
            songAvator = songBean.musicLocal.coverPath;
            songSinger = songBean.musicLocal.artist;
        } else {
            id = songBean.songId;
            songName = songBean.songName;
            songUrl = songBean.songUrl;
            songAvator = songBean.songAvatar;
            songSinger = songBean.singer != null ? songBean.singer.singerName : "";
        }
        if (songUrl == null) songUrl = "";
        if (songAvator == null) songAvator = "";
        if (songSinger == null) songSinger = "";
        tvMvsinger.setText("一 " + songSinger + " 一");
        initTitle(songName);
        initPlayer();
        long downloadCount = DBManager.get().getMusicLocalDao().queryBuilder()
                .where(MusicLocalDao.Properties.SongId.eq(songBean.songId)).count();
        if (downloadCount >= 1L) {
            isDownload = true;
            ivDownload.setImageResource(R.mipmap.have_download_icon);
        } else {
            isDownload = false;
        }
        if (songBean.collect != null) {
            ivCollect.setImageResource(R.mipmap.collect_icon_1);
        } else {
            ivCollect.setImageResource(R.mipmap.collect_icon);
        }
        HttpUtil.getInstance().getApiService().putCount((int) id)
                .compose(RxUtils.ioSchedulerHelper()).subscribe(new SuccessSubscriber<ResponseBean>() {
            @Override
            public void onSuccess(ResponseBean responseBean) {
            }

            @Override
            public void onFailed(String str) {
            }
        });
        HttpUtil.getInstance().getApiService().getIsCollect(songBean.songId)
                .compose(RxUtils.rxSchedulerHelper())
                .subscribe(new SuccessSubscriber<ResponseBean<HaveCollectBean>>() {
                    @Override
                    public void onSuccess(ResponseBean<HaveCollectBean> haveCollectBeanResponseBean) {
                        if (haveCollectBeanResponseBean.getResult() == null) return;
                        if (haveCollectBeanResponseBean.getResult().isCollect == 1) {
                            ivCollect.setImageResource(R.mipmap.collect_icon_1);
                        } else {
                            ivCollect.setImageResource(R.mipmap.collect_icon);
                        }
                        isAgree = haveCollectBeanResponseBean.getResult().songBean.isAgree;
                        dianzanCount = haveCollectBeanResponseBean.getResult().songBean.agreeCount;
                        tvDianzan.setText(dianzanCount + "");
                        if (isAgree == 1) {
                            ivDianzan.setImageResource(R.mipmap.dianzan_red);
                        } else {
                            ivDianzan.setImageResource(R.mipmap.dianzan_grey);
                        }
                        if (haveCollectBeanResponseBean.getResult().songValue > 0.0) {
                            tvPrice.setText("￥" + BigDeUtil.div(haveCollectBeanResponseBean.getResult().songValue, 100.0));
                        }

                        ivDianzan.setOnClickListener(v -> {
                            if (!TaipingApplication.tpApp.getIsLogin()) {
                                LoginActivity.launch(MvDetailActivity.this);
                                return;
                            }
                            if (isAgree == 0) {
                                ivDianzan.setImageResource(R.mipmap.dianzan_red);
                                dianzanCount++;
                                isAgree = 1;
                            } else {
                                isAgree = 0;
                                ivDianzan.setImageResource(R.mipmap.dianzan_grey);
                                dianzanCount--;
                            }
                            tvDianzan.setText(dianzanCount + "");
                            HttpUtil.getInstance().getApiService().dianzanMv(0, (int) songBean.songId)
                                    .compose(RxUtils.rxSchedulerHelper()).subscribe(new SuccessSubscriber<ResponseBean>() {
                                @Override
                                public void onSuccess(ResponseBean responseBean) {
                                }

                                @Override
                                public void onFailed(String str) {
                                }
                            });

                        });
                    }

                    @Override
                    public void onFailed(String str) {
                    }
                });

    }

    private void initPlayer() {
        Logl.e("MvDetailActivity songUrl: " + songUrl);
        mVideoPlayer.setUp(songUrl, JZVideoPlayerStandard.SCREEN_WINDOW_NORMAL, "");
        mVideoPlayer.thumbImageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
        GlideHelper.loadListPic(this, songAvator, mVideoPlayer.thumbImageView);
        mVideoPlayer.startVideo();
        mVideoPlayer.changeStartButtonSize(DpUtil.dip2px(this, 58));
    }


    @Override
    protected BaseQuickAdapter attachAdapter() {
        adapter = new SongSimpleListAdapter(this, 3);
        View viewHeader = LayoutInflater.from(this).inflate(R.layout.adapter_mv_detail_header, null);
        ivAlbum = viewHeader.findViewById(R.id.iv_song);
        ivDianzan = viewHeader.findViewById(R.id.iv_dianzan);
        tvAlbumName = viewHeader.findViewById(R.id.tv_title);
        tvPublishTime = viewHeader.findViewById(R.id.tv_publish_time);
        tvDianzan = viewHeader.findViewById(R.id.tv_dianzan);
        tvLabel = viewHeader.findViewById(R.id.tv_detail_label);
        tvAlbumSinger = viewHeader.findViewById(R.id.tv_artist);
        tvAlbumIntroduce = viewHeader.findViewById(R.id.tv_source_and_readcount);
        btIntro = viewHeader.findViewById(R.id.bt_intro);
        tvPrice = viewHeader.findViewById(R.id.tv_price);
        adapter.addHeaderView(viewHeader);
        return adapter;
    }

    public void initHeaderView(MvDetailBean mvDetailBean) {
        if (mvDetailBean == null) return;
        this.mvDetailBean = mvDetailBean;
        if (mvDetailBean.album != null) {
            ivAlbum.setVisibility(View.VISIBLE);
            tvAlbumName.setText(mvDetailBean.album.albumName + "");
            GlideHelper.loadListPic(this, mvDetailBean.album.albumAvatar, ivAlbum);
        } else {
            tvAlbumName.setText("");
        }
        tvPublishTime.setText("发布时间：" + mvDetailBean.createTime + "");
        tvAlbumSinger.setText("歌手：" + songSinger);
        if (mvDetailBean.songLabel != null && mvDetailBean.songLabel.labelName != null) {
            tvLabel.setVisibility(View.VISIBLE);
            tvLabel.setText(mvDetailBean.songLabel.labelName);
        } else {
            tvLabel.setVisibility(View.GONE);
        }
        tvAlbumIntroduce.setText("简介：" + (mvDetailBean.albumIntroduction != null ? mvDetailBean.albumIntroduction : ""));

        btIntro.setOnClickListener(v -> {
            SureDialog sureDialog = new SureDialog(MvDetailActivity.this);
            sureDialog.setContent(mvDetailBean.albumIntroduction + "");
            sureDialog.show();
        });

        if (mvDetailBean.relevantMv != null && mvDetailBean.relevantMv.size() != 0) {
            adapter.updateItems(mvDetailBean.relevantMv);
        }
    }


    @Override
    protected BaseListActivityPresenter attachPresenter() {
        return new MvDetailPresenter(this, id);
    }

    @Override
    protected int attachLayoutRes() {
        return R.layout.activity_mv_detail;
    }

    @OnClick({R.id.bt_collect, R.id.bt_download, R.id.bt_gift, R.id.bt_share})
    public void onPlayClick(View view) {
        switch (view.getId()) {
            case R.id.bt_collect:
                HttpUtil.getInstance().getApiService().collect(String.valueOf(id))
                        .compose(RxUtils.rxSchedulerHelper()).subscribe(new SuccessSubscriber<ResponseBean>() {
                    @Override
                    public void onSuccess(ResponseBean responseBean) {
                        if (responseBean.getMessage() != null) {
                            if (responseBean.getMessage().startsWith("歌曲收藏")) {
                                ivCollect.setImageResource(R.mipmap.collect_icon_1);
                            } else {
                                ivCollect.setImageResource(R.mipmap.collect_icon);
                            }
                        }
                        ToastUtil.showToast(responseBean.getMessage());
                    }

                    @Override
                    public void onFailed(String str) {
                    }
                });
                break;

            case R.id.bt_download:
                if (songBean.downloadFlag == 1) {
                    new SureDialog(this).setContent("该MV不能下载").show();
                    return;
                }
                if (!TaipingApplication.tpApp.getIsLogin()) {
                    LoginActivity.launch(this);
                    finish();
                    return;
                }
                if (isDownload) {
                    return;
                }
                isDownload = true;
                Music music = new Music();
                music.coverPath = songAvator;
                music.artist = songSinger;
                music.title = songName;
                music.album = mvDetailBean != null ? (mvDetailBean.album != null ? mvDetailBean.album.albumName : "") : "";
                music.songId = id;
                music.songType = 2;
                music.firstZimu = TextUtils.isEmpty(songBean.singNameSyllable) ? "Z" : songBean.singNameSyllable.substring(0, 1).toUpperCase();
                OnlineMusic onlineMusic = new OnlineMusic();
                onlineMusic.pic_small = songAvator;
                onlineMusic.songAvatar = songAvator;
                onlineMusic.songId = id;
                onlineMusic.localMusicJson = GsonUtil.GsonString(music);
                onlineMusic.album_title = music.album;
                onlineMusic.artist_name = songSinger;
                onlineMusic.song_id = String.valueOf(id);
                onlineMusic.title = songName;
                onlineMusic.ting_uid = "340462058";
                new DownloadOnlineMusic(this, onlineMusic) {
                    @Override
                    public void onPrepare() {
                    }

                    @Override
                    public void onExecuteSuccess(Void aVoid) {
                        ToastUtil.showToast(getString(R.string.now_download, onlineMusic.getTitle()));
                    }

                    @Override
                    public void onExecuteFail(Exception e) {
                        ToastUtil.showToast(R.string.unable_to_download);
                    }
                }.execute();
                break;
            case R.id.bt_gift:
                if (!TaipingApplication.tpApp.getIsLogin()) {
                    LoginActivity.launch(this);
                    finish();
                    return;
                }
                GiftDialog.launch(this, (int) songBean.songId);
                break;
            case R.id.bt_share:
                if (!TaipingApplication.tpApp.getIsLogin()) {
                    LoginActivity.launch(this);
                    finish();
                    return;
                }
                ShareDialog.launch(this);
                break;
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        Logl.e("onNewIntent有没有执行。。");
        onPause();
        initAll(intent);
        updateViews(false);
    }

    @Override
    protected void onDestroy() {
        unregisterReceiver(downloadReceiver);
        super.onDestroy();
    }


    @Override
    public void onPause() {
        super.onPause();
        Logl.e("MvDetailActivity onPause");
        mVideoPlayer.releaseAllVideos();
    }

    @Override
    public void onBackPressed() {
        if (mVideoPlayer.backPress()) {
            return;
        }
        super.onBackPressed();
    }

    public static void launch(Context context, SongBean songBean) {
        Intent intent = new Intent(context, MvDetailActivity.class);
        intent.putExtra("songBean", songBean);
        context.startActivity(intent);
    }

    public class PlayDownloadReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            ivDownload.setImageResource(R.mipmap.have_download_icon);
        }
    }

}
