package com.yuntu.mawei.ui.circle.comment;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.widget.EditText;
import android.widget.TextView;

import com.yuntu.mawei.R;
import com.yuntu.mawei.base.home.BaseActivity;
import com.yuntu.mawei.bean.base.ResponseBean;
import com.yuntu.mawei.bean.circle.CircleBean;
import com.yuntu.mawei.bean.rx.SuccessSubscriber;
import com.yuntu.mawei.util.RxUtils;
import com.yuntu.mawei.util.ToastUtil;
import com.yuntu.mawei.util.http.HttpUtil;

import butterknife.BindView;
import butterknife.OnClick;

public class CommentActivity extends BaseActivity {
    @BindView(R.id.title_right)
    TextView tvRigt;
    @BindView(R.id.et_comment)
    EditText etComment;
    CircleBean circleBean;

    @Override
    public int attachLayout() {
        return R.layout.activity_comment;
    }

    @Override
    public void init() {
        initTitle("评论");
        tvRigt.setText("发布");
        circleBean = (CircleBean) getIntent().getSerializableExtra("circleBean");
    }

    @OnClick(R.id.title_right)
    public void publishComment() {
        if (TextUtils.isEmpty(etComment.getText())) {
            ToastUtil.showToast("评论不能为空");
            return;
        }
        HttpUtil.getInstance().getApiService().comment(circleBean.themeId
                , etComment.getText().toString())
                .compose(RxUtils.rxSchedulerHelper())
                .subscribe(new SuccessSubscriber<ResponseBean>() {
                    @Override
                    public void onSuccess(ResponseBean responseBean) {
                        ToastUtil.showToast(responseBean.getMessage());
                        finish();
                    }
                    @Override
                    public void onFailed(String str) {
                        ToastUtil.showToast(str);
                    }
                });

    }

    public static void launch(Context context, CircleBean circleBean) {
        Intent intent = new Intent(context, CommentActivity.class);
        intent.putExtra("circleBean", circleBean);
        context.startActivity(intent);
    }
}
