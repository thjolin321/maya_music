package com.yuntu.mawei.ui.homepage.singer;

import com.yuntu.mawei.R;
import com.yuntu.mawei.base.home.BaseListFragment;
import com.yuntu.mawei.base.home.BaseListFragmentPresenter;
import com.yuntu.mawei.bean.base.ResponseBean;
import com.yuntu.mawei.bean.house.MusicHouseBean;
import com.yuntu.mawei.bean.rx.SuccessSubscriber;
import com.yuntu.mawei.bean.song.SingerBean;
import com.yuntu.mawei.util.RxUtils;
import com.yuntu.mawei.util.http.HttpUtil;

import java.util.List;

public class SingerPresenter extends BaseListFragmentPresenter {

    int type, page;

    public SingerPresenter(BaseListFragment mView, int type) {
        super(mView);
        this.type = type;
    }

    @Override
    public void getData(boolean isPullRefresh) {

        HttpUtil.getInstance().getApiService()
                .getSingerList(1, 1000)
                .compose(RxUtils.rxSchedulerHelper())
                .subscribe(new SuccessSubscriber<ResponseBean<List<SingerBean>>>() {
                    @Override
                    public void onSuccess(ResponseBean<List<SingerBean>> listResponseBean) {
                        mView.loadData(listResponseBean.getResult());
                        page = 2;
                    }

                    @Override
                    public void onFailed(String str) {
                    }
                });

    }

    @Override
    public void getMoreData() {
        HttpUtil.getInstance().getApiService()
                .getSingerList(page, 1000)
                .compose(RxUtils.rxSchedulerHelper())
                .subscribe(new SuccessSubscriber<ResponseBean<List<SingerBean>>>() {
                    @Override
                    public void onSuccess(ResponseBean<List<SingerBean>> listResponseBean) {
                        if (listResponseBean.getResult() == null) return;
                        mView.loadMoreData(listResponseBean.getResult());
                        page++;
                    }

                    @Override
                    public void onFailed(String str) {
                    }
                });
    }
}
