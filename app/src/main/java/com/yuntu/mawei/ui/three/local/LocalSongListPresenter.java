package com.yuntu.mawei.ui.three.local;

import com.yuntu.mawei.base.home.BaseListFragment;
import com.yuntu.mawei.base.home.BaseListFragmentPresenter;
import com.yuntu.mawei.bean.music.Music;
import com.yuntu.mawei.bean.music.MusicLocal;
import com.yuntu.mawei.bean.song.SingerBean;
import com.yuntu.mawei.bean.song.SongBean;
import com.yuntu.mawei.util.Logl;
import com.yuntu.mawei.util.db.DBManager;
import com.yuntu.mawei.util.music.MusicUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tanghao on 2019/1/25
 */
public class LocalSongListPresenter extends BaseListFragmentPresenter {

    int type;

    public LocalSongListPresenter(BaseListFragment mView, int type) {
        super(mView);
        this.type = type;
    }

    @Override
    public void getData(boolean isPullRefresh) {
        Logl.e("LocalSongListPresenter type: " + type);
        List<MusicLocal> listLocal = DBManager.get().getMusicLocalDao().queryBuilder().list();
        List<Music> listMusic = MusicUtil.sortMusicByZimu(type, listLocal);
        List<SongBean> list = new ArrayList<>();
        for (Music music : listMusic) {
            SongBean songBean = new SongBean(music.itemType);
            songBean.zimuTitle = music.firstZimu;
            songBean.singer = new SingerBean();
            songBean.singer.singerName = music.artist;
            songBean.albumIntroduction = music.album;
            songBean.songName = music.title;
            songBean.musicLocal = music;
            list.add(songBean);
        }
        int count = 0;
        for (SongBean songBean : list) {
            if (songBean.getItemType() == 0) {
                count++;
            }
        }
        mView.loadData(list);
        LocalMusicActivity activity = ((LocalMusicActivity) (mView.getActivity()));
        if (activity != null) {
            if (type == 0) {
                activity.songNum = count;
            } else if (type == 1) {
                activity.ringNum = count;
            } else if (type == 2) {
                activity.mvNum = count;
            }
            activity.initIndicator();
        }
    }

    @Override
    public void getMoreData() {
        mView.adapter.loadComplete();
        mView.adapter.noMoreData();
    }
}
