package com.yuntu.mawei.ui.main;

/**
 * @author feng
 * @Date 2017/11/13.
 */

public interface FragmentBackInterface {
    boolean onBackPressed();

}
