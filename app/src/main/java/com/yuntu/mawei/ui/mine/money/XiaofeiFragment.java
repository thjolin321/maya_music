package com.yuntu.mawei.ui.mine.money;

import com.yuntu.mawei.R;
import com.yuntu.mawei.base.home.BaseListFragment;
import com.yuntu.mawei.base.home.BaseListFragmentPresenter;
import com.yuntu.mawei.view.adapter.base.BaseQuickAdapter;

public class XiaofeiFragment extends BaseListFragment {


    @Override
    protected void initViews() {

    }

    @Override
    protected int attachLayoutRes() {
        return R.layout.recyclerview_just;
    }

    @Override
    protected BaseQuickAdapter attachAdapter() {
        return new XiaofeiAdapter(getActivity());
    }

    @Override
    protected BaseListFragmentPresenter attachPresenter() {
        return new XiaofeiPresenter(this);
    }
}
