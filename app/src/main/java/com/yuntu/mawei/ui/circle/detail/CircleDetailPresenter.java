package com.yuntu.mawei.ui.circle.detail;

import com.yuntu.mawei.base.home.BaseListActivity;
import com.yuntu.mawei.base.home.BaseListActivityPresenter;
import com.yuntu.mawei.bean.base.ResponseBean;
import com.yuntu.mawei.bean.circle.CommentBean;
import com.yuntu.mawei.bean.rx.SuccessSubscriber;
import com.yuntu.mawei.util.RxUtils;
import com.yuntu.mawei.util.http.HttpUtil;

import java.util.ArrayList;
import java.util.List;

public class CircleDetailPresenter extends BaseListActivityPresenter {

    int id;
    CircleDetailActivity activity;

    public CircleDetailPresenter(CircleDetailActivity mView, int id) {
        super(mView);
        this.id = id;
        this.activity = mView;
    }

    @Override
    public void getData(boolean isPullRefresh) {
        List<CircleDetailBean> listCircle = new ArrayList<>();
        CircleDetailBean bean = new CircleDetailBean(CircleDetailBean.ITEM_WEB);
        listCircle.add(bean);
        mView.loadData(listCircle);
        listCircle.clear();
        HttpUtil.getInstance().getApiService().getCircleDetail(id)
                .compose(RxUtils.rxSchedulerHelper())
                .subscribe(new SuccessSubscriber<ResponseBean<CircleDetailRoot>>() {
                    @Override
                    public void onSuccess(ResponseBean<CircleDetailRoot> circleDetailRootResponseBean) {
                        CircleDetailRoot circleDetailRoot = circleDetailRootResponseBean.getResult();
                        if (circleDetailRoot != null && circleDetailRoot.listApply != null
                                && circleDetailRoot.listApply.size() != 0) {
                            CircleDetailBean beanTitle = new CircleDetailBean(CircleDetailBean.ITEM_XIAN);
                            listCircle.add(beanTitle);
                            for (CommentBean bean : circleDetailRoot.listApply) {
                                CircleDetailBean beanDetail = new CircleDetailBean(CircleDetailBean.ITEM_COMMENT);
                                beanDetail.commentBean = bean;
                                listCircle.add(beanDetail);
                            }
                            activity.circleBean.commentCount = listCircle.size() - 1;
                            activity.adapterDetail.circleBean.commentCount = listCircle.size() - 1;
                            activity.initCommentCount(listCircle.size() - 1);
                            mView.loadMoreData(listCircle);
                        } else {
                            CircleDetailBean beanTitle = new CircleDetailBean(CircleDetailBean.ITEM_XIAN);
                            listCircle.add(beanTitle);
                            activity.circleBean.commentCount = 0;
                            activity.adapterDetail.circleBean.commentCount = 0;
                            activity.initCommentCount(0);
                            mView.loadMoreData(listCircle);
                        }
                    }

                    @Override
                    public void onFailed(String str) {
                        CircleDetailBean beanTitle = new CircleDetailBean(CircleDetailBean.ITEM_XIAN);
                        listCircle.add(beanTitle);
                        mView.loadMoreData(listCircle);
                    }
                });
    }

    @Override
    public void getMoreData() {
        mView.adapter.noMoreData();
        mView.adapter.loadComplete();
    }
}
