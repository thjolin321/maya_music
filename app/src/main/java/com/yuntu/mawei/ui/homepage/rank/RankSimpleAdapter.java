package com.yuntu.mawei.ui.homepage.rank;

import android.content.Context;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;

import com.yuntu.mawei.R;
import com.yuntu.mawei.bean.song.SongBean;
import com.yuntu.mawei.ui.three.piliang.PiliangActivity;
import com.yuntu.mawei.util.music.MusicUtil;
import com.yuntu.mawei.view.adapter.base.BaseQuickAdapter;
import com.yuntu.mawei.view.adapter.base.BaseViewHolder;

public class RankSimpleAdapter extends BaseQuickAdapter<SongBean> {

    int type; // 0 for rank,1 for sing song;

    public RankSimpleAdapter(Context context, int type) {
        super(context);
        this.type = type;
        View view = LayoutInflater.from(context).inflate(R.layout.adapter_song_simple_title, null);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PiliangActivity.launch(context, getData(), 4);
            }
        });
        addHeaderView(view);
    }

    @Override
    protected int attachLayoutRes() {
        return R.layout.adapter_song_rank_list;
    }

    @Override
    protected void convert(BaseViewHolder holder, SongBean item) {
        if (item.singer != null) {
            holder.setText(R.id.tv_artist_and_album, item.singer.singerName + " - 《" + (item.album != null ? item.album.albumName : "") + "》");
        }
        holder.setText(R.id.tv_song, item.songName);
        ImageView ivRank = holder.getView(R.id.iv_rank_icon);
        ivRank.setVisibility(View.GONE);
        if (type == 0) {
            switch (holder.getAdapterPosition() - 1) {
                case 0:
                    ivRank.setVisibility(View.VISIBLE);
                    ivRank.setImageResource(R.mipmap.rank_icon_1);
                    break;
                case 1:
                    ivRank.setVisibility(View.VISIBLE);
                    ivRank.setImageResource(R.mipmap.rank_icon_2);
                    break;
                case 2:
                    ivRank.setVisibility(View.VISIBLE);
                    ivRank.setImageResource(R.mipmap.rank_icon_3);
                    break;
                default:
                    holder.setText(R.id.tv_rank, (holder.getAdapterPosition() + 1) + "");
                    break;
            }
        } else {
            holder.setText(R.id.tv_rank, (holder.getAdapterPosition() + 1) + "");
        }
        holder.getConvertView().setOnClickListener(v -> {
            MusicUtil.playMusic(mContext, item);
        });
    }
}
