package com.yuntu.mawei.ui.mine.set;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.TextView;

import com.yuntu.mawei.R;
import com.yuntu.mawei.base.home.BaseActivity;
import com.yuntu.mawei.ui.login.ChangePhoneActivity;
import com.yuntu.mawei.ui.login.LoginActivity;
import com.yuntu.mawei.ui.mine.set.message.MyMessageActivity;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by tanghao on 2019/1/24
 */
public class SettingActivity extends BaseActivity {

    @BindView(R.id.tv_net_set)
    TextView tvNetSet;

    @Override
    public int attachLayout() {
        return R.layout.activity_setting;
    }

    @Override
    public void init() {
        initTitle(R.string.set);
    }

    @OnClick({R.id.bt_my_message, R.id.bt_net_set, R.id.bt_exit_login, R.id.bt_password_resetting, R.id.bt_change_phone})
    public void onSetClick(View view) {
        switch (view.getId()) {
            case R.id.bt_my_message:
                MyMessageActivity.launch(SettingActivity.this);
                break;
            case R.id.bt_net_set:
                NetWorkSettingActivity.launch(SettingActivity.this);
                break;
            case R.id.bt_password_resetting:
                PassWordResettingActivity.launch(SettingActivity.this);
                break;
            case R.id.bt_exit_login:
                LoginActivity.launch(SettingActivity.this);
                finish();
                break;
            case R.id.bt_change_phone:
                ChangePhoneActivity.launch(this);
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == 0x321) {
            LoginActivity.launch(this);
            finish();
        }
    }

    public static void launch(Context context) {
        context.startActivity(new Intent(context, SettingActivity.class));
    }

}
