package com.yuntu.mawei.ui.three.recent;

import com.yuntu.mawei.R;
import com.yuntu.mawei.base.home.BaseListFragment;
import com.yuntu.mawei.base.home.BaseListFragmentPresenter;
import com.yuntu.mawei.ui.three.SongSimpleListAdapter;
import com.yuntu.mawei.view.adapter.base.BaseQuickAdapter;

public class RecentRingListFragment extends BaseListFragment {

    
    @Override
    protected void initViews() {

    }

    @Override
    protected int attachLayoutRes() {
        return R.layout.recyclerview_just;
    }

    @Override
    protected BaseQuickAdapter attachAdapter() {
        return new SongSimpleListAdapter(getActivity(),3);
    }

    @Override
    protected BaseListFragmentPresenter attachPresenter() {
        return new RecentSongListPresenter(this,1);
    }
}
