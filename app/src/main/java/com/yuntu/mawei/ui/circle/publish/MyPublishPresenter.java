package com.yuntu.mawei.ui.circle.publish;

import com.yuntu.mawei.base.home.BaseListActivity;
import com.yuntu.mawei.base.home.BaseListActivityPresenter;
import com.yuntu.mawei.bean.base.ResponseBean;
import com.yuntu.mawei.bean.circle.CircleBean;
import com.yuntu.mawei.bean.rx.SuccessSubscriber;
import com.yuntu.mawei.util.RxUtils;
import com.yuntu.mawei.util.http.HttpUtil;

import java.util.ArrayList;
import java.util.List;

public class MyPublishPresenter extends BaseListActivityPresenter {

    int page;

    public MyPublishPresenter(BaseListActivity mView) {
        super(mView);
    }

    @Override
    public void getData(boolean isPullRefresh) {
        HttpUtil.getInstance().getApiService().getMyPublishCircle(1, 10)
                .compose(RxUtils.rxSchedulerHelper())
            .subscribe(new SuccessSubscriber<ResponseBean<List<CircleBean>>>() {
        @Override
        public void onSuccess(ResponseBean<List<CircleBean>> listResponseBean) {
            if (listResponseBean.getResult() == null) return;
            mView.loadData(listResponseBean.getResult());
            page = 2;
        }

        @Override
        public void onFailed(String str) {
        }
    });
}

    @Override
    public void getMoreData() {
        HttpUtil.getInstance().getApiService().getMyPublishCircle(page, 10)
                .compose(RxUtils.rxSchedulerHelper())
                .subscribe(new SuccessSubscriber<ResponseBean<List<CircleBean>>>() {
                    @Override
                    public void onSuccess(ResponseBean<List<CircleBean>> listResponseBean) {
                        if (listResponseBean.getResult() == null) return;
                        mView.loadMoreData(listResponseBean.getResult());
                        page++;
                    }
                    @Override
                    public void onFailed(String str) {
                    }
                });
    }
}
