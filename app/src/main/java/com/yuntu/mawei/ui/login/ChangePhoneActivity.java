package com.yuntu.mawei.ui.login;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.CountDownTimer;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.yuntu.mawei.R;
import com.yuntu.mawei.base.TaipingApplication;
import com.yuntu.mawei.base.home.BaseActivity;
import com.yuntu.mawei.bean.base.ResponseBean;
import com.yuntu.mawei.bean.rx.SuccessSubscriber;
import com.yuntu.mawei.ui.playing.service.AudioPlayer;
import com.yuntu.mawei.util.RxUtils;
import com.yuntu.mawei.util.SharedPreferenceUtil;
import com.yuntu.mawei.util.ToastUtil;
import com.yuntu.mawei.util.http.HttpUtil;

import butterknife.BindView;
import butterknife.OnClick;

public class ChangePhoneActivity extends BaseActivity {

    @BindView(R.id.et_phone)
    TextView etPhone;
    @BindView(R.id.et_yanzheng)
    EditText etYanzheng;
    @BindView(R.id.et_yanzheng_new)
    EditText etYanzhengNew;
    @BindView(R.id.bt_yanzheng)
    TextView tvCountTime;
    @BindView(R.id.bt_yanzheng_new)
    TextView tvCountTimeNew;
    @BindView(R.id.et_new)
    EditText etNew;


    @Override
    public int attachLayout() {
        return R.layout.activity_change_phone;
    }

    @Override
    public void init() {
        initTitle("手机号修改");
        etNew.setEnabled(false);
        etPhone.setText(SharedPreferenceUtil.getInstance().getString("phone", ""));
        etYanzhengNew.setEnabled(false);
    }

    @OnClick({R.id.bt_sure, R.id.bt_yanzheng, R.id.bt_yanzheng_new})
    public void onSure(View v) {
        switch (v.getId()) {
            case R.id.bt_sure:
                if (etPhone.getText().toString().length() != 11) {
                    ToastUtil.showToast(R.string.input_phone_right);
                    return;
                }
                if (TextUtils.isEmpty(etNew.getText())) {
                    ToastUtil.showToast("请输入手机号");
                    return;
                }
                if (etNew.getText().toString().length() != 11) {
                    ToastUtil.showToast(R.string.input_phone_right);
                    return;
                }

                if (TextUtils.isEmpty(etYanzheng.getText())) {
                    ToastUtil.showToast(R.string.input_vertify);
                    return;
                }
                if (TextUtils.isEmpty(etYanzhengNew.getText())) {
                    ToastUtil.showToast(R.string.input_vertify);
                    return;
                }
                HttpUtil.getInstance().getApiService().updatePhone(etNew.getText().toString(), etPhone.getText().toString(),
                        etYanzhengNew.getText().toString(), etYanzheng.getText().toString())
                        .compose(RxUtils.rxSchedulerHelper())
                        .subscribe(new SuccessSubscriber<ResponseBean>() {
                            @Override
                            public void onSuccess(ResponseBean responseBean) {
                                ToastUtil.showToast(responseBean.getMessage());
                                setResult(0x256);
                                TaipingApplication.tpApp.updatePhone(etNew.getText().toString());
                                AudioPlayer.get().changUserPhone();
                                finish();
                            }

                            @Override
                            public void onFailed(String str) {
                                ToastUtil.showToast(str);
                            }
                        });
                break;
            case R.id.bt_yanzheng:
                sendMessage();
                break;
            case R.id.bt_yanzheng_new:
                sendMessageNew();
                break;
        }
    }


    private void sendMessage() {
        if (TextUtils.isEmpty(etPhone.getText())) {
            ToastUtil.showToast("请输入手机号码");
            return;
        }
        String phone = etPhone.getText().toString().trim().replace(" ", "");

        if (phone.length() != 11) {
            ToastUtil.showToast("请验证手机号码是否正确");
            return;
        }
        etNew.setEnabled(true);
        etYanzhengNew.setEnabled(true);
        HttpUtil.getInstance().getApiService().sendMessage(etPhone.getText().toString())
                .compose(RxUtils.rxSchedulerHelper())
                .subscribe(new SuccessSubscriber<ResponseBean>() {
                    @Override
                    public void onSuccess(ResponseBean responseBean) {
                        ToastUtil.showToast("发送验证码成功");
                        countDown();
                    }

                    @Override
                    public void onFailed(String str) {
                        ToastUtil.showToast(str);
                    }
                });
    }

    private void sendMessageNew() {
        if (TextUtils.isEmpty(etNew.getText())) {
            ToastUtil.showToast("请输入手机号码");
            return;
        }
        String phone = etNew.getText().toString().trim().replace(" ", "");

        if (phone.length() != 11) {
            ToastUtil.showToast("请验证手机号码是否正确");
            return;
        }

        HttpUtil.getInstance().getApiService().sendMessage(etNew.getText().toString())
                .compose(RxUtils.rxSchedulerHelper())
                .subscribe(new SuccessSubscriber<ResponseBean>() {
                    @Override
                    public void onSuccess(ResponseBean responseBean) {
                        ToastUtil.showToast("发送验证码成功");
                        countDownNew();
                    }

                    @Override
                    public void onFailed(String str) {
                        ToastUtil.showToast(str);
                    }
                });
    }

    CountDownTimer timer;

    private void countDown() {
        timer = new CountDownTimer(60000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                tvCountTime.setTextColor(Color.parseColor("#8C8C8C"));
                tvCountTime.setEnabled(false);
                tvCountTime.setText((millisUntilFinished / 1000) + "秒后重发");
            }

            @Override
            public void onFinish() {
                tvCountTime.setTextColor(Color.parseColor("#E8413B"));
                tvCountTime.setEnabled(true);
                tvCountTime.setText("重发验证码");
            }
        };
        timer.start();
    }

    CountDownTimer timerNew;

    private void countDownNew() {
        timerNew = new CountDownTimer(60000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                tvCountTimeNew.setTextColor(Color.parseColor("#8C8C8C"));
                tvCountTimeNew.setEnabled(false);
                tvCountTimeNew.setText((millisUntilFinished / 1000) + "秒后重发");
            }

            @Override
            public void onFinish() {
                tvCountTimeNew.setTextColor(Color.parseColor("#E8413B"));
                tvCountTimeNew.setEnabled(true);
                tvCountTimeNew.setText("重发验证码");
            }
        };
        timerNew.start();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
        if (timerNew != null) {
            timerNew.cancel();
            timerNew = null;
        }
    }

    public static void launch(Context context) {
        Intent intent = new Intent(context, ChangePhoneActivity.class);
        ((Activity) context).startActivityForResult(intent, 0x652);
    }

}
