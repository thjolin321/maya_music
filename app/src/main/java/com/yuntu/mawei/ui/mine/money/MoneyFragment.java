package com.yuntu.mawei.ui.mine.money;

import android.view.View;
import android.widget.TextView;

import com.yuntu.mawei.R;
import com.yuntu.mawei.base.home.BaseFragment;
import com.yuntu.mawei.bean.base.ResponseBean;
import com.yuntu.mawei.bean.rx.SuccessSubscriber;
import com.yuntu.mawei.ui.login.MyWebViewActivity;
import com.yuntu.mawei.ui.mine.user.UserBean;
import com.yuntu.mawei.util.BigDeUtil;
import com.yuntu.mawei.util.RxUtils;
import com.yuntu.mawei.util.http.HttpUtil;

import butterknife.BindView;
import butterknife.OnClick;

public class MoneyFragment extends BaseFragment {

    @BindView(R.id.tv_money)
    TextView tvMoney;

    @Override
    public int getLayoutId() {
        return R.layout.fragment_money;
    }

    @Override
    public void initView(View view) {
    }

    @Override
    public void onResume() {
        super.onResume();
        HttpUtil.getInstance().getApiService().getUserInfo().compose(RxUtils.rxSchedulerHelper())
                .subscribe(new SuccessSubscriber<ResponseBean<UserBean>>() {
                    @Override
                    public void onSuccess(ResponseBean<UserBean> userBeanResponseBean) {
                        if (userBeanResponseBean.getResult() == null) return;
                        tvMoney.setText(BigDeUtil.div(userBeanResponseBean.getResult().appUserMoney, 100.0) + "");
                    }

                    @Override
                    public void onFailed(String str) {
                    }
                });
    }


    @OnClick(R.id.bt_sure)
    public void onChongzhi() {
        ChongzhiActiviy.launch(getActivity());
    }

    @OnClick(R.id.bt_shuoming)
    public void onShuoming() {
        MyWebViewActivity.launch(getActivity(), 5);
    }

}
