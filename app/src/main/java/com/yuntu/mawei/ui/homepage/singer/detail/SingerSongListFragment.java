package com.yuntu.mawei.ui.homepage.singer.detail;

import android.os.Bundle;

import com.yuntu.mawei.R;
import com.yuntu.mawei.base.home.BaseListFragment;
import com.yuntu.mawei.base.home.BaseListFragmentPresenter;
import com.yuntu.mawei.ui.homepage.rank.RankSimpleAdapter;
import com.yuntu.mawei.ui.three.MvSimpleListAdapter;
import com.yuntu.mawei.ui.three.SongSimpleListAdapter;
import com.yuntu.mawei.ui.three.collect.CollectSongListPresenter;
import com.yuntu.mawei.view.adapter.base.BaseQuickAdapter;

public class SingerSongListFragment extends BaseListFragment {

    int type; // 0 for song,1 for album,2 for mv
    int singerId;

    public static SingerSongListFragment newInstance(int singerId, int type) {
        SingerSongListFragment fragment = new SingerSongListFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("type", type);
        bundle.putInt("singerId", singerId);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    protected void initViews() {

    }

    @Override
    protected int attachLayoutRes() {
        return R.layout.recyclerview_just;
    }

    @Override
    protected BaseQuickAdapter attachAdapter() {
        type = getArguments().getInt("type");
        singerId = getArguments().getInt("singerId");
        switch (type) {
            case 0:
                return new SingerSimpleListAdapter(getActivity());
            case 1:
                return new AlbumSimpleListAdapter(getActivity());
            case 2:
                return new MvSimpleListAdapter(getActivity(), 2);
            default:
                return new SingerSimpleListAdapter(getActivity());
        }
    }

    @Override
    protected BaseListFragmentPresenter attachPresenter() {
        return new SingerSongListPresenter(this, singerId, type);
    }

    boolean isSecondExecute;

    @Override
    public void onResume() {
        super.onResume();
        if (isSecondExecute && getUserVisibleHint()) {
            updateViews(false);
        }
        isSecondExecute = true;
    }
}
