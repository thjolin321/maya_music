package com.yuntu.mawei.ui.mine.set.message;

import android.content.Context;

import com.yuntu.mawei.R;
import com.yuntu.mawei.view.adapter.base.BaseQuickAdapter;
import com.yuntu.mawei.view.adapter.base.BaseViewHolder;

import java.text.SimpleDateFormat;

public class MyMessageAdapter extends BaseQuickAdapter<MessageBean> {

    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

    public MyMessageAdapter(Context context) {
        super(context);
    }

    @Override
    protected int attachLayoutRes() {
        return R.layout.adapter_message;
    }

    @Override
    protected void convert(BaseViewHolder holder, MessageBean item) {
        holder.setText(R.id.tv_title, item.massageInfo);
        holder.setText(R.id.tv_time, df.format(item.createTime));
    }
}
