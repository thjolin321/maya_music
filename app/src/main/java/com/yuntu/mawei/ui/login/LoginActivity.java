package com.yuntu.mawei.ui.login;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import com.umeng.socialize.UMAuthListener;
import com.umeng.socialize.UMShareAPI;
import com.umeng.socialize.bean.SHARE_MEDIA;
import com.yuntu.mawei.R;
import com.yuntu.mawei.base.TaipingApplication;
import com.yuntu.mawei.base.home.BaseActivity;
import com.yuntu.mawei.bean.base.ResponseBean;
import com.yuntu.mawei.bean.rx.SuccessSubscriber;
import com.yuntu.mawei.ui.main.MainActivity;
import com.yuntu.mawei.ui.playing.service.AudioPlayer;
import com.yuntu.mawei.util.Logl;
import com.yuntu.mawei.util.RxUtils;
import com.yuntu.mawei.util.SharedPreferenceUtil;
import com.yuntu.mawei.util.ToastUtil;
import com.yuntu.mawei.util.http.HttpUtil;
import com.yuntu.mawei.view.loading.LoadingDialog;

import org.bitlet.weupnp.Main;

import java.util.Map;

import butterknife.BindView;
import butterknife.OnClick;

public class LoginActivity extends BaseActivity {

    @BindView(R.id.et_input_phone)
    EditText etPhone;
    @BindView(R.id.et_input_password)
    EditText etPassword;
    @BindView(R.id.bt_password_see)
    ImageView ivSee;
    @BindView(R.id.bt_login)
    View viewLogin;

    private boolean showPassword;


    @Override
    public int attachLayout() {
        return R.layout.activity_login;
    }

    @Override
    public void init() {
        etPhone.addTextChangedListener(textWatcher);
        etPassword.addTextChangedListener(textWatcher);
        AudioPlayer.get().pausePlayer();
        TaipingApplication.tpApp.setToken("");
        TaipingApplication.tpApp.setPhone("");
        TaipingApplication.tpApp.isVIP = false;
        SharedPreferenceUtil.getInstance().putString("phone", "");
    }

    @OnClick({R.id.bt_login, R.id.bt_password_see, R.id.bt_forget, R.id.bt_no_user, R.id.bt_weixin_login})
    public void onLoginClick(View view) {
        switch (view.getId()) {
            case R.id.bt_no_user:
                RegisterActivity.launch(LoginActivity.this);
                break;
            case R.id.bt_login:
                login();
                break;
            case R.id.bt_forget:
                ForgetPasswordActivity.launch(LoginActivity.this);
                break;
            case R.id.bt_password_see:
                if (showPassword) {
                    showPassword = false;
                } else {
                    showPassword = true;
                }
                if (showPassword) {
                    ivSee.setImageResource(R.mipmap.password_unsee);
                    etPassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                } else {
                    ivSee.setImageResource(R.mipmap.password_see);
                    etPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
                }
                break;
            case R.id.bt_weixin_login:
                loginWeixin();
                break;
        }
    }

    private void loginWeixin() {
        UMShareAPI.get(this).getPlatformInfo(LoginActivity.this, SHARE_MEDIA.WEIXIN, authListener);
    }

    UMAuthListener authListener = new UMAuthListener() {
        @Override
        public void onStart(SHARE_MEDIA platform) {
            Logl.e("onStart");
        }

        @Override
        public void onComplete(SHARE_MEDIA platform, int action, Map<String, String> data) {
            String unionid = data.get("unionid");
            Logl.e("有调用 onComplete?");
            HttpUtil.getInstance().getApiService().wxLogin("", "", "").compose(RxUtils.rxSchedulerHelper())
                    .subscribe(new SuccessSubscriber<ResponseBean>() {
                        @Override
                        public void onSuccess(ResponseBean responseBean) {
                            BindActivity.launch(LoginActivity.this, unionid);
                        }

                        @Override
                        public void onFailed(String str) {
                        }
                    });
        }

        @Override
        public void onError(SHARE_MEDIA platform, int action, Throwable t) {
            Logl.e("onError: " + t.getMessage());
            if (t.getMessage() != null) {
                ToastUtil.showToast(t.getMessage());
            }
        }

        @Override
        public void onCancel(SHARE_MEDIA platform, int action) {
            Logl.e("onCancel");
        }
    };


    private void login() {
        if (TextUtils.isEmpty(etPhone.getText())) {
            ToastUtil.showToast(R.string.input_phone);
            return;
        }
        if (TextUtils.isEmpty(etPassword.getText())) {
            ToastUtil.showToast(R.string.input_password);
            return;
        }
        LoadingDialog loadingDialog = new LoadingDialog(LoginActivity.this);
        HttpUtil.getInstance().getApiService()
                .login(etPhone.getText().toString(), etPassword.getText().toString())
                .compose(RxUtils.rxSchedulerHelper()).subscribe(new SuccessSubscriber<ResponseBean<TokenBean>>() {
            @Override
            public void onSuccess(ResponseBean<TokenBean> responseBean) {
                loadingDialog.dismiss();
                if (responseBean.getResult() == null) return;  // 后台bug，后台居然不返回token
                TaipingApplication.tpApp.setToken(responseBean.getResult().token);
                TaipingApplication.tpApp.setPhone(etPhone.getText().toString());
                MainActivity.launch(LoginActivity.this);
                finish();
            }

            @Override
            public void onFailed(String str) {
                loadingDialog.dismiss();
                ToastUtil.showToast(str);
            }
        });
    }

    TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }

        @Override
        public void afterTextChanged(Editable s) {
            if (!TextUtils.isEmpty(etPhone.getText()) && !TextUtils.isEmpty(etPassword.getText())) {
                viewLogin.setBackgroundResource(R.drawable.bg_red_cornor_big);
            } else {
                viewLogin.setBackgroundResource(R.drawable.bg_grey_cornor_big);
            }
        }
    };

    public static void launch(Context context) {
        Intent intent = new Intent(context, LoginActivity.class);
        context.startActivity(intent);
    }


}
