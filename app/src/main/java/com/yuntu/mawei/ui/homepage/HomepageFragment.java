package com.yuntu.mawei.ui.homepage;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.bigkoo.convenientbanner.holder.CBViewHolderCreator;
import com.bigkoo.convenientbanner.listener.OnItemClickListener;
import com.yuntu.mawei.R;
import com.yuntu.mawei.base.home.BaseFragment;
import com.yuntu.mawei.bean.banner.BannerBean;
import com.yuntu.mawei.bean.base.ResponseBean;
import com.yuntu.mawei.bean.rx.SuccessSubscriber;
import com.yuntu.mawei.bean.song.HaveCollectBean;
import com.yuntu.mawei.bean.song.SongBean;
import com.yuntu.mawei.ui.homepage.adapter.MvAdapter;
import com.yuntu.mawei.ui.homepage.adapter.SongAdapter;
import com.yuntu.mawei.ui.homepage.mv.MvActivity;
import com.yuntu.mawei.ui.homepage.mv.detail.MvDetailActivity;
import com.yuntu.mawei.ui.homepage.rank.RankActivity;
import com.yuntu.mawei.ui.homepage.ring.RingActivity;
import com.yuntu.mawei.ui.homepage.singer.SingerActivity;
import com.yuntu.mawei.ui.house.more.HouseMoreActivity;
import com.yuntu.mawei.ui.house.music.SongPagerActivity;
import com.yuntu.mawei.ui.login.BannerWebViewActivity;
import com.yuntu.mawei.ui.search.SearchActivity;
import com.yuntu.mawei.util.RxUtils;
import com.yuntu.mawei.util.ToastUtil;
import com.yuntu.mawei.util.http.HttpUtil;
import com.yuntu.mawei.util.music.MusicUtil;
import com.yuntu.mawei.view.NetworkImageHolderView;
import com.yuntu.mawei.view.adapter.base.BaseQuickAdapter;
import com.yuntu.mawei.view.adapter.helper.RecyclerViewHelper;
import com.yuntu.mawei.widget.MallConvenientBanner;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by tanghao on 2019/1/21
 */
public class HomepageFragment extends BaseFragment {

    @BindView(R.id.homepage_banner)
    MallConvenientBanner banner;
    @BindView(R.id.rv_mv)
    RecyclerView rvMv;
    @BindView(R.id.rv_song)
    RecyclerView rvSong;
    @BindView(R.id.rv_ring)
    RecyclerView rvRing;
    private HomepagePresenter presenter;

    BaseQuickAdapter mvAdapter, songAdapter, ringAdapter;


    @Override
    public int getLayoutId() {
        return R.layout.homepage_fragment;
    }

    @Override
    public void initView(View view) {
        presenter = new HomepagePresenter(this);
        initBanner();
        initMv();
        initSong();
    }

    private void initMv() {
        if (mvAdapter == null) mvAdapter = new MvAdapter(mContext);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext);
        linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        rvMv.setLayoutManager(linearLayoutManager);
        rvMv.setAdapter(mvAdapter);
        rvMv.setNestedScrollingEnabled(false);
    }

    private void initSong() {
        if (songAdapter == null) songAdapter = new SongAdapter(mContext);
        if (ringAdapter == null) ringAdapter = new SongAdapter(mContext);
        RecyclerViewHelper.initRecyclerViewG(getActivity(), rvSong, songAdapter, 3);
        RecyclerViewHelper.initRecyclerViewG(getActivity(), rvRing, ringAdapter, 3);
        presenter.getHotSong();
    }

    public void showSong(List<SongBean> songList) {
        if (songList == null) {
            return;
        }
        if (songList.size() >= 3) {
            songList = songList.subList(0, 3);
        }
        songAdapter.addItems(songList);
    }

    public void showRing(List<SongBean> songList) {
        if (songList == null) {
            return;
        }
        if (songList.size() >= 3) {
            songList = songList.subList(0, 3);
        }
        ringAdapter.addItems(songList);
    }

    public void showMv(List<SongBean> songList) {
        if (songList == null) {
            return;
        }
        mvAdapter.addItems(songList);
    }


    List<BannerBean> imgs = new ArrayList<>();
    private int[] bannerSelect = new int[]{R.drawable.dot_unselected, R.drawable.dot_selected};

    private void initBanner() {
        HttpUtil.getInstance().getApiService().getBanner(0).compose(RxUtils.rxSchedulerHelper()).subscribe(new SuccessSubscriber<ResponseBean<List<BannerBean>>>() {
            @Override
            public void onSuccess(ResponseBean<List<BannerBean>> listResponseBean) {
                if (listResponseBean.getResult() == null || listResponseBean.getResult().size() == 0)
                    return;
                imgs.addAll(listResponseBean.getResult());
                banner.setPages(new CBViewHolderCreator<NetworkImageHolderView>() {
                    @Override
                    public NetworkImageHolderView createHolder() {
                        return new NetworkImageHolderView();
                    }
                }, imgs).setPageIndicator(bannerSelect)
                        .setPageIndicatorAlign(MallConvenientBanner.PageIndicatorAlign.CENTER_HORIZONTAL);
                banner.startTurning(4000);
                banner.setOnItemClickListener(new OnItemClickListener() {
                    @Override
                    public void onItemClick(int position) {
                        if (listResponseBean.getResult().get(position).songId == 0) {
                            BannerWebViewActivity.launch(mContext, listResponseBean.getResult().get(position).bannerUrl,listResponseBean.getResult().get(position).bannerName);
                        } else {
                            HttpUtil.getInstance().getApiService().getIsCollect(listResponseBean.getResult().get(position).songId)
                                    .compose(RxUtils.rxSchedulerHelper()).subscribe(new SuccessSubscriber<ResponseBean<HaveCollectBean>>() {
                                @Override
                                public void onSuccess(ResponseBean<HaveCollectBean> songBeanResponseBean) {
                                    if (songBeanResponseBean.getResult().songBean.songType != 2) {
                                        MusicUtil.playMusic(mContext, songBeanResponseBean.getResult().songBean);
                                    } else {
                                        MvDetailActivity.launch(mContext, songBeanResponseBean.getResult().songBean);
                                    }
                                }
                                @Override
                                public void onFailed(String str) {
                                    ToastUtil.showToast(str);
                                }
                            });
                        }
                    }
                });
            }

            @Override
            public void onFailed(String str) {

            }
        });
    }

    @OnClick({R.id.search_edit, R.id.homepage_top_1, R.id.homepage_top_2,
            R.id.homepage_top_3, R.id.homepage_top_4, R.id.homepage_top_5,
            R.id.bt_song_more, R.id.bt_ring_more, R.id.bt_mv_more})
    public void onHomepageClick(View view) {
        switch (view.getId()) {
            case R.id.homepage_top_1:
                RankActivity.launch(mContext);
                break;
            case R.id.homepage_top_2:
                SingerActivity.launch(mContext);
                break;
            case R.id.homepage_top_3:
                SongPagerActivity.launch(mContext);
                break;
            case R.id.homepage_top_4:
                MvActivity.launch(mContext);
                break;
            case R.id.homepage_top_5:
                RingActivity.launch(mContext);
                break;
            case R.id.search_edit:
                SearchActivity.launch(mContext);
                break;
            case R.id.bt_song_more:
                HouseMoreActivity.launch(mContext, 0);
                break;
            case R.id.bt_mv_more:
                HouseMoreActivity.launch(mContext, 2);
                break;
            case R.id.bt_ring_more:
                HouseMoreActivity.launch(mContext, 1);
                break;
        }
    }

}
