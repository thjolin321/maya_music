package com.yuntu.mawei.ui.circle;

import android.view.View;

import com.yuntu.mawei.R;
import com.yuntu.mawei.base.TaipingApplication;
import com.yuntu.mawei.base.home.BaseListFragment;
import com.yuntu.mawei.base.home.BaseListFragmentPresenter;
import com.yuntu.mawei.ui.circle.publish.PublishActivity;
import com.yuntu.mawei.ui.circle.search.CircleSearchActivity;
import com.yuntu.mawei.ui.login.LoginActivity;
import com.yuntu.mawei.view.adapter.base.BaseQuickAdapter;
import com.yuntu.mawei.view.loading.LoadingView;

import butterknife.BindView;
import butterknife.OnClick;
import in.srain.cube.views.ptr.PtrClassicFrameLayout;
import in.srain.cube.views.ptr.PtrDefaultHandler;
import in.srain.cube.views.ptr.PtrFrameLayout;
import in.srain.cube.views.ptr.PtrHandler;

public class CircleFragment extends BaseListFragment {

    @BindView(R.id.refreshable_view)
    PtrClassicFrameLayout refreshableView;

    @Override
    protected void initViews() {
        refreshableView.setLoadingMinTime(500);
        refreshableView.setDurationToCloseHeader(800);
        LoadingView materialHeader = new LoadingView(getActivity());
        refreshableView.setHeaderView(materialHeader);
        refreshableView.addPtrUIHandler(materialHeader);
        refreshableView.setPtrHandler(new PtrHandler() {
            @Override
            public boolean checkCanDoRefresh(PtrFrameLayout frame, View content, View header) {
                return PtrDefaultHandler.checkContentCanBePulledDown(frame, content, header);
            }

            @Override
            public void onRefreshBegin(final PtrFrameLayout frame) {
                //实现下拉刷新的功能
                updateViews(true);
            }
        });
    }

    public void finishRefresh() {
        if (refreshableView != null) {
            refreshableView.refreshComplete();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        updateViews(false);
    }

    @Override
    protected int attachLayoutRes() {
        return R.layout.circle_fragment;
    }

    @Override
    protected BaseQuickAdapter attachAdapter() {
        return new CircleAdapter(getActivity(), false);
    }

    @Override
    protected BaseListFragmentPresenter attachPresenter() {
        return new CirclePresenter(this);
    }

    @OnClick(R.id.bt_circle_add)
    public void onPublish() {
        if (TaipingApplication.tpApp.getIsLogin()) {
            PublishActivity.launch(getActivity(), -1);
        } else {
            LoginActivity.launch(getActivity());
        }
    }

    @OnClick(R.id.bt_search)
    public void gotoSearch() {
        CircleSearchActivity.launch(getActivity());
    }
}
