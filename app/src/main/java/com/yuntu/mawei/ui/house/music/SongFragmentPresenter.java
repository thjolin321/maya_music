package com.yuntu.mawei.ui.house.music;

import com.yuntu.mawei.R;
import com.yuntu.mawei.base.home.BaseListFragment;
import com.yuntu.mawei.base.home.BaseListFragmentPresenter;
import com.yuntu.mawei.bean.base.ResponseBean;
import com.yuntu.mawei.bean.house.MusicHouseBean;
import com.yuntu.mawei.bean.music.Music;
import com.yuntu.mawei.bean.rx.SuccessSubscriber;
import com.yuntu.mawei.bean.song.HouseSongBean;
import com.yuntu.mawei.util.RxUtils;
import com.yuntu.mawei.util.http.HttpUtil;

import java.util.ArrayList;
import java.util.List;

public class SongFragmentPresenter extends BaseListFragmentPresenter {

    int type;

    public SongFragmentPresenter(BaseListFragment mView, int type) {
        super(mView);
        this.type = type;
    }

    @Override
    public void getData(boolean isPullRefresh) {

        HttpUtil.getInstance().getApiService().getHouseSong(type)
                .compose(RxUtils.rxSchedulerHelper())
                .subscribe(new SuccessSubscriber<ResponseBean<List<MusicHouseBean>>>() {
                    @Override
                    public void onSuccess(ResponseBean<List<MusicHouseBean>> listResponseBean) {
                        if (listResponseBean.getResult() == null) return;
                        mView.loadData(listResponseBean.getResult());
                    }

                    @Override
                    public void onFailed(String str) {
                    }
                });

    }

    @Override
    public void getMoreData() {

    }
}
