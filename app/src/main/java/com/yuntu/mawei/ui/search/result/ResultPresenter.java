package com.yuntu.mawei.ui.search.result;

import com.yuntu.mawei.base.home.BaseListFragmentPresenter;
import com.yuntu.mawei.bean.base.ResponseBean;
import com.yuntu.mawei.bean.rx.SuccessSubscriber;
import com.yuntu.mawei.bean.song.AlbumBean;
import com.yuntu.mawei.bean.song.SongBean;
import com.yuntu.mawei.util.Logl;
import com.yuntu.mawei.util.RxUtils;
import com.yuntu.mawei.util.ToastUtil;
import com.yuntu.mawei.util.http.HttpUtil;

import java.util.ArrayList;
import java.util.List;

import rx.Subscriber;

/**
 * Created by tanghao on 2019/1/25
 */
public class ResultPresenter extends BaseListFragmentPresenter {

    int type, page;
    String str;
    SearchResultFragment fragment;

    public ResultPresenter(SearchResultFragment mView, int type, String str) {
        super(mView);
        this.type = type;
        this.str = str;
        this.fragment = mView;
    }

    @Override
    public void getData(boolean isPullRefresh) {
        HttpUtil.getInstance().getApiService()
                .getSearchResult(str, type, 1, 20)
                .compose(RxUtils.rxSchedulerHelper())
                .subscribe(new Subscriber<ResponseBean<List<SongBean>>>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                    }
                    @Override
                    public void onNext(ResponseBean<List<SongBean>> responseBean) {
                        if (responseBean.getResult() == null){
                            mView.loadData(null);
                        }
                        mView.loadData(responseBean.getResult());
                        if (type == 3) {
                            List<AlbumBean> albumBeanList = new ArrayList<>();
                            for (SongBean songBean : responseBean.getResult()) {
                                if (songBean.album != null) {
                                    albumBeanList.add(songBean.album);
                                }
                            }
                            mView.loadData(albumBeanList);
                        } else {
                            mView.loadData(responseBean.getResult());
                        }
                        page = 2;
                    }
                });
        if (type == 0) {
            HttpUtil.getInstance().getApiService()
                    .getSearchResult(str, 4, 1, 1)
                    .compose(RxUtils.rxSchedulerHelper())
                    .subscribe(new Subscriber<ResponseBean<List<SongBean>>>() {
                        @Override
                        public void onNext(ResponseBean<List<SongBean>> listResponseBean) {
                            if (listResponseBean.getResult() == null
                                    || listResponseBean.getResult().size() == 0) return;
                            fragment.initSinger(listResponseBean.getResult().get(0));
                        }
                        @Override
                        public void onCompleted() {
                        }

                        @Override
                        public void onError(Throwable e) {
                        }
                    });
        }
    }

    @Override
    public void getMoreData() {
        HttpUtil.getInstance().getApiService()
                .getSearchResult(str, type, page, 20)
                .compose(RxUtils.rxSchedulerHelper())
                .subscribe(new SuccessSubscriber<ResponseBean<List<SongBean>>>() {
                    @Override
                    public void onSuccess(ResponseBean<List<SongBean>> responseBean) {
                        if (responseBean.getResult() == null) return;
                        mView.loadMoreData(responseBean.getResult());
                        if (type == 3) {
                            List<AlbumBean> albumBeanList = new ArrayList<>();
                            for (SongBean songBean : responseBean.getResult()) {
                                if (songBean.album != null) {
                                    albumBeanList.add(songBean.album);
                                }
                            }
                            mView.loadData(albumBeanList);
                        } else {
                            mView.loadData(responseBean.getResult());
                        }
                        page++;
                    }

                    @Override
                    public void onFailed(String str) {
                    }
                });
    }
}
