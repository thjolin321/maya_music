package com.yuntu.mawei.ui.homepage.singer.detail;

import com.yuntu.mawei.base.home.BaseListFragment;
import com.yuntu.mawei.base.home.BaseListFragmentPresenter;
import com.yuntu.mawei.bean.base.ResponseBean;
import com.yuntu.mawei.bean.collect.CollectBean;
import com.yuntu.mawei.bean.money.XiaofeiBean;
import com.yuntu.mawei.bean.rx.SuccessSubscriber;
import com.yuntu.mawei.bean.song.AlbumBean;
import com.yuntu.mawei.bean.song.SongBean;
import com.yuntu.mawei.ui.three.collect.MyCollectActivity;
import com.yuntu.mawei.ui.three.recent.RecentActivity;
import com.yuntu.mawei.util.Logl;
import com.yuntu.mawei.util.RxUtils;
import com.yuntu.mawei.util.http.HttpUtil;
import com.yuntu.mawei.util.music.MusicUtil;

import java.util.List;
import java.util.Map;

/**
 * Created by tanghao on 2019/1/25
 */
public class SingerSongListPresenter extends BaseListFragmentPresenter {

    int type; // 0 for song,1 for album,2 for mv
    int singerId;

    public SingerSongListPresenter(BaseListFragment mView, int singerId, int type) {
        super(mView);
        this.type = type;
        this.singerId = singerId;
    }

    @Override
    public void getData(boolean isPullRefresh) {
        switch (type) {
            case 0:
                HttpUtil.getInstance().getApiService()
                        .getSingerSongList(singerId, type, 0, 1, 1000)
                        .compose(RxUtils.rxSchedulerHelper()).subscribe(new SuccessSubscriber<ResponseBean<SingerMapBean>>() {
                    @Override
                    public void onSuccess(ResponseBean<SingerMapBean> listResponseBean) {
                        if (listResponseBean.getResult() == null) return;
                        List<SongBean> list = MusicUtil.sortByZimu(listResponseBean.getResult().map);
                        mView.loadData(list);
                        SingerDetailActivity activity = ((SingerDetailActivity) (mView.getActivity()));
                        if (activity != null) {
                            int songCount = 0;
                            for (SongBean songBean : list) {
                                if (songBean.getItemType() == 0) {
                                    songCount++;
                                }
                            }
                            activity.mvNum = songCount;
                            activity.initIndicator();
                        }
                    }

                    @Override
                    public void onFailed(String str) {
                    }
                });
                break;
            case 1:
                HttpUtil.getInstance().getApiService().getSingerAlbum(singerId, 0, 1, 1000)
                        .compose(RxUtils.rxSchedulerHelper())
                        .subscribe(new SuccessSubscriber<ResponseBean<SingerAlbumMapBean>>() {

                            @Override
                            public void onSuccess(ResponseBean<SingerAlbumMapBean> singerAlbumMapBeanResponseBean) {
                                if (singerAlbumMapBeanResponseBean.getResult() == null) return;
                                List<AlbumBean> list = MusicUtil.sortJustAlbum(singerAlbumMapBeanResponseBean.getResult().map);
                                mView.loadData(list);

                                SingerDetailActivity activity = ((SingerDetailActivity) (mView.getActivity()));
                                if (activity != null) {
                                    int songCount = 0;
                                    for (AlbumBean songBean : list) {
                                        if (songBean.getItemType() == 0) {
                                            songCount++;
                                        }
                                    }
                                    activity.songNum = songCount;
                                    activity.initIndicator();
                                }
                            }

                            @Override
                            public void onFailed(String str) {
                            }
                        });
                break;
            case 2:
                HttpUtil.getInstance().getApiService()
                        .getSingerSongList(singerId, type, 0, 1, 1000)
                        .compose(RxUtils.rxSchedulerHelper()).subscribe(new SuccessSubscriber<ResponseBean<SingerMapBean>>() {
                    @Override
                    public void onSuccess(ResponseBean<SingerMapBean> listResponseBean) {
                        if (listResponseBean.getResult() == null) return;
                        List<SongBean> list = MusicUtil.sortByZimu(listResponseBean.getResult().map);
                        mView.loadData(list);
                        SingerDetailActivity activity = ((SingerDetailActivity) (mView.getActivity()));
                        if (activity != null) {
                            int songCount = 0;
                            for (SongBean songBean : list) {
                                if (songBean.getItemType() == 0) {
                                    songCount++;
                                }
                            }
                            activity.ringNum = songCount;
                            activity.initIndicator();
                        }
                    }

                    @Override
                    public void onFailed(String str) {
                    }
                });
                break;
        }
    }

    @Override
    public void getMoreData() {
        mView.adapter.loadComplete();
        mView.adapter.noMoreData();
    }
}
