package com.yuntu.mawei.ui.homepage.singer;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.yuntu.mawei.R;
import com.yuntu.mawei.base.home.BaseActivity;
import com.yuntu.mawei.ui.house.music.FreeSongFragment;
import com.yuntu.mawei.ui.house.music.PaidSongFragment;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindColor;
import butterknife.BindView;


/**
 * Created by sanmu on 2017/7/6.
 * meail: 992759969@qq.com
 */

public class SingerActivity extends BaseActivity implements View.OnClickListener {
    @BindView(R.id.view_pager)
    ViewPager viewPager;
    @BindView(R.id.quick_login_bootom)
    View quickBootom;
    @BindView(R.id.quick_login)
    LinearLayout quickLogin;
    @BindView(R.id.pass_login_bootom)
    View passBootom;
    @BindView(R.id.pass_login)
    LinearLayout passLogin;
    @BindView(R.id.quick_login_text)
    TextView quickText;
    @BindView(R.id.pass_login_text)
    TextView passText;
    @BindColor(R.color.mall_black)
    int colorBlack;
    @BindColor(R.color.mawei_red)
    int colorRed;
    List<Fragment> fragments = new ArrayList<>();
    FragmentPagerAdapter mAdapter;

    @Override
    public int attachLayout() {
        return R.layout.activity_singer;
    }

    @Override
    public void init() {
        initTitle("歌手");
        selectModle(0);
        quickLogin.setOnClickListener(this);
        passLogin.setOnClickListener(this);
        fragments.add(new SingerFragment());
        fragments.add(new SingerRecentFragment());

        viewPager.addOnPageChangeListener(onPageChangeListener);
        viewPager.setOffscreenPageLimit(0);
        mAdapter = new FragmentPagerAdapter(getSupportFragmentManager()) {
            @Override
            public int getCount() {
                return fragments.size();
            }

            @Override
            public Fragment getItem(int position) {
                return fragments.get(position);
            }

            @Override
            public long getItemId(int position) {
                return position;
            }
        };
        viewPager.setAdapter(mAdapter);
    }


    ViewPager.OnPageChangeListener onPageChangeListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        }

        @Override
        public void onPageSelected(int position) {
            selectModle(position);
        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    };

    void selectModle(int position) {
        if (position == 0) {
            quickBootom.setVisibility(View.VISIBLE);
            passBootom.setVisibility(View.INVISIBLE);
            quickText.setTextColor(colorRed);
            passText.setTextColor(colorBlack);
        } else {
            quickBootom.setVisibility(View.INVISIBLE);
            passBootom.setVisibility(View.VISIBLE);
            quickText.setTextColor(colorBlack);
            passText.setTextColor(colorRed);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.quick_login:
                selectModle(0);
                viewPager.setCurrentItem(0);
                break;
            case R.id.pass_login:
                selectModle(1);
                viewPager.setCurrentItem(1);
                break;
        }
    }

    public static void launch(Context context) {
        context.startActivity(new Intent(context, SingerActivity.class));
    }
}
