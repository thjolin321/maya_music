package com.yuntu.mawei.ui.playing;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.view.View;

import com.yuntu.mawei.R;
import com.yuntu.mawei.bean.music.Music;
import com.yuntu.mawei.ui.playing.service.AudioPlayer;
import com.yuntu.mawei.util.Logl;
import com.yuntu.mawei.view.adapter.base.BaseQuickAdapter;
import com.yuntu.mawei.view.adapter.base.BaseViewHolder;

/**
 * Created by tanghao on 2019/1/30
 */
public class PlayListAdapter extends BaseQuickAdapter<Music> {

    private int colorSelected, colorNormal, colorRed, colorWhite, colorGrey;

    public PlayListAdapter(Context context) {
        super(context);
        colorSelected = ContextCompat.getColor(mContext, R.color.play_list_select_black);
        colorNormal = ContextCompat.getColor(mContext, android.R.color.transparent);
        colorRed = ContextCompat.getColor(mContext, R.color.mawei_red);
        colorWhite = ContextCompat.getColor(mContext, R.color.white);
        colorGrey = Color.parseColor("#969696");
    }

    @Override
    protected int attachLayoutRes() {
        return R.layout.adapter_play_list;
    }

    @Override
    protected void convert(BaseViewHolder holder, Music item) {
        holder.setText(R.id.tv_song, item.title);
        holder.setText(R.id.tv_artist, "-" + item.artist);
        holder.setText(R.id.tv_index, getIndexStr(holder.getLayoutPosition() + 1));
        if (item.songId == AudioPlayer.get().getPlayMusic().songId) {
            holder.setVisible(R.id.iv_playing, true);
            holder.setVisible(R.id.tv_index, false);
            holder.setTextColor(R.id.tv_song, colorRed);
            holder.setTextColor(R.id.tv_artist, colorRed);
            holder.setBackgroundColor(R.id.play_layout, colorSelected);
        } else {
            holder.setVisible(R.id.iv_playing, false);
            holder.setVisible(R.id.tv_index, true);
            holder.setTextColor(R.id.tv_index, colorWhite);
            holder.setTextColor(R.id.tv_song, colorWhite);
            holder.setTextColor(R.id.tv_artist, colorGrey);
            holder.setBackgroundColor(R.id.play_layout, colorNormal);
        }
        holder.setOnClickListener(R.id.iv_cha, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int index = holder.getAdapterPosition();
                getData().remove(index);
                notifyDataSetChanged();
                AudioPlayer.get().delete(index);
            }
        });

        holder.getConvertView().setOnClickListener(v -> {
            AudioPlayer.get().play(holder.getAdapterPosition());
            notifyDataSetChanged();
        });
    }

    private String getIndexStr(int index) {
        if (index < 10) {
            return "0" + index;
        }
        return String.valueOf(index);
    }

}
