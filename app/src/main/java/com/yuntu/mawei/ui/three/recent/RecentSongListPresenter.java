package com.yuntu.mawei.ui.three.recent;

import com.yuntu.mawei.base.home.BaseListFragment;
import com.yuntu.mawei.base.home.BaseListFragmentPresenter;
import com.yuntu.mawei.bean.base.ResponseBean;
import com.yuntu.mawei.bean.collect.CollectBean;
import com.yuntu.mawei.bean.rx.SuccessSubscriber;
import com.yuntu.mawei.bean.song.SongBean;
import com.yuntu.mawei.ui.three.collect.MyCollectActivity;
import com.yuntu.mawei.util.Logl;
import com.yuntu.mawei.util.RxUtils;
import com.yuntu.mawei.util.http.HttpUtil;
import com.yuntu.mawei.util.music.MusicUtil;

import java.util.List;

/**
 * Created by tanghao on 2019/1/25
 */
public class RecentSongListPresenter extends BaseListFragmentPresenter {

    int type;
    public RecentSongListPresenter(BaseListFragment mView,int type) {
        super(mView);
        this.type = type;
    }

    @Override
    public void getData(boolean isPullRefresh) {
        HttpUtil.getInstance().getApiService().getRecentList(type,1,1000).compose(RxUtils.rxSchedulerHelper()).subscribe(new SuccessSubscriber<ResponseBean<CollectBean>>() {
            @Override
            public void onSuccess(ResponseBean<CollectBean> responseBean) {
                if (responseBean == null || responseBean.getResult() == null || responseBean.getResult().songCount == 0)
                    return;
                List<SongBean> list = MusicUtil.sortByZimu(responseBean.getResult().songMap);
                Logl.e("collect list size: " + list.size());
                mView.loadData(list);
                RecentActivity activity = ((RecentActivity) (mView.getActivity()));
                if (activity != null) {
                    if (type == 0) {
                        activity.songNum = responseBean.getResult().songCount;
                    } else if (type == 1) {
                        activity.ringNum = responseBean.getResult().songCount;
                    } else if (type == 2) {
                        activity.mvNum = responseBean.getResult().songCount;
                    }
                    activity.initIndicator();
                }
            }

            @Override
            public void onFailed(String str) {
            }
        });
    }

    @Override
    public void getMoreData() {
        mView.adapter.loadComplete();
        mView.adapter.noMoreData();
    }
}
