package com.yuntu.mawei.ui.homepage.mv.detail;

import com.yuntu.mawei.bean.collect.CollectBean;
import com.yuntu.mawei.bean.song.AlbumBean;
import com.yuntu.mawei.bean.song.SongBean;
import com.yuntu.mawei.bean.song.SongLabelBean;

import java.util.List;

public class MvDetailBean {

    public AlbumBean album;
    public String albumIntroduction;
    public Object collect;
    public double payCount;
    public double songValue;
    public int agreeCount;
    public SongLabelBean songLabel;
    public String createTime;
    public List<SongBean> relevantMv;

}
