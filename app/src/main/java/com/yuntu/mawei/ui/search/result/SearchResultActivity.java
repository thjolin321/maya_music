package com.yuntu.mawei.ui.search.result;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import com.yuntu.mawei.R;
import com.yuntu.mawei.base.home.BaseActivity;
import com.yuntu.mawei.ui.search.SearchHistoryBean;
import com.yuntu.mawei.util.Logl;
import com.yuntu.mawei.util.ToastUtil;
import com.yuntu.mawei.widget.ColorStyleTransitionPagerTitleView;

import net.lucode.hackware.magicindicator.MagicIndicator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.CommonNavigator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.CommonNavigatorAdapter;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.IPagerIndicator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.IPagerTitleView;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.indicators.LinePagerIndicator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import butterknife.BindColor;
import butterknife.BindView;
import butterknife.OnClick;
import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;

public class SearchResultActivity extends BaseActivity {

    @BindView(R.id.indicator)
    MagicIndicator indicator;
    @BindView(R.id.view_pager)
    ViewPager viewPager;
    @BindView(R.id.search_edit)
    EditText searchE;

    @BindColor(R.color.mall_black)
    int colorBlack;
    @BindColor(R.color.mawei_red)
    int colorBlue;

    private List<SearchResultFragment> fragments;
    private FragmentPagerAdapter mAdapter;
    private String str;

    Realm realm;
    RealmResults<SearchHistoryBean> realmlists;

    @Override
    public int attachLayout() {
        return R.layout.activity_search_result;
    }

    @Override
    public void init() {
        str = getIntent().getStringExtra("search");
        realm = Realm.getDefaultInstance();
        initEdit();
        setRedBar();
        initIndicator();
        initFragments();
    }

    @OnClick(R.id.search_cancel)
    public void onSearch() {
        shutDownInput();
        finish();
    }

    private void shutDownInput() {
        try {
            InputMethodManager mInputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            mInputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        } catch (Exception e) {
        }
    }

    public static void launch(Context context, String str) {
        Intent intent = new Intent(context, SearchResultActivity.class);
        intent.putExtra("search", str);
        context.startActivity(intent);
    }


    private void initEdit() {
        searchE.setText(str);
        searchE.setFocusable(true);
        searchE.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView arg0, int arg1, KeyEvent arg2) {
                if (arg1 == EditorInfo.IME_ACTION_SEARCH) {
                    String searchS;
                    if (!(searchS = searchE.getText().toString().trim()).equals("")) {
                        ((InputMethodManager) getSystemService(INPUT_METHOD_SERVICE))
                                .hideSoftInputFromWindow(SearchResultActivity.this.getCurrentFocus().getWindowToken(),
                                        InputMethodManager.HIDE_NOT_ALWAYS);
                        goToSearch(searchS);
                    } else {
                        ToastUtil.showToast("搜索不能为空");
                    }
                }
                return false;
            }
        });
    }

    private void goToSearch(String str) {
        addData(str);
        for (SearchResultFragment fragment : fragments) {
            fragment.refreshCurrent(str);
        }
    }

    public void addData(String s) {
        boolean isAdd = true;
        getRealmData();
        if (realmlists != null) {
            for (SearchHistoryBean hasS : realmlists) {
                if (s.equals(hasS.getContent())) {
                    isAdd = false;
                    try {
                        realm.beginTransaction();
                        hasS.setUpdateTime(new Date().getTime());
                        realm.commitTransaction();
                    } catch (Exception e) {
                        Log.i("this", "Exception1:" + e.toString());
                    } finally {
                    }
                }
            }
        }
        if (isAdd) {
            try {
                SearchHistoryBean searchHistory = new SearchHistoryBean();
                searchHistory.setContent(s);
                long time = new Date().getTime();
                searchHistory.setCreateTime(time);
                searchHistory.setUpdateTime(time);
                realm.executeTransactionAsync(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        realm.copyToRealm(searchHistory);
                    }
                });
            } catch (Exception e) {
                Logl.e("插入出错？" + e.getMessage());
            } finally {
            }
        }
    }

    public void getRealmData() {
        try {
            realmlists = realm.where(SearchHistoryBean.class).findAll();
            realmlists = realmlists.sort("updateTime", Sort.DESCENDING);
        } catch (Exception e) {
        } finally {
        }
        if (realmlists == null) return;
        return;
    }


    private void initFragments() {
        fragments = new ArrayList<>();
        fragments.add(SearchResultFragment.newInstance(0, str));
        fragments.add(SearchResultFragment.newInstance(3, str));
        fragments.add(SearchResultFragment.newInstance(2, str));
        fragments.add(SearchResultFragment.newInstance(1, str));

        mAdapter = new FragmentPagerAdapter(getSupportFragmentManager()) {
            @Override
            public int getCount() {
                return fragments.size();
            }

            @Override
            public Fragment getItem(int position) {
                return fragments.get(position);
            }

            @Override
            public long getItemId(int position) {
                return position;
            }
        };
        viewPager.setAdapter(mAdapter);
        viewPager.addOnPageChangeListener(onPageChangeListener);
    }

    private void initIndicator() {
        List<String> titles = Arrays.asList("歌曲", "专辑", "MV", "铃声");
        CommonNavigator commonNavigator = new CommonNavigator(this);
        commonNavigator.setAdjustMode(true);
        List<String> finalTitles = titles;
        commonNavigator.setAdapter(new CommonNavigatorAdapter() {

            @Override
            public int getCount() {
                return finalTitles.size();
            }

            @Override
            public IPagerTitleView getTitleView(Context context, final int index) {
                ColorStyleTransitionPagerTitleView colorTransitionPagerTitleView = new
                        ColorStyleTransitionPagerTitleView(context);
                colorTransitionPagerTitleView.setLayoutParams(new ViewGroup.LayoutParams
                        (ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
                colorTransitionPagerTitleView.setNormalColor(colorBlack);
                colorTransitionPagerTitleView.setTextSize(14);
                colorTransitionPagerTitleView.setSelectedColor(colorBlue);
                colorTransitionPagerTitleView.setText(finalTitles.get(index));
                colorTransitionPagerTitleView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        viewPager.setCurrentItem(index);
                    }
                });
                return colorTransitionPagerTitleView;
            }

            @Override
            public IPagerIndicator getIndicator(Context context) {
                LinePagerIndicator indicator = new LinePagerIndicator(context);
                indicator.setMode(LinePagerIndicator.MODE_WRAP_CONTENT);
                indicator.setColors(colorBlue);
                return indicator;
            }

            @Override
            public float getTitleWeight(Context context, int index) {
                return 1.0f;
            }
        });

        indicator.setNavigator(commonNavigator);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        realm.close();
    }

    ViewPager.OnPageChangeListener onPageChangeListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            indicator.onPageScrolled(position, positionOffset, positionOffsetPixels);
        }

        @Override
        public void onPageSelected(int position) {
            indicator.onPageSelected(position);
        }

        @Override
        public void onPageScrollStateChanged(int state) {
            indicator.onPageScrollStateChanged(state);
        }
    };

}
