package com.yuntu.mawei.ui.house.music;

import com.yuntu.mawei.R;
import com.yuntu.mawei.base.home.BaseListFragment;
import com.yuntu.mawei.base.home.BaseListFragmentPresenter;
import com.yuntu.mawei.ui.house.SongHouseAdapter;
import com.yuntu.mawei.view.adapter.base.BaseQuickAdapter;

public class FreeSongFragment extends BaseListFragment {

    @Override
    protected void initViews() {
    }

    @Override
    protected int attachLayoutRes() {
        return R.layout.recyclerview_just;
    }

    @Override
    protected BaseQuickAdapter attachAdapter() {
        return new SongHouseAdapter(getActivity());
    }

    @Override
    protected BaseListFragmentPresenter attachPresenter() {
        return new SongFragmentPresenter(this,0);
    }
}
