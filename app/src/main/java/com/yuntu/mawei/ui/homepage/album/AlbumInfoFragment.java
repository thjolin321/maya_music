package com.yuntu.mawei.ui.homepage.album;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.yuntu.mawei.R;
import com.yuntu.mawei.base.home.BaseFragment;
import com.yuntu.mawei.bean.song.AlbumBean;

import butterknife.BindView;

public class AlbumInfoFragment extends BaseFragment {

    AlbumBean albumBean;

    public static AlbumInfoFragment newInstance(AlbumBean albumBean) {
        AlbumInfoFragment fragment = new AlbumInfoFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable("albumBean", albumBean);
        fragment.setArguments(bundle);
        return fragment;
    }

    @BindView(R.id.tv_yuzhong)
    TextView tvYuzhong;
    @BindView(R.id.tv_gongsi)
    TextView tvGongsi;
    @BindView(R.id.tv_leibie)
    TextView tvLeibie;
    @BindView(R.id.tv_intro)
    TextView tvIntro;


    @Override
    public int getLayoutId() {
        return R.layout.fragment_album;
    }

    @Override
    public void initView(View view) {
        albumBean = (AlbumBean) getArguments().getSerializable("albumBean");
        tvYuzhong.setText(albumBean.albumLanguage);
        tvGongsi.setText(albumBean.albumCompany);
        tvLeibie.setText(albumBean.albumType);
        tvIntro.setText(albumBean.albumIntroduction);
    }
}
