package com.yuntu.mawei.ui.circle;

import android.content.Context;
import android.text.TextUtils;

import com.yuntu.mawei.R;
import com.yuntu.mawei.bean.circle.CircleBean;
import com.yuntu.mawei.ui.circle.detail.CircleDetailActivity;
import com.yuntu.mawei.util.GlideHelper;
import com.yuntu.mawei.view.adapter.base.BaseQuickAdapter;
import com.yuntu.mawei.view.adapter.base.BaseViewHolder;


/**
 * Created by tanghao.
 * 新闻列表适配器
 */
public class CircleAdapter extends BaseQuickAdapter<CircleBean> {

    boolean isEdit;

    public CircleAdapter(Context context, boolean isEdit) {
        super(context);
        this.isEdit = isEdit;
    }

    @Override
    protected int attachLayoutRes() {
        return R.layout.adapter_circle;
    }

    @Override
    protected void convert(BaseViewHolder holder, CircleBean item) {
        if (TextUtils.isEmpty(item.imageUrl)) {
            holder.setVisible(R.id.iv_icon, false);
        } else {
            holder.setVisible(R.id.iv_icon, true);
            GlideHelper.loadListPic(mContext, item.imageUrl, holder.getView(R.id.iv_icon));
        }

        holder.setText(R.id.tv_title, item.themeName);
        holder.setText(R.id.tv_user, (isEdit ? ("") : (item.appUserName + "    ")) + item.createTime);
        holder.setVisible(R.id.tv_zhiding, item.isTop == 1);
        item.isEdit = isEdit;
        holder.getConvertView().setOnClickListener(v -> CircleDetailActivity.launch(mContext, item));
    }
}