package com.yuntu.mawei.ui.homepage.mv;

import android.content.Context;
import android.view.View;

import com.yuntu.mawei.R;
import com.yuntu.mawei.bean.song.SingerBean;
import com.yuntu.mawei.bean.song.SongBean;
import com.yuntu.mawei.ui.homepage.mv.detail.MvDetailActivity;
import com.yuntu.mawei.util.GlideHelper;
import com.yuntu.mawei.view.adapter.base.BaseQuickAdapter;
import com.yuntu.mawei.view.adapter.base.BaseViewHolder;

public class MvAdapter extends BaseQuickAdapter<SongBean> {

    public MvAdapter(Context context) {
        super(context);
    }

    @Override
    protected int attachLayoutRes() {
        return R.layout.adapter_item_mv;
    }

    @Override
    protected void convert(BaseViewHolder holder, SongBean item) {
        GlideHelper.loadListPic(mContext, item.songAvatar, holder.getView(R.id.iv_song));
        if (item.singer != null) {
            holder.setText(R.id.tv_singer, item.singer.singerName);
        }
        holder.setText(R.id.tv_play_num, " "+item.listenCount + "");
        holder.setText(R.id.tv_mv, item.songName);
        holder.setText(R.id.tv_duration, secondMapToMinite(item.songTime) + "");
        holder.getConvertView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MvDetailActivity.launch(mContext, item);
            }
        });
    }

    private String secondMapToMinite(int size) {
        String time;
        if (size < 60) {
            time = String.format("00:%02d", size % 60);

        } else if (size < 3600) {
            time = String.format("%02d:%02d", size / 60, size % 60);

        } else {
            time = String.format("%02d:%02d:%02d", size / 3600, size % 3600 / 60, size % 60);
        }
        return time;
    }

}
