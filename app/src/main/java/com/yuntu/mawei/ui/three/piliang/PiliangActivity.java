package com.yuntu.mawei.ui.three.piliang;

import android.content.Context;
import android.content.Intent;
import android.view.View;

import com.yuntu.mawei.R;
import com.yuntu.mawei.base.home.BaseListActivity;
import com.yuntu.mawei.base.home.BaseListActivityPresenter;
import com.yuntu.mawei.bean.base.ResponseBean;
import com.yuntu.mawei.bean.rx.SuccessSubscriber;
import com.yuntu.mawei.bean.song.SongBean;
import com.yuntu.mawei.ui.playing.service.AudioPlayer;
import com.yuntu.mawei.util.Logl;
import com.yuntu.mawei.util.RxUtils;
import com.yuntu.mawei.util.ToastUtil;
import com.yuntu.mawei.util.db.DBManager;
import com.yuntu.mawei.util.http.HttpUtil;
import com.yuntu.mawei.util.music.MusicUtil;
import com.yuntu.mawei.view.adapter.base.BaseQuickAdapter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by tanghao on 2019/2/15
 */
public class PiliangActivity extends BaseListActivity {

    @BindView(R.id.bt_collect)
    View viewCollect;
    @BindView(R.id.bt_delete)
    View viewDelete;
    List<SongBean> list = new ArrayList<>();
    int type; // 1 for local, 2 for collect,3 for recent,4 for rank

    @Override
    protected void initViews() {
        type = getIntent().getIntExtra("type", 0);
        if (type == 2) {
            viewCollect.setVisibility(View.GONE);
        }
        if (type == 4) {
            viewDelete.setVisibility(View.GONE);
        }
    }

    @Override
    protected BaseQuickAdapter attachAdapter() {
        return new PiliangAdapter(this);
    }

    @Override
    protected BaseListActivityPresenter attachPresenter() {
        List<SongBean> listLocal = (List<SongBean>) getIntent().getSerializableExtra("list");
        for (SongBean songBean : listLocal) {
            if (songBean.getItemType() == SongBean.SONG_ITEM) {
                list.add(songBean);
            }
        }
        return new PiliangPresenter(this, list);
    }

    @Override
    protected int attachLayoutRes() {
        return R.layout.activity_piliang;
    }

    @OnClick({R.id.bt_play, R.id.bt_delete, R.id.bt_collect, R.id.title_back, R.id.title_right})
    public void onPiliangClick(View view) {
        switch (view.getId()) {
            case R.id.bt_play:
                List<SongBean> listNow = new ArrayList<>();
                for (SongBean songBean : list) {
                    if (songBean.isSelected) {
                        listNow.add(songBean);
                    }
                }
                if (listNow.size() > 0) {
                    if (listNow.get(0).musicLocal != null) {
                        if (listNow.get(0).musicLocal.songType == 2) {
                            ToastUtil.showToast("视频不能批量播放");
                            return;
                        }
                    } else {
                        if (listNow.get(0).songType == 2) {
                            ToastUtil.showToast("视频不能批量播放");
                            return;
                        }
                    }
                }
                for (int i = 0; i < listNow.size(); i++) {
                    if (i == 0) {
                        MusicUtil.playMusic(PiliangActivity.this, listNow.get(i));
                    } else {
                        AudioPlayer.get().addJust(MusicUtil.mapToMusic(listNow.get(i)));
                    }
                }
                break;
            case R.id.bt_delete:
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < list.size(); i++) {
                    SongBean songBean = list.get(i);
                    if (songBean.isSelected) {
                        sb.append(songBean.songId);
                        sb.append(",");
                        list.remove(i);
                        i--;
                        if (type == 1) {
                            if (songBean.musicLocal != null) {
                                Logl.e("准备删除数据库：" + songBean.musicLocal.id);
                                ToastUtil.showToast("删除成功");
                                DBManager.get().getMusicLocalDao().deleteByKey(songBean.musicLocal.id);
                            }
                        }
                    }
                }
                Logl.e("notifyDataSetChanged:" + sb.toString().length());
                adapter.updateItems(list);
                if (sb.toString().length() == 0) return;
                switch (type) {
                    case 2:
                        String ids = sb.toString().substring(0, sb.toString().length() - 1);
                        HttpUtil.getInstance().getApiService()
                                .collectDelete(ids)
                                .compose(RxUtils.rxSchedulerHelper()).subscribe(new SuccessSubscriber<ResponseBean>() {
                            @Override
                            public void onSuccess(ResponseBean responseBean) {
                                ToastUtil.showToast(responseBean.getMessage());
                            }

                            @Override
                            public void onFailed(String str) {
                                ToastUtil.showToast(str);
                            }
                        });
                        break;
                    case 3:
                        String ids1 = sb.toString().substring(0, sb.toString().length() - 1);
                        HttpUtil.getInstance().getApiService()
                                .recentDelete(ids1)
                                .compose(RxUtils.rxSchedulerHelper()).subscribe(new SuccessSubscriber<ResponseBean>() {
                            @Override
                            public void onSuccess(ResponseBean responseBean) {
                                ToastUtil.showToast(responseBean.getMessage());
                            }

                            @Override
                            public void onFailed(String str) {
                                ToastUtil.showToast(str);
                            }
                        });
                        break;
                }

                break;
            case R.id.bt_collect:
                StringBuilder sb1 = new StringBuilder();
                for (SongBean songBean : list) {
                    if (songBean.isSelected) {
                        sb1.append(songBean.songId);
                        sb1.append(",");
                    }
                }
                if (sb1.toString().length() == 0) return;
                String ids = sb1.toString().substring(0, sb1.toString().length() - 1);
                HttpUtil.getInstance().getApiService()
                        .collectAll(ids)
                        .compose(RxUtils.rxSchedulerHelper()).subscribe(new SuccessSubscriber<ResponseBean>() {
                    @Override
                    public void onSuccess(ResponseBean responseBean) {
                        ToastUtil.showToast(responseBean.getMessage());
                    }

                    @Override
                    public void onFailed(String str) {
                        ToastUtil.showToast(str);
                    }
                });
                break;
            case R.id.title_back:
                for (SongBean songBean : list) {
                    songBean.isSelected = true;
                }
                adapter.notifyDataSetChanged();
                break;
            case R.id.title_right:
                finish();
                break;
        }
    }

    public static void launch(Context context, List<SongBean> list, int type) {
        Intent intent = new Intent(context, PiliangActivity.class);
        intent.putExtra("list", (Serializable) list);
        intent.putExtra("type", type);
        context.startActivity(intent);
    }

}
