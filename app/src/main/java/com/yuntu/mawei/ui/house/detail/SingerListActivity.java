package com.yuntu.mawei.ui.house.detail;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.yuntu.mawei.R;
import com.yuntu.mawei.base.home.BaseListActivity;
import com.yuntu.mawei.base.home.BaseListActivityPresenter;
import com.yuntu.mawei.util.GlideHelper;
import com.yuntu.mawei.view.adapter.base.BaseQuickAdapter;

import butterknife.BindView;

public class SingerListActivity extends BaseListActivity {

    @BindView(R.id.iv_song_top)
    ImageView ivSongTop;
    @BindView(R.id.tv_song_total)
    TextView tvSongTotal;

    String typeImage;
    int songTypeId;
    boolean isRank;

    @Override
    protected void initViews() {
        initTitle(getIntent().getStringExtra("titleName"));
        initTypeData();
    }

    private void initTypeData() {
        typeImage = getIntent().getStringExtra("typeImage");
        songTypeId = getIntent().getIntExtra("songTypeId", 0);
        isRank = getIntent().getBooleanExtra("isNeedShowNum", false);
        tvSongTotal.setVisibility(View.GONE);
        GlideHelper.loadListPic(this, typeImage, ivSongTop);
    }


    @Override
    protected BaseQuickAdapter attachAdapter() {
        return new SingerListAdapter(this);
    }

    @Override
    protected BaseListActivityPresenter attachPresenter() {
        return new SingerListPresenter(this, songTypeId);
    }

    @Override
    protected int attachLayoutRes() {
        return R.layout.activity_song_list;
    }

    public static void launch(Context context, String typeImage, int songTypeId, String titleName) {
        Intent intent = new Intent(context, SingerListActivity.class);
        intent.putExtra("typeImage", typeImage);
        intent.putExtra("songTypeId", songTypeId);
        intent.putExtra("titleName", titleName);
        context.startActivity(intent);
    }


}
