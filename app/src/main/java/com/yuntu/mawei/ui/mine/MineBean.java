package com.yuntu.mawei.ui.mine;

/**
 * Created by tanghao on 2019/1/24
 */
public class MineBean {

    public int id;
    public int icon;
    public int title;
    public String content;
    public String time;

    public MineBean(int id, int icon, int title) {
        this.id = id;
        this.icon = icon;
        this.title = title;
    }
}
