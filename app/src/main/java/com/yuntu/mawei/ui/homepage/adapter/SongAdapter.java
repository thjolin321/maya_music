package com.yuntu.mawei.ui.homepage.adapter;

import android.content.Context;
import android.view.View;

import com.yuntu.mawei.R;
import com.yuntu.mawei.bean.song.SongBean;
import com.yuntu.mawei.util.GlideHelper;
import com.yuntu.mawei.util.Logl;
import com.yuntu.mawei.util.music.MusicUtil;
import com.yuntu.mawei.view.adapter.base.BaseQuickAdapter;
import com.yuntu.mawei.view.adapter.base.BaseViewHolder;


/**
 * Created by tanghao on 2019/1/24
 */
public class SongAdapter extends BaseQuickAdapter<SongBean> {

    public SongAdapter(Context context) {
        super(context);
    }

    @Override
    protected int attachLayoutRes() {
        return R.layout.adapter_song;
    }

    @Override
    protected void convert(BaseViewHolder holder, SongBean item) {
        Logl.e(item.songName);
        GlideHelper.loadListPic(mContext, item.songAvatar, holder.getView(R.id.iv_song));
        holder.setText(R.id.tv_song, item.songName);
        holder.getConvertView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(item.songType==0) {
                    MusicUtil.playMusic(mContext, item);
                }else{
                    MusicUtil.playMusicNoLaunch(mContext, item);
                }
            }
        });
    }
}
