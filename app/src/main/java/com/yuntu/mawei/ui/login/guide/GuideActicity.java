package com.yuntu.mawei.ui.login.guide;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.widget.ImageView;

import com.yuntu.mawei.R;
import com.yuntu.mawei.base.home.BaseActivity;
import com.yuntu.mawei.ui.main.MainActivity;
import com.yuntu.mawei.util.SharedPreferenceUtil;

import org.bitlet.weupnp.Main;

import java.util.ArrayList;

import butterknife.BindView;

/**
 * Created by tanghao on 2015/9/14.
 */
public class GuideActicity extends BaseActivity {

    @BindView(R.id.guide_viewpager)
    ViewPager viewpager;
    @BindView(R.id.dian1)
    ImageView dian1;
    @BindView(R.id.dian2)
    ImageView dian2;

    boolean misScrolled;
    ArrayList<Fragment> list = new ArrayList<>();

    @Override
    public int attachLayout() {
        return R.layout.activity_guide;
    }

    @Override
    public void init() {
        list.add(new YingdaoPicFragment());
        list.add(new YingdaoPicFragmentTwo());
        viewpager.setAdapter(new GuideAdapter(getSupportFragmentManager()));
        viewpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                setDianUi(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {
                switch (state) {
                    case ViewPager.SCROLL_STATE_DRAGGING:
                        misScrolled = false;
                        break;
                    case ViewPager.SCROLL_STATE_SETTLING:
                        misScrolled = true;
                        break;
                    case ViewPager.SCROLL_STATE_IDLE:
                        if (viewpager.getCurrentItem() == viewpager.getAdapter().getCount() - 1 && !misScrolled) {
                            MainActivity.launch(GuideActicity.this);
                            GuideActicity.this.finish();
                        }
                        misScrolled = true;
                        break;
                }
            }
        });
    }

    class GuideAdapter extends FragmentStatePagerAdapter {

        public GuideAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return list.get(position);
        }

        @Override
        public int getCount() {
            return list.size();
        }
    }

    public void setDianUi(int pos) {
        switch (pos) {
            case 0:
                dian1.setImageResource(R.drawable.guide_red);
                dian2.setImageResource(R.drawable.guide_white);
                break;
            case 1:
                dian1.setImageResource(R.drawable.guide_white);
                dian2.setImageResource(R.drawable.guide_red);
                break;
        }
    }

    @Override
    public void onBackPressed() {
        MainActivity.launch(GuideActicity.this);
        super.onBackPressed();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    public static void launch(Context context) {
        context.startActivity(new Intent(context, GuideActicity.class));
        SharedPreferenceUtil.getInstance().putBoolean("first", false);
    }

}
