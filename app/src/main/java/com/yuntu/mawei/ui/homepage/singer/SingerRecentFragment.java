package com.yuntu.mawei.ui.homepage.singer;

import com.yuntu.mawei.R;
import com.yuntu.mawei.base.home.BaseListFragment;
import com.yuntu.mawei.base.home.BaseListFragmentPresenter;
import com.yuntu.mawei.util.Logl;
import com.yuntu.mawei.view.adapter.base.BaseQuickAdapter;

public class SingerRecentFragment extends BaseListFragment {

    @Override
    protected void initViews() {
    }

    @Override
    protected int attachLayoutRes() {
        return R.layout.recyclerview_just;
    }

    @Override
    protected BaseQuickAdapter attachAdapter() {
        return new SingerAdapter(getActivity());
    }

    @Override
    protected BaseListFragmentPresenter attachPresenter() {
        return new SingerRecentPresenter(this, 0);
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            presenter.getData(false);
        }
    }
}
