package com.yuntu.mawei.ui.house;

import com.yuntu.mawei.bean.base.ResponseBean;
import com.yuntu.mawei.bean.rx.SuccessSubscriber;
import com.yuntu.mawei.bean.song.SongBean;
import com.yuntu.mawei.util.Logl;
import com.yuntu.mawei.util.RxUtils;
import com.yuntu.mawei.util.http.HttpUtil;

import java.util.List;

/**
 * Created by tanghao on 2019/1/25
 */
public class MusicHousePresenter {
    public MainHouseFragment fragment;

    public MusicHousePresenter(MainHouseFragment fragment) {
        this.fragment = fragment;
    }

    public void getHotSong() {
        Logl.e("getHotSong");
        HttpUtil.getInstance().getApiService().getHotSong(0,1,10).compose(RxUtils.rxSchedulerHelper()).subscribe(new SuccessSubscriber<ResponseBean<List<SongBean>>>() {
            @Override
            public void onSuccess(ResponseBean<List<SongBean>> listResponseBean) {
                fragment.showSong(listResponseBean.getResult());
            }

            @Override
            public void onFailed(String str) {

            }
        });
    }

}
