package com.yuntu.mawei.ui.house.music;

import com.yuntu.mawei.R;
import com.yuntu.mawei.base.home.BaseListFragment;
import com.yuntu.mawei.base.home.BaseListFragmentPresenter;
import com.yuntu.mawei.ui.house.SongHouseAdapter;
import com.yuntu.mawei.view.adapter.base.BaseQuickAdapter;

public class PaidSongFragment extends BaseListFragment {

    @Override
    protected void initViews() {
    }

    @Override
    protected int attachLayoutRes() {
        return R.layout.recyclerview_just;
    }

    @Override
    protected BaseQuickAdapter attachAdapter() {
        return new SongHouseAdapter(getActivity(), 1);
    }

    @Override
    protected BaseListFragmentPresenter attachPresenter() {
        return new SongFragmentPresenter(this, 1);
    }
}
