package com.yuntu.mawei.ui.three.collect;

import com.yuntu.mawei.R;
import com.yuntu.mawei.base.home.BaseListFragment;
import com.yuntu.mawei.base.home.BaseListFragmentPresenter;
import com.yuntu.mawei.ui.three.SongSimpleListAdapter;
import com.yuntu.mawei.view.adapter.base.BaseQuickAdapter;

public class CollectRingListFragment extends BaseListFragment {
    @Override
    protected void initViews() {

    }

    @Override
    protected int attachLayoutRes() {
        return R.layout.recyclerview_just;
    }

    @Override
    protected BaseQuickAdapter attachAdapter() {
        return new SongSimpleListAdapter(getActivity(),2);
    }

    @Override
    protected BaseListFragmentPresenter attachPresenter() {
        return new CollectSongListPresenter(this,1);
    }
}
