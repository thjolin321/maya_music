package com.yuntu.mawei.ui.three;

import com.yuntu.mawei.base.home.BaseListFragment;
import com.yuntu.mawei.base.home.BaseListFragmentPresenter;
import com.yuntu.mawei.bean.song.SongBean;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tanghao on 2019/1/25
 */
public class SongSimpleListPresenter extends BaseListFragmentPresenter {


    public SongSimpleListPresenter(BaseListFragment mView) {
        super(mView);
    }

    @Override
    public void getData(boolean isPullRefresh) {
        List<SongBean> list = new ArrayList<>();
        list.add(new SongBean(SongBean.SONG_TITLE_ITEM));

        list.add(new SongBean(SongBean.SONG_ITEM));
        list.add(new SongBean(SongBean.SONG_ITEM));
        list.add(new SongBean(SongBean.SONG_ITEM));
        list.add(new SongBean(SongBean.SONG_ITEM));
        list.add(new SongBean(SongBean.SONG_ITEM));
        list.add(new SongBean(SongBean.SONG_ITEM));
        list.add(new SongBean(SongBean.SONG_ITEM));

        mView.loadData(list);
    }

    @Override
    public void getMoreData() {
        mView.adapter.loadComplete();
        mView.adapter.noMoreData();
    }
}
