package com.yuntu.mawei.ui.circle.detail;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.yuntu.mawei.R;
import com.yuntu.mawei.base.TaipingApplication;
import com.yuntu.mawei.base.home.BaseListActivity;
import com.yuntu.mawei.base.home.BaseListActivityPresenter;
import com.yuntu.mawei.bean.base.ResponseBean;
import com.yuntu.mawei.bean.circle.CircleBean;
import com.yuntu.mawei.bean.rx.SuccessSubscriber;
import com.yuntu.mawei.ui.circle.comment.CommentActivity;
import com.yuntu.mawei.ui.circle.comment.JubaoActivity;
import com.yuntu.mawei.ui.circle.publish.PublishActivity;
import com.yuntu.mawei.ui.login.LoginActivity;
import com.yuntu.mawei.util.RxUtils;
import com.yuntu.mawei.util.ToastUtil;
import com.yuntu.mawei.util.http.HttpUtil;
import com.yuntu.mawei.view.ShareDialog;
import com.yuntu.mawei.view.adapter.base.BaseQuickAdapter;

import butterknife.BindView;
import butterknife.OnClick;


public class CircleDetailActivity extends BaseListActivity {

    @BindView(R.id.tv_jubao)
    TextView tvJubao;
    @BindView(R.id.tv_comment)
    TextView tvComment;
    @BindView(R.id.tv_dianzan)
    TextView tvDianzan;
    @BindView((R.id.iv_dianzan))
    ImageView ivDianZan;
    @BindView(R.id.bt_edit)
    View btEdit;
    @BindView(R.id.title_right)
    View btRight;
    @BindView(R.id.title_right_share)
    View btShare;
    public CircleDetailAdapter adapterDetail;
    public CircleBean circleBean;

    @Override
    protected void initViews() {
        initTitle(R.string.home3);
        initData();
    }

    private void initData() {
        Intent intent = getIntent();
        circleBean = (CircleBean) intent.getSerializableExtra("circleBean");
        if (circleBean != null) {
            tvComment.setText("评论 " + circleBean.commentCount);
            tvDianzan.setText("点赞 " + circleBean.thumbCount);
            if (circleBean.thumbId != 0) {
                ivDianZan.setImageResource(R.mipmap.dianzan_red);
            }
            if (circleBean.isEdit) {
                btEdit.setVisibility(View.VISIBLE);
                btRight.setVisibility(View.VISIBLE);
                btShare.setVisibility(View.GONE);
                btRight.setOnClickListener(v -> {
                    HttpUtil.getInstance().getApiService().deleteComment(circleBean.themeId)
                            .compose(RxUtils.rxSchedulerHelper()).subscribe(new SuccessSubscriber<ResponseBean>() {
                        @Override
                        public void onSuccess(ResponseBean responseBean) {
                            ToastUtil.showToast("删除成功");
                            finish();
                        }

                        @Override
                        public void onFailed(String str) {
                            ToastUtil.showToast(str);
                        }
                    });
                });
            }
        }
    }

    public void initCommentCount(int size) {
        tvComment.setText("评论 " + size);
    }


    @Override
    protected BaseQuickAdapter attachAdapter() {
        adapterDetail = new CircleDetailAdapter(this, circleBean);
        return adapterDetail;
    }

    @Override
    protected BaseListActivityPresenter attachPresenter() {
        return new CircleDetailPresenter(this, circleBean.themeId);
    }

    @Override
    protected int attachLayoutRes() {
        return R.layout.activity_circle_detail;
    }

    @OnClick({R.id.bt_jubao, R.id.bt_comment, R.id.bt_dianzan, R.id.bt_edit, R.id.title_right_share})
    public void onCircleClick(View view) {
        switch (view.getId()) {
            case R.id.bt_jubao:
                JubaoActivity.launch(this, circleBean);
                break;
            case R.id.bt_comment:
                CommentActivity.launch(this, circleBean);
                break;
            case R.id.bt_dianzan:
                if (!TaipingApplication.tpApp.getIsLogin()) {
                    LoginActivity.launch(this);
                    return;
                }
                if (circleBean.thumbId == 0) {
                    circleBean.thumbId = 1;
                    ivDianZan.setImageResource(R.mipmap.dianzan_red);
                    circleBean.thumbCount++;
                    tvDianzan.setText("点赞 " + circleBean.thumbCount);
                } else {
                    circleBean.thumbId = 0;
                    ivDianZan.setImageResource(R.mipmap.dianzan_grey);
                    circleBean.thumbCount--;
                    tvDianzan.setText("点赞 " + circleBean.thumbCount);
                }
                HttpUtil.getInstance().getApiService().dianZan(circleBean.themeId)
                        .compose(RxUtils.rxSchedulerHelper())
                        .subscribe(new SuccessSubscriber<ResponseBean>() {
                            @Override
                            public void onSuccess(ResponseBean responseBean) {
                            }

                            @Override
                            public void onFailed(String str) {
                                ToastUtil.showToast(str);
                            }
                        });
                break;
            case R.id.bt_edit:
                if (!TaipingApplication.tpApp.getIsLogin()) {
                    LoginActivity.launch(this);
                    return;
                }
                PublishActivity.launch(this, circleBean.themeId);
                break;
            case R.id.title_right_share:
                ShareDialog.launch(this);
                break;
        }
    }

    boolean isOnce;

    @Override
    protected void onResume() {
        super.onResume();
        if (isOnce) {
            presenter.getData(false);
        }
        isOnce = true;
    }

    public static void launch(Context context, CircleBean circleBean) {
        Intent intent = new Intent(context, CircleDetailActivity.class);
        intent.putExtra("circleBean", circleBean);
        context.startActivity(intent);
    }


}
