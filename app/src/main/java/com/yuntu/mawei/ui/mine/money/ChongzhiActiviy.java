package com.yuntu.mawei.ui.mine.money;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.alipay.sdk.app.PayTask;
import com.yuntu.mawei.R;
import com.yuntu.mawei.base.home.BaseActivity;
import com.yuntu.mawei.bean.base.ResponseBean;
import com.yuntu.mawei.bean.rx.SuccessSubscriber;
import com.yuntu.mawei.constant.api.ApiInterface;
import com.yuntu.mawei.ui.mine.member.MemberBean;
import com.yuntu.mawei.ui.mine.member.MemberBuyActiviy;
import com.yuntu.mawei.util.BigDeUtil;
import com.yuntu.mawei.util.Logl;
import com.yuntu.mawei.util.RxUtils;
import com.yuntu.mawei.util.ToastUtil;
import com.yuntu.mawei.util.http.HttpUtil;

import java.util.List;
import java.util.Map;

import butterknife.BindColor;
import butterknife.BindView;
import butterknife.BindViews;
import butterknife.OnClick;

public class ChongzhiActiviy extends BaseActivity {
    @BindViews({R.id.tv_1, R.id.tv_2, R.id.tv_3, R.id.tv_4, R.id.tv_5, R.id.tv_6})
    List<TextView> viewList1;
    @BindViews({R.id.tv_m_1, R.id.tv_m_2, R.id.tv_m_3, R.id.tv_m_4, R.id.tv_m_5, R.id.tv_m_6})
    List<TextView> viewList2;
    @BindViews({R.id.tv_y_1, R.id.tv_y_2, R.id.tv_y_3, R.id.tv_y_4, R.id.tv_y_5, R.id.tv_y_6})
    List<TextView> viewList3;
    @BindViews({R.id.goods_1, R.id.goods_2, R.id.goods_3, R.id.goods_4, R.id.goods_5, R.id.goods_6})
    List<View> viewList4;
    @BindView(R.id.et_chongzhi)
    EditText etChongzhi;

    @BindView(R.id.bt_weixin)
    ImageView btWeixin;
    @BindView(R.id.bt_zhifubao)
    ImageView btZhifubao;

    @BindColor(R.color.white)
    int white;
    @BindColor(R.color.mall_black)
    int yellow;
    @BindColor(R.color.member_b)
    int black;

    int type = 1; // 0 for 微信，1 for 支付宝
    int index;

    Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 0x123:
                    Logl.e("收到支付宝的消息: " + msg.obj);
                    break;
                case 0x234:
                    break;
            }
            super.handleMessage(msg);
        }
    };

    @Override
    public int attachLayout() {
        return R.layout.activity_chongzhi;
    }

    @Override
    public void init() {
        initTitle("充值");
    }

    public static void launch(Context context) {
        context.startActivity(new Intent(context, ChongzhiActiviy.class));
    }

    @OnClick({R.id.goods_1, R.id.goods_2, R.id.goods_3,
            R.id.goods_4, R.id.goods_5,
            R.id.goods_6, R.id.bt_zhifubao,
            R.id.bt_weixin, R.id.bt_become})
    public void onMenber(View v) {
        switch (v.getId()) {
            case R.id.goods_1:
                onCheck(0);
                break;
            case R.id.goods_2:
                onCheck(1);
                break;
            case R.id.goods_3:
                onCheck(2);
                break;
            case R.id.goods_4:
                onCheck(3);
                break;
            case R.id.goods_5:
                onCheck(4);
                break;
            case R.id.goods_6:
                onCheck(5);
                break;
            case R.id.bt_zhifubao:
                type = 1;
                btWeixin.setImageResource(R.mipmap.gou_grey);
                btZhifubao.setImageResource(R.mipmap.gou_red);
                break;
            case R.id.bt_weixin:
                type = 0;
                btWeixin.setImageResource(R.mipmap.gou_red);
                btZhifubao.setImageResource(R.mipmap.gou_grey);
                break;
            case R.id.bt_become:
                String num = "0";
                if (!TextUtils.isEmpty(etChongzhi.getText())) {
                    num = etChongzhi.getText().toString();
                }
                if (num.startsWith("0")) {
                    num = viewList2.get(index).getText().toString();
                }
                HttpUtil.getInstance().getApiService().pay(type, 10, Integer.parseInt(num) * 100, 0)
                        .compose(RxUtils.rxSchedulerHelper())
                        .subscribe(new SuccessSubscriber<ResponseBean>() {
                            @Override
                            public void onSuccess(ResponseBean responseBean) {
                                Runnable payRunnable = new Runnable() {
                                    @Override
                                    public void run() {
                                        PayTask alipay = new PayTask(ChongzhiActiviy.this);
                                        Map<String, String> result = alipay.payV2(responseBean.getMessage(), true);
                                        Message msg = new Message();
                                        msg.what = 0x123;
                                        msg.obj = result;
                                        mHandler.sendMessage(msg);
                                    }
                                };
                                // 必须异步调用
                                Thread payThread = new Thread(payRunnable);
                                payThread.start();
                            }

                            @Override
                            public void onFailed(String str) {
                                ToastUtil.showToast(str);
                            }
                        });
                break;
        }
    }

    private void onCheck(int index) {
        this.index = index;
        for (int i = 0; i < 6; i++) {
            viewList1.get(i).setTextColor(black);
            viewList2.get(i).setTextColor(yellow);
            viewList3.get(i).setTextColor(yellow);
            viewList4.get(i).setBackgroundResource(R.drawable.member_buy_unselect);
        }
        viewList1.get(index).setTextColor(white);
        viewList2.get(index).setTextColor(white);
        viewList3.get(index).setTextColor(white);
        viewList4.get(index).setBackgroundResource(R.drawable.member_buy_select);
    }

}
