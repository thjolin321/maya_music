package com.yuntu.mawei.ui.house.more;

import com.yuntu.mawei.base.home.BaseListActivity;
import com.yuntu.mawei.base.home.BaseListActivityPresenter;
import com.yuntu.mawei.bean.base.ResponseBean;
import com.yuntu.mawei.bean.rx.SuccessSubscriber;
import com.yuntu.mawei.bean.song.SongBean;
import com.yuntu.mawei.util.RxUtils;
import com.yuntu.mawei.util.http.HttpUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tanghao on 2019/1/25
 */
public class HouseMorePresenter extends BaseListActivityPresenter {

    int type;

    public HouseMorePresenter(int type, BaseListActivity mView) {
        super(mView);
        this.type = type;
    }

    int page = 1;

    @Override
    public void getData(boolean isPullRefresh) {
        if (type == 3) {
            type = 0;
        }
        HttpUtil.getInstance().getApiService().getHotSong(type, page, 10).compose(RxUtils.rxSchedulerHelper()).subscribe(new SuccessSubscriber<ResponseBean<List<SongBean>>>() {
            @Override
            public void onSuccess(ResponseBean<List<SongBean>> listResponseBean) {
                page = 2;
                mView.loadData(listResponseBean.getResult());
            }

            @Override
            public void onFailed(String str) {
            }
        });
    }

    @Override
    public void getMoreData() {
        HttpUtil.getInstance().getApiService().getHotSong(type, page, 10).compose(RxUtils.rxSchedulerHelper()).subscribe(new SuccessSubscriber<ResponseBean<List<SongBean>>>() {
            @Override
            public void onSuccess(ResponseBean<List<SongBean>> listResponseBean) {
                if (listResponseBean.getResult() == null) return;
                mView.loadMoreData(listResponseBean.getResult());
                page++;
            }

            @Override
            public void onFailed(String str) {
            }
        });
    }
}
