package com.yuntu.mawei.ui.three.recent;

import android.os.Bundle;

import com.yuntu.mawei.R;
import com.yuntu.mawei.base.home.BaseListFragment;
import com.yuntu.mawei.base.home.BaseListFragmentPresenter;
import com.yuntu.mawei.ui.three.MvSimpleListAdapter;
import com.yuntu.mawei.ui.three.SongSimpleListAdapter;
import com.yuntu.mawei.view.adapter.base.BaseQuickAdapter;

public class RecentSongListFragment extends BaseListFragment {

    int type;

    public static RecentSongListFragment newInstance(int type) {
        RecentSongListFragment fragment = new RecentSongListFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("type", type);
        fragment.setArguments(bundle);
        return fragment;
    }


    @Override
    protected void initViews() {

    }

    @Override
    protected int attachLayoutRes() {
        return R.layout.recyclerview_just;
    }

    @Override
    protected BaseQuickAdapter attachAdapter() {
        type = getArguments().getInt("type");
        if (type == 2) {
            return new MvSimpleListAdapter(getActivity(), 3);
        } else {
            return new SongSimpleListAdapter(getActivity(), 3);
        }
    }

    @Override
    protected BaseListFragmentPresenter attachPresenter() {
        return new RecentSongListPresenter(this, type);
    }

    boolean isSecondExecute;
    @Override
    public void onResume() {
        super.onResume();
        if (isSecondExecute) {
            updateViews(false);
        }
        isSecondExecute = true;
    }
}
