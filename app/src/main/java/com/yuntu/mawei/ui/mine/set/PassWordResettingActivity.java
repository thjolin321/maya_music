package com.yuntu.mawei.ui.mine.set;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.widget.EditText;

import com.yuntu.mawei.R;
import com.yuntu.mawei.base.home.BaseActivity;
import com.yuntu.mawei.bean.base.ResponseBean;
import com.yuntu.mawei.bean.rx.SuccessSubscriber;
import com.yuntu.mawei.util.RxUtils;
import com.yuntu.mawei.util.ToastUtil;
import com.yuntu.mawei.util.http.HttpUtil;
import com.yuntu.mawei.util.music.MusicUtil;

import butterknife.BindView;
import butterknife.OnClick;

public class PassWordResettingActivity extends BaseActivity {

    @BindView(R.id.et_yuanshi)
    EditText etYuanshi;
    @BindView(R.id.et_new)
    EditText etNew;
    @BindView(R.id.et_new_again)
    EditText etNewAgain;


    @Override
    public int attachLayout() {
        return R.layout.activity_password_resetting;
    }

    @Override
    public void init() {
        initTitle(R.string.password_resetting);
    }

    @OnClick(R.id.bt_sure)
    public void onSure() {
        if (TextUtils.isEmpty(etYuanshi.getText())) {
            ToastUtil.showToast(R.string.input_yuanshi_password);
            return;
        }
        if (TextUtils.isEmpty(etNew.getText())) {
            ToastUtil.showToast(R.string.input_new_password);
            return;
        }
        if (TextUtils.isEmpty(etNewAgain.getText())) {
            ToastUtil.showToast(R.string.input_password_again);
            return;
        }
        if (!etNew.getText().toString().equals(etNewAgain.getText().toString())) {
            ToastUtil.showToast(R.string.password_not_equal);
            return;
        }
        if (!MusicUtil.isLetterOrDigit(etNew.getText().toString())) {
            ToastUtil.showToast(R.string.mima_daxiaoxie);
            return;
        }
        HttpUtil.getInstance().getApiService()
                .resettingPasswordSet(etYuanshi.getText().toString(), etNew.getText().toString())
                .compose(RxUtils.rxSchedulerHelper()).subscribe(new SuccessSubscriber<ResponseBean>() {
            @Override
            public void onSuccess(ResponseBean responseBean) {
                ToastUtil.showToast(responseBean.getMessage());
                setResult(0x321);
                finish();
            }

            @Override
            public void onFailed(String str) {
                ToastUtil.showToast(str);
            }
        });
    }

    public static void launch(Context context) {
        ((Activity) context).startActivityForResult(new Intent(context,
                PassWordResettingActivity.class), 0x123);
    }

}
