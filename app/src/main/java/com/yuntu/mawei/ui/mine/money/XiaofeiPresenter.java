package com.yuntu.mawei.ui.mine.money;

import com.yuntu.mawei.base.TaipingApplication;
import com.yuntu.mawei.base.home.BaseListFragment;
import com.yuntu.mawei.base.home.BaseListFragmentPresenter;
import com.yuntu.mawei.bean.base.ResponseBean;
import com.yuntu.mawei.bean.money.XiaofeiBean;
import com.yuntu.mawei.bean.rx.SuccessSubscriber;
import com.yuntu.mawei.util.RxUtils;
import com.yuntu.mawei.util.ToastUtil;
import com.yuntu.mawei.util.http.HttpUtil;

import java.util.ArrayList;
import java.util.List;

public class XiaofeiPresenter extends BaseListFragmentPresenter {

    public XiaofeiPresenter(BaseListFragment mView) {
        super(mView);
    }

    @Override
    public void getData(boolean isPullRefresh) {
        HttpUtil.getInstance().getApiService().getXiaofeiBean(1,1000)
                .compose(RxUtils.rxSchedulerHelper())
                .subscribe(new SuccessSubscriber<ResponseBean<List<XiaofeiBean>>>() {
            @Override
            public void onSuccess(ResponseBean<List<XiaofeiBean>> listResponseBean) {
                if(listResponseBean.getResult()==null) return;
                mView.loadData(listResponseBean.getResult());
            }
            @Override
            public void onFailed(String str) {
                ToastUtil.showToast(str);
            }
        });
    }

    @Override
    public void getMoreData() {
        mView.adapter.noMoreData();
        mView.adapter.loadComplete();
    }
}
