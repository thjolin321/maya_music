package com.yuntu.mawei.ui.three;

import android.content.Context;

import com.yuntu.mawei.R;
import com.yuntu.mawei.bean.song.SongBean;
import com.yuntu.mawei.ui.homepage.mv.detail.MvDetailActivity;
import com.yuntu.mawei.ui.playing.service.AudioPlayer;
import com.yuntu.mawei.ui.three.piliang.PiliangActivity;
import com.yuntu.mawei.util.GlideHelper;
import com.yuntu.mawei.util.music.MusicUtil;
import com.yuntu.mawei.view.adapter.base.BaseMultiItemQuickAdapter;
import com.yuntu.mawei.view.adapter.base.BaseViewHolder;

public class MvSimpleListAdapter extends BaseMultiItemQuickAdapter<SongBean> {

    int type; // 控制批量操作页面 // 1 for local, 2 for collect,3 for recent

    public MvSimpleListAdapter(Context context, int type) {
        super(context);
        this.type = type;
    }

    @Override
    protected void attachItemType() {
        addItemType(SongBean.SONG_TITLE_ITEM, R.layout.adapter_song_simple_title);
        addItemType(SongBean.SONG_ITEM, R.layout.adapter_mv_simple_list);
    }

    @Override
    protected void convert(BaseViewHolder holder, SongBean item) {
        switch (item.itemType) {
            case SongBean.SONG_TITLE_ITEM:
                holder.setText(R.id.tv_zimu_title, item.zimuTitle);
                if (holder.getLayoutPosition() == 0) {
                    holder.setVisible(R.id.bt_piliang, true);
                    holder.setOnClickListener(R.id.bt_piliang, v -> {
                        PiliangActivity.launch(mContext, getData(), type);
                    });
                } else {
                    holder.setVisible(R.id.bt_piliang, false);
                }
                break;
            case SongBean.SONG_ITEM:
                if (item.musicLocal == null) {
                    GlideHelper.loadListPic(mContext, item.songAvatar, holder.getView(R.id.iv_song));
                    holder.setText(R.id.tv_song, item.songName);
                    if (item.singer != null) {
                        holder.setText(R.id.tv_artist_and_album, item.singer.singerName + " - 《" + (item.album != null ? item.album.albumName : "") + "》");
                    }
                    holder.getConvertView().setOnClickListener(v -> {
                        if (item.songType == 2) {
                            MvDetailActivity.launch(mContext, item);
                        } else {
                            MusicUtil.playMusic(mContext, item);
                        }
                    });
                } else {
                    GlideHelper.loadListPic(mContext, item.musicLocal.coverPath, holder.getView(R.id.iv_song));
                    holder.setText(R.id.tv_song, item.musicLocal.title);
                    if (item.singer != null) {
                        holder.setText(R.id.tv_artist_and_album, item.musicLocal.title + " - 《" + (item.album != null ? item.musicLocal.album : "") + "》");
                    }
                    holder.getConvertView().setOnClickListener(v -> {
                        if (item.musicLocal.songType == 2) {
                            MvDetailActivity.launch(mContext, item);
                        } else {
                            AudioPlayer.get().addAndPlayWithUpdate(item.musicLocal);
                        }
                    });
                }
                break;
        }
    }
}
