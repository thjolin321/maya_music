package com.yuntu.mawei.ui.mine.member;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import com.yuntu.mawei.R;
import com.yuntu.mawei.base.TaipingApplication;
import com.yuntu.mawei.base.home.BaseActivity;
import com.yuntu.mawei.bean.base.ResponseBean;
import com.yuntu.mawei.bean.rx.SuccessSubscriber;
import com.yuntu.mawei.ui.login.LoginActivity;
import com.yuntu.mawei.ui.login.MyWebViewActivity;
import com.yuntu.mawei.ui.mine.user.UserBean;
import com.yuntu.mawei.util.RxUtils;
import com.yuntu.mawei.util.http.HttpUtil;
import com.yuntu.mawei.view.SureDialog;

import java.lang.reflect.Member;
import java.text.SimpleDateFormat;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by tanghao on 2019/2/18
 */
public class MemberActivity extends BaseActivity {

    @BindView(R.id.title_right)
    TextView tvRight;
    @BindView(R.id.tv_vip)
    TextView tvVip;
    @BindView(R.id.member_bg)
    View bgMember;
    @BindView(R.id.tv_overtime)
    TextView tvOverTime;
    @BindView(R.id.bt_become_member)
    View viewBecomeMember;
    @BindView(R.id.bt_become)
    TextView tvUpgrade;

    SimpleDateFormat df = new SimpleDateFormat("yyyy年MM月dd日");

    @Override
    public int attachLayout() {
        return R.layout.activity_member;
    }

    @Override
    public void init() {
        if (!TaipingApplication.tpApp.getIsLogin()) {
            LoginActivity.launch(this);
            finish();
        }
        setBarColor(R.color.member_black);
        initTitle(R.string.mine_str_2);
        tvRight.setText("说明");
    }

    boolean isOnce = true;

    @Override
    protected void onResume() {
        super.onResume();
        HttpUtil.getInstance().getApiService().getUserInfo().compose(RxUtils.rxSchedulerHelper()).subscribe(new SuccessSubscriber<ResponseBean<UserBean>>() {
            @Override
            public void onSuccess(ResponseBean<UserBean> userBeanResponseBean) {
                if (userBeanResponseBean.getResult() == null) return;
                if (isOnce && !TextUtils.isEmpty(userBeanResponseBean.getResult().vipCountdown)) {
                    SureDialog sureDialog = new SureDialog(MemberActivity.this);
                    sureDialog.setContent(userBeanResponseBean.getResult().vipCountdown);
                    sureDialog.show();
                    isOnce = false;
                }
                TaipingApplication.tpApp.isVIP = userBeanResponseBean.getResult().isVIP;
                if (TaipingApplication.tpApp.isVIP) {
                    tvVip.setText("VIP会员");
                    tvOverTime.setText("到期日期：" + df.format(userBeanResponseBean.getResult().appLevelOverTime) + "");
                    bgMember.setBackgroundResource(R.mipmap.become_member_icon);
                    viewBecomeMember.setVisibility(View.GONE);
                    tvUpgrade.setText("立即续费");
                } else {
                    tvVip.setText("普通会员 ");
                    bgMember.setBackgroundResource(R.mipmap.member_top);
                    tvOverTime.setText("");
                    viewBecomeMember.setVisibility(View.VISIBLE);
                    tvUpgrade.setText("立即升级");
                }
            }

            @Override
            public void onFailed(String str) {
            }
        });

    }

    @OnClick({R.id.bt_become_member, R.id.bt_become, R.id.title_right})
    public void onMember(View view) {
        switch (view.getId()) {
            case R.id.bt_become:
                MemberBuyActiviy.launch(this);
                break;
            case R.id.bt_become_member:
                MemberBuyActiviy.launch(this);
                break;
            case R.id.title_right:
                MyWebViewActivity.launch(this, 1);
                break;
        }
    }

    public static void launch(Context context) {
        context.startActivity(new Intent(context, MemberActivity.class));
    }

}
