package com.yuntu.mawei.ui.mine.set;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.yuntu.mawei.R;
import com.yuntu.mawei.base.home.BaseActivity;
import com.yuntu.mawei.ui.login.LoginActivity;
import com.yuntu.mawei.ui.mine.set.message.MyMessageActivity;
import com.yuntu.mawei.util.SharedPreferenceUtil;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by tanghao on 2019/1/24
 */
public class NetWorkSettingActivity extends BaseActivity {

    @BindView(R.id.bt_network_set)
    ImageView ivNetWork;
    private boolean isMobileDownload;

    @Override
    public int attachLayout() {
        return R.layout.activity_network_setting;
    }

    @Override
    public void init() {
        initTitle(R.string.network_set);
        isMobileDownload = SharedPreferenceUtil.getInstance().getBoolean("isMobileDownload", true);
    }

    @OnClick(R.id.bt_network_set)
    public void onSetClick(View view) {
        if (isMobileDownload) {
            isMobileDownload = false;
        } else {
            isMobileDownload = true;
        }
        if(isMobileDownload){
            ivNetWork.setImageResource(R.mipmap.network_set_yes);
        }else{
            ivNetWork.setImageResource(R.mipmap.network_set_no);
        }
        SharedPreferenceUtil.getInstance().putBoolean("isMobileDownload", isMobileDownload);
    }


    public static void launch(Context context) {
        context.startActivity(new Intent(context, NetWorkSettingActivity.class));
    }

}
