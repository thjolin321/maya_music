package com.yuntu.mawei.ui.circle;

import com.yuntu.mawei.base.home.BaseListFragment;
import com.yuntu.mawei.base.home.BaseListFragmentPresenter;
import com.yuntu.mawei.bean.base.ResponseBean;
import com.yuntu.mawei.bean.circle.CircleBean;
import com.yuntu.mawei.bean.rx.SuccessSubscriber;
import com.yuntu.mawei.util.RxUtils;
import com.yuntu.mawei.util.http.HttpUtil;

import java.util.ArrayList;
import java.util.List;

public class CirclePresenter extends BaseListFragmentPresenter {

    int page;

    CircleFragment fragment;

    public CirclePresenter(CircleFragment mView) {
        super(mView);
        fragment = mView;
    }

    @Override
    public void getData(boolean isPullRefresh) {
        HttpUtil.getInstance().getApiService().getCircleList(1, 10,null)
                .compose(RxUtils.rxSchedulerHelper())
                .subscribe(new SuccessSubscriber<ResponseBean<List<CircleBean>>>() {
                    @Override
                    public void onSuccess(ResponseBean<List<CircleBean>> listResponseBean) {
                        if (listResponseBean.getResult() == null) return;
                        mView.loadData(listResponseBean.getResult());
                        fragment.finishRefresh();
                        page = 2;
                    }

                    @Override
                    public void onFailed(String str) {
                    }
                });
    }

    @Override
    public void getMoreData() {
        HttpUtil.getInstance().getApiService().getCircleList(page, 10,null)
                .compose(RxUtils.rxSchedulerHelper())
                .subscribe(new SuccessSubscriber<ResponseBean<List<CircleBean>>>() {
                    @Override
                    public void onSuccess(ResponseBean<List<CircleBean>> listResponseBean) {
                        if (listResponseBean.getResult() == null) return;
                        mView.loadMoreData(listResponseBean.getResult());
                        page++;
                    }

                    @Override
                    public void onFailed(String str) {
                    }
                });
    }
}
