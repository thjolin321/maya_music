package com.yuntu.mawei.constant;

/**
 * Actions
 * Created by tanghao on 2016/1/22.
 */
public interface Actions {
    String VOLUME_CHANGED_ACTION = "android.media.VOLUME_CHANGED_ACTION";
    String ACTION_STOP = "me.wcy.music.ACTION_STOP";
}
