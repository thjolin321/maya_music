package com.yuntu.mawei.constant;

/**
 * Extras
 * Created by tanghao on 2015/12/25.
 */
public interface Extras {
    String EXTRA_NOTIFICATION = "me.wcy.music.notification";
    String EFFECT = "{\"code\":403";// } 判断失效的json头
    String MUSIC_LIST_TYPE = "music_list_type";
    String TING_UID = "ting_uid";
    String MUSIC = "music";
}
