package com.yuntu.mawei.constant.api;

import com.yuntu.mawei.bean.banner.BannerBean;
import com.yuntu.mawei.bean.base.ResponseBean;
import com.yuntu.mawei.bean.base.XieyiBean;
import com.yuntu.mawei.bean.circle.CircleBean;
import com.yuntu.mawei.bean.collect.CollectBean;
import com.yuntu.mawei.bean.house.MusicHouseBean;
import com.yuntu.mawei.bean.house.MusicRankBean;
import com.yuntu.mawei.bean.money.XiaofeiBean;
import com.yuntu.mawei.bean.song.AlbumBean;
import com.yuntu.mawei.bean.song.HaveCollectBean;
import com.yuntu.mawei.bean.song.SingerBean;
import com.yuntu.mawei.bean.song.SongBean;
import com.yuntu.mawei.ui.circle.detail.CircleDetailRoot;
import com.yuntu.mawei.ui.homepage.mv.detail.MvDetailBean;
import com.yuntu.mawei.ui.homepage.singer.detail.SingerAlbumMapBean;
import com.yuntu.mawei.ui.homepage.singer.detail.SingerMapBean;
import com.yuntu.mawei.ui.login.TokenBean;
import com.yuntu.mawei.ui.mine.member.MemberBean;
import com.yuntu.mawei.ui.mine.set.message.MessageBean;
import com.yuntu.mawei.ui.mine.user.UserBean;
import com.yuntu.mawei.ui.playing.gift.GiftBean;

import java.util.List;

import javax.annotation.Nullable;

import okhttp3.MultipartBody;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;
import rx.Observable;

public interface ApiInterface {

    @FormUrlEncoded
    @POST("app/login/register")
    Observable<ResponseBean> register(@Field("phone") String phone,
                                      @Field("password") String password,
                                      @Field("verificationCode") String verificationCode);

    @FormUrlEncoded
    @POST("app/login/login")
    Observable<ResponseBean<TokenBean>> login(@Field("phone") String phone,
                                              @Field("password") String password);

    @FormUrlEncoded
    @POST("app/login/forgetPassword")
    Observable<ResponseBean> vertifyForget(@Field("phone") String phone,
                                           @Field("verificationCode") String verificationCode);

    @FormUrlEncoded
    @POST("app/login/resetPassword")
    Observable<ResponseBean> resettingPassword(@Field("phone") String phone,
                                               @Field("newPassword") String newPassword);

    @FormUrlEncoded
    @POST("app/user/updatePassword")
    Observable<ResponseBean> resettingPasswordSet(@Field("oldPassword") String phone,
                                                  @Field("newPassword") String newPassword);

    @GET("app/user/getUserInfo")
    Observable<ResponseBean<UserBean>> getUserInfo();

    @FormUrlEncoded
    @POST("app/user/updateUserInfo")
    Observable<ResponseBean> updateUserBean(@Field("newUserName") String newUserName,
                                            @Field("newPhoneNumber") String newPhoneNumber,
                                            @Field("newSex") String newSex,
                                            @Field("newAge") String newAge,
                                            @Nullable @Field("newUserAvator") String newUserAvator);

    @FormUrlEncoded
    @POST("app/user/updateUserInfo")
    Observable<ResponseBean> updateUserBean(@Field("newPhoneNumber") String newPhoneNumber);

    @GET("app/banner/getByBannerType")
    Observable<ResponseBean<List<BannerBean>>> getBanner(@Query("type") int type);

    /**
     * 由此分割，下面是关于歌曲的接口
     */

    @GET("app/song/getPopularSong")
    Observable<ResponseBean<List<SongBean>>> getHotSong(@Query("songType") int songType,
                                                        @Query("pageNum") int pageNum,
                                                        @Query("pageSize") int pageSize);


    @GET("app/song/selectSongTypeListWithSong")
    Observable<ResponseBean<List<MusicHouseBean>>> getHouseSong(@Query("isFree") int isFree);

    @GET("app/song/rankRuleListSingerById")
    Observable<ResponseBean<MusicHouseBean>> getSongListWithType(@Query("rankClass") long songTypeId,
                                                                 @Query("pageNum") int pageNum,
                                                                 @Query("pageSize") int pageSize);

    @GET("app/song/selectInitialsSongList")
    Observable<ResponseBean<MusicHouseBean>> getHouseSongListWithType(@Query("songTypeId") long songTypeId,
                                                                      @Query("isFree") int isFree,
                                                                      @Query("pageNum") int pageNum,
                                                                      @Query("pageSize") int pageSize);

    @FormUrlEncoded
    @POST("app/collect/collect")
    Observable<ResponseBean> collect(@Field("songId") String songId);

    @FormUrlEncoded
    @POST("app/collect/addCollect")
    Observable<ResponseBean> collectAll(@Field("songId") String songId);

    @FormUrlEncoded
    @POST("app/collect/cancelCollect")
    Observable<ResponseBean> collectDelete(@Field("songId") String songId);

    @FormUrlEncoded
    @POST("app/song/batchCancelPlay")
    Observable<ResponseBean> recentDelete(@Field("playIds") String playIds);

    @GET("app/collect/myCollect")
    Observable<ResponseBean<CollectBean>> getCollectList(@Query("songType") int songType,
                                                         @Query("pageNum") int pageNum,
                                                         @Query("pageSize") int pageSize);

    @GET("app/song/playRecord")
    Observable<ResponseBean<CollectBean>> getRecentList(@Query("songType") int songType,
                                                        @Query("pageNum") int pageNum,
                                                        @Query("pageSize") int pageSize);

    @GET("app/song/downloadSong")
    Observable<ResponseBean> getDownloadUrl(@Query("songId") long songId);

    @GET("app/song/ranKingListSong")
    Observable<ResponseBean<List<MusicRankBean>>> getRankList(@Query("rankClass") int rankClass);

    @GET("app/song/rankRuleById")
    Observable<ResponseBean<MusicHouseBean>> getRankSongList(@Query("rankId") long rankClass,
                                                             @Query("pageNum") int pageNum,
                                                             @Query("pageSize") int pageSize);

    @GET("app/singer/popularSinger")
    Observable<ResponseBean<List<SingerBean>>> getSingerList(@Query("pageNum") int pageNum,
                                                             @Query("pageSize") int pageSize);

    @GET("app/singer/latelySinger")
    Observable<ResponseBean<List<SingerBean>>> getRecentSingerList(@Query("pageNum") int pageNum,
                                                                   @Query("pageSize") int pageSize);

    @GET("app/song/findHomePageSong")
    Observable<ResponseBean<List<SongBean>>> getHomeSongList(@Query("songType") int songType,
                                                             @Query("isFree") int isFree,
                                                             @Query("pageNum") int pageNum,
                                                             @Query("pageSize") int pageSize);

    @GET("app/singer/getMVDetails")
    Observable<ResponseBean<MvDetailBean>> getMvDetail(@Query("songId") long songId);

    @GET("app/song/songPlay")
    Observable<ResponseBean> playCountAdd(@Query("songId") long songId);

    @FormUrlEncoded
    @POST("app/search/getSearchResult")
    Observable<ResponseBean> searchList(@Field("songId") long songId);


    @Headers("api-ver: 1.0.0")
    @POST("app/user/uploadAvatar")
    @Multipart
    Observable<ResponseBean> uploadMallFileSingle(@Part MultipartBody.Part files);

    @GET("app/massage/findMassageList")
    Observable<ResponseBean<List<MessageBean>>> getMessageList(@Query("pageNum") int pageNum,
                                                               @Query("pageSize") int pageSize);


    @GET("app/commodity/findGift")
    Observable<ResponseBean<List<GiftBean>>> getGiftList(@Query("pageNum") int pageNum,
                                                         @Query("pageSize") int pageSize);

    @FormUrlEncoded
    @POST("app/pay/commodityPay")
    Observable<ResponseBean> pay(@Field("payType") int payType,
                                 @Field("commodityId") long commodityId,
                                 @Field("commodityCount") int commodityCount,
                                 @Field("songId") long songId);

    @GET("app/song/ranKingListSinger")
    Observable<ResponseBean<List<MusicRankBean>>> getSingerList();

    @FormUrlEncoded
    @POST("app/user/getRechargeRecord")
    Observable<ResponseBean<List<XiaofeiBean>>> getXiaofeiBean(@Field("pageNum") int pageNum,
                                                               @Field("pageSize") int pageSize);

    @GET("app/singer/findAlbumBySingId")
    Observable<ResponseBean<SingerAlbumMapBean>> getSingerAlbum(@Query("singerId") int singerId,
                                                                @Query("isSearch") int isSearch,
                                                                @Query("pageNum") int pageNum,
                                                                @Query("pageSize") int pageSize);

    @GET("app/singer/findSongByAlbumId")
    Observable<ResponseBean<List<SongBean>>> getAlbumSongList(@Query("albumId") int albumId,
                                                              @Query("isSearch") int isSearch,
                                                              @Query("pageNum") int pageNum,
                                                              @Query("pageSize") int pageSize);


    @GET("app/singer/songBySingerId")
    Observable<ResponseBean<SingerMapBean>> getSingerSongList(@Query("singerId") int singerId,
                                                              @Query("songType") int songType,
                                                              @Query("isSearch") int isSearch,
                                                              @Query("pageNum") int pageNum,
                                                              @Query("pageSize") int pageSize);

    @GET("app/user/listMusicTheme")
    Observable<ResponseBean<List<CircleBean>>> getCircleList(@Query("pageNum") int pageNum,
                                                             @Query("pageSize") int pageSize,
                                                             @Query("searchInfo") String searchInfo);

    @GET("app/user/listMyMusicTheme")
    Observable<ResponseBean<List<CircleBean>>> getMyPublishCircle(@Query("pageNum") int pageNum,
                                                                  @Query("pageSize") int pageSize);

    @GET("app/deploy/getByDeployType")
    Observable<ResponseBean<XieyiBean>> getXieyi(@Query("type") int type);

    @GET("app/user/getMusicTheme")
    Observable<ResponseBean<CircleDetailRoot>> getCircleDetail(@Query("themeId") int themeId);

    @FormUrlEncoded
    @POST("app/user/thumbMusicTheme")
    Observable<ResponseBean> dianZan(@Field("themeId") int themeId);

    @FormUrlEncoded
    @POST("app/user/commentMusicTheme")
    Observable<ResponseBean> comment(@Field("themeId") int themeId,
                                     @Field("classCapacity") String classCapacity);

    @FormUrlEncoded
    @POST("app/user/reportMusicTheme")
    Observable<ResponseBean> jubao(@Field("themeId") int themeId,
                                   @Field("reportDetails") String reportDetails);

    @GET("app/song/getSongInfo")
    Observable<ResponseBean<HaveCollectBean>> getIsCollect(@Query("songId") long songId);

    @POST("app/search/getPopularSearch")
    Observable<ResponseBean<List<SongBean>>> getHotSearchData();

    @FormUrlEncoded
    @POST("app/search/deleteSearchHistory")
    Observable<ResponseBean> cleartHistoryData(@Field("searchId") String searchId);

    @FormUrlEncoded
    @POST("app/search/getSearchResult")
    Observable<ResponseBean<List<SongBean>>> getSearchResult(@Field("searchInfo") String searchInfo,
                                                             @Field("searchType") int searchType,
                                                             @Field("pageNum") int pageNum,
                                                             @Field("pageSize") int pageSize);


    @POST("app/user/getBuyLevel")
    Observable<ResponseBean<List<MemberBean>>> getMemberList();

    @FormUrlEncoded
    @POST("app/song/pushListenCount")
    Observable<ResponseBean> putCount(@Field("songId") int pageSize);

    @FormUrlEncoded
    @POST("app/user/delMusicTheme")
    Observable<ResponseBean> deleteComment(@Field("themeId") int themeId);

    @FormUrlEncoded
    @POST("app/user/thumbMusicTheme")
    Observable<ResponseBean> dianzanMv(@Field("themeId") int themeId,
                                       @Field("songId") int songId);

    @FormUrlEncoded
    @POST("app/login/wxLogin")
    Observable<ResponseBean> wxLogin(@Field("wxId") String wxId,
                                     @Field("wxName") String wxName,
                                     @Field("wxAvatar") String wxAvatar);

    @FormUrlEncoded
    @POST("app/login/getVerificationCode")
    Observable<ResponseBean> sendMessage(@Field("phone") String phone);


    @FormUrlEncoded
    @POST("app/login/bindingPhone")
    Observable<ResponseBean> wxBind(@Field("wxId") String wxId,
                                    @Field("phone") String phone,
                                    @Field("newPassword") String newPassword,
                                    @Field("verificationCode") String verificationCode);

    @FormUrlEncoded
    @POST("app/login/updatePhone")
    Observable<ResponseBean> updatePhone(@Field("newPhone") String wxId,
                                         @Field("oldPhone") String phone,
                                         @Field("newVerificationCode") String newPassword,
                                         @Field("oldVerificationCode") String verificationCode);

}
