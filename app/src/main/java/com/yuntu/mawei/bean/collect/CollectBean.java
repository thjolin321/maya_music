package com.yuntu.mawei.bean.collect;

import com.yuntu.mawei.bean.song.SongBean;

import java.util.List;
import java.util.Map;

/**
 * Created by tanghao on 2019/2/14
 */
public class CollectBean {
    public int songCount;
    public Map<String,List<SongBean>> songMap;

}
