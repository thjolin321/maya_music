package com.yuntu.mawei.bean.executor;

import android.app.Activity;
import android.text.TextUtils;

import com.yuntu.mawei.BuildConfig;
import com.yuntu.mawei.bean.base.ResponseBean;
import com.yuntu.mawei.bean.collect.CollectBean;
import com.yuntu.mawei.bean.music.OnlineMusic;
import com.yuntu.mawei.bean.rx.SuccessSubscriber;
import com.yuntu.mawei.util.GsonUtil;
import com.yuntu.mawei.util.Logl;
import com.yuntu.mawei.util.RxUtils;
import com.yuntu.mawei.util.http.HttpCallback;
import com.yuntu.mawei.util.http.HttpClient;
import com.yuntu.mawei.util.http.HttpUtil;
import com.yuntu.mawei.util.play.FileUtils;

import java.io.File;
import java.util.logging.Logger;

/**
 * 下载音乐
 * Created by wcy on 2016/1/3.
 */
public abstract class DownloadOnlineMusic extends DownloadMusic {
    private OnlineMusic mOnlineMusic;

    public DownloadOnlineMusic(Activity activity, OnlineMusic onlineMusic) {
        super(activity);
        mOnlineMusic = onlineMusic;
    }

    @Override
    protected void download() {
        final String artist = mOnlineMusic.getArtist_name();
        final String title = mOnlineMusic.getTitle();

        // 下载歌词
        String lrcFileName = FileUtils.getLrcFileName(artist, title);
        File lrcFile = new File(FileUtils.getLrcDir() + lrcFileName);
        if (!TextUtils.isEmpty(mOnlineMusic.getLrclink()) && !lrcFile.exists()) {
            HttpClient.downloadFile(mOnlineMusic.getLrclink(), FileUtils.getLrcDir(), lrcFileName, null);
        }
        // 下载封面
        String albumFileName = FileUtils.getAlbumFileName(artist, title);
        final File albumFile = new File(FileUtils.getAlbumDir(), albumFileName);
        String picUrl = mOnlineMusic.getPic_big();
        if (TextUtils.isEmpty(picUrl)) {
            picUrl = mOnlineMusic.getPic_small();
        }
        if (!albumFile.exists() && !TextUtils.isEmpty(picUrl)) {
            HttpClient.downloadFile(picUrl, FileUtils.getAlbumDir(), albumFileName, null);
        }

        String downloadUrl = BuildConfig.MALL_HOST + "app/song/downloadSong?songId=" + mOnlineMusic.song_id;
        Logl.e("准备下载歌曲");
        downloadMusic(downloadUrl, artist, title, mOnlineMusic.localMusicJson);
        onExecuteSuccess(null);
    }
}
