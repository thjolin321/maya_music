package com.yuntu.mawei.bean.rx;

import com.yuntu.mawei.bean.base.ResponseBean;
import com.yuntu.mawei.util.Logl;

import rx.Subscriber;

/**
 * Created by tanghao on 2019/1/23
 */
public abstract class SuccessWithCodeSubscriber<T> extends Subscriber<T> {
    @Override
    public void onCompleted() {
    }

    @Override
    public void onError(Throwable e) {
        Logl.e("错误信息：" + e.getMessage());
        onFailed(0, null);
    }

    @Override
    public void onNext(T t) {
        if (t instanceof ResponseBean) {
            if (((ResponseBean) t).getCode() == 200) {
                onSuccess(t);
            } else {
                onFailed(((ResponseBean) t).getCode(), ((ResponseBean) t).getMessage());
            }
        }
    }

    public abstract void onSuccess(T t);

    public abstract void onFailed(int code, String str);

}
