package com.yuntu.mawei.bean.circle;

import java.io.Serializable;
import java.util.List;

public class CircleBean implements Serializable {

    public int themeId;
    public String themeName;
    public int themeType;
    public String imageUrl;
    public int isTop;

    public String appUserAvatar;
    public int appUserId;
    public String appUserName;
    public String classCapacity;
    public int commentCount;
    public String createTime;
    public String musicUrl;
    public int thumbCount;
    public int thumbId;
    public String videoUrl;
    public List<CommentBean> listApply;
    public boolean isEdit;

}
