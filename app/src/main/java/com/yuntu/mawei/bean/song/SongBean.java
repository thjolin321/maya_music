package com.yuntu.mawei.bean.song;

import com.yuntu.mawei.bean.base.ResponseBean;
import com.yuntu.mawei.bean.music.Music;
import com.yuntu.mawei.view.adapter.entity.MultiItemEntity;

import java.io.Serializable;

/**
 * Created by tanghao on 2019/1/25
 */
public class SongBean extends MultiItemEntity {

    public static final int SONG_ITEM = 0;
    public static final int SONG_TITLE_ITEM = 1;

    public SongBean(int itemType) {
        super(itemType);
    }

    public SongBean(int itemType, String zimuTitle) {
        super(itemType);
        this.zimuTitle = zimuTitle;
    }

    public AlbumBean album;
    public int albumId;
    public LrcBean lyric;
    public String albumIntroduction;
    public String createBy;
    public String delFlag;
    public int downloadCount;
    public int hotType;
    public int isFree;
    public int listenCount;
    public int lyricId;
    public int nowType;
    public int recommendValue;
    public String remark;
    public String searchValue;
    public SingerBean singer;
    public int singerId;
    public String songAvatar;
    public String songBackground;
    public long songId;
    public int songLabelId;
    public String songName;
    public int songType; // 0 for song, 1 for ring, 2 for mv
    public int songTypeId;
    public String songUrl;
    public String singNameSyllable;
    public double songValue;
    public Object collect;
    public String zimuTitle;
    public boolean isSelected;
    public int songTime;
    public long dbId;
    public int downloadFlag;
    public int isAgree;
    public int isCollect;
    public String songStory;
    public int agreeCount;

    public Music musicLocal; // just for local music;

}
