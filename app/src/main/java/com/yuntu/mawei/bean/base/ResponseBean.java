package com.yuntu.mawei.bean.base;

import javax.annotation.Nullable;

/**
 * Created by feng on 2017/10/26.
 */

public class ResponseBean<T> {


    /**
     * state : SUCCESS
     * code : 200
     * msg : 成功
     * result : http://www.baidu.com
     */

    private String state;
    private int code;
    private String msg;
    @Nullable
    private Pagination pagination;
    @Nullable
    private T result;

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return msg;
    }

    public void setMessage(String msg) {
        this.msg = msg;
    }

    @Nullable
    public Pagination getPagination() {
        return pagination;
    }

    public void setPagination(Pagination pagination) {
        this.pagination = pagination;
    }

    public T getResult() {
        return result;
    }

    public void setResult(T result) {
        this.result = result;
    }


    @Override
    public String toString() {
        return "ResponseBean{" +
                "state='" + state + '\'' +
                ", code=" + code +
                ", msg='" + msg + '\'' +
                ", pagination=" + pagination +
                ", result=" + result +
                '}';
    }
}
