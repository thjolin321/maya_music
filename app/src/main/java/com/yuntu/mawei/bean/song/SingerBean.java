package com.yuntu.mawei.bean.song;

import java.io.Serializable;

/**
 * Created by tanghao on 2019/1/29
 */
public class SingerBean implements Serializable{

    public String createBy;
    public String delFlag;
    public String listenCount;
    public String searchValue;
    public String singerAvatar;
    public int singerId;
    public String singerIntroduction;
    public String singerName;
    public String singerSex;
    public String singerStatus;
    public String updateBy;
    public Object updateTime;
    public int songNumber;
    public int albumNumber;


}
