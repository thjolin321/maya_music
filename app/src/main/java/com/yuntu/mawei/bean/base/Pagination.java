package com.yuntu.mawei.bean.base;

/**
 * Created by tanghao on 2017/9/11.
 * Email: 86882259@qq.com
 */

public class Pagination {

    public int totalRow;
    public int pageSize;
    public int pageIndex;
    public int pageCount;

    public int getTotalRow() {
        return totalRow;
    }

    public void setTotalRow(int totalRow) {
        this.totalRow = totalRow;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public int getPageIndex() {
        return pageIndex;
    }

    public void setPageIndex(int pageIndex) {
        this.pageIndex = pageIndex;
    }

    public int getPageCount() {
        return pageCount;
    }

    public void setPageCount(int pageCount) {
        this.pageCount = pageCount;
    }
}
