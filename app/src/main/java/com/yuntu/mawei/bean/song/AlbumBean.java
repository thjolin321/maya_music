package com.yuntu.mawei.bean.song;

import com.yuntu.mawei.view.adapter.entity.MultiItemEntity;

import java.io.Serializable;

/**
 * Created by tanghao on 2019/1/30
 */
public class AlbumBean extends MultiItemEntity implements Serializable {

    public static final int SONG_ITEM = 0;
    public static final int SONG_TITLE_ITEM = 1;


    public AlbumBean(int itemType) {
        super(itemType);
    }

    public AlbumBean(int itemType, String zimuTitle) {
        super(itemType);
        this.zimuTitle = zimuTitle;
    }

    public int albumId;
    public String albumAvatar;
    public String albumName;
    public String zimuTitle;
    public String createTime;

    public String albumType;
    public String albumLanguage;
    public String albumCompany;
    public String albumIntroduction;


}
