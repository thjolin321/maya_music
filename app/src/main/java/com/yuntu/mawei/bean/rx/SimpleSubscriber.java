package com.yuntu.mawei.bean.rx;

import com.yuntu.mawei.util.Logl;

import rx.Subscriber;

/**
 * Created by tanghao on 2019/1/23
 */
public abstract class SimpleSubscriber<T> extends Subscriber<T> {
    @Override
    public void onCompleted() {
    }

    @Override
    public void onError(Throwable e) {
        Logl.e(e.toString());
    }
}
