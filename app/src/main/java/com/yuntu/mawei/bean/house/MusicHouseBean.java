package com.yuntu.mawei.bean.house;

import com.yuntu.mawei.bean.music.Music;
import com.yuntu.mawei.bean.song.SingerBean;
import com.yuntu.mawei.bean.song.SongBean;

import java.util.List;
import java.util.Map;

public class MusicHouseBean {

    public int icon;
    public int type;
    public int songTypeId;
    public int songCount;
    public String typeName;
    public String songTypeImage;
    public List<SongBean> songList;
    public List<SingerBean> singerList;
    public Map<String,List<SongBean>> songMap;

    public MusicHouseBean(int icon, int type, List<SongBean> songList) {
        this.icon = icon;
        this.type = type;
        this.songList = songList;
    }
}
