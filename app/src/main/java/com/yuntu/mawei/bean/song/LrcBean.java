package com.yuntu.mawei.bean.song;

import java.io.Serializable;

/**
 * Created by tanghao on 2019/1/30
 */
public class LrcBean implements Serializable{
    public int lyricId;
    public String lyricUrl;
    public String songName;
}
