package com.yuntu.mawei.bean.house;

import com.yuntu.mawei.bean.song.SingerBean;
import com.yuntu.mawei.bean.song.SongBean;

import java.util.List;
import java.util.Map;

public class MusicRankBean {

    public int rankId;
    public int rankType;
    public int rankValue;
    public String rankName;
    public String rankImage;
    public List<SongBean> songList;
    public List<SingerBean> singerList;
    public Map<String,List<SongBean>> songMap;

}
