package com.yuntu.mawei.bean.music;

import android.text.TextUtils;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.NotNull;
import org.greenrobot.greendao.annotation.Property;
import org.greenrobot.greendao.annotation.Unique;

import java.io.Serializable;

/**
 * 单曲信息
 * Created by wcy on 2015/11/27.
 */
@Entity(nameInDb = "MusicLocal")
public class MusicLocal implements Serializable {
    public static final long serialVersionUID = 536871008;

    @Id(autoincrement = true)
    @Property(nameInDb = "id")
    public Long id;

    @NotNull
    @Property(nameInDb = "type")
    public int type; // 歌曲类型:本地/网络
    @Unique
    @Property(nameInDb = "songId")
    public long songId; // [本地]歌曲ID
    @Property(nameInDb = "title")
    public String title; // 音乐标题
    @Property(nameInDb = "artist")
    public String artist; // 艺术家
    @Property(nameInDb = "album")
    public String album; // 专辑
    @Property(nameInDb = "albumId")
    public long albumId; // [本地]专辑ID
    @Property(nameInDb = "coverPath")
    public String coverPath; // [在线]专辑封面路径
    @NotNull
    @Property(nameInDb = "duration")
    public long duration; // 持续时间
    @NotNull
    @Property(nameInDb = "path")
    public String path; // 播放地址
    @Property(nameInDb = "fileName")
    public String fileName; // [本地]文件名
    @Property(nameInDb = "fileSize")
    public long fileSize; // [本地]文件大小

    public boolean isCollect; // 是否收藏
    public String songBackground;
    public String lrclink;
    public int songType;
    public String firstZimu;
    public int itemType; // 0 for item, 1 for title
    public int musicType; // 0 for song ,1 for ring ,2 for mv
    public long spareNum1;
    public long spareNum2;
    public long spareNum3;
    public String spareStr1;
    public String spareStr2;
    public String spareStr3;

    public Music mapToMusic() {
        Music music = new Music();
        music.id = id;
        music.type = type;
        music.songId = songId;
        music.title = title;
        music.artist = artist;
        music.album = album;
        music.albumId = albumId;
        music.coverPath = coverPath;
        music.duration = duration;
        music.path = path;
        music.fileName = fileName;
        music.fileSize = fileSize;
        music.isCollect = isCollect;
        music.songBackground = songBackground;
        music.lrclink = lrclink;
        music.songType = songType;
        music.firstZimu = firstZimu;
        music.itemType = itemType;
        music.musicType = musicType;
        music.spareNum1 = spareNum1;
        music.spareNum2 = spareNum2;
        music.spareNum3 = spareNum3;
        music.spareStr1 = spareStr1;
        music.spareStr2 = spareStr2;
        music.spareStr3 = spareStr3;
        return music;
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof MusicLocal)) {
            return false;
        }
        MusicLocal music = (MusicLocal) o;
        if (music.songId > 0 && music.songId == this.songId) {
            return true;
        }
        if (TextUtils.equals(music.title, this.title)
                && TextUtils.equals(music.artist, this.artist)
                && TextUtils.equals(music.album, this.album)
                && music.duration == this.duration) {
            return true;
        }
        return false;
    }

    @Generated(hash = 345909716)
    public MusicLocal(Long id, int type, long songId, String title, String artist,
                      String album, long albumId, String coverPath, long duration,
                      @NotNull String path, String fileName, long fileSize, boolean isCollect,
                      String songBackground, String lrclink, int songType, String firstZimu,
                      int itemType, int musicType, long spareNum1, long spareNum2,
                      long spareNum3, String spareStr1, String spareStr2, String spareStr3) {
        this.id = id;
        this.type = type;
        this.songId = songId;
        this.title = title;
        this.artist = artist;
        this.album = album;
        this.albumId = albumId;
        this.coverPath = coverPath;
        this.duration = duration;
        this.path = path;
        this.fileName = fileName;
        this.fileSize = fileSize;
        this.isCollect = isCollect;
        this.songBackground = songBackground;
        this.lrclink = lrclink;
        this.songType = songType;
        this.firstZimu = firstZimu;
        this.itemType = itemType;
        this.musicType = musicType;
        this.spareNum1 = spareNum1;
        this.spareNum2 = spareNum2;
        this.spareNum3 = spareNum3;
        this.spareStr1 = spareStr1;
        this.spareStr2 = spareStr2;
        this.spareStr3 = spareStr3;
    }

    @Generated(hash = 1777517830)
    public MusicLocal() {
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getType() {
        return this.type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public long getSongId() {
        return this.songId;
    }

    public void setSongId(long songId) {
        this.songId = songId;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getArtist() {
        return this.artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public String getAlbum() {
        return this.album;
    }

    public void setAlbum(String album) {
        this.album = album;
    }

    public long getAlbumId() {
        return this.albumId;
    }

    public void setAlbumId(long albumId) {
        this.albumId = albumId;
    }

    public String getCoverPath() {
        return this.coverPath;
    }

    public void setCoverPath(String coverPath) {
        this.coverPath = coverPath;
    }

    public long getDuration() {
        return this.duration;
    }

    public void setDuration(long duration) {
        this.duration = duration;
    }

    public String getPath() {
        return this.path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getFileName() {
        return this.fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public long getFileSize() {
        return this.fileSize;
    }

    public void setFileSize(long fileSize) {
        this.fileSize = fileSize;
    }

    public boolean getIsCollect() {
        return this.isCollect;
    }

    public void setIsCollect(boolean isCollect) {
        this.isCollect = isCollect;
    }

    public String getSongBackground() {
        return this.songBackground;
    }

    public void setSongBackground(String songBackground) {
        this.songBackground = songBackground;
    }

    public String getLrclink() {
        return this.lrclink;
    }

    public void setLrclink(String lrclink) {
        this.lrclink = lrclink;
    }

    public int getSongType() {
        return this.songType;
    }

    public void setSongType(int songType) {
        this.songType = songType;
    }

    public String getFirstZimu() {
        return this.firstZimu;
    }

    public void setFirstZimu(String firstZimu) {
        this.firstZimu = firstZimu;
    }

    public int getItemType() {
        return this.itemType;
    }

    public void setItemType(int itemType) {
        this.itemType = itemType;
    }

    public int getMusicType() {
        return this.musicType;
    }

    public void setMusicType(int musicType) {
        this.musicType = musicType;
    }

    public long getSpareNum1() {
        return this.spareNum1;
    }

    public void setSpareNum1(long spareNum1) {
        this.spareNum1 = spareNum1;
    }

    public long getSpareNum2() {
        return this.spareNum2;
    }

    public void setSpareNum2(long spareNum2) {
        this.spareNum2 = spareNum2;
    }

    public long getSpareNum3() {
        return this.spareNum3;
    }

    public void setSpareNum3(long spareNum3) {
        this.spareNum3 = spareNum3;
    }

    public String getSpareStr1() {
        return this.spareStr1;
    }

    public void setSpareStr1(String spareStr1) {
        this.spareStr1 = spareStr1;
    }

    public String getSpareStr2() {
        return this.spareStr2;
    }

    public void setSpareStr2(String spareStr2) {
        this.spareStr2 = spareStr2;
    }

    public String getSpareStr3() {
        return this.spareStr3;
    }

    public void setSpareStr3(String spareStr3) {
        this.spareStr3 = spareStr3;
    }

}
