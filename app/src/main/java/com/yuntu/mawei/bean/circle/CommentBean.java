package com.yuntu.mawei.bean.circle;

import java.io.Serializable;

public class CommentBean implements Serializable {

    public String appUserAvatar;
    public int appUserId;
    public String appUserName;
    public String classCapacity;
    public int themeId;
    public int thumbCount;

}
