package com.yuntu.mawei.bean.banner;

import com.yuntu.mawei.bean.song.SongBean;

import java.util.List;

public class BannerBean {

    public int bannerId;
    public String bannerName;
    public int bannerStatus;
    public int bannerType;
    public String bannerUrl;
    public String createBy;
    public String navigationUrl;
    public String remark;
    public String searchValue;
    public int songId;
    public String songName;
    public int sort;
    public String updateBy;

}
