package com.yuntu.mawei.bean.executor;

import android.app.Activity;
import android.text.TextUtils;
import android.util.Log;

import com.yuntu.mawei.base.TaipingApplication;
import com.yuntu.mawei.bean.music.Music;
import com.yuntu.mawei.bean.music.OnlineMusic;
import com.yuntu.mawei.util.Logl;
import com.yuntu.mawei.util.http.HttpCallback;
import com.yuntu.mawei.util.http.HttpClient;
import com.yuntu.mawei.util.play.FileUtils;

import java.io.File;

/**
 * 播放在线音乐
 * Created by wcy on 2016/1/3.
 */
public abstract class PlayOnlineMusic extends PlayMusic {
    private OnlineMusic mOnlineMusic;

    public PlayOnlineMusic(Activity activity, OnlineMusic onlineMusic) {
        super(activity, 3);
        mOnlineMusic = onlineMusic;
    }

    @Override
    protected void getPlayInfo() {
        Log.e("TAG", "getPlayInfo");
        String artist = mOnlineMusic.getArtist_name();
        String title = mOnlineMusic.getTitle();

        music = new Music();
        music.setType(Music.Type.ONLINE);
        music.setTitle(title);
        music.setArtist(artist);
        music.setPath(mOnlineMusic.path);
        music.setDuration(mOnlineMusic.duration);
        music.setAlbum(mOnlineMusic.getAlbum_title());
        music.coverPath = mOnlineMusic.songAvatar;
        music.songId = mOnlineMusic.songId;
        music.lrclink = mOnlineMusic.lrclink;
        music.songBackground = mOnlineMusic.pic_big;
        music.isCollect = mOnlineMusic.isCollect;
        music.album = mOnlineMusic.album;
        music.songType = mOnlineMusic.songType;
        music.firstZimu = mOnlineMusic.firstZimu;
        music.isFree = mOnlineMusic.isFree;
        music.spareStr1 = mOnlineMusic.spareStr1;
        music.spareNum1 = mOnlineMusic.downloadFlag;
        music.spareStr2 = TaipingApplication.tpApp.phone;

        // 下载歌词
        String lrcFileName = FileUtils.getLrcFileName(artist, title);
        File lrcFile = new File(FileUtils.getLrcDir() + lrcFileName);
        if (!lrcFile.exists() && !TextUtils.isEmpty(mOnlineMusic.getLrclink())) {
            downloadLrc(mOnlineMusic.getLrclink(), lrcFileName);
        } else {
            mCounter++;
        }

        // 下载封面
        String albumFileName = FileUtils.getAlbumFileName(artist, title);
        File albumFile = new File(FileUtils.getAlbumDir(), albumFileName);
        String picUrl = mOnlineMusic.getPic_big();
        if (TextUtils.isEmpty(picUrl)) {
            picUrl = mOnlineMusic.getPic_small();
        }
        if (!albumFile.exists() && !TextUtils.isEmpty(picUrl)) {
            downloadAlbum(picUrl, albumFileName);
        } else {
            mCounter++;
        }

        checkCounter();

    }

    private void downloadLrc(String url, String fileName) {
        HttpClient.downloadFile(url, FileUtils.getLrcDir(), fileName, new HttpCallback<File>() {
            @Override
            public void onSuccess(File file) {
            }

            @Override
            public void onFail(Exception e) {
            }

            @Override
            public void onFinish() {
                checkCounter();
            }
        });
    }

    private void downloadAlbum(String picUrl, String fileName) {
        HttpClient.downloadFile(picUrl, FileUtils.getAlbumDir(), fileName, new HttpCallback<File>() {
            @Override
            public void onSuccess(File file) {
            }

            @Override
            public void onFail(Exception e) {
            }

            @Override
            public void onFinish() {
                checkCounter();
            }
        });
    }
}
