package com.yuntu.mawei.base.home;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.ButterKnife;

/**
 * Created by sanmu on 2017/3/15.
 * meail: 992759969@qq.com
 */

public abstract class BaseFragment extends Fragment {

    public Context mContext;


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Nullable
    @Override
    public final View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View mRootView = inflater.inflate(getLayoutId(), container, false);
        if (mRootView != null) {
            ButterKnife.bind(this, mRootView);
        }
        initView(mRootView);
        return mRootView;
    }

    /**
     * @return
     */
    public abstract int getLayoutId();

    /**
     * @param view
     */
    public abstract void initView(View view);

}
