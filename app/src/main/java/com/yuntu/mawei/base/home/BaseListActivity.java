package com.yuntu.mawei.base.home;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.gyf.barlibrary.ImmersionBar;
import com.trello.rxlifecycle.LifecycleTransformer;
import com.trello.rxlifecycle.components.support.RxAppCompatActivity;
import com.umeng.analytics.MobclickAgent;
import com.yuntu.mawei.R;
import com.yuntu.mawei.view.adapter.base.BaseQuickAdapter;
import com.yuntu.mawei.view.adapter.helper.PullRefreshHelper;
import com.yuntu.mawei.view.adapter.helper.RecyclerViewHelper;
import com.yuntu.mawei.view.adapter.listener.OnRequestDataListener;
import com.yuntu.mawei.base.TaipingApplication;
import com.yuntu.mawei.util.Baseutils;
import com.yuntu.mawei.util.Logl;
import com.yuntu.mawei.view.EmptyLayout;
import com.yuntu.mawei.view.loading.LoadingView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.srain.cube.views.ptr.PtrClassicFrameLayout;
import in.srain.cube.views.ptr.PtrDefaultHandler;
import in.srain.cube.views.ptr.PtrFrameLayout;
import in.srain.cube.views.ptr.PtrHandler;

/**
 * Created by tanghao on 2017/12/11.
 * Email: 86882259@qq.com
 */

public abstract class BaseListActivity<T> extends RxAppCompatActivity {

    /**
     * 把 EmptyLayout 放在基类统一处理，@Nullable 表明 View 可以为 null，详细可看 ButterKnife
     */
    @Nullable
    @BindView(R.id.empty_layout)
    protected EmptyLayout mEmptyLayout;

    @Nullable
    @BindView(R.id.refreshable_view)
    protected PtrClassicFrameLayout refreshableView;

    @Nullable
    @BindView(R.id.title_back)
    public View tvBack;

    @Nullable
    @BindView(R.id.title_content)
    TextView tvTitle;

    @BindView(R.id.rv_news_list)
    public RecyclerView recyclerView;

    @Nullable
    @BindView(R.id.layout_title)
    View layoutTitle;

    public BaseQuickAdapter adapter;

    public BaseListActivityPresenter presenter;

    public int loadMoreNeedSize = 4;

    public boolean isNeedDevide;

    public String hintText = "暂无数据";
    public String hintIcon = "\uE6c8";
    public boolean canUpdate = true;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        beforeCreate();
        super.onCreate(savedInstanceState);
        setContentView(attachLayoutRes());
        ButterKnife.bind(this);
        TaipingApplication.tpApp.addActivity(this);
        Baseutils.intance().getHeightAndWidth(this);
        Logl.e("BaseListActivity 设置bar");
        ImmersionBar.with(this).fitsSystemWindows(false).transparentStatusBar().init();
        if (layoutTitle != null) ImmersionBar.setTitleBar(this, layoutTitle);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        initViews();
        adapter = attachAdapter();
        presenter = attachPresenter();
        initSwipeRefresh();
        afterCreate();
    }

    public void setRedBar(){
        ImmersionBar.with(this).fitsSystemWindows(true).statusBarColor(R.color.main_bar).init();
    }

    /**
     * 初始化视图控件
     */
    protected abstract void initViews();

    protected void beforeCreate() {
    }

    protected void afterCreate() {
    }


    public void initSwipeRefresh() {
        RecyclerViewHelper.initRecyclerViewV(this, recyclerView, isNeedDevide, adapter);
        if (canUpdate) {
            updateViews(false);
        }
        if (refreshableView != null) {
            refreshableView.setLoadingMinTime(500);
            refreshableView.setDurationToCloseHeader(800);
            LoadingView materialHeader = new LoadingView(this);
            refreshableView.setHeaderView(materialHeader);
            refreshableView.addPtrUIHandler(materialHeader);
            refreshableView.setPtrHandler(new PtrHandler() {
                @Override
                public boolean checkCanDoRefresh(PtrFrameLayout frame, View content, View header) {
                    return PtrDefaultHandler.checkContentCanBePulledDown(frame, content, header);
                }

                @Override
                public void onRefreshBegin(final PtrFrameLayout frame) {
                    //实现下拉刷新的功能
                    updateViews(true);
                }
            });
        }
    }

    /**
     * 处理标题栏
     *
     * @param title
     */
    public void initTitle(String title) {
        tvTitle.setText(title);
        tvBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    public void initTitle(int title) {
        tvTitle.setText(title);
        tvBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    public void showLoading() {
        if (mEmptyLayout != null) {
            mEmptyLayout.setEmptyStatus(EmptyLayout.STATUS_LOADING);
            PullRefreshHelper.enableRefresh(refreshableView, false);
        }
    }

    public void hideLoading() {
        if (mEmptyLayout != null) {
            mEmptyLayout.hide();
            PullRefreshHelper.enableRefresh(refreshableView, true);
        }
    }

    public void showNetError(EmptyLayout.OnRetryListener onRetryListener) {
        if (mEmptyLayout != null) {
            Logl.e("BaseListFragment:showNetError");
            mEmptyLayout.setEmptyStatus(EmptyLayout.STATUS_NO_NET);
            mEmptyLayout.setRetryListener(onRetryListener);
            PullRefreshHelper.enableRefresh(refreshableView, false);
        }
    }

    public void finishRefresh() {
        if (refreshableView != null) {
            refreshableView.refreshComplete();
        }
    }

    public void refreshData() {
        if (presenter != null) {
            presenter.getData(true);
        }
    }

    public <T> LifecycleTransformer<T> bindToLife() {
        return this.<T>bindToLifecycle();
    }

    protected abstract BaseQuickAdapter attachAdapter();

    protected abstract BaseListActivityPresenter attachPresenter();

    /**
     * 没有数据时的提示
     *
     * @param text
     * @param icon
     */
    public void initHintStr(String text, String icon) {
        if (!TextUtils.isEmpty(text)) {
            hintText = text;
        }
        if (!TextUtils.isEmpty(icon)) {
            hintIcon = icon;
        }
    }

    public void loadNoData() {
        Logl.e("调用 no data");
        if (mEmptyLayout != null) {
            mEmptyLayout.switchNoda(hintText, hintIcon, new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                }
            });
        }
    }

    public void setLoadMoreNeedSize(int loadMoreNeedSize) {
        this.loadMoreNeedSize = loadMoreNeedSize;
    }

    /**
     * 绑定布局文件
     *
     * @return 布局文件ID
     */
    @LayoutRes
    protected abstract int attachLayoutRes();

    /**
     * 更新视图控件
     *
     * @param isRefresh 新增参数，用来判断是否为下拉刷新调用，下拉刷新的时候不应该再显示加载界面和异常界面
     */
    public void updateViews(boolean isRefresh) {
        presenter.getData(isRefresh);
    }

    public boolean yiciSetMore = true;

    public void loadData(List<T> data) {
        if (adapter == null) {
            return;
        }
        if (data == null) {
            data = new ArrayList<>();
        }
        if (data.size() == 0) {
            View emptyView = LayoutInflater.from(this).inflate(R.layout.adapter_empty, null);
            adapter.setEmptyView(emptyView);
        }
        adapter.updateItems(data);
        if (yiciSetMore) {
            if (data.size() >= loadMoreNeedSize) {
                adapter.setRequestDataListener(new OnRequestDataListener() {
                    @Override
                    public void onLoadMore() {
                        // 下拉刷新
                        presenter.getMoreData();
                    }
                });
                yiciSetMore = false;
            }
        }
    }

    public void loadMoreData(List<T> data) {
        Logl.e("size:" + data.size());
        if (data.size() == 0) {
            adapter.noMoreData();
        } else {
            adapter.addItems(data);
            adapter.loadComplete();
        }
    }

    public void loadMoreData(List<T> data, String hint) {
        if (data.size() == 0) {
            adapter.noMoreData(hint);
        } else {
            adapter.addItems(data);
            adapter.loadComplete();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        MobclickAgent.onResume(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        ImmersionBar.with(this).destroy();
    }


    @Override
    public void finish() {
        super.finish();
        TaipingApplication.tpApp.removeActivity(this);
    }
}
