package com.yuntu.mawei.base;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Handler;
import android.support.multidex.MultiDex;
import android.text.TextUtils;
import android.util.Log;
import android.widget.TextView;

import com.orhanobut.logger.AndroidLogAdapter;
import com.orhanobut.logger.Logger;
import com.umeng.commonsdk.UMConfigure;
import com.umeng.socialize.PlatformConfig;
import com.yuntu.mawei.BuildConfig;
import com.yuntu.mawei.ui.main.MainActivity;
import com.yuntu.mawei.ui.playing.service.AppCache;
import com.yuntu.mawei.ui.playing.service.AudioPlayer;
import com.yuntu.mawei.ui.playing.service.PlayService;
import com.yuntu.mawei.util.GlideHelper;
import com.yuntu.mawei.util.Logl;
import com.yuntu.mawei.util.SharedPreferenceUtil;
import com.yuntu.mawei.util.db.DBManager;

import java.util.Stack;

import io.realm.Realm;
import io.realm.RealmConfiguration;


/**
 * Created by tanghao on 2017/3/15.
 * meail: 992759969@qq.com
 */

public class TaipingApplication extends Application {

    public static TaipingApplication tpApp;
    public static Context appInstanse;

    private String sCacheDir; // 当前文件储存地址
    private String VERSION_CODE = ""; // 当前版本号 210
    private String VERSION_NAME = ""; // 当前版本号 2.1.0
    private String token = "";
    private boolean isLogin = false; // 是否登录唯一判断
    public String phone = "";

    private SharedPreferenceUtil sharedPreferenceUtil;
    public boolean isVIP = false;
    public Stack<Activity> mActivitys = new Stack<>();
    public boolean isShowPlay;
    private Handler handler = new Handler();

    @Override
    public void onCreate() {
        super.onCreate();
        tpApp = this;
        appInstanse = getApplicationContext();
        initApp();
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(base);
    }

    public void initApp() {
        AppCache.get().init(this);
        DBManager.get().init(this);
        startService(new Intent(this, PlayService.class));
        initLogger();
        initSharedPreference();
        initVersionCode();
        initUmeng();
        initCacheDir();
        MultiDex.install(this);

    }

    public void initUmeng() {
        UMConfigure.init(this, UMConfigure.DEVICE_TYPE_PHONE, "1155d31a612ef0cb54a79594fb6c7044");
        PlatformConfig.setWeixin("wx377cf905544c9c70", "36d7a5913d6159d0d7398477510ced77");
        PlatformConfig.setSinaWeibo("260429446", "907806fa39a156afbde3c146dbef122b", "http://sns.whalecloud.com");
        PlatformConfig.setQQZone("1106247008", "P41U2hmFqejRpJXT");
    }

    Runnable runnable = new Runnable() {
        @Override
        public void run() {
            isShowPlay = false;
        }
    };

    public void setisShowPlayTrue() {
        isShowPlay = true;
        handler.postDelayed(runnable, 4000);
    }

    private void initLogger() {
        Logger.addLogAdapter(new AndroidLogAdapter());
    }

    private void initSharedPreference() {
        sharedPreferenceUtil = SharedPreferenceUtil.getInstance();
        token = sharedPreferenceUtil.getString("token", "");
        phone = sharedPreferenceUtil.getString("phone", "");
        Logl.e("token: " + token);
        if (!TextUtils.isEmpty(token)) isLogin = true;
    }

    /**
     * 获得版本号
     */
    private void initVersionCode() {
        try {
            VERSION_CODE = String.valueOf(getPackageManager().getPackageInfo(getPackageName(), 0).versionCode);
            VERSION_NAME = String.valueOf(getPackageManager().getPackageInfo(getPackageName(), 0).versionName);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }

    /**
     * 初始化储存地址
     */
    private void initCacheDir() {
        if (getApplicationContext().getExternalCacheDir() != null && isExistSDCard()) {
            sCacheDir = getApplicationContext().getExternalCacheDir().toString();
        } else {
            sCacheDir = getApplicationContext().getCacheDir().toString();
        }
    }

    public void setPhone(String phoneHttp) {
        if (phoneHttp == null) {
            phone = "";
            AudioPlayer.get().initList();
            sharedPreferenceUtil.putString("phone", "");
            return;
        }
        if (!phone.equals(phoneHttp)) {
            phone = phoneHttp;
            sharedPreferenceUtil.putString("phone", phone);
        }
        AudioPlayer.get().initList();
    }

    public void updatePhone(String phoneHttp) {
        if (phoneHttp == null) {
            phone = "";
            sharedPreferenceUtil.putString("phone", "");
            return;
        }
        if (!phone.equals(phoneHttp)) {
            phone = phoneHttp;
            sharedPreferenceUtil.putString("phone", phone);
        }
    }

    private boolean isExistSDCard() {
        return android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED);
    }

    public static Context getAppContext() {
        return appInstanse;
    }

    public String getAppCacheDir() {
        return sCacheDir;
    }

    public String getVersionCode() {
        return VERSION_CODE;
    }

    public String getVersionName() {
        return VERSION_NAME;
    }

    public boolean getIsLogin() {
        return isLogin;
    }

    public void setIsLogin(boolean isLogin) {
        this.isLogin = isLogin;
    }

    public static TaipingApplication getInstanse() {
        return tpApp;
    }

    public void addActivity(Activity activity) {
        mActivitys.add(activity);
    }

    public void removeActivity(Activity activity) {
        mActivitys.remove(activity);
    }

    public void setToken(String token) {
        if (token == null) token = "";
        this.token = token;
        if ("".equals(token)) {
            isLogin = false;
        } else {
            isLogin = true;
        }
        if (TextUtils.isEmpty(token)) setIsLogin(false);
        sharedPreferenceUtil.putStringRightNow("token", token);
    }

    public String getToken() {
        return token;
    }


}

