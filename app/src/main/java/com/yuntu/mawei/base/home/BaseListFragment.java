package com.yuntu.mawei.base.home;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.trello.rxlifecycle.LifecycleTransformer;
import com.trello.rxlifecycle.components.support.RxFragment;
import com.yuntu.mawei.R;
import com.yuntu.mawei.view.adapter.base.BaseQuickAdapter;
import com.yuntu.mawei.view.adapter.helper.PullRefreshHelper;
import com.yuntu.mawei.view.adapter.helper.RecyclerViewHelper;
import com.yuntu.mawei.view.adapter.listener.OnRequestDataListener;
import com.yuntu.mawei.util.Logl;
import com.yuntu.mawei.view.EmptyLayout;
import com.yuntu.mawei.view.loading.LoadingView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.srain.cube.views.ptr.PtrClassicFrameLayout;
import in.srain.cube.views.ptr.PtrDefaultHandler;
import in.srain.cube.views.ptr.PtrFrameLayout;
import in.srain.cube.views.ptr.PtrHandler;

/**
 * Created by tanghao on 2017/3/28.
 */

public abstract class BaseListFragment<T> extends RxFragment {

    public final static String FRAGMENT_KEY = "fragment_key";

    @Nullable
    @BindView(R.id.empty_layout)
    protected EmptyLayout mEmptyLayout;

    @Nullable
    @BindView(R.id.refreshable_view)
    protected PtrClassicFrameLayout refreshableView;

    @BindView(R.id.rv_news_list)
    public RecyclerView recyclerView;

    public BaseQuickAdapter adapter;

    public BaseListFragmentPresenter presenter;

    public View mRootView;
    private boolean mIsMulti = false;
    protected boolean isStart = true;

    public boolean isNeedDivide;
    public boolean isSpaceDivider;

    /**
     * viewpager切换时，是否更新数据，
     */
    public boolean isSwitchRefresh = false;

    public String hintText = "暂无数据";
    public String hintIcon = "\uE6c8";

    protected void beforeCreated() {
    }

    protected boolean isReloadRecycler() {
        return false;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Logl.e("BaseListFragment onCreateView");
        if (mRootView == null) {
            mRootView = inflater.inflate(attachLayoutRes() != 0 ? attachLayoutRes() : R.layout.fragment_news_list_grey, null);
            ButterKnife.bind(this, mRootView);
            beforeCreated();
            adapter = attachAdapter();
            presenter = attachPresenter();

            if (!isReloadRecycler()) {
                if (isSpaceDivider) {
                    RecyclerViewHelper.initRecyclerViewVSpace(getActivity(), recyclerView, isNeedDivide, adapter);
                } else {
                    RecyclerViewHelper.initRecyclerViewV(getActivity(), recyclerView, isNeedDivide, adapter);
                }
            }
            if (isStart) {
                updateViews(false);
            }
            if (refreshableView != null) {
                refreshableView.setLoadingMinTime(500);
                refreshableView.setDurationToCloseHeader(800);
                LoadingView materialHeader = new LoadingView(getActivity());
                refreshableView.setHeaderView(materialHeader);
                refreshableView.addPtrUIHandler(materialHeader);
                refreshableView.setPtrHandler(new PtrHandler() {
                    @Override
                    public boolean checkCanDoRefresh(PtrFrameLayout frame, View content, View header) {
                        return PtrDefaultHandler.checkContentCanBePulledDown(frame, content, header);
                    }

                    @Override
                    public void onRefreshBegin(final PtrFrameLayout frame) {
                        //实现下拉刷新的功能
                        updateViews(true);
                    }
                });
            }
        }
        ViewGroup parent = (ViewGroup) mRootView.getParent();
        if (parent != null) {
            parent.removeView(mRootView);
        }
        initViews();
        return mRootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (getUserVisibleHint() && mRootView != null && !mIsMulti) {
            mIsMulti = true;
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        if (isVisibleToUser && isVisible() && mRootView != null && !mIsMulti) {
            mIsMulti = true;
        } else {
            super.setUserVisibleHint(isVisibleToUser);
        }

        if (isVisibleToUser && isSwitchRefresh) {
            if (presenter != null && adapter != null) {
                Logl.e("BaseListFragment:isVisibleToUser:确实调用了，在刷新");
                presenter.getData(true);
            }
        }
    }

    /**
     * 初始化视图控件
     */
    protected abstract void initViews();

    public void showLoading() {
        Logl.e("BaseListFragment:showLoading");
        if (mEmptyLayout != null) {
            mEmptyLayout.setEmptyStatus(EmptyLayout.STATUS_LOADING);
            PullRefreshHelper.enableRefresh(refreshableView, false);
        }
    }

    public void hideLoading() {
        if (mEmptyLayout != null) {
            Logl.e("BaseListFragment:hideLoading");
            mEmptyLayout.hide();
            PullRefreshHelper.enableRefresh(refreshableView, true);
        }
    }

    public void showNetError(EmptyLayout.OnRetryListener onRetryListener) {
        if (mEmptyLayout != null) {
            Logl.e("BaseListFragment:showNetError");
            mEmptyLayout.setEmptyStatus(EmptyLayout.STATUS_NO_NET);
            mEmptyLayout.setRetryListener(onRetryListener);
            PullRefreshHelper.enableRefresh(refreshableView, false);
        }
    }

    public void finishRefresh() {
        if (refreshableView != null) {
            refreshableView.refreshComplete();
        }
    }

    public void refreshData() {
        if (presenter != null) {
            presenter.getData(true);
        }
    }

    public <T> LifecycleTransformer<T> bindToLife() {
        return this.<T>bindToLifecycle();
    }

    /**
     * 绑定布局文件
     *
     * @return 布局文件ID
     */
    protected abstract int attachLayoutRes();

    protected abstract BaseQuickAdapter attachAdapter();

    protected abstract BaseListFragmentPresenter attachPresenter();

    /**
     * 没有数据时的提示
     *
     * @param text
     * @param icon
     */
    public void initHintStr(String text, String icon) {
        if (!TextUtils.isEmpty(text)) {
            hintText = text;
        }
        if (!TextUtils.isEmpty(icon)) {
            hintIcon = icon;
        }
    }

    public void loadNoData() {
        if (mEmptyLayout != null) {
            mEmptyLayout.switchNoda(hintText, hintIcon, new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                }
            });
        }
    }

    /**
     * @param str1 没数据提示文字
     * @param str2 没数据提示图标
     */
    public void loadNoData(String str1, String str2) {
        if (mEmptyLayout != null) {
            mEmptyLayout.switchNoda(str1, str2, new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                }
            });
        }
    }

    public void showEmpty() {
        if (mEmptyLayout != null) {
            mEmptyLayout.NoCollageData(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onEmptyClick();
                }
            });
            PullRefreshHelper.enableRefresh(refreshableView, false);
        }
    }

    protected void onEmptyClick() {

    }

    /**
     * 更新视图控件
     *
     * @param isPullRefresh 新增参数，用来判断是否为下拉刷新调用，下拉刷新的时候不应该再显示加载界面和异常界面
     */
    protected void updateViews(boolean isPullRefresh) {
        presenter.getData(isPullRefresh);
    }

    protected boolean yiciSetMore = true;

    public void loadData(List<T> data) {
        if (adapter == null) {
            return;
        }
        if (data == null) {
            data = new ArrayList<>();
        }
        if (data.size() == 0) {
            View emptyView = LayoutInflater.from(getActivity()).inflate(R.layout.adapter_empty, null);
            adapter.setEmptyView(emptyView);
        }
        adapter.updateItems(data);
        if (yiciSetMore) {
            if (data.size() >= 6) {
                adapter.setRequestDataListener(new OnRequestDataListener() {

                    @Override
                    public void onLoadMore() {
                        presenter.getMoreData();

                    }
                });
                yiciSetMore = false;
            }
        }
    }

    public void loadMoreData(List<T> data) {
        if (data.size() == 0) {
            adapter.noMoreData();
        } else {
            adapter.addItems(data);
            adapter.loadComplete();
        }
    }

    protected void intent2Activity(Class<?> pClass) {
        Intent intent = new Intent(getContext(), pClass);
        startActivity(intent);
    }

    protected void intent2Activity(Class<?> pClass, Bundle bundle) {
        Intent intent = new Intent(getContext(), pClass);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        startActivity(intent);
    }

}
