package com.yuntu.mawei.base.home;

/**
 * Created by tanghao on 2017/12/4.
 * Email: 86882259@qq.com
 */

public abstract class BaseListFragmentPresenter {

    public BaseListFragment mView;

    public BaseListFragmentPresenter(BaseListFragment mView) {
        this.mView = mView;
    }

    /**
     * @param isPullRefresh 此参数表示是否是下拉刷新
     */
    public abstract void getData(boolean isPullRefresh);

    public abstract void getMoreData();

}
