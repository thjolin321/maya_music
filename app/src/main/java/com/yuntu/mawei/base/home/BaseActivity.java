package com.yuntu.mawei.base.home;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import com.gyf.barlibrary.ImmersionBar;
import com.trello.rxlifecycle.components.support.RxAppCompatActivity;
import com.umeng.analytics.MobclickAgent;
import com.yuntu.mawei.R;
import com.yuntu.mawei.base.TaipingApplication;
import com.yuntu.mawei.util.Baseutils;
import com.yuntu.mawei.util.Logl;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by sanmu on 2017/3/15.
 * meail: 992759969@qq.com
 */

public abstract class BaseActivity extends RxAppCompatActivity {

    @Nullable
    @BindView(R.id.title_back)
    public View tvBack;

    @Nullable
    @BindView(R.id.title_content)
    TextView tvTitle;
    @Nullable
    @BindView(R.id.layout_title)
    View layoutTitle;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(attachLayout());
        ButterKnife.bind(this);
        TaipingApplication.tpApp.addActivity(this);
        Baseutils.intance().getHeightAndWidth(this);
        Logl.e("设置bar");
        ImmersionBar.with(this).fitsSystemWindows(false).transparentStatusBar().init();
        if (layoutTitle != null) ImmersionBar.setTitleBar(this, layoutTitle);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        init();
    }

    public void setRedBar(){
        ImmersionBar.with(this).fitsSystemWindows(true).statusBarColor(R.color.main_bar).init();
    }

    public void setBarColor(int color){
        ImmersionBar.with(this).fitsSystemWindows(false).statusBarColor(color).init();
    }

    public abstract int attachLayout();

    public abstract void init();

    @Override
    protected void onStart() {
        super.onStart();
        request();
    }

    public void request() {

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ImmersionBar.with(this).destroy();
    }

    @Override
    protected void onResume() {
        super.onResume();
        MobclickAgent.onResume(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
    }

    @Override
    public void startActivity(Intent intent) {
        try {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            if (imm != null) {
                imm.hideSoftInputFromWindow(this.getCurrentFocus().getWindowToken(), 0);
            }
        } catch (Exception e) {

        }
        super.startActivity(intent);
        overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
    }


    @Override
    public void startActivityForResult(Intent intent, int requestCode) {
        try {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            if (imm != null) {
                imm.hideSoftInputFromWindow(this.getCurrentFocus().getWindowToken(), 0);
            }
        } catch (Exception e) {

        }
        super.startActivityForResult(intent, requestCode);
        overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
    }


    @Override
    public void finish() {
        try {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            if (imm != null) {
                imm.hideSoftInputFromWindow(this.getCurrentFocus().getWindowToken(), 0);
            }
        } catch (Exception e) {
        }
        super.finish();
        TaipingApplication.tpApp.removeActivity(this);
    }

    public void initTitle(String title) {
        tvTitle.setText(title);
        tvBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    public void initTitle(int title) {
        tvTitle.setText(title);
        tvBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }


}
