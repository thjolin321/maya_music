package com.yuntu.mawei.util;

import android.util.Log;

import com.orhanobut.logger.Logger;
import com.yuntu.mawei.BuildConfig;

/**
 * Created by Administrator on 2016/11/4.
 */

public class Logl {

    public static void e(String msg) {
        if (BuildConfig.LOGGER && msg != null) {
            Log.e("PRETTY_LOGGER", msg);
        }
    }

    public static void i(String msg) {
        Logger.i(msg);
    }

    public static void w(String msg) {
        Logger.w(msg);
    }

    public static void d(String msg) {
        Logger.d(msg);
    }

    public static void v(String msg) {
        Logger.v(msg);
    }

}
