package com.yuntu.mawei.util.music;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.text.TextUtils;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.yuntu.mawei.BuildConfig;
import com.yuntu.mawei.R;
import com.yuntu.mawei.base.TaipingApplication;
import com.yuntu.mawei.bean.base.ResponseBean;
import com.yuntu.mawei.bean.executor.PlayOnlineMusic;
import com.yuntu.mawei.bean.music.Music;
import com.yuntu.mawei.bean.music.MusicLocal;
import com.yuntu.mawei.bean.music.OnlineMusic;
import com.yuntu.mawei.bean.rx.SuccessSubscriber;
import com.yuntu.mawei.bean.song.AlbumBean;
import com.yuntu.mawei.bean.song.SongBean;
import com.yuntu.mawei.ui.playing.PlayActivity;
import com.yuntu.mawei.ui.playing.service.AudioPlayer;
import com.yuntu.mawei.util.FastBlurUtil;
import com.yuntu.mawei.util.GlideHelper;
import com.yuntu.mawei.util.GsonUtil;
import com.yuntu.mawei.util.Logl;
import com.yuntu.mawei.util.RxUtils;
import com.yuntu.mawei.util.ToastUtil;
import com.yuntu.mawei.util.http.HttpUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import jp.wasabeef.glide.transformations.BlurTransformation;

/**
 * Created by tanghao on 2019/1/29
 */
public class MusicUtil {

    public static void playMusic(Context mContext, SongBean songBean) {
        Logl.e("playMusic:在调用 ");
        if (TaipingApplication.tpApp.isShowPlay) return;
        TaipingApplication.tpApp.setisShowPlayTrue();
        new PlayOnlineMusic((Activity) mContext, mapToOnlineMusic(songBean)) {
            @Override
            public void onPrepare() {
                Logl.e("onPrepare");
            }

            @Override
            public void onExecuteSuccess(Music music) {
                if (!music.isFree && !TaipingApplication.tpApp.isVIP) {
                    if (AudioPlayer.get().getMusicList() == null) {
                        AudioPlayer.get().addNoPlay(music);
                    } else {
                        boolean isExist = false;
                        for (Music music1 : AudioPlayer.get().getMusicList()) {
                            if (music1.songId == songBean.songId) {
                                isExist = true;
                                break;
                            }
                        }
                        if (isExist) {
                            AudioPlayer.get().addAndPlay(music);
                        } else {
                            AudioPlayer.get().addNoPlay(music);
                        }
                    }
                } else {
                    AudioPlayer.get().addAndPlay(music);
                }
                PlayActivity.launch((Activity) mContext);
            }

            @Override
            public void onExecuteFail(Exception e) {
                Logl.e("onExecuteFail:");
                ToastUtil.showToast(R.string.unable_to_play);
            }
        }.execute();
    }

    public static void playMusicNoLaunch(Context mContext, SongBean songBean) {
        Logl.e("playMusic:在调用 ");
        new PlayOnlineMusic((Activity) mContext, mapToOnlineMusic(songBean)) {
            @Override
            public void onPrepare() {
                Logl.e("onPrepare");
            }

            @Override
            public void onExecuteSuccess(Music music) {
                if (!music.isFree && !TaipingApplication.tpApp.isVIP) {
                    AudioPlayer.get().addNoPlay(music);
                    PlayActivity.launch((Activity) mContext);
                } else {
                    AudioPlayer.get().addAndPlay(music);
                }
            }

            @Override
            public void onExecuteFail(Exception e) {
                Logl.e("onExecuteFail:");
                ToastUtil.showToast(R.string.unable_to_play);
            }
        }.execute();
    }

    public static Music mapToMusic(SongBean songBean) {
        Music music = new Music();
        if (songBean.singer != null) {
            music.spareStr1 = songBean.singer.singerIntroduction;
            music.artist = songBean.singer.singerName;
        }
        if (songBean.album != null) {
            music.album = songBean.album.albumName;
        }
        music.albumId = songBean.albumId;
        music.artist = songBean.singer.singerName;
        music.coverPath = checkHttp(songBean.songAvatar);
        music.duration = songBean.songTime * 1000;
        music.fileSize = 0;
        music.path = checkHttp(songBean.songUrl);
        music.songId = songBean.songId;
        music.title = songBean.songName;
        music.type = 1;
        return music;
    }

    public static OnlineMusic mapToOnlineMusic(SongBean songBean) {
        OnlineMusic music = new OnlineMusic();
        music.pic_big = checkHttp(songBean.songBackground);
        music.pic_small = checkHttp(songBean.songAvatar);
        music.lrclink = checkHttp(songBean.lyric != null ? songBean.lyric.lyricUrl : "");
        music.album_title = songBean.songName;
        music.artist_name = songBean.singer != null ? songBean.singer.singerName : "";
        music.spareStr1 = songBean.singer != null ? songBean.singer.singerIntroduction : "";
        music.song_id = String.valueOf(songBean.songId);
        music.title = songBean.songName;
        music.ting_uid = "340462058";
        music.path = checkHttp(songBean.songUrl);
        music.duration = songBean.songTime * 1000;
        music.songAvatar = songBean.songAvatar;
        music.album = songBean.album != null ? songBean.album.albumName : "";
        music.songId = songBean.songId;
        music.isCollect = (songBean.collect != null);
        music.isFree = (songBean.isFree == 0);
        music.songType = songBean.songType;
        music.downloadFlag = songBean.downloadFlag;
        music.firstZimu = TextUtils.isEmpty(songBean.singNameSyllable) ? "Z" : songBean.singNameSyllable.substring(0, 1).toUpperCase();
        return music;
    }

    public static OnlineMusic mapToOnlineMusic(Music songBean) {
        OnlineMusic music = new OnlineMusic();
        music.pic_big = checkHttp(songBean.songBackground);
        music.pic_small = checkHttp(songBean.coverPath);
        music.lrclink = songBean.lrclink;
        music.album_title = songBean.album;
        music.artist_name = songBean.artist;
        music.song_id = String.valueOf(songBean.songId);
        music.title = songBean.title;
        music.ting_uid = "340462058";
        music.path = songBean.path;
        music.duration = songBean.duration;
        music.songAvatar = songBean.coverPath;
        music.songId = songBean.songId;
        music.localMusicJson = GsonUtil.GsonString(songBean);
        return music;
    }


    public static String checkHttp(String url) {
        if (url == null) {
            return "";
        }
        return url;
    }


    public static List<SongBean> sortByZimu(Map<String, List<SongBean>> songMap) {
        List<SongBean> list = new ArrayList<>();
        for (int i = 0; i < 26; i++) {
            String index = String.valueOf((char) (65 + i));
            if (songMap.get(index) != null) {
                list.add(new SongBean(SongBean.SONG_TITLE_ITEM, index));
                list.addAll(songMap.get(index));
            }
        }
        return list;
    }

    public static List<SongBean> sortJustSong(Map<String, List<SongBean>> songMap) {
        List<SongBean> list = new ArrayList<>();
        for (int i = 0; i < 26; i++) {
            String index = String.valueOf((char) (65 + i));
            if (songMap.get(index) != null) {
                list.addAll(songMap.get(index));
            }
        }
        return list;
    }

    public static List<AlbumBean> sortJustAlbum(Map<String, List<AlbumBean>> songMap) {
        List<AlbumBean> list = new ArrayList<>();
        for (int i = 0; i < 26; i++) {
            String index = String.valueOf((char) (65 + i));
            if (songMap.get(index) != null) {
                list.add(new AlbumBean(AlbumBean.SONG_TITLE_ITEM, index));
                list.addAll(songMap.get(index));
            }
        }
        return list;
    }

    public static List<Music> sortMusicByZimu(int songType, List<MusicLocal> musicList) {
        Logl.e(GsonUtil.GsonString(musicList));
        List<Music> listType = new ArrayList<>();
        for (MusicLocal music : musicList) {
            if (songType == music.songType) {
                listType.add(music.mapToMusic());
            }
        }
        List<Music> list = new ArrayList<>();
        for (int i = 0; i < 26; i++) {
            String index = String.valueOf((char) (65 + i));
            boolean isTitleYici = true;
            for (Music music : listType) {
                if (index.equals(music.firstZimu)) {
                    if (isTitleYici) {
                        isTitleYici = false;
                        Music musicTitle = new Music();
                        musicTitle.itemType = 1;
                        musicTitle.firstZimu = index;
                        list.add(musicTitle);
                    }
                    list.add(music);
                } else if (TextUtils.isEmpty(music.firstZimu)) {
                    list.add(music);
                }
            }
        }
        return list;
    }

    public static void initBg(Activity activity, String url, ImageView imageView) {
        Glide.with(activity)
                .load(url)
                .dontAnimate()
                .error(R.mipmap.music_defaul_icon)
                .placeholder(R.mipmap.music_defaul_icon)
                // 设置高斯模糊
                .bitmapTransform(new BlurTransformation(activity, 14, 5))
                .into(imageView);

        if (!TextUtils.isEmpty(url)) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        Bitmap blurBitmap = FastBlurUtil.GetUrlBitmap(url, 5);
                        activity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                imageView.setImageBitmap(blurBitmap);
                            }
                        });
                    } catch (Exception e) {
                    }
                }

            }).start();
        }
    }

    public static boolean isLetterOrDigit(String str) {
        if (str == null) return false;
        if (str.length() > 18) return false;
        if (str.length() < 6) return false;
        boolean isDigit = false;//定义一个boolean值，用来表示是否包含数字
        boolean isLetter = false;//定义一个boolean值，用来表示是否包含字母
        for (int i = 0; i < str.length(); i++) {
            if (Character.isDigit(str.charAt(i))) {   //用char包装类中的判断数字的方法判断每一个字符
                isDigit = true;
            } else if (Character.isLetter(str.charAt(i))) {  //用char包装类中的判断字母的方法判断每一个字符
                isLetter = true;
            }
        }
        String regex = "^[a-zA-Z0-9]+$";
        boolean isRight = isDigit && isLetter && str.matches(regex);
        return isRight;
    }

    public static String getIndex(SongBean songBean, List<SongBean> list) {
        for (int i = 0; i < list.size(); i++) {
            if (songBean.songId == list.get(i).songId) {
                return getIndexStr(i + 1);
            }
        }
        return "1";
    }

    private static String getIndexStr(int index) {
        if (index < 10) {
            return "0" + index;
        }
        return String.valueOf(index);
    }

}
