package com.yuntu.mawei.util;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.telephony.TelephonyManager;
import android.text.format.Formatter;
import android.util.Log;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

/**
 * Created by sanmu on 2017/3/16.
 * meail: 992759969@qq.com
 */

public class Baseutils {
    private static Baseutils intance;
    private String sCacheDir = null;
    public int DM_width;//手机屏幕宽
    public int DM_height;//手机屏幕高

    public String imei;//手机IMEI号:设备唯一标示
    public String imsi;//手机IESI号:手机型号
    public String mtype; // 手机型号
    public String mtyb;//手机品牌
    public String numer; // 手机号码，有的可得，有的不可得

    private Baseutils() {

    }

    public static Baseutils intance() {
        return intance == null ? intance = new Baseutils() : intance;
    }

    /**
     * 初始化储存地址
     */
    public String getCacheDir(Context context) {
        if (sCacheDir == null) {
            if (context.getExternalCacheDir() != null && ExistSDCard()) {
                sCacheDir = context.getExternalCacheDir().toString();
            } else {
                sCacheDir = context.getCacheDir().toString();
            }
        }
        return sCacheDir;

    }

    private boolean ExistSDCard() {
        return android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED);
    }

    /**
     * 获取android当前可用内存大小
     */
    public String getAvailMemory(Activity activity) {// 获取android当前可用内存大小

        ActivityManager am = (ActivityManager) activity.getSystemService(Context.ACTIVITY_SERVICE);
        ActivityManager.MemoryInfo mi = new ActivityManager.MemoryInfo();
        am.getMemoryInfo(mi);
        //mi.availMem; 当前系统的可用内存

        return Formatter.formatFileSize(activity.getBaseContext(), mi.availMem);// 将获取的内存大小规格化
    }

    /**
     * 获得系统总内存
     */
    public String getTotalMemory(Activity activity) {
        String str1 = "/proc/meminfo";// 系统内存信息文件
        String str2;
        String[] arrayOfString;
        long initial_memory = 0;

        try {
            FileReader localFileReader = new FileReader(str1);
            BufferedReader localBufferedReader = new BufferedReader(
                    localFileReader, 8192);
            str2 = localBufferedReader.readLine();// 读取meminfo第一行，系统总内存大小

            arrayOfString = str2.split("\\s+");
            for (String num : arrayOfString) {
                Log.i(str2, num + "\t");
            }

            initial_memory = Integer.valueOf(arrayOfString[1]).intValue() * 1024;// 获得系统总内存，单位是KB，乘以1024转换为Byte
            localBufferedReader.close();

        } catch (IOException e) {
        }
        return Formatter.formatFileSize(activity.getBaseContext(), initial_memory);// Byte转换为KB或者MB，内存大小规格化
    }

    /**
     * 获得手机屏幕宽高
     *
     * @return
     */
    public Baseutils getHeightAndWidth(Activity activity) {
        if (DM_width == 0 || DM_height == 0) {
            DM_width = activity.getWindowManager().getDefaultDisplay().getWidth();
            DM_height = activity.getWindowManager().getDefaultDisplay().getHeight();
        }
        return intance;
    }


    /**
     * 获取IMEI号，IESI号，手机型号
     */
    public Baseutils getInfo(Activity activity) {
        if (imei == null || imsi == null || mtype == null || mtyb == null) {
            TelephonyManager mTm = (TelephonyManager) activity.getSystemService(activity.TELEPHONY_SERVICE);
            imei = mTm.getDeviceId();//UUID
            imsi = mTm.getSubscriberId();// 机型
            mtype = android.os.Build.MODEL; // 手机型号
            mtyb = android.os.Build.BRAND;//手机品牌
            numer = mTm.getLine1Number(); // 手机号码，有的可得，有的不可得
        }
        return intance;
    }

    /**
     * 手机CPU信息
     */
    public static String[] getCpuInfo() {
        String str1 = "/proc/cpuinfo";
        String str2 = "";
        String[] cpuInfo = {"", ""};  //1-cpu型号  //2-cpu频率
        String[] arrayOfString;
        try {
            FileReader fr = new FileReader(str1);
            BufferedReader localBufferedReader = new BufferedReader(fr, 8192);
            str2 = localBufferedReader.readLine();
            arrayOfString = str2.split("\\s+");
            for (int i = 2; i < arrayOfString.length; i++) {
                cpuInfo[0] = cpuInfo[0] + arrayOfString[i] + " ";
            }
            str2 = localBufferedReader.readLine();
            arrayOfString = str2.split("\\s+");
            cpuInfo[1] += arrayOfString[2];
            localBufferedReader.close();
        } catch (IOException e) {

        }
        return cpuInfo;
    }


}
