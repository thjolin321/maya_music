package com.yuntu.mawei.util;

import android.text.TextUtils;
import android.view.Gravity;
import android.widget.Toast;

import com.yuntu.mawei.base.TaipingApplication;

public class ToastUtil {

    private static Toast toast = null;

    public static void showToast(String text) {
        if (toast == null) {
            toast = Toast.makeText(TaipingApplication.getAppContext(), text, Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.CENTER, 0, 0);
        } else {
            if (TextUtils.isEmpty(text)) {
                return;
            }
            toast.setText(text);
        }
        toast.show();
    }

    public static void showToast(int id) {
        if (toast == null) {
            toast = Toast.makeText(TaipingApplication.getAppContext(), id, Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.CENTER, 0, 0);
        } else {
            toast.setText(id);
        }
        toast.show();
    }
}
