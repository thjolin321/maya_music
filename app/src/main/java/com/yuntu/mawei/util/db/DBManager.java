package com.yuntu.mawei.util.db;

import android.content.Context;

import com.yuntu.mawei.storage.db.greendao.DaoMaster;
import com.yuntu.mawei.storage.db.greendao.DaoSession;
import com.yuntu.mawei.storage.db.greendao.MusicDao;
import com.yuntu.mawei.storage.db.greendao.MusicLocalDao;

import org.greenrobot.greendao.database.Database;


/**
 * Created by wcy on 2018/1/27.
 */
public class DBManager {
    private static final String DB_NAME = "mawei_database";
    private MusicDao musicDao;
    private MusicLocalDao musicLocalDao;

    public static DBManager get() {
        return SingletonHolder.instance;
    }

    private static class SingletonHolder {
        private static DBManager instance = new DBManager();
    }

    public void init(Context context) {
        DaoMaster.DevOpenHelper helper = new DaoMaster.DevOpenHelper(context, DB_NAME);
        Database db = helper.getWritableDb();
        DaoSession daoSession = new DaoMaster(db).newSession();
        musicDao = daoSession.getMusicDao();
        musicLocalDao = daoSession.getMusicLocalDao();
    }

    private DBManager() {
    }

    public MusicDao getMusicDao() {
        return musicDao;
    }

    public MusicLocalDao getMusicLocalDao() {
        return musicLocalDao;
    }

}
