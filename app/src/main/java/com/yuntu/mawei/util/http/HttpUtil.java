package com.yuntu.mawei.util.http;

import android.os.Build;
import android.support.annotation.NonNull;

import com.google.gson.Gson;
import com.orhanobut.logger.Logger;
import com.yuntu.mawei.BuildConfig;
import com.yuntu.mawei.base.TaipingApplication;
import com.yuntu.mawei.bean.base.ResponseBean;
import com.yuntu.mawei.bean.money.ImageUploadBean;
import com.yuntu.mawei.constant.api.ApiInterface;
import com.yuntu.mawei.util.Logl;
import com.yuntu.mawei.util.RxUtils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import okhttp3.Cache;
import okhttp3.CacheControl;
import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by tanghao on 2017/3/15.
 * meail: 86882259@qq.com
 */
public class HttpUtil {
    private static ApiInterface sApiService = null;
    private static Retrofit sRetrofit = null;
    private static OkHttpClient sOkHttpClient = null;

    //设缓存有效期为3天
    private static final long CACHE_STALE_SEC = 60 * 60 * 24 * 3;
    //查询缓存的Cache-Control设置，为if-only-cache时只查询缓存而不会请求服务器，max-stale可以配合设置缓存失效时间
    private static final String CACHE_CONTROL_CACHE = "only-if-cached, max-stale=" + CACHE_STALE_SEC;
    //查询网络的Cache-Control设置
    static final String CACHE_CONTROL_NETWORK = "Cache-Control: public, max-age=3600";

    public void init() {
        initOkHttp();
        initRetrofit();
        sApiService = sRetrofit.create(ApiInterface.class);
    }

    private HttpUtil() {
        init();
    }

    public static HttpUtil getInstance() {
        return SingletonHolder.INSTANCE;
    }

    private static class SingletonHolder {
        private static final HttpUtil INSTANCE = new HttpUtil();
    }

    private static void initOkHttp() {
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        TokenInterceptor tokenInterceptor = new TokenInterceptor();
        builder.addInterceptor(tokenInterceptor);
        if (BuildConfig.LOGGER) {
            builder.addInterceptor(LogIntercepter());
        }
        Cache cache = new Cache(new File(TaipingApplication.tpApp.getCacheDir(), "HttpCache"),
                1024 * 1024 * 100);
        Interceptor cacheInterceptor = new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request oldRequest = chain.request();
                // 添加新的参数
                okhttp3.HttpUrl.Builder authorizedUrlBuilder = oldRequest.url()
                        .newBuilder()
                        .scheme(oldRequest.url().scheme())
                        .host(oldRequest.url().host());

                Request request = oldRequest.newBuilder()
                        .method(oldRequest.method(), oldRequest.body())
                        .url(authorizedUrlBuilder.build())
                        .addHeader("token", TaipingApplication.tpApp.getToken())
                        .addHeader("Cache-Control", "public, max-age=" + 0)
                        .addHeader("Content-Type", "application/json;charset=UTF-8")
                        .addHeader("app-ver", TaipingApplication.tpApp.getVersionName())
                        .addHeader("Connection", "close")
                        .addHeader("itemId", "1")
                        .build();

                if (!NetworkUtils.isConnected()) {
                    request = request.newBuilder().cacheControl(CacheControl.FORCE_CACHE).build();
                }

                Response originalResponse = chain.proceed(request);

                if (NetworkUtils.isConnected()) {
                    // 有网的时候读接口上的@Headers里的配置，你可以在这里进行统一的设置
                    String cacheControl = request.cacheControl().toString();
                    return originalResponse.newBuilder()
                            .header("Cache-Control", cacheControl)
                            .removeHeader("Pragma")
                            .build();
                } else {
                    return originalResponse.newBuilder()
                            .header("Cache-Control", "public, " + CACHE_CONTROL_CACHE)
                            .removeHeader("Pragma")
                            .build();
                }
            }
        };
        builder.retryOnConnectionFailure(false)
                .addInterceptor(cacheInterceptor)
                .connectTimeout(60, TimeUnit.SECONDS)
                .writeTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS);
        builder.cache(cache);
        sOkHttpClient = builder.build();
    }

    private static Interceptor LogIntercepter() {

        Interceptor interceptor = new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Response response = chain.proceed(chain.request());
                Logger.i("url地址=" + response.request().url());
                MediaType mediaType = response.body().contentType();
                String content = response.body().string();
//                Logger.json(content);
                return response.newBuilder().body(ResponseBody.create(mediaType, content)).build();
            }
        };
        return interceptor;
    }

    public ApiInterface getApiService() {
        return sApiService;
    }


    private static void initRetrofit() {
        sRetrofit = new Retrofit.Builder()
                .baseUrl(BuildConfig.MALL_HOST)
                .client(sOkHttpClient)
                .addConverterFactory(GsonConverterFactory.create(new Gson()))
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build();
    }

    static String brand, model, ver;

    static {
        brand = Build.BRAND;
        model = Build.MODEL;
        ver = Build.VERSION.RELEASE + "";
    }

    public Observable<ResponseBean> uploadUserAvator(String path) {
        return sApiService.uploadMallFileSingle(prepareFilePart("avatar", path)).compose(RxUtils.rxSchedulerHelper());
    }

    @NonNull
    public static MultipartBody.Part prepareFilePart(String partName, String path) {
        File file = new File(path);
        RequestBody requestFile =
                RequestBody.create(MediaType.parse("multipart/form-data"), file);
        return MultipartBody.Part.createFormData(partName, file.getName(), requestFile);
    }

}
