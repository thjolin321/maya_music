package com.yuntu.mawei.util.http;

import android.content.Intent;

import com.orhanobut.logger.Logger;
import com.yuntu.mawei.BuildConfig;
import com.yuntu.mawei.base.TaipingApplication;
import com.yuntu.mawei.constant.Extras;
import com.yuntu.mawei.constant.api.ApiInterface;
import com.yuntu.mawei.ui.login.LoginActivity;
import com.yuntu.mawei.util.Logl;
import com.yuntu.mawei.util.SharedPreferenceUtil;
import com.yuntu.mawei.util.ToastUtil;

import java.io.IOException;
import java.nio.charset.Charset;

import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okio.Buffer;
import okio.BufferedSource;

/**
 * Created by tanghao on 2017/7/17.
 * Email: 86882259@qq.com
 */

public class TokenInterceptor implements Interceptor {

    private static final Charset UTF8 = Charset.forName("UTF-8");

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request request = chain.request();
        // try the request
        Response originalResponse = chain.proceed(request);
        if (BuildConfig.LOGGER) {
            Logl.e("url地址=" + originalResponse.request().url());
        }
        ResponseBody responseBody = originalResponse.body();
        BufferedSource source = responseBody.source();
        source.request(Long.MAX_VALUE);
        Buffer buffer = source.buffer();
        Charset charset = UTF8;
        MediaType contentType = responseBody.contentType();
        if (contentType != null) {
            charset = contentType.charset(UTF8);
        }
        String bodyString = buffer.clone().readString(charset);
        if (BuildConfig.LOGGER) {
            Logger.json(bodyString);
        }
        Logl.e("bodySring: " + bodyString);
        if (bodyString.startsWith(Extras.EFFECT)) {   //根据和服务端的约定判断token过期 403
            Logl.e("应该要登录啊");
            Intent intent = new Intent(TaipingApplication.tpApp.getApplicationContext(), LoginActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.putExtra("is_new_login", true);
            TaipingApplication.tpApp.getApplicationContext().startActivity(intent);
            TaipingApplication.tpApp.setIsLogin(false);
        }
        try {
            buffer.close();
        } catch (Exception e) {
        }
        return originalResponse;
    }
}
