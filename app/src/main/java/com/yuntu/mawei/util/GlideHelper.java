package com.yuntu.mawei.util;

import android.content.Context;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.yuntu.mawei.R;
import com.yuntu.mawei.util.music.MusicUtil;

import java.io.File;

/**
 * Created by tanghao on 2017/4/13.
 * Email: 86882259@qq.com
 * 用于封装glide库，统一显示是否在非wifi情况下是否显示图片
 * 不加 dontAnimate()，有的机型会出现图片变形的情况
 */

public final class GlideHelper {

    /**
     * @param context
     * @param url
     * @param view
     */

    public static void loadListPic(Context context, String url, ImageView view) {
        if (url == null) {
            return;
        }
        if (url.startsWith("http")) {
            Glide.with(context).load(url).centerCrop().dontAnimate().placeholder(R.mipmap.music_defaul_icon).into(view);
        } else {
            Glide.with(context).load(MusicUtil.checkHttp(url)).centerCrop().dontAnimate().placeholder(R.mipmap.music_defaul_icon).into(view);
        }
    }

    public static void loadPicPlayPage(Context context, String url, ImageView view) {
        if (url == null) {
            return;
        }
        if (url.startsWith("http")) {
            Glide.with(context).load(url).centerCrop().dontAnimate().into(view);
        } else {
            Glide.with(context).load(MusicUtil.checkHttp(url)).centerCrop().dontAnimate().into(view);
        }
    }

    public static void setFileToView(Context context, File file, ImageView view) {
        Glide.with(context).load(file).into(view);
    }


}
