package com.yuntu.mawei.util;

import java.math.BigDecimal;
import java.text.DecimalFormat;

/**
 * Created by tanghao on 2017/11/14.
 * Email: 86882259@qq.com
 */

public class BigDeUtil {

    public static DecimalFormat decimalFormat = new DecimalFormat("###################.##");

    public static String add(double v1, double v2) {
        BigDecimal b1 = new BigDecimal(v1);
        BigDecimal b2 = new BigDecimal(v2);
        return decimalFormat.format(b1.add(b2));
    }

    /**
     * 提供精确的减法运算。
     *
     * @param v1 被减数
     * @param v2 减数
     * @return 两个参数的差
     */
    public static String sub(double v1, double v2) {
        BigDecimal b1 = new BigDecimal(v1);
        BigDecimal b2 = new BigDecimal(v2);
        return decimalFormat.format(b1.subtract(b2));
    }

    /**
     * 提供精确的乘法运算。
     *
     * @param v1 被乘数
     * @param v2 乘数
     * @return 两个参数的积
     */
    public static String mul(double v1, double v2) {
        BigDecimal b1 = new BigDecimal(v1);
        BigDecimal b2 = new BigDecimal(v2);
        return decimalFormat.format(b1.multiply(b2));
    }

    public static double mulDouble(double v1, double v2) {
        BigDecimal b1 = new BigDecimal(v1);
        BigDecimal b2 = new BigDecimal(v2);
        return b1.multiply(b2).doubleValue();
    }

    public static double addDouble(double v1, double v2) {
        BigDecimal b1 = new BigDecimal(v1);
        BigDecimal b2 = new BigDecimal(v2);
        return b1.add(b2).doubleValue();
    }

    public static String toXiaoShu(double v1) {
        return decimalFormat.format(v1);
    }

    public static double div(double v1, double v2) {
        return divD(v1, v2);
    }

    public static double divD(double v1, double v2) {
        BigDecimal b1 = new BigDecimal(v1);
        BigDecimal b2 = new BigDecimal(v2);
        return b1.divide(b2, 2, BigDecimal.ROUND_HALF_UP).doubleValue();
    }

    public static String bigFormat(double v1) {
        return decimalFormat.format(v1);
    }

    public static String bigFormat(String v1) {
        return decimalFormat.format(v1);
    }

}
