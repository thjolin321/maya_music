package com.yuntu.mawei.util;

import com.yuntu.mawei.base.TaipingApplication;

import java.io.File;

public class FileUtil {

    public static boolean isFileExists(String filename) {
        try {
            File file = new File(TaipingApplication.tpApp.getAppCacheDir() + "/" + filename);
            if (file.exists()) {
                return true;
            }
        }catch (Exception e){
            return false;
        }
        return false;
    }

    public static boolean isFileExistsQuanMing(String filename) {
        try {
            File file = new File(filename);
            if (file.exists()) {
                return true;
            }
        }catch (Exception e){
            return false;
        }
        return false;
    }


    public static boolean delete(File file) {
        if (file.isFile()) {
            return file.delete();
        }

        if (file.isDirectory()) {
            File[] childFiles = file.listFiles();
            if (childFiles == null || childFiles.length == 0) {
                return file.delete();
            }

            for (File childFile : childFiles) {
                delete(childFile);
            }
            return file.delete();
        }
        return false;
    }

    //删除文件
    public static void deleFile(File file) {
        file.delete();
    }
}
