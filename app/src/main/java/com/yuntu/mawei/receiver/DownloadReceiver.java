package com.yuntu.mawei.receiver;

import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;

import com.yuntu.mawei.R;
import com.yuntu.mawei.bean.music.DownloadMusicInfo;
import com.yuntu.mawei.bean.music.Music;
import com.yuntu.mawei.bean.music.MusicLocal;
import com.yuntu.mawei.ui.playing.service.AppCache;
import com.yuntu.mawei.util.GsonUtil;
import com.yuntu.mawei.util.Logl;
import com.yuntu.mawei.util.ToastUtil;
import com.yuntu.mawei.util.db.DBManager;

import java.io.File;

/**
 * 下载完成广播接收器
 * Created by tanghao on 2015/12/30.
 */
public class DownloadReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        long id = intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, 0);
        DownloadMusicInfo downloadMusicInfo = AppCache.get().getDownloadList().get(id);
        if (downloadMusicInfo != null) {
            ToastUtil.showToast(context.getString(R.string.download_success, downloadMusicInfo.getTitle()));
            String musicPath = downloadMusicInfo.getMusicPath();
            String coverPath = downloadMusicInfo.getCoverPath();
            MusicLocal musicLocal = GsonUtil.GsonToBean(downloadMusicInfo.getCoverPath(), MusicLocal.class);
            musicLocal.path = musicPath;

            DBManager.get().getMusicLocalDao().insert(musicLocal);
            if (!TextUtils.isEmpty(musicPath) && !TextUtils.isEmpty(coverPath)) {
                File musicFile = new File(musicPath);
                File coverFile = new File(coverPath);
            }
        }
    }
}
