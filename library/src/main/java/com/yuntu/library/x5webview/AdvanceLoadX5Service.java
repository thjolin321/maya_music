package com.yuntu.library.x5webview;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

import com.tencent.smtt.sdk.QbSdk;

/**
 * Created by sanmu on 2017/4/7.
 * meail: 992759969@qq.com
 */

public class AdvanceLoadX5Service extends Service {

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        initX5();
    }

    private void initX5() {
        //  预加载X5内核
        QbSdk.initX5Environment(getApplicationContext(), cb);
    }

    QbSdk.PreInitCallback cb = new QbSdk.PreInitCallback() {

        @Override
        public void onViewInitFinished(boolean arg0) {
            // TODO Auto-generated method stub
            //初始化完成回调
        }

        @Override
        public void onCoreInitFinished() {
            // TODO Auto-generated method stub
        }
    };
}
