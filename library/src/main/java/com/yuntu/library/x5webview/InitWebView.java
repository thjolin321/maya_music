package com.yuntu.library.x5webview;

import android.content.Context;

import com.tencent.smtt.sdk.QbSdk;

/**
 * Created by sanmu on 2017/4/7.
 * meail: 992759969@qq.com
 */

public class InitWebView {

    public static void preinitX5WebCore(Context context) {
        if (!QbSdk.isTbsCoreInited()) {
            QbSdk.preInit(context, null);// 设置X5初始化完成的回调接口

        }
    }
}
